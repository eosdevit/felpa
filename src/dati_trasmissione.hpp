/**
 *  \file
 *  \remark This file is part of FELPA.
 *
 *  \copyright Copyright (C) 2015-2024 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#if !defined(FELPA_DATI_TRASMISSIONE_HPP)
#define      FELPA_DATI_TRASMISSIONE_HPP

#include <string>

#include "codice_destinatario.hpp"
#include "contatti_trasmittente.hpp"
#include "formato_trasmissione.hpp"
#include "id_trasmittente.hpp"

namespace felpa
{

///
/// Consente l'identificazione del soggetto trasmittente ed il corretto
/// recapito del documento elettronico al destinatario.
///
/// La valorizzazione di questi dati e' indispensabile ai fini di un corretto
/// recapito del documento elettronico.
///
class DatiTrasmissione
{
public:
  explicit DatiTrasmissione(formato_fattura = formato_fattura::FPR);

  [[nodiscard]] bool empty() const;

  [[nodiscard]] static const char *tag();

  // ********* Data member pubblici *********
  /// Identificativo fiscale del soggetto trasmittente.
  IdTrasmittente id_trasmittente;

  /// E' un campo ad uso esclusivo del soggetto trasmittente pensato come
  /// elemento identificativo del file trasmesso.
  ///
  /// Non e' stabilito alcun criterio particolare di valorizzazione; la
  /// modalita' di valorizzazione del campo, previsto per contenere un
  /// identificativo alfanumerico del file trasmesso, e' demandata alla
  /// valutazione dell'utente secondo esigenza, ma comunque nel rispetto delle
  /// caratteristiche stabilite dallo schema XSD.
  ///
  /// \warning OBBLIGATORIETA': SI, sempre
  std::string progressivo_invio;

  /// Identifica il soggetto al quale e' destinata la fattura.
  /// \warning OBBLIGATORIETA: SI, sempre.
  CodiceDestinatario codice_destinatario;

  /// Serve per fornire informazioni aggiuntive utili per contattare il
  /// soggetto trasmittente.
  ContattiTrasmittente contatti_trasmittente;

  /// Codice identificativo del tipo di trasmissione che si sta effettuando e
  /// del relativo formato.
  FormatoTrasmissione formato_trasmissione;

  /// Indirizzo di Posta Elettronica Certificata al quale viene recapitata la
  /// fattura.
  /// \remark
  /// Viene valorizzato nei soli casi di destinatario diverso da Pubblica
  /// Amministrazione, qualora il destinatario utilizzi il canale PEC per
  /// ricevere le fatture. Puo' essere valorizzato solo se il valore di
  /// `CodiceDestinatario` e' uguale a `0000000`.
  std::string pec_destinatario;
};  // class DatiTrasmissione

[[nodiscard]] bool operator==(const DatiTrasmissione &,
                              const DatiTrasmissione &);

}  // namespace felpa

#endif  // include guard
