/**
 *  \file
 *  \remark This file is part of FELPA.
 *
 *  \copyright Copyright (C) 2015-2024 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#if !defined(FELPA_CONDIZIONI_PAGAMENTO_HPP)
#define      FELPA_CONDIZIONI_PAGAMENTO_HPP

#include <string>

namespace felpa
{

///
/// Identifica le condizioni di pagamento.
///
class CondizioniPagamento
{
public:
  explicit CondizioniPagamento(const std::string & = "");

  [[nodiscard]] bool empty() const { return codice_.empty(); }
  [[nodiscard]] bool completo() const;

  [[nodiscard]] operator std::string () const { return codice_; }
  CondizioniPagamento &operator=(const std::string &);

  [[nodiscard]] static const std::string &tag();

private:
  [[nodiscard]] static bool valido(const std::string &);

  std::string codice_;
};  // class CondizioniPagamento

}  // namespace felpa

#endif  // include guard
