/**
 *  \file
 *  \remark This file is part of FELPA.
 *
 *  \copyright Copyright (C) 2015-2024 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#if !defined(FELPA_DATI_GENERALI_HPP)
#define      FELPA_DATI_GENERALI_HPP

#include <vector>

#include "dati_contratto.hpp"
#include "dati_convenzione.hpp"
#include "dati_ddt.hpp"
#include "dati_fatture_collegate.hpp"
#include "dati_generali_documento.hpp"
#include "dati_ordine_acquisto.hpp"
#include "dati_ricezione.hpp"
#include "dati_sal.hpp"
#include "dati_trasporto.hpp"

namespace felpa
{

///
/// Contiene i dati generali del documento fattura e dei documenti ad essa
/// correlati.
///
class DatiGenerali
{
public:
  DatiGenerali() = default;

  [[nodiscard]] bool empty() const;

  [[nodiscard]] static const std::string &tag();

  // ********* Data member pubblici *********
  /// \warning OBBLIGATORIETA': SI, sempre.
  DatiGeneraliDocumento dati_generali_documento {};

  /// Dati relativi all’ordine di acquisto dal quale scaturisce la
  /// cessione/prestazione oggetto del documento fattura.
  ///
  /// Replicando piu' volte il blocco e' possibile correlare alla fattura piu'
  /// di un ordine di acquisto.
  std::vector<DatiOrdineAcquisto> dati_ordine_acquisto {};

  /// Dati relativi al contratto dal quale scaturisce la cessione/prestazione
  /// oggetto del documento fattura.
  ///
  /// Replicando piu' volte il blocco e' possibile correlare alla fattura piu'
  /// di un contratto.
  std::vector<DatiContratto> dati_contratto {};

  /// Dati relativi alla convenzione collegata alla fattura.
  ///
  /// Replicando piu' volte il blocco e' possibile correlare alla fattura piu'
  /// di una convenzione.
  std::vector<DatiConvenzione> dati_convenzione {};

  /// Dati relativi alla ricezione dei beni/servizi oggetto del documento
  /// fattura.
  std::vector<DatiRicezione> dati_ricezione {};

  /// Dati relativi alla fattura alla quale si collega il documento in oggetto.
  ///
  /// Replicando piu' volte il blocco e' possibile correlare alla fattura piu'
  /// di una fattura.
  std::vector<DatiFattureCollegate> dati_fatture_collegate {};

  /// Questo blocco puo' essere presente anche più di una volta per dare la
  /// possibilita' di riferire la fattura a piu' fasi di avanzamento lavori.
  ///
  /// \warning OBBLIGATORIETA': SI, ma solo se il documento rientra nel caso di
  /// modalita' di fatturazione a stato di avanzamento lavori (SAL) con fasi
  /// definite.
  std::vector<DatiSAL> dati_sal {};

  /// Da valorizzare nei casi in cui sia presente un documento di trasporto
  /// collegato alla fattura (fatturazione differita).
  std::vector<DatiDDT> dati_ddt {};

  DatiTrasporto dati_trasporto {};
};  // class DatiGenerali

[[nodiscard]] bool operator==(const DatiGenerali &, const DatiGenerali &);

}  // namespace felpa

#endif  // include guard
