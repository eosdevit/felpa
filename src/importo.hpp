/**
 *  \file
 *  \remark This file is part of FELPA.
 *
 *  \copyright Copyright (C) 2015-2018 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#if !defined(FELPA_IMPORTO_HPP)
#define      FELPA_IMPORTO_HPP

#include <cmath>

#include "numero_type.hpp"

namespace felpa
{

///
/// Importo di uno sconto / maggiorazione.
///
class Importo : public NumeroType
{
public:
  ///
  /// \remark
  /// Sono accettati anche numeri negativi, ma vengono subito convertiti in
  /// positivi. La tipologia dell'importo viene identificata in base al campo
  /// `Tipo`.
  ///
  explicit Importo(const std::string &i = "")
    : NumeroType("Importo", 2, anche_negativo)
  {
    assign(i);

    if (!i.empty())
    {
      const double val(to_double());
      if (val < 0.0)
        assign(-val);
    }
  }

  ///
  /// \remark
  /// Sono accettati anche numeri negativi, ma vengono subito convertiti in
  /// positivi. La tipologia dell'importo viene identificata in base al campo
  /// `Tipo`.
  ///
  explicit Importo(double i) : NumeroType("Importo", 2, anche_negativo)
  {
    assign(std::fabs(i));
  }

  Importo &operator=(const std::string &i)
  {
    return *this = Importo(i);
  }

  Importo &operator=(double i)
  {
    return *this = Importo(i);
  }
};  // class Importo

}  // namespace felpa

#endif  // include guard
