/**
 *  \file
 *  \remark This file is part of FELPA.
 *
 *  \copyright Copyright (C) 2015-2018 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#if !defined(FELPA_IMPORTO_PAGAMENTO_HPP)
#define      FELPA_IMPORTO_PAGAMENTO_HPP

#include <cmath>

#include "numero_type.hpp"

namespace felpa
{

class ImportoPagamento : public NumeroType
{
public:
  explicit ImportoPagamento(const std::string &i = "")
    : NumeroType("ImportoPagamento", 2, anche_negativo)
  {
    assign(i);
  }

  explicit ImportoPagamento(double i)
    : NumeroType("ImportoPagamento", 2, anche_negativo)
  {
    assign(i);
  }

  ImportoPagamento &operator=(const std::string &i)
  {
    assign(i);
    return *this;
  }

  ImportoPagamento &operator=(double i)
  {
    assign(i);
    return *this;
  }
};  // class ImportoPagamento

}  // namespace felpa

#endif  // include guard
