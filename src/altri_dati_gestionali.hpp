/**
 *  \file
 *  \remark This file is part of FELPA.
 *
 *  \copyright Copyright (C) 2021-2022 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#if !defined(FELPA_ALTRI_DATI_GESTIONALI_HPP)
#define      FELPA_ALTRI_DATI_GESTIONALI_HPP

#include <string>

#include "riferimento_data.hpp"
#include "riferimento_numero.hpp"

namespace felpa
{

///
/// Permette di rappresentare dati utili alla gestione automatica della
/// fattura elettronica e dei dati in essa contenuti.
/// Gli elementi componenti possono, per esempio, esser utilizzati per scambio
/// di informazioni pattuite tra le parti.
///
/// \remark
/// Per specifiche operazioni gli elementi che compongono questo blocco
/// possono essere usati per ottemperare a determinate previsioni di norma.
///
class AltriDatiGestionali
{
public:
  AltriDatiGestionali();

  [[nodiscard]] bool empty() const;

  [[nodiscard]] static const std::string &tag();

  // ********* Data member pubblici *********
  /// Codice che identifica la tipologia di informazione.
  ///
  /// \note
  /// Al fine di riportare il dato della targa in fattura (es. DPR del
  /// 09/06/2000 nr 277), l'elemento va valorizzato con la stringa "TARGA".
  /// Al fine di riportare in fattura il riferimento ad uno scontrino o ad un
  /// documento commerciale, nei casi in cui l'operazione "fatturata" sia
  /// stata cosi' precedentemente rendicontata, l'elemento `TipoDato` va
  /// valorizzato con la stringa "N.DOC.COMM".
  /// Al fine di riportare in fattura il riferimento ad operazioni di cui
  /// all'articolo 21 comma 6 bis lettera a) del d.P.R. n.633/72, l'elemento va
  /// va valorizzato con la stringa "INVCONT".
  /// Al fine di riportare in fattura il riferimento ad operazioni non
  /// imponibili, ai sensi dell'art. 8, comma 1, lett. c) del decreto del
  /// Presidente della Repubblica 26 ottobre 1972, n. 633 e da trasmettere al
  /// sistema SDI, nei confronti di un esportatore abituale, l'elemento
  /// va valorizzato con la stringa "INTENTO".
  /// Nel caso si volessero rappresentare operazioni che, seppur formalmente
  /// rientranti nel campo applicativo dell'imposta di bollo, non devono
  /// essere assoggettate all'imposta per via di specifiche disposizioni
  /// normative, l'elemento va valorizzato:
  /// - con la stringa "NB1" se l'imposta di bollo non e' applicabile ai
  ///   documenti assicurativi in quanto assorbita nell'imposta sulle
  ///   assicurazioni;
  /// - con la stringa "NB2" se l'imposta di bollo non e' applicabile ai
  ///   documenti emessi da soggetti appartenenti al terzo settore;
  /// - con la stringa "NB3" se l'imposta di bollo non e' applicabile ai
  ///   documenti emessi fra banca e cliente correntista, poiche' l'imposta di
  ///   bollo su tali documenti e' gia' assorbita nell'imposta di bollo
  ///   sull'estratto conto.
  ///
  /// \warning OBBLIGATORIETA': SI.
  std::string tipo_dato;

  /// Valore alfanumerico riferito alla tipologia di informazione.
  ///
  /// \note
  /// Al fine di riportare il dato della targa in fattura (es. DPR del
  /// 09/06/2000 nr 277), l'elemento va valorizzato con la targa del mezzo per
  /// il quale si effettua il rifornimento.
  /// Al fine di riportare in fattura il riferimento ad uno scontrino o ad un
  /// documento commerciale, nei casi in cui l'operazione *fatturata* sia stata
  /// cosi' precedentemente rendicontata, l'elemento va valorizzato con
  /// l'identificativo alfanumerico dello scontrino.
  /// Al fine di riportare in fattura il riferimento ad una dichiarazione di
  /// intenti, l'elemento va valorizzato riportando il protocollo di ricezione
  /// della dichiarazione d'intento e il suo progressivo separato dal segno "-"
  /// oppure dal segno "/" (es. 08060120341234567-000001).
  std::string riferimento_testo;

  /// Valore numerico riferito alla tipologia di informazione.
  RiferimentoNumero riferimento_numero;

  /// Data riferita alla tipologia di informazione.
  /// Al fine di riportare in fattura il riferimento ad uno scontrino o ad un
  /// documento commerciale, nei casi in cui l'operazione *fatturata* sia stata
  /// cosi' precedentemente rendicontata, l'elemento va valorizzato con la data
  /// di emissione dello scontrino (o del documento commerciale).
  /// Al fine di riportare in fattura il riferimento ad una dichiarazione di
  /// intento, l'elemento deve riportare la data della ricevuta telematica
  /// rilasciata dall'Agenzia delle entrate e contenente il protocollo della
  /// dichiarazione d'intento.
  RiferimentoData riferimento_data;
};  // class ScontoMaggiorazione

[[nodiscard]] bool operator==(const AltriDatiGestionali &,
                              const AltriDatiGestionali &);

}  // namespace felpa

#endif  // include guard
