/**
 *  \file
 *  \remark This file is part of FELPA.
 *
 *  \copyright Copyright (C) 2015-2021 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#if !defined(FELPA_DATI_ANAGRAFICI_VETTORE_HPP)
#define      FELPA_DATI_ANAGRAFICI_VETTORE_HPP

#include "dati_anagrafici_type.hpp"

namespace felpa
{

class DatiAnagraficiVettore : public DatiAnagraficiType
{
public:
  DatiAnagraficiVettore();

  // ********* Data members pubblici *********
  /// Numero identificativo della licenza di guida (es. numero patente).
  std::string numero_licenza_guida;

  bool operator==(const DatiAnagraficiVettore &) const;
};  // class DatiAnagraficiVettore

}  // namespace felpa

#endif  // include guard
