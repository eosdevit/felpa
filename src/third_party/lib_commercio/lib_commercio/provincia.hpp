/**
 *  \file
 *  \remark This file is part of libCommercio.
 *
 *  \copyright Copyright (C) 2023 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#include <cassert>
#include <map>
#include <regex>
#include <set>
#include <stdexcept>
#include <string>
#include <vector>

#include "utils.hpp"

#if !defined(LIB_COMMERCIO_PROVINCIA_HPP)
#define      LIB_COMMERCIO_PROVINCIA_HPP

namespace lib_commercio
{

///
/// Codici relativi alle province italiane.
///
class provincia
{
public:
  explicit provincia(const std::string &);

  struct info
  {
    std::string codice {};
    std::string nome {};

    /// Il codice ISTAT e' un identificativo numerico assegnato a fini
    /// statistici alle unita' amministrative italiane (esistenti o soppresse)
    /// da parte dell'Istituto nazionale di statistica. Il codice fu istituito
    /// nel 1966.
    unsigned istat {};

    [[nodiscard]] bool empty() const;
  };

  [[nodiscard]] static info trova(const std::string &);
  [[nodiscard]] static info trova(unsigned);

  [[nodiscard]] static bool verifica_codice(const std::string &);

private:
  std::string cod_;
};


/*********************************************************************
 * DETTAGLI DI IMPLEMENTAZIONE
 *********************************************************************/
namespace details
{
[[nodiscard]] inline const std::vector<provincia::info> &db_province()
{
  static const std::vector<provincia::info> db =
  {
    {"AG", "AGRIGENTO", 84},
    {"AL", "ALESSANDRIA", 6},
    {"AN", "ANCONA", 42},
    {"AO", "AOSTA", 7},
    {"AP", "ASCOLI PICENO", 44},
    {"AR", "AREZZO", 51},
    {"AT", "ASTI", 5},
    {"AV", "AVELLINO", 64},
    {"BA", "BARI", 272},
    {"BL", "BELLUNO", 25},
    {"BN", "BENEVENTO", 62},
    {"BG", "BERGAMO", 16},
    {"BI", "BIELLA", 96},
    {"BO", "BOLOGNA", 237},
    {"BZ", "BOLZANO", 21},
    {"BS", "BRESCIA", 17},
    {"BR", "BRINDISI", 74},
    {"BT", "BARLETTA-ANDRIA-TRANI", 110},
    {"CA", "CAGLIARI", 292},
    {"CL", "CALTANISSETTA", 85},
    {"CB", "CAMPOBASSO", 70},
    {"CE", "CASERTA", 61},
    {"CT", "CATANIA", 287},
    {"CZ", "CATANZARO", 79},
    {"CH", "CHIETI", 69},
    {"CO", "COMO", 13},
    {"CS", "COSENZA", 78},
    {"CR", "CREMONA", 19},
    {"KR", "CROTONE", 101},
    {"CN", "CUNEO", 4},
    {"EN", "ENNA", 86},
    {"FM", "FERMO", 109},
    {"FE", "FERRARA", 38},
    {"FI", "FIRENZE", 248},
    {"FG", "FOGGIA", 71},
    {"FC", "FORLI'-CESENA", 40},
    {"FR", "FROSINONE", 60},
    {"GE", "GENOVA", 210},
    {"GO", "GORIZIA", 31},
    {"GR", "GROSSETO", 53},
    {"IM", "IMPERIA", 8},
    {"IS", "ISERNIA", 94},
    {"SP", "LA SPEZIA", 11},
    {"AQ", "L'AQUILA", 66},
    {"LT", "LATINA", 59},
    {"LE", "LECCE", 75},
    {"LC", "LECCO", 97},
    {"LI", "LIVORNO", 49},
    {"LO", "LODI", 98},
    {"LU", "LUCCA", 46},
    {"MC", "MACERATA", 43},
    {"MN", "MANTOVA", 20},
    {"MS", "MASSA-CARRARA", 45},
    {"MT", "MATERA", 77},
    {"ME", "MESSINA", 283},
    {"MI", "MILANO", 215},
    {"MO", "MODENA", 36},
    {"MB", "MONZA-BRIANZA", 108},
    {"NA", "NAPOLI", 263},
    {"NO", "NOVARA", 3},
    {"NU", "NUORO", 91},
    {"OR", "ORISTANO", 95},
    {"PD", "PADOVA", 28},
    {"PA", "PALERMO", 282},
    {"PR", "PARMA", 34},
    {"PV", "PAVIA", 18},
    {"PG", "PERUGIA", 54},
    {"PU", "PESARO URBINO", 41},
    {"PE", "PESCARA", 68},
    {"PC", "PIACENZA", 33},
    {"PI", "PISA", 50},
    {"PT", "PISTOIA", 47},
    {"PN", "PORDENONE", 93},
    {"PZ", "POTENZA", 76},
    {"PO", "PRATO", 100},
    {"RG", "RAGUSA", 88},
    {"RA", "RAVENNA", 39},
    {"RC", "REGGIO CALABRIA", 280},
    {"RE", "REGGIO EMILIA", 35},
    {"RI", "RIETI", 57},
    {"RN", "RIMINI", 99},
    {"RM", "ROMA", 258},
    {"RO", "ROVIGO", 29},
    {"SA", "SALERNO", 65},
    {"SS", "SASSARI", 90},
    {"SV", "SAVONA", 9},
    {"SI", "SIENA", 52},
    {"SR", "SIRACUSA", 89},
    {"SO", "SONDRIO", 14},
    {"SU", "SUD SARDEGNA", 111},
    {"TA", "TARANTO", 73},
    {"TE", "TERAMO", 67},
    {"TR", "TERNI", 55},
    {"TO", "TORINO", 201},
    {"TP", "TRAPANI", 81},
    {"TN", "TRENTO", 22},
    {"TV", "TREVISO", 26},
    {"TS", "TRIESTE", 32},
    {"UD", "UDINE", 30},
    {"VA", "VARESE", 12},
    {"VE", "VENEZIA", 227},
    {"VB", "VERBANIA", 103},
    {"VC", "VERCELLI", 2},
    {"VR", "VERONA", 23},
    {"VV", "VIBO VALENTIA", 102},
    {"VI", "VICENZA", 24},
    {"VT", "VITERBO", 56}
  };

  return db;
}

}  // namespace details

/*********************************************************************
 * IMPLEMENTAZIONE
 *********************************************************************/
inline provincia::provincia(const std::string &cod)
  : cod_(trova(cod).codice)
{
  if (cod_.empty())
    throw std::invalid_argument("Provincia sconosciuta");
}

///
/// Trova i dati riguardanti la provincia identificata da `cod`.
///
/// \param[in] cod codice o nome della provincia di interesse
/// \return        informazioni recuperate (puo' essere vuoto)
///
/// La ricerca di nomi di province e' case insensitive e supporta le varianti
/// con trattini e/o spazi (per esempio "Massa-Carrara" e "MASSA CARRARA").
///
inline provincia::info provincia::trova(const std::string &cod)
{
  static std::map<std::string, std::size_t> codice_map;
  if (cod.length() == 2)
  {
    if (codice_map.empty())
    {
      for (std::size_t i(0); i < details::db_province().size(); ++i)
      {
        assert(details::db_province()[i].codice.length() == 2);
        codice_map.emplace(details::db_province()[i].codice, i);
      }
    }

    const auto it(codice_map.find(cod));
    return it == codice_map.end() ? provincia::info()
                                  : details::db_province()[it->second];
  }

  static std::map<std::string, std::size_t> nome_map;
  if (nome_map.empty())
    for (std::size_t i(0); i < details::db_province().size(); ++i)
      nome_map.emplace(details::db_province()[i].nome, i);

  const std::string ucod(toupper(cod));

  if (const auto it(nome_map.find(ucod));
      it != nome_map.end())
    return details::db_province()[it->second];

  if (const std::size_t primo_spazio(ucod.find(' '));
      primo_spazio != std::string::npos)
  {
    std::string ucod1(ucod);
    ucod1[primo_spazio] = '-';

    if (const auto it(nome_map.find(ucod1)); it != nome_map.end())
      return details::db_province()[it->second];
  }

  if (const std::size_t primo_trattino(ucod.find('-'));
      primo_trattino != std::string::npos)
  {
    std::string ucod1(ucod);
    ucod1[primo_trattino] = ' ';

    if (const auto it(nome_map.find(ucod1)); it != nome_map.end())
      return details::db_province()[it->second];
  }

  return provincia::info();
}

///
/// Trova i dati riguardanti la provincia identificata da `num`.
///
/// \param[in] num codice ISTAT della provincia di interesse
/// \return        informazioni recuperate (puo' essere vuoto)
///
inline provincia::info provincia::trova(unsigned num)
{
  static std::map<unsigned, std::size_t> istat_map;
  if (istat_map.empty())
  {
    for (std::size_t i(0); i < details::db_province().size(); ++i)
      istat_map.emplace(details::db_province()[i].istat, i);
  }

  const auto it(istat_map.find(num));

  return it == istat_map.end() ? provincia::info()
                               : details::db_province()[it->second];
}

///
/// Verifica la validita' di un presunto codice provincia.
///
/// \param[in] cod codice da verificare
/// \return        `true` se il codice e' valido
///
/// La ricerca di nomi di province e' case insensitive e supporta le varianti
/// con trattini e/o spazi (per esempio "Massa-Carrara" e "MASSA CARRARA").
///
inline bool provincia::verifica_codice(const std::string &cod)
{
  return trova(cod).codice == cod;
}

inline bool provincia::info::empty() const
{
  return codice.empty();
}

}  // namespace lib_commercio

#endif  // include guard
