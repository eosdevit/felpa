/**
 *  \file
 *  \remark This file is part of FELPA.
 *
 *  \copyright Copyright (C) 2015-2021 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#include <cassert>
#include <ctime>

#include "dati_trasmissione.hpp"
#include "fattura_elettronica_eccezioni.hpp"
#include "fattura_elettronica_dettagli.hpp"
#include "utility.hpp"

namespace felpa
{

const char *DatiTrasmissione::tag()
{
  static const std::string TAG("DatiTrasmissione");
  return TAG.c_str();
}

namespace dettagli
{

std::string cod_progressivo()
{
  // Contiamo i secondi passati dal 2019-12-25 e li codifichiamo in base 62
  // (per comporre il progressivo della fattura elettronica si possono
  // utilizzare 5 cifre in base 62).
  // Con 62^5 = 916132832 possibili valori, possiamo attribuire un codice
  // univoco ad ogni secondo per 29 anni a partire dalla data sopra indicata.
  std::tm tm_inizio;
  tm_inizio.tm_year  = 2018 - 1900;  // anni a partire dal 1900
  tm_inizio.tm_mon   = 12 - 1;       // intervallo [0, 11]
  tm_inizio.tm_mday  = 25;           // intervallo [1, 31]
  tm_inizio.tm_hour  = 0;
  tm_inizio.tm_min   = 0;
  tm_inizio.tm_sec   = 0;
  tm_inizio.tm_isdst = -1;

  const auto inizio(std::mktime(&tm_inizio));
  const auto ora(time(nullptr));

  const auto delta(std::difftime(ora, inizio));  // secondi dal 2019-12-25

  // I caratteri ammessi per comporre la parte "progressivo" del nome sono:
  // * 10 cifre decimali ("0123456789");
  // * 26 caratteri maiuscoli ("ABCDEFGHIJKLMNOPQRSTUVWXYZ");
  // * 26 caratteri minuscoli ("abcdefghijklmnopqrstuvwxyz")
  //
  // Per un totale di 10 + 26 + 26 = 62 caratteri.
  const int base(62);

  return base_conversion(delta, base);
}

}  // namespace dettagli

DatiTrasmissione::DatiTrasmissione(formato_fattura formato)
  : id_trasmittente(), progressivo_invio(dettagli::cod_progressivo()),
    codice_destinatario(), contatti_trasmittente(),
    formato_trasmissione(formato), pec_destinatario()
{
}

bool DatiTrasmissione::empty() const
{
  return id_trasmittente.empty()
         && codice_destinatario.empty()
         && contatti_trasmittente.empty()
         && pec_destinatario.empty();
}

bool operator==(const DatiTrasmissione &lhs, const DatiTrasmissione &rhs)
{
  return lhs.id_trasmittente == rhs.id_trasmittente
         && lhs.progressivo_invio == rhs.progressivo_invio
         && lhs.codice_destinatario == rhs.codice_destinatario
         && lhs.contatti_trasmittente == rhs.contatti_trasmittente
         && lhs.formato_trasmissione == rhs.formato_trasmissione
         && lhs.pec_destinatario == rhs.pec_destinatario;
}

namespace dettagli
{

constexpr char PROGRESSIVO_INVIO_TAG[] = "ProgressivoInvio";
constexpr char PEC_DESTINATARIO_TAG[] = "PECDestinatario";

constexpr std::size_t PROGRESSIVO_INVIO_SIZE = 10;

bool carica(tinyxml2::XMLConstHandle &ft, DatiTrasmissione *dt)
{
  auto h(ft.FirstChildElement(DatiTrasmissione::tag()));
  if (!h.ToElement())
    return false;

  const bool idt(carica(h, &dt->id_trasmittente));

  if (auto *he = h.FirstChildElement(PROGRESSIVO_INVIO_TAG).ToElement())
    dt->progressivo_invio =
      coalesce(he->GetText()).substr(0, PROGRESSIVO_INVIO_SIZE);

  const bool ftr(carica(h, &dt->formato_trasmissione));

  carica(h, &dt->codice_destinatario);

  carica(h, &dt->contatti_trasmittente);

  if (auto *he = h.FirstChildElement(PEC_DESTINATARIO_TAG).ToElement())
    dt->pec_destinatario = coalesce(he->GetText());

  return dt->progressivo_invio.length() && idt && ftr;
}

void registra(tinyxml2::XMLPrinter &ft, const DatiTrasmissione &d)
{
  ft.OpenElement(DatiTrasmissione::tag());

  registra(ft, d.id_trasmittente);

  if (d.progressivo_invio.empty())
    throw eccezione::prog_invio_mancante(std::string(PROGRESSIVO_INVIO_TAG)
                                         + ": progressivo invio mancante");
  ft.OpenElement(PROGRESSIVO_INVIO_TAG);
  push_text(ft, d.progressivo_invio.substr(0, PROGRESSIVO_INVIO_SIZE));
  ft.CloseElement();

  registra(ft, d.formato_trasmissione);

  if (d.codice_destinatario.empty())
    throw eccezione::cod_destinatario_mancante(
      std::string(d.codice_destinatario.tag())
      + ": codice destinatario mancante");

  if (d.formato_trasmissione.pubblico() && !d.codice_destinatario.pubblico())
    throw eccezione::cod_destinatario_errato(
      std::string(d.codice_destinatario.tag())
      + ": codice destinatario non PA in fattura FPA");

  if (d.formato_trasmissione.privato() && !d.codice_destinatario.privato())
    throw eccezione::cod_destinatario_errato(
      std::string(d.codice_destinatario.tag())
      + ": codice destinatario non PR in fattura FPR");

  registra(ft, d.codice_destinatario);

  registra(ft, d.contatti_trasmittente);

  if (std::string(d.codice_destinatario) != "0000000"
      && !d.pec_destinatario.empty())
    throw eccezione::pec_destinatario_ridondante(
      "PECDestinatario: specificata contemporaneamente a codice destinatario");
  if (!d.pec_destinatario.empty())
  {
    ft.OpenElement(PEC_DESTINATARIO_TAG);
    push_text(ft, d.pec_destinatario);
    ft.CloseElement();
  }

  ft.CloseElement();
}

}  // namespace dettagli

}  // namespace felpa
