/**
 *  \file
 *  \remark This file is part of FELPA.
 *
 *  \copyright Copyright (C) 2015-2019 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#include <cassert>

#include "dati_anagrafici_cessionario.hpp"
#include "fattura_elettronica_eccezioni.hpp"
#include "fattura_elettronica_dettagli.hpp"

namespace felpa
{

DatiAnagraficiCessionario::DatiAnagraficiCessionario()
  : DatiAnagraficiType("DatiAnagrafici")
{
}

namespace dettagli
{

bool carica(tinyxml2::XMLConstHandle &ft, DatiAnagraficiCessionario *dac)
{
  return carica(ft, true, dac);
}

void registra(tinyxml2::XMLPrinter &ft, const DatiAnagraficiCessionario &dac)
{
  if (dac.id_fiscale_iva.empty() && dac.codice_fiscale.empty())
    throw eccezione::dati_cessionario_fiscale_mancante(
      "CessionarioCommittente: dati anagrafici incompleti");

  registra(ft, dac, true);
}

}  // namespace dettagli

}  // namespace felpa
