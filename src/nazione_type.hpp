/**
 *  \file
 *  \remark This file is part of FELPA.
 *
 *  \copyright Copyright (C) 2015 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#if !defined(FELPA_NAZIONE_TYPE_HPP)
#define      FELPA_NAZIONE_TYPE_HPP

#include <set>
#include <string>

namespace felpa
{

///
/// Codice del paese assegnante l'identificativo fiscale al soggetto
/// trasmittente (secondo lo standard ISO3166-1 alpha-2).
///
class NazioneType
{
public:
  [[nodiscard]] operator std::string () const { return codice_; }
  [[nodiscard]] const std::string &tag() const { return tag_; }

  [[nodiscard]] bool italia() const;
  [[nodiscard]] bool ue() const;

  void assign(const std::string &);

protected:
  NazioneType(const std::string &, const std::string &);

  [[nodiscard]] bool operator==(const NazioneType &rhs) const
  { return std::string(*this) == std::string(rhs); }

private:
  // Funzioni private di supporto.
  [[nodiscard]] static bool valido(const std::string &);

  // Data member privati.
  std::string tag_;
  std::string codice_ {};
};  // class NazioneType

}  // namespace felpa

#endif  // include guard
