/**
 *  \file
 *  \remark This file is part of FELPA.
 *
 *  \copyright Copyright (C) 2015-2018 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#include <cassert>

#include "imposta.hpp"
#include "fattura_elettronica_eccezioni.hpp"
#include "fattura_elettronica_dettagli.hpp"

namespace felpa
{

Imposta::Imposta(const std::string &i)
  : NumeroType("Imposta", 2, anche_negativo)
{
  assign(i);
}

Imposta::Imposta(double i) : NumeroType("Imposta", 2, anche_negativo)
{
  assign(i);
}

Imposta &Imposta::operator=(const std::string &p)
{
  return *this = Imposta(p);
}

Imposta &Imposta::operator=(double p)
{
  return *this = Imposta(p);
}

}  // namespace felpa
