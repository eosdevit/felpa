/**
 *  \file
 *  \remark This file is part of FELPA.
 *
 *  \copyright Copyright (C) 2018-2024 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#include <cassert>

#include "ritenuta.hpp"
#include "fattura_elettronica_dettagli.hpp"
#include "fattura_elettronica_eccezioni.hpp"
#include "utility.hpp"

namespace felpa
{

const std::string &Ritenuta::tag()
{
  static const std::string TAG("Ritenuta");
  return TAG;
}

namespace dettagli
{

bool carica(tinyxml2::XMLConstHandle &ft, Ritenuta *r)
{
  if (auto *h = ft.FirstChildElement(Ritenuta::tag().c_str()).ToElement())
  {
    *r = coalesce(h->GetText());
    return true;
  }

  return false;
}

void registra(tinyxml2::XMLPrinter &ft, const Ritenuta &r)
{
  if (r)
  {
    ft.OpenElement(Ritenuta::tag().c_str());
    push_text(ft, r);
    ft.CloseElement();
  }
}

}  // namespace dettagli

bool Ritenuta::valido(const std::string &id)
{
  return id == Ritenuta::SI || id == Ritenuta::NO;;
}

Ritenuta::Ritenuta(const std::string &c) : codice_(c)
{
  if (!valido(codice_))
    throw eccezione::ritenuta(tag() + ": valore scorretto");

  assert(codice_.length() == 2);
}

Ritenuta &Ritenuta::operator=(const std::string &r)
{
  return *this = Ritenuta(r);
}

}  // namespace felpa
