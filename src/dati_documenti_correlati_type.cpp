/**
 *  \file
 *  \remark This file is part of FELPA.
 *
 *  \copyright Copyright (C) 2015-2024 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#include <cassert>

#include "dati_documenti_correlati_type.hpp"
#include "fattura_elettronica_dettagli.hpp"
#include "fattura_elettronica_eccezioni.hpp"
#include "utility.hpp"

namespace felpa
{

DatiDocumentiCorrelatiType::DatiDocumentiCorrelatiType(const std::string &t)
  : tag_(t)
{
}

bool DatiDocumentiCorrelatiType::operator==(
  const DatiDocumentiCorrelatiType &rhs) const
{
  assert(tag_ == rhs.tag_);

  return riferimento_numero_linea == rhs.riferimento_numero_linea
         && id_documento == rhs.id_documento
         && std::string(data) == std::string(rhs.data)
         && num_item == rhs.num_item
         && codice_commessa_convenzione == rhs.codice_commessa_convenzione
         && codice_cup == rhs.codice_cup
         && codice_cig == rhs.codice_cig;
}

bool DatiDocumentiCorrelatiType::empty() const
{
  return id_documento.empty();
}

namespace dettagli
{

constexpr char CODICE_CIG_TAG[] = "CodiceCIG";
constexpr char CODICE_COMMESSA_CONVENZIONE_TAG[] = "CodiceCommessaConvenzione";
constexpr char CODICE_CUP_TAG[] = "CodiceCUP";
constexpr char ID_DOCUMENTO_TAG[] = "IdDocumento";
constexpr char NUM_ITEM_TAG[] = "NumItem";
constexpr char RIFERIMENTO_NUMERO_LINEA_TAG[] = "RiferimentoNumeroLinea";

constexpr std::size_t CODICE_CIG_SIZE = 15;
constexpr std::size_t CODICE_COMMESSA_CONVENZIONE_SIZE = 100;
constexpr std::size_t CODICE_CUP_SIZE = 15;
constexpr std::size_t ID_DOCUMENTO_SIZE = 20;
constexpr std::size_t MAX_RIFERIMENTO_NUMERO_LINEA = 9999;
constexpr std::size_t NUM_ITEM_SIZE = 20;

bool carica(tinyxml2::XMLConstHandle &ft, DatiDocumentiCorrelatiType *dt)
{
  tinyxml2::XMLConstHandle h(ft.FirstChildElement(dt->tag().c_str()));
  if (!h.ToElement())
    h = ft;

  dt->riferimento_numero_linea.clear();
  bool rnl(true);
  for (auto *he(h.FirstChildElement(RIFERIMENTO_NUMERO_LINEA_TAG).ToElement());
       he;
       he = he->NextSiblingElement(RIFERIMENTO_NUMERO_LINEA_TAG))
  {
    if (!he->GetText())
      continue;

    try
    {
      const auto val(std::stoi(he->GetText()));
      dt->riferimento_numero_linea.insert(val);
    }
    catch (std::logic_error &)
    {
      rnl = false;
    }
  }

  bool idd(false);
  if (auto *he = h.FirstChildElement(ID_DOCUMENTO_TAG).ToElement())
  {
    idd = true;
    dt->id_documento = coalesce(he->GetText()).substr(0, ID_DOCUMENTO_SIZE);
  }

  carica(h, &dt->data);

  if (auto *he = h.FirstChildElement(NUM_ITEM_TAG).ToElement())
    dt->num_item = coalesce(he->GetText()).substr(0, NUM_ITEM_SIZE);

  if (auto *he =
      h.FirstChildElement(CODICE_COMMESSA_CONVENZIONE_TAG).ToElement())
    dt->codice_commessa_convenzione =
      coalesce(he->GetText()).substr(0, CODICE_COMMESSA_CONVENZIONE_SIZE);

  if (auto *he = h.FirstChildElement(CODICE_CUP_TAG).ToElement())
    dt->codice_cup = coalesce(he->GetText()).substr(0, CODICE_CUP_SIZE);

  if (auto *he = h.FirstChildElement(CODICE_CIG_TAG).ToElement())
    dt->codice_cig = coalesce(he->GetText()).substr(0, CODICE_CIG_SIZE);

  return rnl && idd;
}

void registra(tinyxml2::XMLPrinter &ft, const DatiDocumentiCorrelatiType &dt)
{
  ft.OpenElement(dt.tag().c_str());

  for (const auto &rif : dt.riferimento_numero_linea)
  {
    if (rif == 0 || rif > MAX_RIFERIMENTO_NUMERO_LINEA)
      throw eccezione::dati_doc_corr_linea_scorretta(
        dt.tag() + ": riferimento numero linea scorretto");

    ft.OpenElement(RIFERIMENTO_NUMERO_LINEA_TAG);
    ft.PushText(rif);
    ft.CloseElement();
  }

  if (dt.id_documento.empty())
    throw eccezione::dati_doc_corr_id_doc_mancante(
      dt.tag() + ": id documento mancante");

  ft.OpenElement(ID_DOCUMENTO_TAG);
  push_text(ft, dt.id_documento.substr(0, ID_DOCUMENTO_SIZE));
  ft.CloseElement();

  if (!dt.data.empty())
    registra(ft, dt.data);

  if (!dt.num_item.empty())
  {
    ft.OpenElement(NUM_ITEM_TAG);
    push_text(ft, dt.num_item.substr(0, NUM_ITEM_SIZE));
    ft.CloseElement();
  }

  if (!dt.codice_commessa_convenzione.empty())
  {
    ft.OpenElement(CODICE_COMMESSA_CONVENZIONE_TAG);
    push_text(ft,
              dt.codice_commessa_convenzione.substr(
                0, CODICE_COMMESSA_CONVENZIONE_SIZE));
    ft.CloseElement();
  }

  if (!dt.codice_cup.empty())
  {
    ft.OpenElement(CODICE_CUP_TAG);
    push_text(ft, dt.codice_cup.substr(0, CODICE_CUP_SIZE));
    ft.CloseElement();
  }

  if (!dt.codice_cig.empty())
  {
    ft.OpenElement(CODICE_CIG_TAG);
    push_text(ft, dt.codice_cig.substr(0, CODICE_CIG_SIZE));
    ft.CloseElement();
  }

  ft.CloseElement();
}

}  // namespace dettagli

}  // namespace felpa
