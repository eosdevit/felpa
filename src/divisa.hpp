/**
 *  \file
 *  \remark This file is part of FELPA.
 *
 *  \copyright Copyright (C) 2015-2019 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#if !defined(FELPA_DIVISA_HPP)
#define      FELPA_DIVISA_HPP

#include <string>

namespace felpa
{

///
/// Serve per identificare la moneta di conto. Contiene il codice ISO 4217
/// alpha-3:2001 identificante una divisa.
///
class Divisa
{
public:
  explicit Divisa(const std::string & = "EUR");

  operator std::string () const { return codice_; }

  Divisa &operator=(const std::string &);

  static const std::string &tag();

private:
  // Funzioni private di supporto.
  bool valido(const std::string &);

  // Data member privati.
  std::string codice_;
};  // class Divisa

extern bool operator==(const Divisa &, const Divisa &);

}  // namespace felpa

#endif  // include guard
