/**
 *  \file
 *  \remark This file is part of FELPA.
 *
 *  \copyright Copyright (C) 2015-2023 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#include <cassert>

#include "id_fiscale_type.hpp"
#include "fattura_elettronica_eccezioni.hpp"
#include "fattura_elettronica_dettagli.hpp"
#include "utility.hpp"

constexpr std::size_t MAX_LUNGHEZZA_ID = 28;

namespace felpa
{

IdFiscaleType::IdFiscaleType(const std::string &t) : id_paese(), id_codice(),
                                                     tag_(t)
{
  assert(!tag_.empty());
}

bool IdFiscaleType::valido() const
{
  if (id_paese.italia())
  {
    const auto cf_check([](const std::string &s)
    {
      return s.length() == 16;
    });

    const auto piva_check([](const std::string &s)
    {
      if (s.length() != 11)
        return false;

      unsigned cod(0);
      for (int i(0); i < 10; ++i)
      {
        if (!isdigit(s[i]))
          return false;

        const unsigned cifra(s[i] - '0');
        cod += (i & 1) ? (2 * cifra) / 10 + (2 * cifra) % 10 : cifra;
      }

      cod = (10 - cod % 10) % 10;
      return cod == static_cast<unsigned>(s[10] - '0');
    });

    return cf_check(id_codice) || piva_check(id_codice);
  }

  return id_codice.length() <= MAX_LUNGHEZZA_ID;
}

namespace dettagli
{

constexpr char ID_CODICE_TAG[] = "IdCodice";

bool carica(tinyxml2::XMLConstHandle &ft, IdFiscaleType *id)
{
  auto h(ft.FirstChildElement(id->tag().c_str()));
  if (!h.ToElement())
    return false;

  const bool ip(carica(h, &id->id_paese));

  if (auto *he = h.FirstChildElement(ID_CODICE_TAG).ToElement())
    id->id_codice = coalesce(he->GetText()).substr(0, MAX_LUNGHEZZA_ID);

  return ip && id->id_codice.length();
}

void registra(tinyxml2::XMLPrinter &ft, const IdFiscaleType &id)
{
  ft.OpenElement(id.tag().c_str());

  registra(ft, id.id_paese);

  if (id.id_codice.empty() || !id.valido())
    throw eccezione::id_fiscale_codice(id.tag() + ": IdCodice scorretto");
  ft.OpenElement(ID_CODICE_TAG);
  std::string id_codice(id.id_codice.substr(0, MAX_LUNGHEZZA_ID));
  if (id_codice.empty())
    id_codice = "OO99999999999";
  push_text(ft, id_codice);
  ft.CloseElement();

  ft.CloseElement();
}

}  // namespace dettagli

}  // namespace felpa
