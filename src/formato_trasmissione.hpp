/**
 *  \file
 *  \remark This file is part of FELPA.
 *
 *  \copyright Copyright (C) 2018-2024 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#if !defined(FELPA_FORMATO_TRASMISSIONE_HPP)
#define      FELPA_FORMATO_TRASMISSIONE_HPP

#include <string>
#include <vector>

namespace felpa
{

enum class formato_fattura {FPA = 0, FPR};

extern const std::vector<std::string> formato_fattura_str;

///
/// Codice identificativo del tipo di trasmissione che si sta effettuando e
/// del relativo formato.
///
/// Riveste un'utilita' di carattere tecnico. Il formato e' alfanumerico (5
/// caratteri). I valori ammessi sono:
/// - FPA12 trasmissione fattura verso PA ver. 1.2.x
/// - FPR12 trasmissione fattura verso privati ver 1.2.x
///
class FormatoTrasmissione
{
public:
  FormatoTrasmissione(formato_fattura);

  [[nodiscard]] operator std::string () const;
  FormatoTrasmissione &operator=(formato_fattura);

  [[nodiscard]] bool privato() const {return formato_==formato_fattura::FPR;}
  [[nodiscard]] bool pubblico() const {return formato_==formato_fattura::FPA;}

  [[nodiscard]] static const std::string &tag();

  [[nodiscard]] bool operator==(FormatoTrasmissione) const;

private:
  formato_fattura formato_;
};  // class FormatoTrasmissione

}  // namespace felpa

#endif  // include guard
