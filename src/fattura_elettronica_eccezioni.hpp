/**
 *  \file
 *  \remark This file is part of FELPA.
 *
 *  \copyright Copyright (C) 2015 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#if !defined(FELPA_ECCEZIONI_HPP)
#define      FELPA_ECCEZIONI_HPP

#include <stdexcept>

namespace felpa::eccezione
{

class felpa : public std::runtime_error
{
public:
  felpa(const std::string &msg) : std::runtime_error("[FELPA] " + msg) {}
};

class aliquota_iva_mancante : public felpa { using felpa::felpa; };
class altri_dati_gestionali_tipo_dato : public felpa { using felpa::felpa; };
class anagrafica_3 : public felpa { using felpa::felpa; };
class anagrafica_cod_eori : public felpa { using felpa::felpa; };
class anagrafica_cognome_mancante : public felpa { using felpa::felpa; };
class anagrafica_lunghezza_titolo : public felpa { using felpa::felpa; };
class anagrafica_nome_mancante : public felpa { using felpa::felpa; };
class causale_pagamento_vuota : public felpa { using felpa::felpa; };
class cod_causale_pagamento : public felpa { using felpa::felpa; };
class cod_destinatario_errato : public felpa { using felpa::felpa; };
class cod_destinatario_estero_per_italia : public felpa { using felpa::felpa; };
class cod_destinatario_mancante : public felpa { using felpa::felpa; };
class cod_esig_iva : public felpa { using felpa::felpa; };
class cod_regime_fiscale : public felpa { using felpa::felpa; };
class cod_tipo_mancante : public felpa { using felpa::felpa; };
class cod_tipo_cod_valore_incompatibili : public felpa { using felpa::felpa; };
class cod_valore_mancante : public felpa { using felpa::felpa; };
class codice_fiscale_per_estero : public felpa { using felpa::felpa; };
class cond_pag_cod_scorretto : public felpa { using felpa::felpa; };
class data_doc_mancante : public felpa { using felpa::felpa; };
class dati_cessionario_fiscale_mancante : public felpa { using felpa::felpa; };
class dati_doc_corr_id_doc_mancante : public felpa { using felpa::felpa; };
class dati_doc_corr_linea_scorretta : public felpa { using felpa::felpa; };
class dati_pagamento : public felpa { using felpa::felpa; };
class dett_linee_conflitto_natura : public felpa { using felpa::felpa; };
class dett_linee_dex_mancante : public felpa { using felpa::felpa; };
class dett_linee_numero_mancante : public felpa { using felpa::felpa; };
class dett_linee_pu_mancante : public felpa { using felpa::felpa; };
class dett_linee_vuoto : public felpa { using felpa::felpa; };
class dett_pagamento_importo_mancante : public felpa { using felpa::felpa; };
class dett_pagamento_modalita_mancante : public felpa { using felpa::felpa; };
class divisa_cod_scorretto : public felpa { using felpa::felpa; };
class doc_dati_cedente_fiscale_iva_mancante: public felpa {using felpa::felpa;};
class doc_dati_cedente_prov_albo_mancante : public felpa {using felpa::felpa;};
class formato_data : public felpa { using felpa::felpa; };
class formato_data_ora : public felpa { using felpa::felpa; };
class id_fiscale_codice : public felpa { using felpa::felpa; };
class imponibile_importo_mancante : public felpa { using  felpa::felpa; };
class indirizzo_cap_scorretto : public felpa { using felpa::felpa; };
class indirizzo_comune_mancante : public felpa { using felpa::felpa; };
class indirizzo_formato_provincia : public felpa { using felpa::felpa; };
class indirizzo_mancante : public felpa { using felpa::felpa; };
class indirizzo_nr_2 : public felpa { using felpa::felpa; };
class iscrizione_rea_cod_numero_mancante : public felpa {using felpa::felpa;};
class iscrizione_rea_cod_ufficio_mancante : public felpa {using felpa::felpa;};
class iscrizione_rea_cod_ufficio_scorretto: public felpa {using felpa::felpa;};
class iscrizione_rea_socio_unico_scorretto: public felpa {using felpa::felpa;};
class mod_pagamento_cod_scorretto : public felpa { using felpa::felpa; };
class natura_cod_scorretto : public felpa { using felpa::felpa; };
class nazione_cod_scorretto : public felpa { using felpa::felpa; };
class numero_cod : public felpa { using felpa::felpa; };
class numero_doc_mancante : public felpa { using felpa::felpa; };
class pec_destinatario_ridondante : public felpa { using felpa::felpa; };
class prog_invio_mancante : public felpa { using felpa::felpa; };
class rea_stato_liquidazione_scorretto : public felpa { using felpa::felpa; };
class registrazione : public felpa { using felpa::felpa; };
class riferimento_sal_errato : public felpa { using felpa::felpa; };
class ritenuta : public felpa { using felpa::felpa; };
class sconto_mag_2 : public felpa { using felpa::felpa; };
class sconto_mag_cod_tipo_scorretto : public felpa { using felpa::felpa; };
class soggetto_emittente_scorretto : public felpa { using felpa::felpa; };
class split_and_reverse : public felpa { using felpa::felpa; };
class stesso_cedente_cessionario : public felpa {using felpa::felpa;}; // 00471
class tipo_cessione_cod_scorretto : public felpa { using felpa::felpa; };
class tipo_doc_cod_scorretto : public felpa { using felpa::felpa; };
class tipo_ritenuta : public felpa { using felpa::felpa; };
class tipo_ritenuta_vuoto : public felpa { using felpa::felpa; };
class tutti_esteri : public felpa {using felpa::felpa; };

}  // namespace felpa::eccezione

#endif  // include guard
