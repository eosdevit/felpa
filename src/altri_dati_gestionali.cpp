/**
 *  \file
 *  \remark This file is part of FELPA.
 *
 *  \copyright Copyright (C) 2021 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#include <cassert>

#include "altri_dati_gestionali.hpp"
#include "fattura_elettronica_eccezioni.hpp"
#include "fattura_elettronica_dettagli.hpp"
#include "utility.hpp"

namespace felpa
{

const std::string &AltriDatiGestionali::tag()
{
  static const std::string TAG("AltriDatiGestionali");
  return TAG;
}

AltriDatiGestionali::AltriDatiGestionali()
  : tipo_dato(), riferimento_testo(), riferimento_numero(), riferimento_data()
{
}

bool AltriDatiGestionali::empty() const
{
  return *this == AltriDatiGestionali();
}

bool operator==(const AltriDatiGestionali &lhs, const AltriDatiGestionali &rhs)
{
  return lhs.tipo_dato == rhs.tipo_dato
         && lhs.riferimento_testo == rhs.riferimento_testo
         && std::string(lhs.riferimento_numero)
            == std::string(rhs.riferimento_numero)
         && lhs.riferimento_data == rhs.riferimento_data;
}

namespace dettagli
{

constexpr char RIFERIMENTO_TESTO_TAG[] = "RiferimentoTesto";
constexpr char TIPO_DATO_TAG[] = "TipoDato";

constexpr std::size_t RIFERIMENTO_TESTO_SIZE = 60;
constexpr std::size_t TIPO_DATO_SIZE = 10;

bool carica(tinyxml2::XMLConstHandle &ft, AltriDatiGestionali *a)
{
  tinyxml2::XMLConstHandle h(ft.FirstChildElement(a->tag().c_str()));
  if (!h.ToElement())
    h = ft;

  if (auto *he = h.FirstChildElement(TIPO_DATO_TAG).ToElement())
    a->tipo_dato = coalesce(he->GetText()).substr(0, TIPO_DATO_SIZE);

  if (auto *he = h.FirstChildElement(RIFERIMENTO_TESTO_TAG).ToElement())
    a->riferimento_testo = coalesce(he->GetText());

  carica(h, &a->riferimento_numero);
  carica(h, &a->riferimento_data);

  return a->tipo_dato.length();
}

void registra(tinyxml2::XMLPrinter &ft, const AltriDatiGestionali &a)
{
  if (a.empty())
    return;

  if (a.tipo_dato.empty())
    throw eccezione::altri_dati_gestionali_tipo_dato(
      AltriDatiGestionali::tag() + ": TipoDato mancante");

  ft.OpenElement(AltriDatiGestionali::tag().c_str());

  ft.OpenElement(TIPO_DATO_TAG);
  push_text(ft, a.tipo_dato.substr(0, TIPO_DATO_SIZE));
  ft.CloseElement();

  if (!a.riferimento_testo.empty())
  {
    ft.OpenElement(RIFERIMENTO_TESTO_TAG);
    push_text(ft, a.riferimento_testo.substr(0, RIFERIMENTO_TESTO_SIZE));
    ft.CloseElement();
  }

  registra(ft, a.riferimento_numero);

  if (!a.riferimento_data.empty())
    registra(ft, a.riferimento_data);

  ft.CloseElement();
}

}  // namespace dettagli

}  // namespace felpa
