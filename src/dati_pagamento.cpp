/**
 *  \file
 *  \remark This file is part of FELPA.
 *
 *  \copyright Copyright (C) 2015-2024 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#include <cassert>

#include "dati_pagamento.hpp"
#include "fattura_elettronica_dettagli.hpp"
#include "fattura_elettronica_eccezioni.hpp"

namespace felpa
{

const std::string &DatiPagamento::tag()
{
  static const std::string TAG("DatiPagamento");
  return TAG;
}

bool DatiPagamento::empty() const
{
  return condizioni_pagamento.empty() || dettaglio_pagamento.empty();
}

bool operator==(const DatiPagamento &lhs, const DatiPagamento &rhs)
{
  return std::string(lhs.condizioni_pagamento)
         == std::string(rhs.condizioni_pagamento)
         && lhs.dettaglio_pagamento == rhs.dettaglio_pagamento;
}

namespace dettagli
{

bool carica(tinyxml2::XMLConstHandle &ft, DatiPagamento *dp)
{
  auto h(ft.FirstChildElement(dp->tag().c_str()));
  if (!h.ToElement())
    return false;

  const bool cop(carica(h, &dp->condizioni_pagamento));

  dp->dettaglio_pagamento.clear();
  const char *dp_tag(DettaglioPagamento::tag().c_str());
  for (auto *he(h.FirstChildElement(dp_tag).ToElement());
       he;
       he = he->NextSiblingElement(dp_tag))
  {
    tinyxml2::XMLConstHandle heh(he);
    DettaglioPagamento dett_pag;
    if (carica(heh, &dett_pag))
      dp->dettaglio_pagamento.push_back(dett_pag);
  }

  return cop;
}

void registra(tinyxml2::XMLPrinter &ft, const DatiPagamento &dp)
{
  if (dp.empty())
    return;

  const auto rate(dp.dettaglio_pagamento.size());
  if (dp.condizioni_pagamento.completo() && rate > 1)
    throw eccezione::dati_pagamento(DatiPagamento::tag()
                                    + ": numero di rate scorretto");

  ft.OpenElement(dp.tag().c_str());

  registra(ft, dp.condizioni_pagamento);

  for (const auto &r : dp.dettaglio_pagamento)
    registra(ft, r);

  ft.CloseElement();
}

}  // namespace dettagli

}  // namespace felpa
