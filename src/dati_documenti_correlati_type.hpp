/**
 *  \file
 *  \remark This file is part of FELPA.
 *
 *  \copyright Copyright (C) 2015-2024 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#if !defined(FELPA_DATI_DOCUMENTI_CORRELATI_TYPE_HPP)
#define      FELPA_DATI_DOCUMENTI_CORRELATI_TYPE_HPP

#include <set>
#include <string>

#include "data.hpp"

namespace felpa
{

class DatiDocumentiCorrelatiType
{
public:
  [[nodiscard]] const std::string &tag() const { return tag_; }

  [[nodiscard]] bool empty() const;

  // ********* Data member pubblici *********
  std::set<unsigned> riferimento_numero_linea {};
  std::string id_documento {};
  Data data {};
  std::string num_item {};
  std::string codice_commessa_convenzione {};
  std::string codice_cup {};
  std::string codice_cig {};

protected:
  explicit DatiDocumentiCorrelatiType(const std::string &);

  [[nodiscard]] bool operator==(const DatiDocumentiCorrelatiType &) const;

private:
  std::string tag_;
};  // class DatiDocumentiCorrelatiType

}  // namespace felpa

#endif  // include guard
