/**
 *  \file
 *  \remark This file is part of FELPA.
 *
 *  \copyright Copyright (C) 2015-2020 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#include <cassert>

#include "quantita.hpp"
#include "fattura_elettronica_eccezioni.hpp"
#include "fattura_elettronica_dettagli.hpp"
#include "utility.hpp"

namespace felpa
{

Quantita::Quantita(const std::string &q)
  : NumeroType("Quantita", 6, solo_positivo)
{
  assign(q);
}

Quantita::Quantita(double q) : NumeroType("Quantita", 6, solo_positivo)
{
  // Il valore zero viene utilizzato col senso di non disponibile, cosi' da non
  // valorizzare la quantita' nei casi in cui la prestazione non sia
  // quantificabile o in corrispondenza di "righe nota".
  if (zero(q))
    assign("");
  else
    assign(q);
}

Quantita &Quantita::operator=(const std::string &q)
{
  return *this = Quantita(q);
}

Quantita &Quantita::operator=(double q)
{
  return *this = Quantita(q);

}

}  // namespace felpa
