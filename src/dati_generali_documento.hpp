/**
 *  \file
 *  \remark This file is part of FELPA.
 *
 *  \copyright Copyright (C) 2015-2024 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#if !defined(FELPA_DATI_GENERALI_DOCUMENTO_HPP)
#define      FELPA_DATI_GENERALI_DOCUMENTO_HPP

#include <vector>

#include "data.hpp"
#include "dati_ritenuta.hpp"
#include "dati_bollo.hpp"
#include "divisa.hpp"
#include "importo_totale_documento.hpp"
#include "sconto_maggiorazione.hpp"
#include "tipo_documento.hpp"

namespace felpa
{

class DatiGeneraliDocumento
{
public:
  DatiGeneraliDocumento() = default;

  [[nodiscard]] bool empty() const;

  [[nodiscard]] static const std::string &tag();

  // ********* Data member pubblici *********
  /// \warning OBBLIGATORIETA': SI, sempre.
  TipoDocumento tipo_documento {};

  /// \warning OBBLIGATORIETA': SI, sempre.
  Divisa divisa {};

  /// Data di emissione del documento.
  /// \warning OBBLIGATORIETA': SI, sempre.
  Data data {};

  /// Numero progressivo attribuito dal cedente/prestatore al documento (anche
  /// valori alfanumerici). Richiesto dalla normativa art. 21 DPR 633/1972.
  ///
  /// \note
  /// Non puo' essere emesso un documento che presenta lo stesso IdFiscaleIva
  /// del cedente/prestatore, lo stesso TipoDocumento, lo stesso anno nel campo
  /// Data e lo stesso Numero di un documento precedentemente trasmesso (e non
  /// scartato) al Sistema di Interscambio, fatti salvi i casi previsti dalla
  /// normativa.
  ///
  /// \warning OBBLIGATORIETA': SI, sempre.
  std::string numero {};

  /// Da valorizzare nei casi in cui sia applicabile la ritenuta.
  std::vector<DatiRitenuta> dati_ritenuta {};

  /// Da valorizzare nei casi in cui sia prevista l'imposta di bollo.
  DatiBollo dati_bollo {};

  /// Utile quando l'importo dello sconto praticato non riduce l'imponibile ai
  /// fini dell'imposta sul valore aggiunto (per esempio lavori lavori che
  /// accedono all'agevolazione fiscale per il recupero del patrimonio edilizio
  /// e per l’efficienza energetica
  /// \remark
  /// E' un dato non obbligatorio e non utilizzato ai fini del calcolo. Indica
  /// al cliente, il totale degli sconti applicati sul documento. Sia il totale
  /// documento che lo sconto non sono soggetti a controlli.
  std::vector<ScontoMaggiorazione> sconto_maggiorazione {};

  /// Serve per dare evidenza dell'ammontare totale del documento, diminuito
  /// dell'eventuale sconto o aumentato dell'eventale maggiorazione e
  /// comprensivo di imposta a debito del cessionario / committente.
  /// \warning OBBLIGATORIETA': consigliata.
  ImportoTotaleDocumento importo_totale_documento {};

  /// Descrizione della causale del documento.
  /// Non e' stabilito alcun criterio particolare; la modalita' di
  /// valorizzazione del campo, previsto per contenere una descrizione in
  /// formato alfanumerico, e' demandata, nel rispetto delle caratteristiche
  /// stabilite dallo schema XSD, alla valutazione dell'utente.
  std::string causale {};
};  // class DatiGeneraliDocumento

[[nodiscard]] bool operator==(const DatiGeneraliDocumento &,
                              const DatiGeneraliDocumento &);

}  // namespace felpa

#endif  // include guard
