/**
 *  \file
 *  \remark This file is part of FELPA.
 *
 *  \copyright Copyright (C) 2015-2020 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#include "anagrafica.hpp"
#include "fattura_elettronica_eccezioni.hpp"
#include "fattura_elettronica_dettagli.hpp"

namespace felpa
{

const std::string &Anagrafica::tag()
{
  static const std::string TAG("Anagrafica");
  return TAG;
}

Anagrafica::Anagrafica() : denominazione(), nome(), cognome(), titolo(),
                           cod_eori()
{
}

bool operator==(const Anagrafica &a1, const Anagrafica &a2)
{
  return a1.denominazione == a2.denominazione
         && a1.nome == a2.nome && a1.cognome == a2.cognome
         && a1.titolo == a2.titolo && a1.cod_eori == a2.cod_eori;
}

namespace dettagli
{

constexpr char COD_EORI_TAG[] = "CodEORI";
constexpr char COGNOME_TAG[] = "Cognome";
constexpr char DENOMINAZIONE_TAG[] = "Denominazione";
constexpr char NOME_TAG[] = "Nome";
constexpr char TITOLO_TAG[] = "Titolo";

constexpr std::size_t COD_EORI_MIN_SIZE  = 13;
constexpr std::size_t COD_EORI_MAX_SIZE  = 17;
constexpr std::size_t COGNOME_SIZE       = 60;
constexpr std::size_t DENOMINAZIONE_SIZE = 80;
constexpr std::size_t NOME_SIZE          = 60;
constexpr std::size_t TITOLO_SIZE        = 10;

bool carica(tinyxml2::XMLConstHandle &ft, Anagrafica *a)
{
  auto h(ft.FirstChildElement(Anagrafica::tag().c_str()));
  if (!h.ToElement())
    return false;

  if (auto *hde = h.FirstChildElement(DENOMINAZIONE_TAG).ToElement();
      hde && hde->GetText())
    a->denominazione = std::string(hde->GetText()).substr(0,
                                                          DENOMINAZIONE_SIZE);

  if (auto *hno = h.FirstChildElement(NOME_TAG).ToElement();
      hno && hno->GetText())
    a->nome = std::string(hno->GetText()).substr(0, NOME_SIZE);

  if (auto *hco = h.FirstChildElement(COGNOME_TAG).ToElement();
      hco && hco->GetText())
    a->cognome = std::string(hco->GetText()).substr(0, COGNOME_SIZE);;

  if (auto *hti = h.FirstChildElement(TITOLO_TAG).ToElement();
      hti && hti->GetText())
    a->titolo = std::string(hti->GetText()).substr(0, TITOLO_SIZE);;

  if (auto *hce = h.FirstChildElement(COD_EORI_TAG).ToElement();
      hce && hce->GetText())
    a->cod_eori = hce->GetText();

  return (a->denominazione.length() || (a->nome.length()
                                        && a->cognome.length()))
         && (a->cod_eori.empty()
             || (a->cod_eori.length() >= COD_EORI_MIN_SIZE
                 && a->cod_eori.length() <= COD_EORI_MAX_SIZE));
}

void registra(tinyxml2::XMLPrinter &ft, const Anagrafica &a)
{
  ft.OpenElement(Anagrafica::tag().c_str());

  if (!a.denominazione.empty() && !(a.nome.empty() && a.cognome.empty()))
    throw eccezione::anagrafica_3(
      Anagrafica::tag()
      + ": specificati contemporaneamente denominazione e nome/cognome");

  if (a.denominazione.empty())
  {
    if (a.nome.empty())
      throw eccezione::anagrafica_nome_mancante(Anagrafica::tag()
                                                + ": nome mancante");
    ft.OpenElement(NOME_TAG);
    push_text(ft, a.nome.substr(0, NOME_SIZE));
    ft.CloseElement();

    if (a.cognome.empty())
      throw eccezione::anagrafica_cognome_mancante(Anagrafica::tag()
                                                   + ": cognome mancante");
    ft.OpenElement(COGNOME_TAG);
    push_text(ft, a.cognome.substr(0, 60));
    ft.CloseElement();
  }
  else  // !a.denominazione.empty()
  {
    ft.OpenElement(DENOMINAZIONE_TAG);
    push_text(ft, a.denominazione.substr(0, DENOMINAZIONE_SIZE));
    ft.CloseElement();
  }

  if (!a.titolo.empty())
  {
    if (a.titolo.length() < 2)
      throw eccezione::anagrafica_lunghezza_titolo(
        Anagrafica::tag() + ": lunghezza minima del titolo scorretta");

    ft.OpenElement(TITOLO_TAG);
    push_text(ft, a.titolo.substr(0, TITOLO_SIZE));
    ft.CloseElement();
  }

  if (!a.cod_eori.empty())
  {
    if (a.cod_eori.length() < COD_EORI_MIN_SIZE
        || a.cod_eori.length() > COD_EORI_MAX_SIZE)
      throw eccezione::anagrafica_cod_eori(Anagrafica::tag()
                                           + ": lunghezza codice EORI");
    ft.OpenElement(COD_EORI_TAG);
    push_text(ft, a.cod_eori);
    ft.CloseElement();
  }

  ft.CloseElement();
}

}  // namespace dettagli

}  // namespace felpa
