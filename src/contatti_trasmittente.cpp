/**
 *  \file
 *  \remark This file is part of FELPA.
 *
 *  \copyright Copyright (C) 2015-2021 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#include <cassert>

#include "contatti_trasmittente.hpp"
#include "fattura_elettronica_dettagli.hpp"
#include "utility.hpp"

namespace felpa
{

constexpr std::size_t TELEFONO_SIZE =  12;
constexpr std::size_t EMAIL_SIZE    = 256;

const std::string &ContattiTrasmittente::tag()
{
  static const std::string TAG("ContattiTrasmittente");
  return TAG;
}

ContattiTrasmittente::ContattiTrasmittente(const std::string &t,
                                           const std::string &e)
  : telefono(t), email(e)
{
  assert(telefono.length() <= TELEFONO_SIZE);
  assert(email.length() <= EMAIL_SIZE);
}

bool ContattiTrasmittente::empty() const
{
  return telefono.empty() && email.empty();
}

bool operator==(const ContattiTrasmittente &lhs,
                const ContattiTrasmittente &rhs)
{
  return lhs.telefono == rhs.telefono && lhs.email == rhs.email;
}

namespace dettagli
{

constexpr char TELEFONO_TAG[] = "Telefono";
constexpr char EMAIL_TAG[] = "Email";

bool carica(tinyxml2::XMLConstHandle &ft, ContattiTrasmittente *ct)
{
  auto h(ft.FirstChildElement(ContattiTrasmittente::tag().c_str()));
  if (!h.ToElement())
    return false;

  if (auto *he = h.FirstChildElement(TELEFONO_TAG).ToElement())
    ct->telefono = coalesce(he->GetText()).substr(0, TELEFONO_SIZE);

  if (auto *he = h.FirstChildElement(EMAIL_TAG).ToElement())
    ct->email = coalesce(he->GetText()).substr(0, EMAIL_SIZE);

  return true;
}

void registra(tinyxml2::XMLPrinter &ft, const ContattiTrasmittente &ct)
{
  if (ct.empty())
    return;

  ft.OpenElement(ContattiTrasmittente::tag().c_str());

  if (!ct.telefono.empty())
  {
    ft.OpenElement(TELEFONO_TAG);
    push_text(ft, ct.telefono.substr(0, TELEFONO_SIZE));
    ft.CloseElement();
  }

  if (!ct.email.empty())
  {
    ft.OpenElement(EMAIL_TAG);
    push_text(ft, ct.email.substr(0, EMAIL_SIZE));
    ft.CloseElement();
  }

  ft.CloseElement();
}

}  // namespace dettagli

}  // namespace felpa
