/**
 *  \file
 *  \remark This file is part of FELPA.
 *
 *  \copyright Copyright (C) 2015-2021 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#include <cassert>

#include "dati_riepilogo.hpp"
#include "fattura_elettronica_dettagli.hpp"
#include "fattura_elettronica_eccezioni.hpp"
#include "utility.hpp"

namespace felpa
{

DatiRiepilogo::DatiRiepilogo(EsigibilitaIva ei)
  : aliquota_iva(), natura(), imponibile_importo(), imposta(),
    esigibilita_iva(ei), riferimento_normativo()
{
}

bool DatiRiepilogo::empty() const
{
  return aliquota_iva.empty() || imponibile_importo.empty() || imposta.empty();
}

const std::string &DatiRiepilogo::tag()
{
  static const std::string TAG("DatiRiepilogo");
  return TAG;
}

DatiRiepilogo compila(DatiRiepilogo dr)
{
  if (dr.imposta.empty() && !dr.aliquota_iva.empty())
  {
    const double imposta(dr.aliquota_iva.to_double() / 100.0
                         * dr.imponibile_importo.to_double());

    dr.imposta = imposta;
  }

  return dr;
}

bool operator==(const DatiRiepilogo &lhs, const DatiRiepilogo &rhs)
{
  return std::string(lhs.aliquota_iva) == std::string(rhs.aliquota_iva)
         && std::string(lhs.natura) == std::string(rhs.natura)
         && std::string(lhs.imponibile_importo)
            == std::string(rhs.imponibile_importo)
         && std::string(lhs.imposta) == std::string(rhs.imposta)
         && (lhs.natura.empty() || std::string(lhs.esigibilita_iva)
                                   == std::string(rhs.esigibilita_iva))
         && std::string(lhs.riferimento_normativo)
            == std::string(rhs.riferimento_normativo);
}

namespace dettagli
{

constexpr char RIFERIMENTO_NORMATIVO_TAG[] = "RiferimentoNormativo";

constexpr std::size_t RIFERIMENTO_NORMATIVO_SIZE = 100;

bool carica(tinyxml2::XMLConstHandle &ft, DatiRiepilogo *dr)
{
  tinyxml2::XMLConstHandle h(ft.FirstChildElement(dr->tag().c_str()));
  if (!h.ToElement())
    h = ft;

  const bool ai(carica(h, &dr->aliquota_iva));

  carica(h, &dr->natura);

  const bool ii(carica(h, &dr->imponibile_importo));

  const bool im(carica(h, &dr->imposta));

  carica(h, &dr->esigibilita_iva);

  if (auto *he = h.FirstChildElement(RIFERIMENTO_NORMATIVO_TAG).ToElement())
    dr->riferimento_normativo = coalesce(he->GetText())
                                .substr(0, RIFERIMENTO_NORMATIVO_SIZE);

  return ai && ii && im;
}

void registra(tinyxml2::XMLPrinter &ft, const DatiRiepilogo &dr)
{
  ft.OpenElement(dr.tag().c_str());

  if (dr.aliquota_iva.empty())
    throw eccezione::aliquota_iva_mancante(dr.tag()
                                           + ": aliquota IVA mancante");
  registra(ft, dr.aliquota_iva);

  registra(ft, dr.natura);

  if (dr.imponibile_importo.empty())
    throw eccezione::imponibile_importo_mancante(dr.tag()
                                                 + ": imponibile mancante");
  registra(ft, dr.imponibile_importo);

  const DatiRiepilogo drc(compila(dr));
  registra(ft, drc.imposta);

  // Split payment e reverse charge non sono compatibili.
  if (dr.natura.reverse_charge() && dr.esigibilita_iva.split_payment())
    throw eccezione::split_and_reverse(dr.tag()
                                       + ": split payment e reverse charge");

  if (dr.natura.empty())
    registra(ft, dr.esigibilita_iva);

  if (!dr.riferimento_normativo.empty())
  {
    ft.OpenElement(RIFERIMENTO_NORMATIVO_TAG);
    push_text(ft, dr.riferimento_normativo.substr(0,
                                                  RIFERIMENTO_NORMATIVO_SIZE));
    ft.CloseElement();
  }

  ft.CloseElement();
}

}  // namespace dettagli

}  // namespace felpa
