/**
 *  \file
 *  \remark This file is part of FELPA.
 *
 *  \copyright Copyright (C) 2022 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#if !defined(FELPA_SOGGETTO_EMITTENTE_HPP)
#define      FELPA_SOGGETTO_EMITTENTE_HPP

#include "dati_trasmissione.hpp"
#include "cedente_prestatore.hpp"
#include "cessionario_committente.hpp"

namespace felpa
{

///
/// Si utilizza quando la fattura e' emessa da un soggetto diverso dal
/// cedente/prestatore.
/// Si indica `CC` se la fattura e' stata compilata da parte del
/// cessionario/committente, `TZ` se e' stata compilata da un soggetto terzo.
///
/// \warning
/// OBBLIGATORIETA': SI, se la fattura e' emessa da un soggetto diverso dal
/// cedente/prestatore (e' un dato richiesto dalla normativa art. 21 DPR
/// 633/1972).
///
class SoggettoEmittente
{
public:
  explicit SoggettoEmittente(const std::string & = "");

  operator std::string () const { return codice_; }

  SoggettoEmittente &operator=(const std::string &);

  [[nodiscard]] bool empty() const;

  [[nodiscard]] static const std::string &tag();

private:
  std::string codice_;
};  // class SoggettoEmittente

[[nodiscard]] bool operator==(const SoggettoEmittente &,
                              const SoggettoEmittente &);

}  // namespace felpa

#endif  // include guard
