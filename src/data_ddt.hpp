/**
 *  \file
 *  \remark This file is part of FELPA.
 *
 *  \copyright Copyright (C) 2018 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#if !defined(FELPA_DATA_DDT_HPP)
#define      FELPA_DATA_DDT_HPP

#include "data_type.hpp"

namespace felpa
{

class DataDDT : public DataType
{
public:
  DataDDT() : DataType("DataDDT") {}

  DataDDT &operator=(const std::string &d)
  {
    assign(d);
    return *this;
  }
};  // class DataDDT

}  // namespace felpa

#endif  // include guard
