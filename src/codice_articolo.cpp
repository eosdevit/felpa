/**
 *  \file
 *  \remark This file is part of FELPA.
 *
 *  \copyright Copyright (C) 2015 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#include <algorithm>
#include <cassert>
#include <set>

#include "codice_articolo.hpp"
#include "fattura_elettronica_dettagli.hpp"
#include "fattura_elettronica_eccezioni.hpp"
#include "utility.hpp"

namespace felpa
{

const std::string &CodiceArticolo::tag()
{
  static const std::string TAG("CodiceArticolo");
  return TAG;
}

CodiceArticolo::CodiceArticolo() : codice_tipo(), codice_valore()
{
}

bool CodiceArticolo::empty() const
{
  return codice_tipo.empty() && codice_valore.empty();
}

bool operator==(const CodiceArticolo &lhs, const CodiceArticolo &rhs)
{
  return lhs.codice_tipo == rhs.codice_tipo
         && lhs.codice_valore == rhs.codice_valore;
}

namespace dettagli
{

constexpr std::size_t CODICE_VALORE_SIZE = 35;
constexpr std::size_t CODICE_TIPO_SIZE   = 35;

constexpr char CODICE_TIPO_TAG[] = "CodiceTipo";
constexpr char CODICE_VALORE_TAG[] = "CodiceValore";

bool carica(tinyxml2::XMLConstHandle &ft, CodiceArticolo *ca)
{
  auto h(ft.FirstChildElement(CodiceArticolo::tag().c_str()));
  if (!h.ToElement())
    return false;

  if (auto *he = h.FirstChildElement(CODICE_TIPO_TAG).ToElement())
    ca->codice_tipo = coalesce(he->GetText()).substr(0, CODICE_TIPO_SIZE);

  if (auto *he = h.FirstChildElement(CODICE_VALORE_TAG).ToElement())
    ca->codice_valore = coalesce(he->GetText()).substr(0, CODICE_VALORE_SIZE);

  return true;
}

void registra(tinyxml2::XMLPrinter &ft, const CodiceArticolo &ca)
{
  const std::set<std::string> ta13 = {"27101245", "27101249", "27101942",
                                      "27101944", "27102011"};
  if (ca.codice_tipo == "CARB" && ta13.find(ca.codice_valore) == ta13.end())
    throw eccezione::cod_tipo_cod_valore_incompatibili(
      ca.tag()
      + ": se la fattura e' relativa alla vendita di gasolio o benzina, "
        "l'elemento CodiceValore deve contenere un valore riportato nella "
        "tabella di riferimento per i prodotti energetici TA13");

  ft.OpenElement(CodiceArticolo::tag().c_str());

  if (ca.codice_tipo.empty())
    throw eccezione::cod_tipo_mancante(std::string(CODICE_TIPO_TAG)
                                       + ": valore mancante");
  ft.OpenElement(CODICE_TIPO_TAG);
  push_text(ft, ca.codice_tipo.substr(0, CODICE_TIPO_SIZE));
  ft.CloseElement();

  if (ca.codice_valore.empty())
    throw eccezione::cod_valore_mancante(std::string(CODICE_VALORE_TAG)
                                         + ": valore mancante");

  ft.OpenElement(CODICE_VALORE_TAG);
  push_text(ft, ca.codice_valore.substr(0, CODICE_VALORE_SIZE));
  ft.CloseElement();

  ft.CloseElement();
}

}  // namespace dettagli

}  // namespace felpa
