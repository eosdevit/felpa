/**
 *  \file
 *  \remark This file is part of FELPA.
 *
 *  \copyright Copyright (C) 2015 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#if !defined(FELPA_CODICE_ARTICOLO_HPP)
#define      FELPA_CODICE_ARTICOLO_HPP

#include <string>

namespace felpa
{

class CodiceArticolo
{
public:
  CodiceArticolo();

  bool empty() const;

  static const std::string &tag();

  // ********* Data member pubblici *********

  /// Serve per identificare lo standard di codifica del prodotto. Per esempio:
  /// "EAN", "SSC", "TARIC", "CPV", "Codice Art. fornitore",
  /// "Codice Art. cliente". Se la fattura e' relativa alla vendita di gasolio
  /// o benzina, l'elemento deve essere valorizzato con la dicitura `CARB`.
  std::string codice_tipo;

  /// Identificazione univoca in relazione alla tipologia di codice.
  /// Se l’elemento `codice_tipo` e' uguale a `CARB`, `codice_valore` deve
  /// contenere uno dei seguenti codici:
  /// - `27101245` (per vendita di Benzina senza piombo ottani => 95 e < 98);
  /// - `27101249` (per vendita di Benzina senza piombo ottani => 98);
  /// - `27101942` (per "Olii da gas, destinati ad altri usi, aventi tenore, in
  ///   peso, di zolfo inferiore o uguale a 0,001% - Con un tenore di carbonio
  ///   di origine biologica pari ad almeno l'80% in peso");
  /// - `27101944` (per "Olii da gas, destinati ad altri usi, aventi tenore, in
  ///   peso, di zolfo inferiore o uguale a 0,001% - Altri", per indicare
  ///   gasolio con carbonio di origine non biologica);
  /// - `27102011` (per vendita di Olio da gas denaturato tenore in peso di
  ///   zolfo nell'olio da gas <= 0,001%)
  ///
  /// riportati nella tabella di riferimento per i prodotti energetici TA13
  /// pubblicata sul sito dell'Agenzia delle Dogane.
  std::string codice_valore;
};  // class DettaglioLinee


bool operator==(const CodiceArticolo &, const CodiceArticolo &);

}  // namespace felpa

#endif  // include guard
