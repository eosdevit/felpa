/**
 *  \file
 *  \remark This file is part of FELPA.
 *
 *  \copyright Copyright (C) 2018-2019 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#include <cassert>
#include <type_traits>

#include "formato_trasmissione.hpp"
#include "fattura_elettronica_eccezioni.hpp"
#include "fattura_elettronica_dettagli.hpp"


namespace felpa
{

const std::vector<std::string> formato_fattura_str = {"FPA12", "FPR12"};

const std::string &FormatoTrasmissione::tag()
{
  static const std::string TAG("FormatoTrasmissione");
  return TAG;
}

namespace dettagli
{

bool carica(tinyxml2::XMLConstHandle &ft, FormatoTrasmissione *f)
{
  auto *h(ft.FirstChildElement(FormatoTrasmissione::tag().c_str()).ToElement());
  if (!h || !h->GetText())
    return false;

  for (std::size_t i(0); i < formato_fattura_str.size(); ++i)
    if (formato_fattura_str[i] == h->GetText())
    {
      *f = FormatoTrasmissione(formato_fattura(i));
      return true;
    }

  return false;
}

void registra(tinyxml2::XMLPrinter &ft, const FormatoTrasmissione &f)
{
  ft.OpenElement(FormatoTrasmissione::tag().c_str());
  push_text(ft, f);
  ft.CloseElement();
}

}  // namespace dettagli

FormatoTrasmissione::FormatoTrasmissione(formato_fattura id) : formato_(id)
{
}

FormatoTrasmissione &FormatoTrasmissione::operator=(formato_fattura id)
{
  *this = FormatoTrasmissione(id);
  return *this;
}

FormatoTrasmissione::operator std::string () const
{
  using utype = std::underlying_type<formato_fattura>::type;
  return formato_fattura_str[static_cast<utype>(formato_)];
}

bool FormatoTrasmissione::operator==(FormatoTrasmissione f) const
{
  return formato_ == f.formato_;
}

}  // namespace felpa
