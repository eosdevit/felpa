/**
 *  \file
 *  \remark This file is part of FELPA.
 *
 *  \copyright Copyright (C) 2015-2021 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#include <cassert>

#include "iscrizione_rea.hpp"
#include "fattura_elettronica_eccezioni.hpp"
#include "fattura_elettronica_dettagli.hpp"

namespace felpa
{

const std::string &IscrizioneRea::tag()
{
  static const std::string TAG("IscrizioneREA");
  return TAG;
}

IscrizioneRea::IscrizioneRea()
  : ufficio(), numero_rea(), capitale_sociale(), socio_unico(),
    stato_liquidazione("LN")
{
}

bool operator==(const IscrizioneRea &lhs, const IscrizioneRea &rhs)
{
  return lhs.ufficio == rhs.ufficio
         && lhs.numero_rea == rhs.numero_rea
         && std::string(lhs.capitale_sociale)
            == std::string(rhs.capitale_sociale)
         && lhs.socio_unico == rhs.socio_unico
         && lhs.stato_liquidazione == rhs.stato_liquidazione;
}

bool IscrizioneRea::empty() const
{
  return ufficio.empty() && numero_rea.empty();
}

namespace dettagli
{

bool carica(tinyxml2::XMLConstHandle &ft, IscrizioneRea *ir)
{
  auto h(ft.FirstChildElement(IscrizioneRea::tag().c_str()));
  if (!h.ToElement())
    return false;

  auto *huf(h.FirstChildElement("Ufficio").ToElement());
  const bool uf(huf && huf->GetText());
  if (uf)
    ir->ufficio = huf->GetText();

  auto *hnr(h.FirstChildElement("NumeroREA").ToElement());
  const bool nr(hnr && hnr->GetText());
  if (nr)
    ir->numero_rea = hnr->GetText();

  if (auto *hcs = h.FirstChildElement("CapitaleSociale").ToElement())
  {
    if (hcs->GetText())
      ir->capitale_sociale = hcs->GetText();
  }

  if (auto *hsu = h.FirstChildElement("SocioUnico").ToElement())
  {
    if (hsu->GetText())
      ir->socio_unico = hsu->GetText();
  }

  if (auto *hsl = h.FirstChildElement("StatoLiquidazione").ToElement())
  {
    if (hsl && hsl->GetText())
      ir->stato_liquidazione = hsl->GetText();
  }

  return uf && nr;
}

void registra(tinyxml2::XMLPrinter &ft, const IscrizioneRea &ir)
{
  if (ir.empty())
    return;

  ft.OpenElement(IscrizioneRea::tag().c_str());

  if (ir.ufficio.empty())
    throw eccezione::iscrizione_rea_cod_ufficio_mancante(
      IscrizioneRea::tag() + ": codice ufficio iscrizione REA mancante");
  if (ir.ufficio.length() != 2)
    throw eccezione::iscrizione_rea_cod_ufficio_scorretto(
      IscrizioneRea::tag() + ": codice ufficio iscrizione REA scorretto");
  ft.OpenElement("Ufficio");
  push_text(ft, ir.ufficio);
  ft.CloseElement();

  if (ir.numero_rea.empty())
    throw eccezione::iscrizione_rea_cod_numero_mancante(
      IscrizioneRea::tag() + ": numero iscrizione REA mancante");
  ft.OpenElement("NumeroREA");
  push_text(ft, ir.numero_rea.substr(0,20));
  ft.CloseElement();

  if (!ir.capitale_sociale.empty())
  {
    ft.OpenElement("CapitaleSociale");
    push_text(ft, ir.capitale_sociale);
    ft.CloseElement();
  }

  if (!ir.socio_unico.empty())
  {
    if (ir.socio_unico != "SU" && ir.socio_unico != "SM")
      throw eccezione::iscrizione_rea_socio_unico_scorretto(
        IscrizioneRea::tag() + ": codice SocioUnico scorretto");

    ft.OpenElement("SocioUnico");
    push_text(ft, ir.socio_unico);
    ft.CloseElement();
  }

  if (ir.stato_liquidazione != "LN" && ir.stato_liquidazione != "LS")
    throw eccezione::rea_stato_liquidazione_scorretto(
      IscrizioneRea::tag() + ": codice destinatario mancante");
  ft.OpenElement("StatoLiquidazione");
  push_text(ft, ir.stato_liquidazione);
  ft.CloseElement();

  ft.CloseElement();
}

}  // namespace dettagli

}  // namespace felpa
