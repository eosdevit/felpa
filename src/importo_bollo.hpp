/**
 *  \file
 *  \remark This file is part of FELPA.
 *
 *  \copyright Copyright (C) 2019 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#if !defined(FELPA_IMPORTO_BOLLO_HPP)
#define      FELPA_IMPORTO_BOLLO_HPP

#include "numero_type.hpp"

namespace felpa
{

///
/// Importa dell'imposta di bollo.
///
class ImportoBollo : public NumeroType
{
public:
  explicit ImportoBollo(const std::string &i = "")
    : NumeroType("ImportoBollo", 2, solo_positivo)
  {
    assign(i);
  }

  explicit ImportoBollo(double i)
    : NumeroType("ImportoBollo", 2, solo_positivo)
  {
    assign(i);
  }

  ImportoBollo &operator=(const std::string &i)
  {
    assign(i);
    return *this;
  }

  ImportoBollo &operator=(double i)
  {
    assign(i);
    return *this;
  }
};  // class ImportoBollo

}  // namespace felpa

#endif  // include guard
