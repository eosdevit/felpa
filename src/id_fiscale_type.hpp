/**
 *  \file
 *  \remark This file is part of FELPA.
 *
 *  \copyright Copyright (C) 2015-2023 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#if !defined(FELPA_ID_FISCALE_TYPE_HPP)
#define      FELPA_ID_FISCALE_TYPE_HPP

#include "id_paese.hpp"

namespace felpa
{

///
/// Identificativo fiscale (raggruppa nazione e specifico codice).
///
class IdFiscaleType
{
public:
  const std::string &tag() const { return tag_; }

  bool empty() const { return id_codice.empty(); }

  bool valido() const;

  // ********* Data member pubblici *********
  /// Codice del paese assegnante l'identificativo fiscale.
  IdPaese id_paese;

  /// Consente di identificare, ai fini fiscali, insieme al campo `IdPaese`, il
  /// soggetto che ha effettuato la cessione del bene o la prestazione del
  /// servizio oggetto di fatturazione.
  ///
  /// Il campo deve contenere, nel caso di soggetto residente in:
  /// - Italia (ovvero ivi operante mediante Stabile Organizzazione), il numero
  ///   di partita IVA.
  /// - nazione estera, un valore alfanumerico identificativo della
  ///   controparte, fino ad un massimo di 28 caratteri alfanumerici, su cui il
  ///   SdI non effettua controlli di validità. Se non specificato viene
  ///   utilizzato, di default, `OO99999999999`.
  ///
  /// \warning OBBLIGATORIETA': SI, sempre.
  std::string id_codice;

protected:
  explicit IdFiscaleType(const std::string &);

  bool operator==(const IdFiscaleType &rhs) const
  { return id_paese == rhs.id_paese && id_codice == rhs.id_codice; }

private:
  std::string tag_;
};  // class IdFiscaleType

}  // namespace felpa

#endif  // include guard
