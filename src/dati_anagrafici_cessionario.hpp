/**
 *  \file
 *  \remark This file is part of FELPA.
 *
 *  \copyright Copyright (C) 2015 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#if !defined(FELPA_DATI_ANAGRAFICI_CESSIONARIO_HPP)
#define      FELPA_DATI_ANAGRAFICI_CESSIONARIO_HPP

#include "dati_anagrafici_type.hpp"

namespace felpa
{

class DatiAnagraficiCessionario : public DatiAnagraficiType
{
public:
  explicit DatiAnagraficiCessionario();

  [[nodiscard]] bool operator==(const DatiAnagraficiType &rhs) const
  { return DatiAnagraficiType::operator==(rhs); }
};  // class DatiAnagraficiCessionario

}  // namespace felpa

#endif  // include guard
