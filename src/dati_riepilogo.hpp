/**
 *  \file
 *  \remark This file is part of FELPA.
 *
 *  \copyright Copyright (C) 2015-2024 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#if !defined(FELPA_DATI_RIEPILOGO_HPP)
#define      FELPA_DATI_RIEPILOGO_HPP

#include "aliquota_iva.hpp"
#include "esigibilita_iva.hpp"
#include "imponibile_importo.hpp"
#include "imposta.hpp"
#include "natura.hpp"

namespace felpa
{

class DatiRiepilogo
{
public:
  explicit DatiRiepilogo(EsigibilitaIva);

  [[nodiscard]] bool empty() const;

  [[nodiscard]] static const std::string &tag();

  // ********* Data member pubblici *********
  /// \warning OBBLIGATORIETA': SI, sempre.
  AliquotaIva aliquota_iva;

  /// \warning
  /// OBBLIGATORIETA': SI, se tra le righe di dettaglio ne figura almeno una
  /// che abbia il campo Natura valorizzato.
  Natura natura;

  /// \warning OBBLIGATORIETA': SI, sempre.
  ImponibileImporto imponibile_importo;

  /// Indica il valore dell'imposta sul valore aggiunto, corrispondente
  /// all'applicazione dell'aliquota IVA sul relativo imponibile.
  ///
  /// \warning OBBLIGATORIETA': SI, sempre.
  Imposta imposta;

  /// \warning
  /// OBBLIGATORIETA': SI, se si e' nel campo delle operazioni imponibili.
  EsigibilitaIva esigibilita_iva;

  /// Normativa di riferimento (obbligatorio nei casi di operazioni di cui
  /// all'elemento `Natura`).
  ///
  /// \remark Lunghezza massima 100 caratteri.
  std::string riferimento_normativo;
};  // class DatiRiepilogo

[[nodiscard]] DatiRiepilogo compila(DatiRiepilogo);

[[nodiscard]] bool operator==(const DatiRiepilogo &, const DatiRiepilogo &);

}  // namespace felpa

#endif  // include guard
