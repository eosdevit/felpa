/**
 *  \file
 *  \remark This file is part of FELPA.
 *
 *  \copyright Copyright (C) 2018-2024 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#if !defined(FELPA_DATI_DDT_HPP)
#define      FELPA_DATI_DDT_HPP

#include <set>
#include <string>

#include "data_ddt.hpp"

namespace felpa
{

///
/// Utilizzati quando e' presente un documento di trasporto che certifica il
/// trasferimento del bene dal cedente al cessionario e che deve essere
/// riportato nella fattura differita.
///
class DatiDDT
{
public:
  DatiDDT() = default;

  [[nodiscard]] bool empty() const;

  [[nodiscard]] static const std::string &tag();

  // ********* Data member pubblici *********
  /// Insieme delle linee di dettaglio della fattura alla quale si riferisce il
  /// documento di trasporto.
  /// Nel caso in cui il documento di trasporto si riferisca all'intera
  /// fattura, questo elemento non deve essere valorizzato.
  std::set<unsigned> riferimento_numero_linea {};

  /// Numero progressivo del documento di trasporto che il cedente ha
  /// attribuito al momento della sua emissione (art. 21, comma 4,
  /// DPR 633/1972).
  ///
  /// \remark Formato alfanumerico.
  std::string numero_ddt {};

  /// Data del documento di trasporto.
  DataDDT data_ddt {};
};  // class DatiDDT

[[nodiscard]] bool operator==(const DatiDDT &, const DatiDDT &);

}  // namespace felpa

#endif  // include guard
