/**
 *  \file
 *  \remark This file is part of FELPA.
 *
 *  \copyright Copyright (C) 2015-2024 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#include <cassert>

#include "dati_generali.hpp"
#include "fattura_elettronica_dettagli.hpp"
#include "fattura_elettronica_eccezioni.hpp"

namespace felpa
{

bool DatiGenerali::empty() const
{
  return dati_generali_documento.empty();
}

bool operator==(const DatiGenerali &lhs, const DatiGenerali &rhs)
{
  return lhs.dati_generali_documento == rhs.dati_generali_documento
         && lhs.dati_ordine_acquisto == rhs.dati_ordine_acquisto
         && lhs.dati_contratto == rhs.dati_contratto
         && lhs.dati_convenzione == rhs.dati_convenzione
         && lhs.dati_ricezione == rhs.dati_ricezione
         && lhs.dati_fatture_collegate == rhs.dati_fatture_collegate
         && lhs.dati_sal == rhs.dati_sal
         && lhs.dati_ddt == rhs.dati_ddt
         && lhs.dati_trasporto == rhs.dati_trasporto;
}

const std::string &DatiGenerali::tag()
{
  static const std::string TAG("DatiGenerali");
  return TAG;
}

namespace dettagli
{

bool carica(tinyxml2::XMLConstHandle &ft, DatiGenerali *dg)
{
  auto h(ft.FirstChildElement(dg->tag().c_str()));
  if (!h.ToElement())
    return false;

  const bool dgd(carica(h, &dg->dati_generali_documento));

  {
    dg->dati_ordine_acquisto.clear();
    const std::string doa_tag(DatiOrdineAcquisto().tag());
    for (auto *he(h.FirstChildElement(doa_tag.c_str()).ToElement());
         he;
         he = he->NextSiblingElement(doa_tag.c_str()))
    {
      tinyxml2::XMLConstHandle heh(he);
      if (DatiOrdineAcquisto tmp; carica(heh, &tmp))
        dg->dati_ordine_acquisto.push_back(tmp);
    }
  }

  {
    dg->dati_contratto.clear();
    const std::string dco_tag(DatiContratto().tag());
    for (auto *he(h.FirstChildElement(dco_tag.c_str()).ToElement());
         he;
         he = he->NextSiblingElement(dco_tag.c_str()))
    {
      tinyxml2::XMLConstHandle heh(he);
      if (DatiContratto tmp; carica(heh, &tmp))
        dg->dati_contratto.push_back(tmp);
    }
  }

  {
    dg->dati_convenzione.clear();
    const std::string dcv_tag(DatiConvenzione().tag());
    for (auto *he(h.FirstChildElement(dcv_tag.c_str()).ToElement());
         he;
         he = he->NextSiblingElement(dcv_tag.c_str()))
    {
      tinyxml2::XMLConstHandle heh(he);
      if (DatiConvenzione tmp; carica(heh, &tmp))
        dg->dati_convenzione.push_back(tmp);
    }
  }

  {
    dg->dati_ricezione.clear();
    const std::string dri_tag(DatiRicezione().tag());
    for (auto *he(h.FirstChildElement(dri_tag.c_str()).ToElement());
         he;
         he = he->NextSiblingElement(dri_tag.c_str()))
    {
      tinyxml2::XMLConstHandle heh(he);
      if (DatiRicezione tmp; carica(heh, &tmp))
        dg->dati_ricezione.push_back(tmp);
    }
  }

  {
    dg->dati_ddt.clear();
    const std::string ddt_tag(DatiDDT().tag());
    for (auto *he(h.FirstChildElement(ddt_tag.c_str()).ToElement());
         he;
         he = he->NextSiblingElement(ddt_tag.c_str()))
    {
      tinyxml2::XMLConstHandle heh(he);
      if (DatiDDT tmp; carica(heh, &tmp))
        dg->dati_ddt.push_back(tmp);
    }
  }

  {
    dg->dati_sal.clear();
    const std::string sal_tag(DatiSAL().tag());
    for (auto *he(h.FirstChildElement(sal_tag.c_str()).ToElement());
         he;
         he = he->NextSiblingElement(sal_tag.c_str()))
    {
      tinyxml2::XMLConstHandle heh(he);
      if (DatiSAL tmp; carica(heh, &tmp))
        dg->dati_sal.push_back(tmp);
    }
  }

  {
    dg->dati_fatture_collegate.clear();
    const std::string dft_tag(DatiFattureCollegate().tag());
    for (auto *he(h.FirstChildElement(dft_tag.c_str()).ToElement());
         he;
         he = he->NextSiblingElement(dft_tag.c_str()))
    {
      tinyxml2::XMLConstHandle heh(he);
      if (DatiFattureCollegate tmp; carica(heh, &tmp))
        dg->dati_fatture_collegate.push_back(tmp);
    }
  }

  carica(h, &dg->dati_trasporto);

  return dgd;
}

void registra(tinyxml2::XMLPrinter &ft, const DatiGenerali &dg)
{
  ft.OpenElement(dg.tag().c_str());

  registra(ft, dg.dati_generali_documento);

  for (const auto &i : dg.dati_ordine_acquisto)
    registra(ft, i);

  for (const auto &i : dg.dati_contratto)
    registra(ft, i);

  for (const auto &i : dg.dati_convenzione)
    registra(ft, i);

  for (const auto &i : dg.dati_ricezione)
    registra(ft, i);

  for (const auto &i : dg.dati_fatture_collegate)
    registra(ft, i);

  for (const auto &i : dg.dati_sal)
    registra(ft, i);

  for (const auto &i : dg.dati_ddt)
    registra(ft, i);

  registra(ft, dg.dati_trasporto);

  ft.CloseElement();
}

}  // namespace dettagli

}  // namespace felpa
