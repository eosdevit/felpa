/**
 *  \file
 *  \remark This file is part of FELPA.
 *
 *  \copyright Copyright (C) 2015-2018 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#if !defined(FELPA_CAPITALE_SOCIALE_HPP)
#define      FELPA_CAPITALE_SOCIALE_HPP

#include "numero_type.hpp"

namespace felpa
{

class CapitaleSociale : public NumeroType
{
public:
  explicit CapitaleSociale(const std::string &v = "")
    : NumeroType("CapitaleSociale", 2, solo_positivo)
  {
    assign(v);
  }

  explicit CapitaleSociale(double v)
    : NumeroType("CapitaleSociale", 2, solo_positivo)
  {
    assign(v);
  }

  CapitaleSociale &operator=(const std::string &v)
  {
    assign(v);
    return *this;
  }

  CapitaleSociale &operator=(double v)
  {
    assign(v);
    return *this;
  }
};  // class CapitaleSociale

}  // namespace felpa

#endif  // include guard
