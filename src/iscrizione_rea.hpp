/**
 *  \file
 *  \remark This file is part of FELPA.
 *
 *  \copyright Copyright (C) 2015-2021 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#if !defined(FELPA_ISCRIZIONE_REA_HPP)
#define      FELPA_ISCRIZIONE_REA_HPP

#include <string>

#include "capitale_sociale.hpp"

namespace felpa
{

///
/// Utilizzato per societa' iscritte nel registro delle imprese (obbligo di
/// indicare il numero di scrizione da art. 2250 codice civile).
///
class IscrizioneRea
{
public:
  IscrizioneRea();

  bool empty() const;

  static const std::string &tag();

  // ********* Data member pubblici *********
  /// Deve contenere la sigla della provincia dell'ufficio del registro delle
  /// imprese presso il quale e' iscritto e registrato il cedente/prestatore.
  ///
  /// \warning
  /// OBBLIGATORIETA': si, se indicato dalla normativa in materia di obblighi
  /// di pubblicita' delle societa' (art. 2250 codice civile).
  std::string ufficio;

  /// Deve contenere il numero con il quale il cedente/prestatore risulta
  /// registrato presso il registro delle imprese.
  ///
  /// \warning
  /// OBBLIGATORIETA': si, se indicato dalla normativa in materia di obblighi
  /// di pubblicita' delle societa' (art. 2250 codice civile).
  std::string numero_rea;

  /// Previsto dalla normativa in materia di obblighi di pubblicita' delle
  /// societa' (art. 2250 codice civile).
  ///
  /// Deve contenere l'importo del capitale sociale effettivamente versato come
  /// risultante dall'ultimo bilancio. E' previsto un valore numerico composto
  /// da un intero e da due decimali; i decimali, separati dall'intero con il
  /// carattere punto ('.'), vanno sempre indicati anche se pari a zero.
  ///
  /// \warning
  /// OBBLIGATORIETA': se il cedente/prestatore e' una societa' di capitali
  /// (SpA, SApA, Srl).
  CapitaleSociale capitale_sociale;

  /// Previsto dalla normativa in materia di obblighi di pubblicita' delle
  /// societa' (art. 2250 codice civile).
  ///
  /// Deve contenere il valore "SU" nel caso di socio unico, oppure il valore
  /// "SM" nel caso di societa' pluripersonale.
  ///
  /// \warning
  /// OBBLIGATORIETA': se il cedente/prestatore e' una Societa' a
  /// responsabilita' limitata.
  std::string socio_unico;

  /// Previsto dalla normativa in materia di obblighi di pubblicita' delle
  /// societa' (art. 2250 codice civile).
  ///
  /// Deve contenere il valore "LS", nel caso di societa' in stato di
  /// liquidazione, oppure "LN" nel caso di societa' non in liquidazione.
  ///
  /// \warning OBBLIGATORIETA': Si, sempre.
  std::string stato_liquidazione;
};  // class IscrizioneRea

bool operator==(const IscrizioneRea &, const IscrizioneRea &);

}  // namespace felpa

#endif  // include guard
