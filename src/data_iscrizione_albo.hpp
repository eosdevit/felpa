/**
 *  \file
 *  \remark This file is part of FELPA.
 *
 *  \copyright Copyright (C) 2015-2020 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#if !defined(FELPA_DATA_ISCRIZIONE_ALBO_HPP)
#define      FELPA_DATA_ISCRIZIONE_ALBO_HPP

#include "data_type.hpp"

namespace felpa
{

class DataIscrizioneAlbo : public DataType
{
public:
  DataIscrizioneAlbo() : DataType("DataIscrizioneAlbo") {}

  DataType &operator=(const std::string &d)
  {
    assign(d);
    return *this;
  }
};  // class DataIscrizioneAlbo

}  // namespace felpa

#endif  // Include guard
