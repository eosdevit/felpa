/**
 *  \file
 *  \remark This file is part of FELPA.
 *
 *  \copyright Copyright (C) 2015-2021 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#if !defined(FELPA_TIPO_CESSIONE_PRESTAZIONE_HPP)
#define      FELPA_TIPO_CESSIONE_PRESTAZIONE_HPP

#include <string>

namespace felpa
{

///
/// Dato funzionale alla corretta determinazione della base imponibile.
///
class TipoCessionePrestazione
{
public:
  explicit TipoCessionePrestazione(const std::string & = "");

  bool empty() const { return codice_.empty(); }

  operator std::string () const { return codice_; }

  TipoCessionePrestazione &operator=(const std::string &);

  static const std::string &tag();

private:
  // ********* Funzioni private di supporto *********
  bool valido(const std::string &);

  // ********* Data member privati *********
  std::string codice_;
};  // class TipoCessionePrestazione

}  // namespace felpa

#endif  // include guard
