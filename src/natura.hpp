/**
 *  \file
 *  \remark This file is part of FELPA.
 *
 *  \copyright Copyright (C) 2015-2024 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#if !defined(FELPA_NATURA_HPP)
#define      FELPA_NATURA_HPP

#include <string>

namespace felpa
{

///
/// Serve per dare evidenza della natura dell'operazione per la quale non e'
/// prevista applicazione IVA.
///
class Natura
{
public:
  explicit Natura(const std::string & = "");

  [[nodiscard]] bool empty() const { return codice_.empty(); }
  [[nodiscard]] bool reverse_charge() const;

  [[nodiscard]] operator std::string () const { return codice_; }
  Natura &operator=(const std::string &);

  [[nodiscard]] static const std::string &tag();

private:
  // Funzioni private di supporto.
  [[nodiscard]] static bool valido(const std::string &);

  // Data member pubblici.
  std::string codice_;
};  // class Natura

[[nodiscard]] bool operator==(const Natura &, const Natura &);

}  // namespace felpa

#endif  // include guard
