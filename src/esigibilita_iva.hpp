/**
 *  \file
 *  \remark This file is part of FELPA.
 *
 *  \copyright Copyright (C) 2015-2024 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#if !defined(FELPA_ESIGIBILITA_IVA_HPP)
#define      FELPA_ESIGIBILITA_IVA_HPP

#include <string>

namespace felpa
{

///
/// Indica il regime di esigibilita' dell'IVA (differita od immediata) o
/// l'eventuale assoggettamento dell'operazione al meccanismo della scissione
/// del pagamento.
///
class EsigibilitaIva
{
public:
  explicit EsigibilitaIva(const std::string &);

  [[nodiscard]] operator std::string () const { return codice_; }
  EsigibilitaIva &operator=(const std::string &);

  [[nodiscard]] bool split_payment() const { return codice_ == "S"; }

  [[nodiscard]] static const std::string &tag();

private:
  // ********* Funzioni private di supporto *********
  [[nodiscard]] static bool valido(const std::string &);

  // ********* Data member privati *********
  std::string codice_;
};  // class EsigibilitaIva

}  // namespace felpa

#endif  // include guard
