/**
 *  \file
 *  \remark This file is part of FELPA.
 *
 *  \copyright Copyright (C) 2018-2023 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#if !defined(FELPA_CODICE_DESTINATARIO_HPP)
#define      FELPA_CODICE_DESTINATARIO_HPP

#include <string>

namespace felpa
{

///
/// Indica il soggetto al quale e' destinata la fattura.
///
/// - Nei casi di fattura destinata ad una Pubblica Amministrazione il suo
///   valore, di 6 caratteri, deve corrispondere a quello presente nel campo
///   Codice Ufficio riportato nella rubrica "Indice PA" (WWW.INDICEPA.GOV.IT).
/// - Nei casi di fattura destinata ad un soggetto diverso da Pubblica
///   Amministrazione il suo valore, di 7 caratteri, deve essere uno dei
///   codici che il Sistema di Interscambio attribuisce ai soggetti con
///   canale accreditato in ricezione.
///
/// \note
/// - In alternativa, e' possibile valorizzare il campo con il codice "Ufficio
///   "centrale" o con il valore di default "999999", quando ricorrono le
///   condizioni previste dalle disposizioni della circolare interpretativa
///   del Ministero dell'Economia e delle Finanze n.1 del 31 marzo 2014;
/// - nei casi di fattura destinata ad un soggetto diverso da Pubblica
///   Amministrazione che riceve tramite PEC, l'elemento informativo deve
///   essere valorizzato con la stringa `0000000`;
/// - nel caso di cliente estero l'elemento deve essere valorizzato con la
///   stringa `XXXXXXX`.
///
class CodiceDestinatario
{
public:
  explicit CodiceDestinatario(const std::string & = "");

  operator std::string () const { return codice_; }
  CodiceDestinatario &operator=(const std::string &);

  static const std::string &tag();

  [[nodiscard]] bool empty() const { return codice_.empty(); }
  [[nodiscard]] bool estero() const { return codice_ == "XXXXXXX"; }
  [[nodiscard]] bool privato() const { return codice_.length() == 7; }
  [[nodiscard]] bool pubblico() const { return codice_.length() == 6; }

private:
  // Funzioni private di supporto.
  static bool valido(const std::string &);

  // Data member privati.
  std::string codice_;
};  // class CodiceDestinatario

[[nodiscard]] bool operator==(const CodiceDestinatario &,
                              const CodiceDestinatario &);

}  // namespace felpa

#endif  // include guard
