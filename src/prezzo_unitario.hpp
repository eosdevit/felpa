/**
 *  \file
 *  \remark This file is part of FELPA.
 *
 *  \copyright Copyright (C) 2015-2018 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#if !defined(FELPA_PREZZO_UNITARIO_HPP)
#define      FELPA_PREZZO_UNITARIO_HPP

#include "numero_type.hpp"

namespace felpa
{

class PrezzoUnitario : public NumeroType
{
public:
  explicit PrezzoUnitario(const std::string &p = "")
    : NumeroType("PrezzoUnitario", 4, anche_negativo)
  {
    assign(p);
  }

  explicit PrezzoUnitario(double p)
    : NumeroType("PrezzoUnitario", 4, anche_negativo)
  {
    assign(p);
  }

  PrezzoUnitario &operator=(const std::string &p)
  {
    assign(p);
    return *this;
  }

  PrezzoUnitario &operator=(double p)
  {
    assign(p);
    return *this;
  }

};  // class PrezzoUnitario

}  // namespace felpa

#endif  // include guard
