/**
 *  \file
 *  \remark This file is part of FELPA.
 *
 *  \copyright Copyright (C) 2018-2019 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#if !defined(FELPA_IMPORTO_RITENUTA_HPP)
#define      FELPA_IMPORTO_RITENUTA_HPP

#include "numero_type.hpp"

namespace felpa
{

///
/// Importa della ritenuta.
///
class ImportoRitenuta : public NumeroType
{
public:
  explicit ImportoRitenuta(const std::string &i = "")
    : NumeroType("ImportoRitenuta", 2, anche_negativo)
  {
    assign(i);
  }

  explicit ImportoRitenuta(double i)
    : NumeroType("ImportoRitenuta", 2, anche_negativo)
  {
    assign(i);
  }

  ImportoRitenuta &operator=(const std::string &i)
  {
    assign(i);
    return *this;
  }

  ImportoRitenuta &operator=(double i)
  {
    assign(i);
    return *this;
  }
};  // class ImportoRitenuta

}  // namespace felpa

#endif  // include guard
