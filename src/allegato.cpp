/**
 *  \file
 *  \remark This file is part of FELPA.
 *
 *  \copyright Copyright (C) 2019-2021 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#include <cassert>

#include "allegato.hpp"
#include "fattura_elettronica_dettagli.hpp"
#include "fattura_elettronica_eccezioni.hpp"
#include "utility.hpp"

namespace felpa
{

const std::string &Allegato::tag()
{
  static const std::string TAG("Allegati");
  return TAG;
}

Allegato::Allegato() : nome_attachment(), algoritmo_compressione(),
                       formato_attachment(), descrizione_attachment(),
                       attachment()
{
}

Allegato::Allegato(const std::string &nf)
  : nome_attachment(extract_filename(nf)), algoritmo_compressione(),
    formato_attachment(), descrizione_attachment(),
    attachment(base64_encode(nf))
{
}

bool Allegato::empty() const
{
  return nome_attachment.empty() || attachment.empty();
}

bool operator==(const Allegato &lhs, const Allegato &rhs)
{
  return lhs.nome_attachment == rhs.nome_attachment
         && lhs.algoritmo_compressione == rhs.algoritmo_compressione
         && lhs.attachment == rhs.attachment
         && lhs.descrizione_attachment == rhs.descrizione_attachment
         && lhs.formato_attachment == rhs.formato_attachment;
}

namespace dettagli
{

constexpr char ALGORITMO_COMPRESSIONE_TAG[] = "AlgoritmoCompressione";
constexpr char FORMATO_ATTACHMENT_TAG[] = "FormatoAttachment";
constexpr char NOME_ATTACHMENT_TAG[] = "NomeAttachment";

constexpr std::size_t ALGORITMO_COMPRESSIONE_SIZE =  10;
constexpr std::size_t DESCRIZIONE_ATTACHMENT_SIZE = 100;
constexpr std::size_t     FORMATO_ATTACHMENT_SIZE =  10;
constexpr std::size_t        NOME_ATTACHMENT_SIZE =  60;

bool carica(tinyxml2::XMLConstHandle &ft, Allegato *a)
{
  tinyxml2::XMLConstHandle h(ft.FirstChildElement(a->tag().c_str()));
  if (!h.ToElement())
    h = ft;

  if (auto *na = h.FirstChildElement(NOME_ATTACHMENT_TAG).ToElement())
    a->nome_attachment =
      coalesce(na->GetText()).substr(0, NOME_ATTACHMENT_SIZE);

  if (auto *ac = h.FirstChildElement(ALGORITMO_COMPRESSIONE_TAG).ToElement())
    a->algoritmo_compressione =
      coalesce(ac->GetText()).substr(0, ALGORITMO_COMPRESSIONE_SIZE);

  if (auto *fa = h.FirstChildElement(FORMATO_ATTACHMENT_TAG).ToElement())
    a->formato_attachment =
      coalesce(fa->GetText()).substr(0, FORMATO_ATTACHMENT_SIZE);

  if (auto *da = h.FirstChildElement("DescrizioneAttachment").ToElement())
    a->descrizione_attachment =
      coalesce(da->GetText()).substr(0, DESCRIZIONE_ATTACHMENT_SIZE);

  if (auto *at = h.FirstChildElement("Attachment").ToElement())
    a->attachment = coalesce(at->GetText());

  return a->nome_attachment.length() && !a->attachment.empty();
}

void registra(tinyxml2::XMLPrinter &ft, const Allegato &a)
{
  if (a.empty())
    return;

  ft.OpenElement(Allegato::tag().c_str());

  ft.OpenElement(NOME_ATTACHMENT_TAG);
  push_text(ft, a.nome_attachment.substr(0, NOME_ATTACHMENT_SIZE));
  ft.CloseElement();

  if (!a.algoritmo_compressione.empty())
  {
    ft.OpenElement(ALGORITMO_COMPRESSIONE_TAG);
    push_text(ft,
              a.algoritmo_compressione.substr(0, ALGORITMO_COMPRESSIONE_SIZE));
    ft.CloseElement();
  }

  if (!a.formato_attachment.empty())
  {
    ft.OpenElement(FORMATO_ATTACHMENT_TAG);
    push_text(ft, a.formato_attachment.substr(0, FORMATO_ATTACHMENT_SIZE));
    ft.CloseElement();
  }

  if (!a.descrizione_attachment.empty())
  {
    ft.OpenElement("DescrizioneAttachment");
    push_text(ft, a.descrizione_attachment.substr(0,
                                                  DESCRIZIONE_ATTACHMENT_SIZE));
    ft.CloseElement();
  }

  ft.OpenElement("Attachment");
  push_text(ft, a.attachment);
  ft.CloseElement();

  ft.CloseElement();
}

}  // namespace dettagli

}  // namespace felpa
