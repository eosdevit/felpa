/**
 *  \file
 *  \remark This file is part of FELPA.
 *
 *  \copyright Copyright (C) 2015-2021 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#include <cassert>
#include "dati_anagrafici_cedente.hpp"
#include "fattura_elettronica_eccezioni.hpp"
#include "fattura_elettronica_dettagli.hpp"
#include "utility.hpp"

namespace felpa
{

DatiAnagraficiCedente::DatiAnagraficiCedente()
  : DatiAnagraficiType("DatiAnagrafici"), albo_professionale(),
    provincia_albo(), numero_iscrizione_albo(), data_iscrizione_albo(),
    regime_fiscale()
{
}

bool DatiAnagraficiCedente::operator==(const DatiAnagraficiCedente &rhs) const
{
  return DatiAnagraficiType::operator==(rhs)
         && albo_professionale == rhs.albo_professionale
         && provincia_albo == rhs.provincia_albo
         && numero_iscrizione_albo == rhs.numero_iscrizione_albo
         && data_iscrizione_albo == rhs.data_iscrizione_albo
         && regime_fiscale == rhs.regime_fiscale;
}

namespace dettagli
{

constexpr char ALBO_PROFESSIONALE_TAG[] = "AlboProfessionale";
constexpr char PROVINCIA_ALBO_TAG[] = "ProvinciaAlbo";
constexpr char NUMERO_ISCRIZIONE_ALBO_TAG[] = "NumeroIscrizioneAlbo";

constexpr std::size_t ALBO_PROFESSIONALE_SIZE     = 13;
constexpr std::size_t PROVINCIA_ALBO_SIZE         =  2;
constexpr std::size_t NUMERO_ISCRIZIONE_ALBO_SIZE = 60;

bool carica(tinyxml2::XMLConstHandle &ft, DatiAnagraficiCedente *dac)
{
  tinyxml2::XMLConstHandle h(ft.FirstChildElement(dac->tag().c_str()));
  if (!h.ToElement())
    return false;

  const bool dat(carica(h, false, dac));

  if (auto *he = h.FirstChildElement(ALBO_PROFESSIONALE_TAG).ToElement())
    dac->albo_professionale =
      coalesce(he->GetText()).substr(0, ALBO_PROFESSIONALE_SIZE);

  if (auto *he = h.FirstChildElement(PROVINCIA_ALBO_TAG).ToElement())
    dac->provincia_albo = coalesce(he->GetText());

  if (auto *he = h.FirstChildElement(NUMERO_ISCRIZIONE_ALBO_TAG).ToElement())
    dac->numero_iscrizione_albo =
      coalesce(he->GetText()).substr(0, NUMERO_ISCRIZIONE_ALBO_SIZE);

  carica(h, &dac->data_iscrizione_albo);

  const bool rf(carica(h, &dac->regime_fiscale));

  return dat && rf
         && (dac->provincia_albo.empty()
             || dac->provincia_albo.length() == PROVINCIA_ALBO_SIZE);
}

void registra(tinyxml2::XMLPrinter &ft, const DatiAnagraficiCedente &dac)
{
  ft.OpenElement(dac.tag().c_str());

  if (dac.id_fiscale_iva.empty())
    throw eccezione::doc_dati_cedente_fiscale_iva_mancante(
      "DatiAnagraficiCedente/IdFiscaleIva: codice mancante");

  registra(ft, dac, false);

  if (!dac.albo_professionale.empty())
  {
    ft.OpenElement(ALBO_PROFESSIONALE_TAG);
    push_text(ft, dac.albo_professionale.substr(0, ALBO_PROFESSIONALE_SIZE));
    ft.CloseElement();
  }

  if (!dac.provincia_albo.empty())
  {
    if (dac.provincia_albo.length() != PROVINCIA_ALBO_SIZE)
      throw eccezione::doc_dati_cedente_prov_albo_mancante(
        "ProvinciaAlbo: codice scorretto o mancante");
    ft.OpenElement(PROVINCIA_ALBO_TAG);
    push_text(ft, dac.provincia_albo);
    ft.CloseElement();
  }

  if (!dac.numero_iscrizione_albo.empty())
  {
    ft.OpenElement(NUMERO_ISCRIZIONE_ALBO_TAG);
    push_text(ft,
              dac.numero_iscrizione_albo.substr(0,
                                                NUMERO_ISCRIZIONE_ALBO_SIZE));
    ft.CloseElement();
  }

  if (!dac.data_iscrizione_albo.empty())
    registra(ft, dac.data_iscrizione_albo);

  registra(ft, dac.regime_fiscale);

  ft.CloseElement();
}

}  // namespace dettagli

}  // namespace felpa
