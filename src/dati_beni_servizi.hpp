/**
 *  \file
 *  \remark This file is part of FELPA.
 *
 *  \copyright Copyright (C) 2015-2024 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#if !defined(FELPA_DATI_BENI_SERVIZI_HPP)
#define      FELPA_DATI_BENI_SERVIZI_HPP

#include <map>
#include <vector>

#include "dati_riepilogo.hpp"
#include "dettaglio_linee.hpp"

namespace felpa
{

///
/// Contiene i dati relativi all'operazione cui la fattura si riferisce: la
/// cessione di beni o la prestazione di servizi.
///
class DatiBeniServizi
{
public:
  explicit DatiBeniServizi(EsigibilitaIva);

  [[nodiscard]] bool empty() const;

  void aggiungi_riferimento_normativo(const Natura &, const std::string &);
  [[nodiscard]] std::string riferimento_normativo(const Natura &) const;

  [[nodiscard]] static const std::string &tag();

  // ********* Data member pubblici *********
  EsigibilitaIva default_esigibilita_iva;

  std::vector<DettaglioLinee> dettaglio_linee;
  std::vector<DatiRiepilogo>   dati_riepilogo;

private:
  friend DatiBeniServizi completa(DatiBeniServizi, EsigibilitaIva);
  std::map<std::string, std::string> riferimenti_normativi_;
};  // class DatiBeniServizi

///
/// Completa i totali numerici delle sezioni `DettaglioLinee` e
/// `DatiRiepilogo` (imposte ed imponibili suddivisi per aliquota).
///
/// \note
/// Nel caso in cui la sezione `DatiRiepilogo` sia gia' stata compilata (**in
/// ogni sua parte**), vengono riutilizzati i dati disponibili senza effettuare
/// nuovi calcoli o sovrascritture i dati preesistenti.
///
[[nodiscard]] DatiBeniServizi completa(DatiBeniServizi, EsigibilitaIva);

[[nodiscard]] inline DatiBeniServizi completa(const DatiBeniServizi &dbs)
{
  return completa(dbs, dbs.default_esigibilita_iva);
}

[[nodiscard]] bool operator==(const DatiBeniServizi &,
                              const DatiBeniServizi &);

}  // namespace felpa

#endif  // include guard
