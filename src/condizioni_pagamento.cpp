/**
 *  \file
 *  \remark This file is part of FELPA.
 *
 *  \copyright Copyright (C) 2015-2020 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#include <cassert>
#include <set>

#include "condizioni_pagamento.hpp"
#include "fattura_elettronica_eccezioni.hpp"
#include "fattura_elettronica_dettagli.hpp"

namespace felpa
{

const std::string &CondizioniPagamento::tag()
{
  static const std::string TAG("CondizioniPagamento");
  return TAG;
}

namespace dettagli
{

const std::set<std::string> condizioni_pagamento(
{
  "",
  "TP01",  // pagamento a rate
  "TP02",  // pagamento completo
  "TP03"   // anticipo
});

bool carica(tinyxml2::XMLConstHandle &ft, CondizioniPagamento *cp)
{
  auto *h(ft.FirstChildElement(cp->tag().c_str()).ToElement());
  if (!h)
    return false;

  auto *scp(h->GetText());
  if (!scp)
    return false;

  *cp = scp;
  return true;
}

void registra(tinyxml2::XMLPrinter &ft, const CondizioniPagamento &cp)
{
  ft.OpenElement(CondizioniPagamento::tag().c_str());
  push_text(ft, cp);
  ft.CloseElement();
}

}  // namespace dettagli

bool CondizioniPagamento::valido(const std::string &id)
{
  return dettagli::condizioni_pagamento.find(id) !=
         dettagli::condizioni_pagamento.end();
}

///
/// \return `true` se la modalita' di pagamento prevede un'unica tranche.
///
bool CondizioniPagamento::completo() const
{
  return codice_ == "TP02";
}

CondizioniPagamento::CondizioniPagamento(const std::string &id) : codice_(id)
{
  if (!valido(codice_))
    throw eccezione::cond_pag_cod_scorretto(tag() + ": codice non valido");

  assert(codice_.empty() || codice_.length() == 4);
}

CondizioniPagamento &CondizioniPagamento::operator=(const std::string &s)
{
  *this = CondizioniPagamento(s);
  return *this;
}

}  // namespace felpa
