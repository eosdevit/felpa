/**
 *  \file
 *  \remark This file is part of FELPA.
 *
 *  \copyright Copyright (C) 2018-2020 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#include <cassert>
#include <set>

#include "causale_pagamento.hpp"
#include "fattura_elettronica_dettagli.hpp"
#include "fattura_elettronica_eccezioni.hpp"

namespace felpa
{

// Stessi codici utilizzati per la compilazione del modello 770S.
const std::string &CausalePagamento::tag()
{
  static const std::string TAG("CausalePagamento");
  return TAG;
}

namespace dettagli
{

const std::set<std::string> causale_pagamento(
{
  "",   // nessuna causale
  "A",  // prestazioni di lavoro autonomo rientranti nell'esercizio di arte o
        // professione abituale
  "B",  // utilizzazione economica, da parte dell'autore o dell'inventore, di
        // opere dell'ingegno, di brevetti industriali e di processi, formule o
        // informazioni relativi ad esperienze acquisite in campo industriale,
        // commerciale o scientifico
  "C",  // utili derivanti da contratti di associazione in partecipazione e da
        // contratti di cointeressenza, quando l’apporto e' costituito
        // esclusivamente dalla prestazione di lavoro
  "D",  // utili spettanti ai soci promotori ed ai soci fondatori delle
        // societa' di capitali
  "E",  // levata di protesti cambiari da parte dei segretari comunali
  "G",  // indennita' corrisposte per la cessazione di attività sportiva
        // professionale
  "H",  // indennita' corrisposte per la cessazione dei rapporti di agenzia
        // delle persone fisiche e delle società di persone con esclusione
        // delle somme maturate entro il 31 dicembre 2003, gia' imputate per
        // competenza e tassate come reddito d'impresa
  "I",  // indennita' corrisposte per la cessazione da funzioni notarili
  "L",  // redditi derivanti dall'utilizzazione economica di opere
        // dell'ingegno, di brevetti industriali e di processi, formule e
        // informazioni relativi a esperienze acquisite in campo industriale,
        // commerciale o scientifico, che sono percepiti dagli aventi causa a
        // titolo gratuito (ad es. eredi e legatari dell'autore e inventore)
  "L1", // redditi derivanti dall'utilizzazione economica di opere
        // dell'ingegno, di brevetti industriali e di processi, formule e
        // informazioni relativi a esperienze acquisite in campo industriale,
        // commerciale o scientifico, che sono percepiti da soggetti che
        // abbiano acquistato a titolo oneroso i diritti alla loro
        // utilizzazione
  "M",  // prestazioni di lavoro autonomo non esercitate abitualmente
  "M1", // redditi derivanti dall’assunzione di obblighi di fare, di non fare o
        // permettere
  "M2", // prestazioni di lavoro autonomo non esercitate abitualmente per le
        // quali sussiste l'obbligo di iscrizione alla Gestione Separata ENPAPI
  "N",  // indennita' di trasferta, rimborso forfetario di spese, premi e
        // compensi erogati: nell'esercizio diretto di attivita' sportive
        // dilettantistiche; in relazione a rapporti di collaborazione
        // coordinata e continuativa di carattere amministrativo-gestionale di
        // natura non professionale resi a favore di societa' e associazioni
        // sportive dilettantistiche e di cori, bande e filodrammatiche da
        // parte del direttore e dei collaboratori tecnici
  "O",  // prestazioni di lavoro autonomo non esercitate abitualmente, per le
        // quali non sussiste l'obbligo di iscrizione alla gestione separata
        // (Circ. INPS n. 104/2001)
  "O1", // redditi derivanti dall'assunzione di obblighi di fare, di non fare o
        // permettere, per le quali non sussiste l'obbligo di iscrizione alla
        // gestione separata (Circ. INPS n. 104/2001)
  "P",  // compensi corrisposti a soggetti non residenti privi di stabile
        // organizzazione per l'uso o la concessione in uso di attrezzature
        // industriali, commerciali o scientifiche che si trovano nel
        // territorio dello Stato ovvero a società svizzere o stabili
        // organizzazioni di societa' svizzere che possiedono i requisiti di
        // cui all'art. 15, comma 2 dell'Accordo tra la Comunita' europea e la
        // Confederazione svizzera del 26 ottobre 2004 (pubblicato in G.U.C.E.
        // del 29 dicembre 2004 n. L385/30)
  "Q",  // provvigioni corrisposte ad agente o rappresentante di commercio
        // monomandatario
  "R",  // provvigioni corrisposte ad agente o rappresentante di commercio
        // plurimandatario
  "S",  // provvigioni corrisposte a commissionario
  "T",  // provvigioni corrisposte a mediatore
  "U",  // provvigioni corrisposte a procacciatore di affari
  "V",  // provvigioni corrisposte a incaricato per le vendite a domicilio;
        // provvigioni corrisposte a incaricato per la vendita porta a porta e
        // per la vendita ambulante di giornali quotidiani e periodici (L. 25
        // febbraio 1987, n. 67)
  "V1", // redditi derivanti da attivita' commerciali non esercitate
        // abitualmente (ad esempio, provvigioni corrisposte per prestazioni
        // occasionali ad agente o rappresentante di commercio, mediatore,
        // procacciatore d’affari)
  "W",  // corrispettivi erogati nel 2018 per prestazioni relative a contratti
        // d'appalto cui si sono resi applicabili le disposizioni contenute
        // nell'art. 25-ter del D.P.R. n. 600 del 29 settembre 1973
  "X",  // canoni corrisposti nel 2004 da societa' o enti residenti ovvero da
        // stabili organizzazioni di societa' estere di cui all'art. 26-quater,
        // comma 1, lett. a) e b) del D.P.R. 600 del 29 settembre 1973, a
        // societa' o stabili organizzazioni di societa', situate in altro
        // stato membro dell'Unione Europea in presenza dei requisiti di cui al
        // citato art. 26-quater, del D.P.R. 600 del 29 settembre 1973, per i
        // quali e' stato effettuato, nell'anno 2006, il rimborso della
        // ritenuta ai sensi dell'art. 4 del D.Lgs. 30 maggio 2005 n. 143
  "Y",  // canoni corrisposti dal 1 gennaio 2005 al 26 luglio 2005 da societa'
        // o enti residenti ovvero da stabili organizzazioni di societa' estere
        // di cui all'art. 26-quater, comma 1, lett. a) e b) del D.P.R. n. 600
        // del 29 settembre 1973, a societa' o stabili organizzazioni di
        // societa', situate in altro stato membro dell’Unione Europea in
        // presenza dei requisiti di cui al citato art. 26-quater, del D.P.R.
        // n. 600 del 29 settembre 1973, per i quali e' stato effettuato,
        // nell'anno 2006, il rimborso della ritenuta ai sensi dell'art. 4 del
        // D.Lgs. 30 maggio 2005 n. 143
  "ZO"  // titolo diverso dai precedenti
});

bool carica(tinyxml2::XMLConstHandle &ft, CausalePagamento *c)
{
  auto *h(ft.FirstChildElement(c->tag().c_str()).ToElement());
  if (!h || !h->GetText())
    return false;

  *c = h->GetText();
  return true;
}

void registra(tinyxml2::XMLPrinter &ft, const CausalePagamento &c)
{
  if (c.empty())
    throw eccezione::causale_pagamento_vuota(
      c.tag() + ": causale pagamento non specificata");

  {
    ft.OpenElement(CausalePagamento::tag().c_str());
    push_text(ft, c);
    ft.CloseElement();
  }
}

}  // namespace dettagli

bool CausalePagamento::valido(const std::string &id)
{
  return dettagli::causale_pagamento.find(id)
         != dettagli::causale_pagamento.end();
}

CausalePagamento::CausalePagamento(const std::string &c) : codice_(c)
{
  if (!valido(codice_))
    throw eccezione::cod_causale_pagamento(tag() + ": valore scorretto");
}

CausalePagamento &CausalePagamento::operator=(const std::string &c)
{
  return *this = CausalePagamento(c);
}

bool CausalePagamento::empty() const
{
  return codice_.empty();
}

}  // namespace felpa
