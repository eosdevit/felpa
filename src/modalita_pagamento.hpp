/**
 *  \file
 *  \remark This file is part of FELPA.
 *
 *  \copyright Copyright (C) 2015-2024 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#if !defined(FELPA_MODALITA_PAGAMENTO_HPP)
#define      FELPA_MODALITA_PAGAMENTO_HPP

#include <string>

namespace felpa
{

class ModalitaPagamento
{
public:
  explicit ModalitaPagamento(const std::string & = "");

  [[nodiscard]] bool empty() const { return codice_.empty(); }

  [[nodiscard]] operator std::string () const { return codice_; }
  ModalitaPagamento &operator=(const std::string &);

  [[nodiscard]] static const std::string &tag();

private:
  // Funzioni private di supporto.
  [[nodiscard]] static bool valido(const std::string &);

  // Data member pubblici.
  std::string codice_;
};  // class ModalitaPagamento

[[nodiscard]] bool richiede_iban(ModalitaPagamento);

}  // namespace felpa

#endif  // include guard
