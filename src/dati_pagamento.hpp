/**
 *  \file
 *  \remark This file is part of FELPA.
 *
 *  \copyright Copyright (C) 2015-2024 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#if !defined(FELPA_DATI_PAGAMENTO_HPP)
#define      FELPA_DATI_PAGAMENTO_HPP

#include <vector>

#include "condizioni_pagamento.hpp"
#include "dettaglio_pagamento.hpp"

namespace felpa
{

///
/// Utilizzati per dare evidenza delle informazioni relative alle condizioni,
/// ai termini ed alle modalita' di pagamento.
///
class DatiPagamento
{
public:
  DatiPagamento() = default;

  [[nodiscard]] bool empty() const;

  [[nodiscard]] static const std::string &tag();

  // ********* Data member pubblici *********
  CondizioniPagamento condizioni_pagamento {};
  std::vector<DettaglioPagamento> dettaglio_pagamento {};
};  // class DatiPagamento

[[nodiscard]] bool operator==(const DatiPagamento &, const DatiPagamento &);

}  // namespace felpa

#endif  // include guard
