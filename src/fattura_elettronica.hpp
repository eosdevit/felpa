/**
 *  \file
 *  \remark This file is part of FELPA.
 *
 *  \copyright Copyright (C) 2015 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#if !defined(FELPA_FATTURA_ELETTRONICA_HPP)
#define      FELPA_FATTURA_ELETTRONICA_HPP

#include "fattura_elettronica_header.hpp"
#include "fattura_elettronica_body.hpp"
#include "fattura_elettronica_dettagli.hpp"
#include "fattura_elettronica_eccezioni.hpp"
#include "fic_export.hpp"
#include "utility.hpp"

namespace felpa
{

class FatturaElettronica
{
public:
  FatturaElettronica();
  FatturaElettronica(const EsigibilitaIva &, formato_fattura);
  explicit FatturaElettronica(const std::string &);

  bool carica(const std::string &);
  void registra(std::string = "") const;

  std::string filename() const;

  [[nodiscard]] bool acquisto() const;

  // ********* Public data members *********
  const std::string versione;
  FatturaElettronicaHeader header;
  FatturaElettronicaBody     body;

private:
  static const std::string &tag();
};  // class FatturaElettronica

}  // namespace felpa

#endif  // include guard
