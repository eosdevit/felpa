/**
 *  \file
 *  \remark This file is part of libCommercio.
 *
 *  \copyright Copyright (C) 2023 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#include <functional>
#include <regex>
#include <stdexcept>
#include <string>

#include "nazione.hpp"
#include "partita_iva.hpp"

#if !defined(LIB_COMMERCIO_VAT_HPP)
#define      LIB_COMMERCIO_VAT_HPP

namespace lib_commercio
{

///
/// A value-added tax identification number or VAT identification number is an
/// identifier used in many countries, including the countries of the European
/// Union, for value-added tax purposes.
///
class vat
{
public:
  enum vat_status {sconosciuto = -1, errato, corretto};

  vat(const std::string & = "");

  struct componenti
  {
    std::string nazione {};
    std::string numero  {};

    [[nodiscard]] bool empty() const
    { return nazione.empty() && numero.empty(); }
  };

  operator std::string() const { return vat_; }
  [[nodiscard]] componenti scomponi() const { return scomponi(vat_); }

  [[nodiscard]] static componenti scomponi(std::string);
  [[nodiscard]] static vat_status verifica(const std::string &);

private:
  std::string vat_;
};  // class vat

[[nodiscard]] bool operator==(const vat &, const vat &);
[[nodiscard]] bool operator<(const vat &, const vat &);


/*********************************************************************
 * IMPLEMENTAZIONE
 *********************************************************************/
inline vat::vat(const std::string &s) : vat_(erase_spaces(s))
{
  if (verifica(s) != corretto)
    throw std::invalid_argument("Codice VAT scorretto o sconosciuto");
}

///
/// Estrae le informazioni contenute nel VAT.
///
/// \param[in] vat un codice VAT
/// \return        componenti del VAT
///
/// \note
/// - Nel caso di VAT troppo corto, viene restituita una struttura vuota;
/// - spazi, trattini e punti vengono rimossi.
///
inline vat::componenti vat::scomponi(std::string vat)
{
  vat = erase_spaces(vat);

  componenti ret;
  if (vat.length() <= 2)
    return ret;

  if (const std::string n(toupper(vat.substr(0, 2)));
      nazione::verifica_iso3166_alpha2(n))
  {
    ret.nazione = n;
    ret.numero  = toupper(vat.substr(2));
  }
  else if (n == "EL")
  {
    // Per la Grecia il VAT e' preceduto dal codice "EL" (ISO639-1 ovvero
    // lingua) anziche' "GR" (ISO3166-2 ovvero nazione).
    ret.nazione = "GR";
    ret.numero  = toupper(vat.substr(2));
  }
  else
    ret.numero = toupper(vat);

  return ret;
}

///
/// Verifica la correttezza formale di un codice VAT.
///
/// \param[in] vat numero di identificazione VAT
/// \return       `true` se il VAT e' corretto
///
/// \note
/// - Nel caso degli Stati Uniti vengono accettati SSN, EIN ed ITIN;
/// - per il Brasile vengono accettati CNPJ e CPF.
///
inline vat::vat_status vat::verifica(const std::string &vat)
{
  const componenti c(scomponi(vat));

  if (c.nazione.empty() || c.numero.empty())
    return errato;

  const auto chk([&c](const std::string &pattern)
                 {
                   return std::regex_match(c.numero, std::regex(pattern));
                 });

  // U.S. taxpayer identification numbers include a Social Security Number
  // (SSN), which is issued to individuals, and an Employer Identification
  // Number (EIN), which is issued to individuals or entities.
  // In addition, an Individual Taxpayer Identification Number (ITIN) is issued
  // to individuals who are required to have a U.S. taxpayer identification
  // number but who do not have and are not eligible to obtain an SSN.
  //
  // Brazil: Cadastro Nacional de Pessoa Jurídica (Companies) or Cadastro de
  // Pessoa Física (Legal People).
  const std::map<std::string, std::function<bool()>> check =
  {
    {"AL", [chk]{ return chk("[JKL][0-9]{8}[A-Z]"); }},
    {"AR", [chk]{ return chk("[0-9]{11}"); }},
    {"AT", [chk]{ return chk("[U][0-9]{8}"); }},
    {"AU", [chk]{ return chk("[0-9]{11}"); }},
    {"BE", [chk]{ return chk("[01][0-9]{9}"); }},
    {"BG", [chk]{ return chk("[0-9]{9,10}"); }},
    {"BR", [chk]{ return chk("[0-9]{11}|[0-9]{14}"); }},
    {"BY", [chk]{ return chk("[0-9]{9}"); }},
    {"CA", [chk]{ return chk("[0-9]{9}"); }},
    {"CH", [chk]{ return chk("CHE[0-9]{9}(?:IVA|TVA|MWST)?"); }},
    {"CY", [chk]{ return chk("[0-9]{8}[A-Z]"); }},
    {"CZ", [chk]{ return chk("[0-9]{8,10}"); }},
    {"DE", [chk]{ return chk("[0-9]{9}"); }},
    {"DK", [chk]{ return chk("[0-9]{8}"); }},
    {"EE", [chk]{ return chk("[0-9]{9}"); }},
    {"ES", [chk]{ return chk("[A-Z][0-9]{7}[A-Z0-9]|[0-9]{8}[A-Z]"); }},
    {"FI", [chk]{ return chk("[0-9]{8}"); }},
    {"FR", [chk]{ return chk("[A-Z0-9]{2}[0-9]{9}"); }},
    {"GB", [chk]{ return chk("\\d{9}|\\d{12}|GD[0-4]\\d{2}|HA[5-9]\\d{2}"); }},
    {"GR", [chk]{ return chk("[0-9]{9}"); }},
    {"HR", [chk]{ return chk("[0-9]{11}"); }},
    {"HU", [chk]{ return chk("[0-9]{8}"); }},
    {"ID", [chk]{ return chk("[0-9]{15}"); }},
    {"IE", [chk]{ return chk("[0-9]{7}[A-Z]{1,2}"); }},
    {"IL", [chk]{ return chk("[0-9]{9}"); }},
    {"IS", [chk]{ return chk("[0-9]{5,6}"); }},
    {"IN", [chk]{ return chk("\\d{2}[A-Z]{5}\\d{4}[A-Z][A-Z\\d][Z][A-Z\\d]");}},
    {"IT", [&c]{ return partita_iva::verifica(c.numero); }},
    {"HU", [chk]{ return chk("[0-9]{8}"); }},
    {"KZ", [chk]{ return chk("[0-9]{12}"); }},
    {"LT", [chk]{ return chk("[0-9]{9}|[0-9]{12}"); }},
    {"LU", [chk]{ return chk("[0-9]{8}"); }},
    {"LV", [chk]{ return chk("[0-9]{11}"); }},
    {"MK", [chk]{ return chk("[0-9]{13}"); }},
    {"MT", [chk]{ return chk("[0-9]{8}"); }},
    {"NL", [chk]{ return chk("[0-9]{9}[B][0-9]{2}"); }},
    {"NO", [chk]{ return chk("[0-9]{9}(?:MVA)?"); }},
    {"NZ", [chk]{ return chk("[0-9]{9}"); }},
    {"PH", [chk]{ return chk("[0-9]{12}"); }},
    {"PL", [chk]{ return chk("[0-9]{10}"); }},
    {"PT", [chk]{ return chk("[0-9]{9}"); }},
    {"RO", [chk]{ return chk("[0-9]{2,10}"); }},
    {"RS", [chk]{ return chk("[0-9]{9}"); }},
    {"RU", [chk]{ return chk("[0-9]{10}|[0-9]{12}"); }},
    {"SA", [chk]{ return chk("[0-9]{15}"); }},
    {"SK", [&c, chk]{ return chk("[0-9]{10}")
                             && std::stoull(c.numero) % 11 == 0; }},
    {"SE", [chk]{ return chk("[0-9]{12}"); }},
    {"SI", [chk]{ return chk("[0-9]{8}"); }},
    {"SM", [chk]{ return chk("[0-9]{5}"); }},
    {"TR", [chk]{ return chk("[0-9]{10}"); }},
    {"UA", [chk]{ return chk("[0-9]{12}"); }},
    {"US", [chk]{ return chk("[0-9]{9}"); }},
  };

  if (const auto it(check.find(c.nazione)); it != check.end())
    return it->second() ? corretto : errato;

  return sconosciuto;
}

inline bool operator==(const vat &lhs, const vat &rhs)
{
  return static_cast<std::string>(lhs) == static_cast<std::string>(rhs);
}

inline bool operator<(const vat &lhs, const vat &rhs)
{
  return static_cast<std::string>(lhs) < static_cast<std::string>(rhs);
}

}  // namespace lib_commercio

#endif  // include guard
