# [FELPA](https://bitbucket.org/morinim/felpa) #

## Caratteristiche ##

FELPA è una libreria, sviluppata in C++, per semplificare la lettura, scrittura, compilazione e verifica delle fatture elettroniche in base alle specifiche tecniche v1.9.

Il software è distribuito con licenza d'uso [Mozilla Public License v2.0](https://www.mozilla.org/MPL/2.0/).

Il nome è un acronimo (**F**attura **EL**ettronica **P**ubblica **A**mministrazione) scelto quando la fattura elettronica era obbligatoria per la sola pubblica amministrazione. La libreria si è in seguito evoluta per supportare la fatturazione verso privati (B2B e B2C).

## Requisiti ##

Parte delle funzionalità è fornita dalla libreria [lib_commercio](https://bitbucket.org/eosdevit/lib_commercio/), anch'essa *open source* e sviluppata da [EOS](https://www.eosdev.it).

FELPA si avvale, inoltre, delle librerie TinyXML v2 (https://github.com/leethomason/tinyxml2) e JSON for Modern C++ (https://github.com/nlohmann/json), entrambe incluse nel *repository*.

Tutte le librerie elencate sono incluse nel *repository*.

È richiesto un compilatore C++17. Al momento gli ambienti di sviluppo verificati sono:

* G++
* CLANG++
* C++Builder Rio / Sydney / Alexandria

## Osservazioni ##

### UTF ###
Felpa è *UTF-8 pure*: la libreria considera tutte le stringhe in ingresso ed in uscita codificate in formato UTF-8.

Per fare un esempio con il C++Builder in ambiente Windows:

```
UnicodeString str(OpenDialog1->FileName);
UTF8String    ustr(str);

std::string   s(ustr.c_str(), ustr.Length())  // questa e' la stringa da usare
```

Il tutto funziona poiché UTF-8 è compatibile con i codici ASCII a 7 bit. Se il valore di un byte è superiore a 127, significa che ha inizio un carattere multi-byte (a seconda del valore del primo byte si può capire da quanti byte è composto il carattere, da 2 a 4).

Dal momento che l'UTF-8 è basato sui char-byte e le stringhe sono *0-terminated*, si possono utilizzare le funzioni della libreria standard per gran parte delle necessità.

La cosa più importante da ricordare è che il conteggio dei caratteri potrebbe differire da quello dei byte (una funzioni come `strlen()` restituisce il numero dei byte non dei caratteri).

### Fatture in Cloud ###

Ci sono alcune limitazioni nella conversione delle fatture dal formato XML al JSON utilizzato in Fatture in Cloud (https://developers.fattureincloud.it/):

- non sono supportati gli allegati.

Sono state adottate alcune convenzioni aggiuntive:

- l'identificativo dell'aliquota iva (`items_list.vat.id`) può essere specificato/forzato valorizzando `FatturaElettronicaBody.DatiBeniServizi.RiferimentoNormativo` con una stringa del tipo `[FIC 12]`. Se questa stringa manca viene identificato automaticamente un identificativo fra quelli predefiniti dalla piattaforma. Questa scelta funziona discretamente con aliquote iva diverse da `0` (**per l'aliquota `0` fallisce quasi sempre**).