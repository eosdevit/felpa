/**
 *  \file
 *  \remark This file is part of FELPA.
 *
 *  \copyright Copyright (C) 2015-2021 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#if !defined(FELPA_NAZIONE_HPP)
#define      FELPA_NAZIONE_HPP

#include "nazione_type.hpp"

namespace felpa
{

class Nazione : public NazioneType
{
public:
  explicit Nazione(const std::string &s = "IT") : NazioneType("Nazione", s) {}

  Nazione &operator=(const std::string &s)
  {
    assign(s);
    return *this;
  }

  [[nodiscard]] bool operator==(const NazioneType &rhs) const
  { return NazioneType::operator==(rhs); }
};  // class Nazione

}  // namespace felpa

#endif  // include guard
