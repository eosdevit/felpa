/**
 *  \file
 *  \remark This file is part of FELPA.
 *
 *  \copyright Copyright (C) 2018-2020 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#include <cassert>
#include <set>

#include "tipo_ritenuta.hpp"
#include "fattura_elettronica_dettagli.hpp"
#include "fattura_elettronica_eccezioni.hpp"

namespace felpa
{

const std::string &TipoRitenuta::tag()
{
  static const std::string TAG("TipoRitenuta");
  return TAG;
}

const std::string     TipoRitenuta::PERSONE_FISICHE("RT01");
const std::string  TipoRitenuta::PERSONE_GIURIDICHE("RT02");
const std::string     TipoRitenuta::CONTRIBUTO_INPS("RT03");
const std::string TipoRitenuta::CONTRIBUTO_ENASARCO("RT04");
const std::string    TipoRitenuta::CONTRIBUTO_ENPAM("RT05");
const std::string TipoRitenuta::ALTRO_PREVIDENZIALE("RT06");

namespace dettagli
{

const std::set<std::string> tipo_ritenuta(
{
  "",      // nessuna ritenuta
  TipoRitenuta::PERSONE_FISICHE, TipoRitenuta::PERSONE_GIURIDICHE,
  TipoRitenuta::CONTRIBUTO_INPS, TipoRitenuta::CONTRIBUTO_ENASARCO,
  TipoRitenuta::CONTRIBUTO_ENPAM, TipoRitenuta::ALTRO_PREVIDENZIALE
});

bool carica(tinyxml2::XMLConstHandle &ft, TipoRitenuta *r)
{
  auto *h(ft.FirstChildElement(TipoRitenuta::tag().c_str()).ToElement());
  if (!h || !h->GetText())
    return false;

  *r = h->GetText();
  return true;
}

void registra(tinyxml2::XMLPrinter &ft, const TipoRitenuta &r)
{
  if (r.empty())
    throw eccezione::tipo_ritenuta_vuoto(
      r.tag() + ": tipo ritenuta non specificato");

  {
    ft.OpenElement(TipoRitenuta::tag().c_str());
    push_text(ft, r);
    ft.CloseElement();
  }
}

}  // namespace dettagli

bool TipoRitenuta::valido(const std::string &id)
{
  return dettagli::tipo_ritenuta.find(id) != dettagli::tipo_ritenuta.end();
}

TipoRitenuta::TipoRitenuta(const std::string &c) : codice_(c)
{
  if (!valido(codice_))
    throw eccezione::tipo_ritenuta(tag() + ": valore scorretto");
}

TipoRitenuta &TipoRitenuta::operator=(const std::string &r)
{
  return *this = TipoRitenuta(r);
}

bool TipoRitenuta::empty() const
{
  return codice_.empty();
}

}  // namespace felpa
