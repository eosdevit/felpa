/**
 *  \file
 *  \remark This file is part of FELPA.
 *
 *  \copyright Copyright (C) 2018-2024 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#include <cassert>
#include <cmath>
#include <regex>

#if defined(WIN32) || defined(_WIN32) || defined(__WIN32) || defined(_WIN64)
#  include <System.hpp>
#endif

#include "utility.hpp"

namespace felpa
{

std::string base_conversion(std::int64_t n, int base)
{
  // Caratteri stampabili, utilizzabili in "tutti" i filesystem per comporre
  // nomi di file.
  // Sono inseriti in modo tale da avere codice ASCII crescente.
  static const std::string coding_table("0123456789"
                                        "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                                        "abcdefghijklmnopqrstuvwxyz");

  assert(static_cast<unsigned>(base) <= coding_table.size());

  std::string ret;
  while (ret.length() < 5)
  {
    const auto digit(n % base);
    ret.insert(0, 1, coding_table[digit]);
    n /= base;
  }

  return ret;
}

std::string coalesce(const char s[], const std::string &def)
{
  return s == nullptr ? def : s;
}

bool ends_with(const std::string &s, const std::string &ending)
{
  return s.length() >= ending.length() &&
         s.compare(s.length() - ending.length(), ending.length(), ending) == 0;

  /*
  if (ending.length() > s.length())
    return false;

  return std::equal(ending.rbegin(), ending.rend(), s.rbegin());
  */
}

double to_float_def(const std::string &s, double def)
{
  char *end;
  double v(std::strtod(s.c_str(), &end));

  return end == s.data() ? def : v;
}

///
/// Base64 encoding/decoding.
///
/// \param[in] src dati da codificare
/// \return    una stringa contenente i dati codificati in base64 (vuota in
///            caso di errore)
///
/// \copyright Copyright (C) 2005-2011 Jouni Malinen <j@w1.fi>
///
std::string base64_encode(const std::vector<unsigned char> &src)
{
  static const char base64_table[65] =
    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

  const std::size_t olen(4 *((src.size() + 2) / 3));  // 3-byte blocks to 4-byte
  if (olen < src.size())
    return {};  // integer overflow

  std::string ostr;
  ostr.resize(olen);
  auto *out(&ostr[0]);

  const auto *end(src.data() + src.size());
  const auto  *in(             src.data());

  auto *pos(out);
  while (end - in >= 3)
  {
    *pos++ = base64_table[in[0] >> 2];
    *pos++ = base64_table[((in[0] & 0x03) << 4) | (in[1] >> 4)];
    *pos++ = base64_table[((in[1] & 0x0f) << 2) | (in[2] >> 6)];
    *pos++ = base64_table[in[2] & 0x3f];
    in += 3;
  }

  if (end - in)
  {
    *pos++ = base64_table[in[0] >> 2];
    if (end - in == 1)
    {
      *pos++ = base64_table[(in[0] & 0x03) << 4];
      *pos++ = '=';
    }
    else
    {
      *pos++ = base64_table[((in[0] & 0x03) << 4) | (in[1] >> 4)];
      *pos++ = base64_table[(in[1] & 0x0f) << 2];
    }
    *pos++ = '=';
  }

  return ostr;
}

std::string base64_encode(const std::string &filename)
{
  std::ifstream is(filename, std::ifstream::binary);

  if (!is)
    return {};

  // Get length of file.
  is.seekg(0, is.end);
  const auto length(is.tellg());
  is.seekg(0, is.beg);

  std::vector<unsigned char> buffer(length);
  is.read(reinterpret_cast<std::ifstream::char_type *>(&buffer[0]), length);
  is.close();

  return base64_encode(buffer);

}

///
/// \see https://stackoverflow.com/a/37109258/3235496
///
std::vector<unsigned char> base64_decode(const std::string &s)
{
  static const unsigned B64index[256] =
  {
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0, 62, 63, 62, 62, 63, 52, 53, 54, 55, 56, 57, 58, 59, 60,
   61,  0,  0,  0,  0,  0,  0,  0,  0,  1,  2,  3,  4,  5,  6,  7,  8,  9, 10,
   11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25,  0,  0,  0,  0,
   63,  0, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42,
   43, 44, 45, 46, 47, 48, 49, 50, 51
  };

  if (s.empty())
    return {};

  auto *p(reinterpret_cast<const unsigned char *>(&s[0]));

  std::size_t j(0);
  std::size_t pad1(s.length() % 4 || p[s.length() - 1] == '=');
  std::size_t pad2(pad1 && (s.length() % 4 > 2 || p[s.length() - 2] != '='));

  const size_t last((s.length() - pad1) / 4 << 2);
  std::vector<unsigned char> result(last / 4 * 3 + pad1 + pad2, 0);

  for (std::size_t i(0); i < last; i += 4)
  {
    const unsigned n(B64index[p[i]] << 18 | B64index[p[i + 1]] << 12
                     | B64index[p[i + 2]] << 6 | B64index[p[i + 3]]);
    result[j++] = n >> 16;
    result[j++] = n >> 8 & 0xFF;
    result[j++] = n & 0xFF;
  }

  if (pad1)
  {
    unsigned n(B64index[p[last]] << 18 | B64index[p[last + 1]] << 12);
    result[j++] = n >> 16;

    if (pad2)
    {
      n |= B64index[p[last + 2]] << 6;
      result[j++] = n >> 8 & 0xFF;
    }
  }

  return result;
}

std::string extract_filename(std::string fp)
{
  const auto last_slash_idx(fp.find_last_of("\\/"));
  if (last_slash_idx != std::string::npos)
    fp.erase(0, last_slash_idx + 1);

  return fp;
}

int extract_p7m_content(const std::string &in, const std::string &out)
{
  const auto add_param([](const std::string &p)
                       {
                         return p.find(" ") == std::string::npos
                                ? p
                                : "\"" + p + "\"";
                       });

  // Nella terminologia di `openssl` un file `.p7m` e' in formato `DER`.
  std::string cmd("openssl smime -verify -inform DER -in " + add_param(in)
                  + " -noverify -out " + add_param(out));

#if defined(WIN32) || defined(_WIN32) || defined(__WIN32) || defined(_WIN64)
  const String cwq("\"" + String(cmd.c_str()) + "\"");
  return _wsystem(cwq.c_str());
#else
  return std::system(cmd.c_str());
#endif
}

bool zero(double x)
{
  return std::fabs(x) < 0.0000001;
}

std::string serie_e_numero::rimuovi_separatori(std::string s)
{
  const std::string sep("-/.\\:");
  s.erase(std::remove_if(s.begin(), s.end(),
                         [&sep](char c)
                         {
                           return sep.find(c) != std::string::npos;
                         }),
          s.end());

  return s;
}

///
/// Scompone l'ID del documento (`<Numero>`) nell'abituale coppia serie/numero.
///
/// Tradizionalmente le fatture cartacee erano univocamente identificate da una
/// serie di numerazione ed un numero univoco progressivo. Questo approccio
/// e' stato modificato ed e' rimasto solo il requisito dell'univocita'.
///
/// \remark
/// Non sempre e' possibile identificare serie e numero (anche per un essere
/// umano) senza informazioni aggiuntive. Per esempio `12/22` puo' indicare,
/// indifferentemente, il documento `12` della serie `22` o viceversa. La
/// funzione assume, arbitrariamente, che la sequenza di numeri piu' "a destra"
/// costituisca il numero fattura. La serie e' il blocco rimanente.
///
serie_e_numero::serie_e_numero(const std::string &text)
{
  const std::regex re("^(.*?)(\\d+)(\\D*)$");
  // Group               <1>  <2>   <3>
  // 1: any number of any char not greedy
  // 2: one or more digits
  // 3: any number of non digit char

  if (std::smatch match;
      std::regex_search(text, match, re) && !match.str(2).empty())
  {
    const auto serie_s(rimuovi_separatori(match.str(1)));
    const auto serie_d(rimuovi_separatori(match.str(3)));

    const auto n(match.str(2));

    if (zero(to_float_def(n, 0.0)))  // situazione "anomala"
    {
      if (!zero(to_float_def(serie_d, 0.0)))
      {
        serie_  = serie_s + n;
        numero_ = serie_d;
      }
      else if (!zero(to_float_def(serie_s, 0.0)))
      {
        serie_  = serie_d + n;
        numero_ = serie_s;
      }
    }
    else  // situazione "standard"
    {
      serie_  = serie_s + serie_d;
      numero_ = n;
    }
  }
}

}  // namespace felpa
