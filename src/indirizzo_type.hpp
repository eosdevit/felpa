/**
 *  \file
 *  \remark This file is part of FELPA.
 *
 *  \copyright Copyright (C) 2015-2021 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#if !defined(FELPA_INDIRIZZO_TYPE_HPP)
#define      FELPA_INDIRIZZO_TYPE_HPP

#include "nazione.hpp"

namespace felpa
{

class IndirizzoType
{
public:
  const std::string &tag() const { return tag_; }

  [[nodiscard]] bool empty() const;

  // ********* Data member pubblici *********
  /// Contiene l'indirizzo (Via, Piazza...) di un soggetto, anche se non in
  /// Italia.
  ///
  /// \note
  /// Pur essendo previsto un campo ad hoc (`NumeroCivico`), e' possibile
  /// valorizzare il campo con l'indirizzo completo di numero civico.
  std::string indirizzo;

  /// \warning
  /// OBBLIGATORIETA': SI, ma solo se non e' stato indicato il numero civico
  /// all'interno del campo Indirizzo.
  std::string numero_civico;

  /// Per indirizzi esteri, il CAP puo' essere sostituito da un valore numerico
  /// che non crei "collisioni" con il sistema postale italiano (p.e. tutti "0"
  /// o tutti "9").
  /// \warning OBBLIGATORIETA': SI, sempre.
  std::string cap;

  /// \warning OBBLIGATORIETA': SI, sempre.
  std::string comune;

  /// \warning OBBLIGATORIETA': SI, ma solo se la sede si trova in Italia.
  std::string provincia;

  /// \warning OBBLIGATORIETA': SI, sempre.
  Nazione nazione;

protected:
  explicit IndirizzoType(const std::string &);

  bool operator==(const IndirizzoType &) const;

private:
  std::string tag_;
};  // class IndirizzoType

}  // namespace felpa

#endif  // include guard
