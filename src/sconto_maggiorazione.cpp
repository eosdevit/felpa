/**
 *  \file
 *  \remark This file is part of FELPA.
 *
 *  \copyright Copyright (C) 2015-2024 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#include <cassert>

#include "sconto_maggiorazione.hpp"
#include "fattura_elettronica_eccezioni.hpp"
#include "fattura_elettronica_dettagli.hpp"
#include "utility.hpp"

namespace felpa
{

const std::string &ScontoMaggiorazione::tag()
{
  static const std::string TAG("ScontoMaggiorazione");
  return TAG;
}

bool ScontoMaggiorazione::empty() const
{
  return percentuale.empty() && importo.empty();
}

bool operator==(const ScontoMaggiorazione &lhs, const ScontoMaggiorazione &rhs)
{
  return lhs.tipo == rhs.tipo
         && std::string(lhs.percentuale) == std::string(rhs.percentuale)
         && std::string(lhs.importo) == std::string(rhs.importo);
}

namespace dettagli
{

constexpr char TIPO_TAG[] = "Tipo";

constexpr std::size_t TIPO_SIZE = 2;

bool carica(tinyxml2::XMLConstHandle &ft, ScontoMaggiorazione *sm)
{
  tinyxml2::XMLConstHandle h(ft.FirstChildElement(sm->tag().c_str()));
  if (!h.ToElement())
    h = ft;

  if (auto *he = h.FirstChildElement(TIPO_TAG).ToElement())
    sm->tipo = coalesce(he->GetText()).substr(0, TIPO_SIZE);

  const bool pe(carica(h, &sm->percentuale));
  const bool im(carica(h, &sm->importo));

  return (pe || im) && !(pe && im);
}

void registra(tinyxml2::XMLPrinter &ft, const ScontoMaggiorazione &sm)
{
  if (sm.empty())
    return;

  ft.OpenElement(ScontoMaggiorazione::tag().c_str());

  if (sm.tipo != ScontoMaggiorazione::SCONTO
      && sm.tipo != ScontoMaggiorazione::MAGGIORAZIONE)
    throw eccezione::sconto_mag_cod_tipo_scorretto(
      ScontoMaggiorazione::tag()
      + ": codice del tipo di sconto/maggiorazione scorretto");

  ft.OpenElement(TIPO_TAG);
  push_text(ft, sm.tipo);
  ft.CloseElement();

  if (!sm.importo.empty() && !sm.percentuale.empty())
    throw eccezione::sconto_mag_2(
      ScontoMaggiorazione::tag()
      + ": specificati contemporaneamente Percentuale ed Importo");

  if (!sm.percentuale.empty())
    registra(ft, sm.percentuale);

  if (!sm.importo.empty())
    registra(ft, sm.importo);

  ft.CloseElement();
}

}  // namespace dettagli

}  // namespace felpa
