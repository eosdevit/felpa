/**
 *  \file
 *  \remark This file is part of FELPA.
 *
 *  \copyright Copyright (C) 2015 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#if !defined(FELPA_DATI_ANAGRAFICI_TYPE_HPP)
#define      FELPA_DATI_ANAGRAFICI_TYPE_HPP

#include <string>

#include "id_fiscale_iva.hpp"
#include "anagrafica.hpp"
#include "data_iscrizione_albo.hpp"
#include "regime_fiscale.hpp"

namespace felpa
{

///
/// E' la base delle classi `DatiAnagraficiCedenteType` e
/// `DatiAnagraficiCessionarioType`.
///
/// Il primo tipo e' sostanzialmente l'estensione del secondo con attributi
/// in gran parte opzionali.
///
class DatiAnagraficiType
{
public:
  [[nodiscard]] const std::string &tag() const { return tag_; }

  [[nodiscard]] virtual bool empty() const
  {
    return id_fiscale_iva.empty() && codice_fiscale.empty();
  }

  // ********* Data member pubblici *********
  IdFiscaleIva id_fiscale_iva;

  /// Serve per fornire un elemento aggiuntivo di identificazione del soggetto
  /// (spesso richiesto per il pagamento).
  ///
  /// Il campo, se valorizzato, deve contenere il codice fiscale del
  /// cedente/prestatore che sara' composto di 11 caratteri numerici, se
  /// trattasi di persona giuridica, oppure di 16 caratteri alfanumerici, se
  /// trattasi di persona fisica.
  ///
  /// \warning OBBLIGATORIETA': consigliata.
  std::string codice_fiscale;

  Anagrafica anagrafica;

protected:
  explicit DatiAnagraficiType(const std::string &);

  [[nodiscard]] bool operator==(const DatiAnagraficiType &) const;

private:
  std::string tag_;
};  // class DatiAnagraficiType

}  // namespace felpa

#endif  // include guard
