/**
 *  \file
 *  \remark This file is part of FELPA.
 *
 *  \copyright Copyright (C) 2019-2024 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#include <cassert>

#include "dati_bollo.hpp"
#include "fattura_elettronica_dettagli.hpp"
#include "fattura_elettronica_eccezioni.hpp"

namespace felpa
{

const std::string &DatiBollo::tag()
{
  static const std::string TAG("DatiBollo");
  return TAG;
}

namespace dettagli
{

constexpr char BOLLO_VIRTUALE_TAG[] = "BolloVirtuale";

bool carica(tinyxml2::XMLConstHandle &ft, DatiBollo *db)
{
  tinyxml2::XMLConstHandle h(ft.FirstChildElement(DatiBollo::tag().c_str()));
  if (!h.ToElement())
    return false;

  if (auto *hbv = h.FirstChildElement(BOLLO_VIRTUALE_TAG).ToElement();
      hbv && hbv->GetText())
    db->bollo_virtuale = hbv->GetText();

  carica(h, &db->importo_bollo);

  return db->bollo_virtuale == DatiBollo::SI || db->importo_bollo.empty();
}

void registra(tinyxml2::XMLPrinter &ft, const DatiBollo &db)
{
  if (db.empty())
    return;

  ft.OpenElement(DatiBollo::tag().c_str());

  ft.OpenElement(BOLLO_VIRTUALE_TAG);
  push_text(ft, DatiBollo::SI);
  ft.CloseElement();

  registra(ft, db.importo_bollo);

  ft.CloseElement();
}

}  // namespace dettagli

bool DatiBollo::empty() const
{
  return bollo_virtuale != DatiBollo::SI;
}

bool operator==(const DatiBollo &lhs, const DatiBollo &rhs)
{
  return
    (lhs.empty() && rhs.empty())
    || (lhs.bollo_virtuale == rhs.bollo_virtuale
        && std::string(lhs.importo_bollo) == std::string(rhs.importo_bollo));
}

}  // namespace felpa
