/**
 *  \file
 *  \remark This file is part of FELPA.
 *
 *  \copyright Copyright (C) 2015-2020 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#include <cassert>

#include "data_ora_type.hpp"
#include "fattura_elettronica_dettagli.hpp"
#include "fattura_elettronica_eccezioni.hpp"

namespace felpa
{

namespace dettagli
{

bool carica(tinyxml2::XMLConstHandle &ft, DataOraType *d)
{
  auto *h(ft.FirstChildElement(d->tag().c_str()).ToElement());
  if (!h)
    return false;

  auto *dat_ora(h->GetText());
  if (!dat_ora)
    return false;

  d->assign(dat_ora);
  return true;
}

void registra(tinyxml2::XMLPrinter &ft, const DataOraType &d)
{
  ft.OpenElement(d.tag().c_str());
  push_text(ft, d);
  ft.CloseElement();
}

}  // namespace dettagli

bool DataOraType::valida(const std::string &d)
{
  if (d.empty())
    return true;

  if (d.length() < 16)
    return false;

  bool ok_data(std::isdigit(d[0]) && std::isdigit(d[1]) &&
               std::isdigit(d[2]) && std::isdigit(d[3]) &&
               d[4] == '-' &&
               std::isdigit(d[5]) && std::isdigit(d[6]) &&
               d[7] == '-' &&
               std::isdigit(d[8]) && std::isdigit(d[9]));

  bool ok_ora_corta(ok_data &&
                    d[10] == 'T' &&
                    std::isdigit(d[11]) && std::isdigit(d[12]) &&
                    d[13] == ':' &&
                    std::isdigit(d[14]) && std::isdigit(d[15]));

  if (d.length() == 16)
    return ok_data && ok_ora_corta;

  if (d.length() < 18)
    return false;

  return d[16] == ':' && std::isdigit(d[17]) && std::isdigit(d[18]);
}

DataOraType::DataOraType(const std::string &t, const std::string &d)
  : tag_(t), data_ora_(d)
{
  assert(!tag_.empty());

  if (!valida(data_ora_))
    throw eccezione::formato_data_ora(tag() + ": formato DataOra scorretto");
}

void DataOraType::assign(const std::string &d)
{
  if (!valida(d))
    throw eccezione::formato_data_ora(tag() + ": formato DataOra scorretto");

  data_ora_ = d;
}

}  // namespace felpa
