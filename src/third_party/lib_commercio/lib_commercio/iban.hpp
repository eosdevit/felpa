/**
 *  \copyright Copyright (C) 2023 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#if !defined(LIB_COMMERCIO_IBAN_HPP)
#define      LIB_COMMERCIO_IBAN_HPP

#include <cassert>
#include <cctype>
#include <string>

#include "nazione.hpp"
#include "utils.hpp"

namespace lib_commercio
{

/// Memorizzazione e verifica coordinate IBAN.
class iban
{
public:
  explicit iban(const std::string & = "");
  explicit iban(const char []);

  [[nodiscard]] std::string codice_banca() const;
  [[nodiscard]] std::string codice_filiale() const;
  [[nodiscard]] std::string numero_conto() const;

  [[nodiscard]] std::string codice() const;
  [[nodiscard]] std::string semplice() const;

  [[nodiscard]] bool empty() const;

  enum errori_formato {corretto, troppo_corto, troppo_lungo, formato_nazione,
                       cifre_di_controllo, formato_nbk, formato_bc,
                       valore_check_digit};
  [[nodiscard]] static errori_formato verifica(std::string);

  [[nodiscard]] static std::string check_digit(const std::string &);

  static constexpr unsigned MAX_LEN_CODICE = 34;
  static constexpr unsigned MAX_LEN_SEMPLICE = 3 * MAX_LEN_CODICE / 2;

#if defined(__BORLANDC__)
  explicit iban(const String &);
  [[nodiscard]] static errori_formato verifica(const String &);
#endif

private:
  void verifica_costruttore();

  std::string iban_;
};


/*********************************************************************
 * IMPLEMENTAZIONE
 *********************************************************************/
///
/// \param[in] cod coordinate bancarie
///
/// \note
/// Questo costruttore e' necessario in quanto l'inizializzazione di una
/// `std::string`, partendo da un puntatore `NULL`, non e' ben definita
/// (vedere <http://stackoverflow.com/q/17464514/3235496>).
inline iban::iban(const char cod[])
  : iban_(cod ? erase_spaces(cod) : std::string())
{
  verifica_costruttore();
}

///
/// \param[in] cod coordinate bancarie
///
inline iban::iban(const std::string &cod) : iban_(erase_spaces(cod))
{
  verifica_costruttore();
}

inline void iban::verifica_costruttore()
{
  if (!empty())
    if (const errori_formato err(verifica(iban_)); err != corretto)
      throw err;
}

///
/// \return `true` se il codice IBAN non e' stato specificato
///
inline bool iban::empty() const
{
  return iban_.empty();
}

///
/// \return IBAN "elettronico" con le cifre senza alcuno spazio tra di loro (da
///         utilizzare quando si effettuano transazioni)
///
inline std::string iban::codice() const
{
  return iban_;
}

///
/// \return IBAN "cartaceo" con le cifre raggruppate a 4 a 4 per facilitare la
///         lettura e verifica
///
inline std::string iban::semplice() const
{
  if (iban_.substr(0, 2) == "IT")
    return iban_.substr(0, 4) + " "    // nazione e check digit
           + iban_.substr(4, 1) + " "  // CIN
           +iban_.substr(5, 5) + " "   // ABI
           +iban_.substr(10, 5) +  " " // CAB
           +iban_.substr(15, 12);      // conto corrente

  std::string ret;

  for (std::size_t i(0); i < iban_.length(); ++i)
  {
    ret += iban_[i];

    if (i && i % 4 == 0)
      ret += ' ';
  }

  return ret;
}

///
/// \return account number contenuto nell'IBAN (conto corrente per l'Italia)
///
inline std::string iban::numero_conto() const
{
  if (iban_.substr(0, 2) == "IT")
    return iban_.substr(15, 12);

  return "";
}

///
/// \return branch code contenuto nell'IBAN (CAB nel caso dell'Italia)
///
inline std::string iban::codice_filiale() const
{
  if (iban_.substr(0, 2) == "IT")
    return iban_.substr(10, 5);

  return "";
}

///
/// \return national bank code contenuto nell'IBAN (ABI nel caso dell'Italia)
///
inline std::string iban::codice_banca() const
{
  if (iban_.substr(0, 2) == "IT")
    return iban_.substr(5, 5);

  return "";
}

///
/// \return il check digit che serve, nell'ambito del codice IBAN, per
///         riconoscere la presenza di eventuali errori di trascrizione
///
inline std::string iban::check_digit(const std::string &s)
{
  // Nel calcolo del check digit si suppone che siano noti i codici ABI e CAB,
  // il numero di conto corrente ed il CIN, con i quali si costruisce il codice
  // BBAN. Per determinare il check digit si procede come descritto di seguito:
  // - a destra del codice BBAN si aggiunge la stringa IT00;
  // - nella stringa ottenuta si sostituiscono i caratteri alfabetici con un
  //   numero di due cifre ottenuto dalla posizione della lettera nell'alfabeto
  //   a cui va sommato 9. Così, ad esempio, 'A' diventa 10, 'B' diventa 11, 'C'
  //   diventa 12 e cosi' via fino a 'Z', che diventa 35;
  // - si ottiene un numero di almeno 30 cifre e si considera il resto della
  //   divisione fra tale numero e 97 (compreso fra 0 e 96);
  // - il resto ottenuto va sottratto a 98 e ottenendo il codice desiderato.

  const std::string base(s.substr(4) + s.substr(0, 2) + "00");

  std::string numero;
  for (auto b : base)
    if (std::isdigit(b))
      numero += b;
    else
    {
      const unsigned val(10 + std::toupper(b) - 'A');

      numero += '0' + (val / 10);
      numero += '0' + (val % 10);
    }

  // Any computer programming language or software package that is used to
  // compute mod 97 directly must have the ability to handle integers of more
  // than 30 digits. In practice, this can only be done by software that either
  // supports arbitrary-precision arithmetic or that can handle 220 bit
  // (unsigned) integers, features that are often not standard. If the
  // application software in use does not provide the ability to handle
  // integers of this size, the modulo operation can be performed in a
  // piece-wise manner.
  // Piece-wise calculation of D mod 97 can be done in many ways. One such way
  // is as follows:
  //
  // 1. starting from the leftmost digit, construct a number using the first 9
  //    digits of D and call it N;
  // 2. calculate N mod 97;
  // 3. construct a new 9-digit N by concatenating above result (step 2) with
  //    the next 7 digits of D. If there are fewer than 7 digits remaining in D
  //    but at least one, then construct a new N, which will have less than 9
  //    digits, from the above result (step 2) followed by the remaining digits
  //    of D;
  // 4. repeat steps 2–3 until all the digits of D have been processed.
  //
  // The result of the final calculation in step 2 will be D mod 97 = N mod 97.

  unsigned long d(0), n(0);
  std::string s_d(numero.substr(0, 9)), s_n;
  std::size_t da(9);

  do
  {
    d = std::stoi(s_n + s_d);
    n = d % 97;

    if (da >= numero.length())
      break;

    s_d = numero.substr(da, 7);
    s_n = "00";
    s_n[0] += n / 10;
    s_n[1] += n % 10;

    da += 7;
  } while (true);

  const unsigned long val(98 - n);
  std::string ret("00");
  ret[0] += val / 10;
  ret[1] += val % 10;

  return ret;
}

///
/// \param[in] cod coordinate iban da verificare
/// \return        esito della verifica di formato (`corretto` per un codice
///                valido)
///
inline iban::errori_formato iban::verifica(std::string cod)
{
  cod = erase_spaces(cod);

  if (cod.length() < 16)
    return troppo_corto;

  if (cod.length() > 34)
    return troppo_lungo;

  if (!std::isupper(cod[0]) || !std::isupper(cod[1])
      || !nazione::verifica_iso3166_alpha2(cod.substr(0,2)))
    return formato_nazione;

  if (check_digit(cod) != cod.substr(2, 2))
    return valore_check_digit;

  if (cod.substr(0, 2) == "IT")
  {
    if (cod.length() < 27)
      return troppo_corto;
    if (cod.length() > 27)
      return troppo_lungo;

    if (!std::isdigit(cod[2]) || !std::isdigit(cod[3]))
      return cifre_di_controllo;

    for (std::size_t i(5); i < 10; ++i)
      if (!std::isdigit(cod[i]))
        return formato_nbk;

    for (std::size_t i(10); i < 15; ++i)
      if (!std::isdigit(cod[i]))
        return formato_bc;
  }

  return corretto;
}

#if defined(__BORLANDC__)
///
/// \param[in] cod coordinate bancarie
///
inline iban::iban(const String &cod) : iban(details::to_stdstring(cod))
{
}

///
/// \param[in] cod coordinate iban da verificare
/// \return        esito della verifica di formato (`corretto` per un codice
///                valido)
///
inline iban::errori_formato iban::verifica(const String &cod)
{
  return verifica(details::to_stdstring(cod));
}
#endif

}  // namespace lib_commercio

#endif  // include guard
