/**
 *  \file
 *  \remark This file is part of FELPA.
 *
 *  \copyright Copyright (C) 2015 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#if !defined(FELPA_ANAGRAFICA_HPP)
#define      FELPA_ANAGRAFICA_HPP

#include <string>

namespace felpa
{

class Anagrafica
{
public:
  [[nodiscard]] static const std::string &tag();

  Anagrafica();

  /// Contiene il nome di una ditta o la denominazione/ragione sociale di una
  /// persona giuridica.
  ///
  /// Non e' ammessa la contemporanea valorizzazione del campo `nome` e/o
  /// `cognome`.
  ///
  /// E' un dato richiesto dalla normativa art.21 DPR 633/1972
  ///
  /// \warning
  /// OBBLIGATORIETA': SI, ma solo se il per una persona giuridica; e' in
  /// alternativa ai campi `nome` e `cognome`.
  std::string denominazione;

  /// Il campo deve contenere il nome di una persona fisica. Non e' ammessa la
  /// contemporanea valorizzazione del campo `denominazione`; viceversa e'
  /// richiesta la contemporanea valorizzazione del campo `cognome`.
  ///
  /// \warning
  /// OBBLIGATORIETA': SI, ma solo se il per una persona fisica; e' in
  /// alternativa al campo `denominazione`.
  std::string nome;

  /// Il campo deve contenere il cognome di una persona fisica. Non e' ammessa
  /// la contemporanea valorizzazione del campo `denominazione`; viceversa e'
  /// richiesta la contemporanea valorizzazione del campo `nome`.
  ///
  /// \warning
  /// OBBLIGATORIETA': SI, ma solo se il per una persona fisica; e' in
  /// alternativa al campo Denominazione.
  std::string cognome;

  /// Costituisce un completamento delle informazioni anagrafiche (titolo
  /// onorifico).
  ///
  /// Non e' previsto alcun formato particolare. La valorizzazione della voce,
  /// nel rispetto delle caratteristiche stabilite dallo schema XSD, e'
  /// demandata alla valutazione dell'utente secondo esigenza.
  std::string titolo;

  /// Identifica gli operatori economici nei rapporti con le autorita' doganali
  /// sull'intero territorio dell'Unione Europea (al regolamento CE n. 312 del
  /// 16 aprile 2009).
  ///
  /// Se valorizzato, deve contenere il numero del codice *Economic Operator
  /// Registration and Identification*.
  std::string cod_eori;
};  // class Anagrafica

[[nodiscard]] bool operator==(const Anagrafica &, const Anagrafica &);

}  // namespace felpa

#endif  // include guard
