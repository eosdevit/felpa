/**
 *  \file
 *  \remark This file is part of FELPA.
 *
 *  \copyright Copyright (C) 2018-2024 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#if !defined(FELPA_RITENUTA_HPP)
#define      FELPA_RITENUTA_HPP

#include <string>

namespace felpa
{

///
/// Serve per indicare se una linea della fattura si riferisce ad una
/// cessione/prestazione soggetta a ritenuta.
///
class Ritenuta
{
public:
  explicit Ritenuta(const std::string & = NO);

  [[nodiscard]] operator std::string () const { return codice_; }
  [[nodiscard]] explicit operator bool () const { return codice_ == SI; }

  Ritenuta &operator=(const std::string &);

  [[nodiscard]] static const std::string &tag();

  static constexpr char NO[] = "NO";
  static constexpr char SI[] = "SI";

private:
  // Funzioni private di supporto.
  [[nodiscard]] bool valido(const std::string &);

  // Data member privati.
  std::string codice_;
};  // class Ritenuta

}  // namespace felpa

#endif  // include guard
