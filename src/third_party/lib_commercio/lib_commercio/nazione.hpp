/**
 *  \file
 *  \remark This file is part of libCommercio.
 *
 *  \copyright Copyright (C) 2023 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#include <cassert>
#include <map>
#include <regex>
#include <set>
#include <stdexcept>
#include <string>
#include <vector>

#include "utils.hpp"

#if !defined(LIB_COMMERCIO_NAZIONE_HPP)
#define      LIB_COMMERCIO_NAZIONE_HPP

namespace lib_commercio
{

///
/// Codice del paese assegnante l'identificativo fiscale al soggetto
/// trasmittente (secondo lo standard ISO3166-1 alpa-2).
///
class nazione
{
public:
  explicit nazione(const std::string &);
  explicit nazione(unsigned);

  struct info
  {
    std::string nome {};
    std::string name {};

    unsigned iso3166_num {};

    /// Il codice ISO 3166-1 alpha-2 e' la parte piu' conosciuta dello standard
    /// ISO 3166-1 ed e' utilizzato per indicare i luoghi.
    ///
    /// \warning Da non confondere con ISO 639-1 (lingue)
    /// \ref http://it.wikipedia.org/wiki/ISO_3166-1_alpha-2
    std::string iso3166_alpha2 {};
    std::string iso3166_alpha3 {};

    std::string internet {};
    std::string   valuta {};

    [[nodiscard]] bool empty() const;
  };

  [[nodiscard]] static bool ue(const std::string &);
  [[nodiscard]] static bool uk(const std::string &);

  [[nodiscard]] static info trova(const std::string &);
  [[nodiscard]] static info trova(unsigned);

  [[nodiscard]] static bool verifica_iso3166_alpha2(const std::string &);

private:
  std::string cod_;
};


/*********************************************************************
 * DETTAGLI DI IMPLEMENTAZIONE
 *********************************************************************/
namespace details
{
/* MANCANTI
  "BF",  // Burkina Faso
  "BI",  // Burundi
  "BJ",  // Benin
  "BM",  // Bermuda
  "BS",  // Bahamas
  "BT",  // Bhutan
  "BV",  // Isola Bouvet
  "BW",  // Botswana
  "BZ",  // Belize
  "CC",  // Isole Cocos e Keeling
  "CD",  // Repubblica Democratica del Congo (ex Zaire)
  "CK",  // Isole Cook
  "CV",  // Capo Verde
  "CW",  // Curacao (precedentemente nelle Antille Olandesi)
  "CX",  // Isola del Natale
  "DJ",  // Gibuti
  "DM",  // Dominica
  "DO",  // Repubblica Dominicana
  "EH",  // Sahara Occidentale ex Sahara Spagnolo
  "FJ",  // Figi
  "FK",  // Isole Falkland
  "FO",  // Isole Faer Oer
  "GD",  // Grenada
  "GG",  // Guernsey
  "GN",  // Guinea
  "GS",  // Georgia del Sud e isole Sandwich meridionali
  "GT",  // Guatemala
  "GW",  // Guinea-Bissau
  "GY",  // Guyana
  "HK",  // Hong Kong
  "HM",  // Isole Heard e McDonald
  "HT",  // Haiti
  "JE",  // Jersey
  "KG",  // Kirghizistan
  "KH",  // Cambogia
  "KI",  // Kiribati
  "KM",  // Comore
  "KN",  // Saint Kitts e Nevis
  "KP",  // Corea del Nord
  "KY",  // Isole Cayman
  "LA",  // Laos
  "LC",  // Santa Lucia
  "LK",  // Sri Lanka
  "LS",  // Lesotho
  "MG",  // Madagascar
  "MM",  // Birmania 	Myanmar
  "MO",  // Macao
  "MP",  // Isole Marianne Settentrionali
  "MR",  // Mauritania
  "MS",  // Montserrat
  "MW",  // Malawi
  "MZ",  // Mozambico
  "NA",  // Namibia
  "NF",  // Isola Norfolk
  "NI",  // Nicaragua
  "NR",  // Nauru
  "NU",  // Niue
  "PF",  // Polinesia Francese
  "PG",  // Papua Nuova Guinea
  "PN",  // Isole Pitcairn
  "PR",  // Porto Rico
  "PS",  // Stato di Palestina
  "SB",  // Isole Salomone
  "SH",  // Sant'Elena, Isola di Ascensione e Tristan da Cunha
  "SJ",  // Svalbard e Jan Mayen
  "SL",  // Sierra Leone
  "SS",  // Sudan del Sud
  "ST",  // Sao Tome e Principe
  "SX",  // Sint Maarten
  "SZ",  // Swaziland
  "TJ",  // Tagikistan
  "TK",  // Tokelau
  "TM",  // Turkmenistan
  "TO",  // Tonga
  "TT",  // Trinidad e Tobago
  "TV",  // Tuvalu
  "UM",  // Isole minori esterne degli Stati Uniti
  "VC",  // Saint Vincent e Grenadine
  "VI",  // Isole Vergini Americane
  "VU",  // Vanuatu
  "WF",  // Wallis e Futuna
  "WS",  // Samoa
  "YE",  // Yemen
  "ZM",  // Zambia
*/
[[nodiscard]] inline const std::vector<nazione::info> &db_nazioni()
{
  static const std::vector<nazione::info> db =
  {
    {"AFGHANISTAN", "AFGHANISTAN", 4, "AF", "AFG", "af", "AFN"},
    {"ALBANIA", "ALBANIA",  8, "AL", "ALB", "al", "ALL"},
    {"ALGERIA", "ALGERIA", 12, "DZ", "DZA", "dz", "DZD"},
    {"ANDORRA", "ANDORRA", 20, "AD", "AND", "ad", "EUR"},
    {"ANGOLA", "ANGOLA", 24, "AO", "AGO", "ao", "AOA"},
    {"ANGUILLA", "ANGUILLA", 660, "AI", "AIA", "ai", "XCD"},
    {"ANTARDIDE", "ANTARCTICA", 10, "AQ", "ATA", "aq", ""},
    {"ANTIGUA E BARBUDA", "ANTIGUA AND BARBUDA", 28, "AG", "ATG", "ag", "XCD"},
    {"ARABIA SAUDITA", "SAUDI ARABIA", 682, "SA", "SAU", "sa", "SAR"},
    {"ARGENTINA", "ARGENTINA", 32, "AR", "ARG", "ar", "ARS"},
    {"ARMENIA", "ARMENIA", 51, "AM", "ARM", "am", "AMD"},
    {"ARUBA", "ARUBA", 533, "AW", "ABW", "aw", "AWG"},
    {"AUSTRALIA", "AUSTRALIA", 36, "AU", "AUS", "au", "AUD"},
    {"AUSTRIA", "AUSTRIA", 40, "AT", "AUT", "at", "EUR"},
    {"AZERBAIGIAN", "AZERBAIJAN", 31, "AZ", "AZE", "az", "AZN"},
    {"BANGLADESH", "BANGLADESH", 50, "BD", "BGD", "bd", "BDT"},
    {"BAHREIN", "BAHRAIN", 48, "BH", "BHR", "bh", "BHD"},
    {"BARBADOS", "BARBADOS", 52, "BB", "BRB", "bb", "BBD"},
    {"BELGIO", "BELGIUM", 56, "BE", "BEL", "be", "EUR"},
    {"BIELORUSSIA","BELARUS", 112, "BY", "BLR", "by", "BYN"},
    {"BOLIVIA", "BOLIVIA", 68, "BO", "BOL", "bo", "BOB"},
    {"BOSNIA ED ERZEGOVINA","BOSNIA AND HERZEGOVINA",70,"BA","BIH","BA","BAM"},
    {"BRASILE", "BRASIL", 76, "BR", "BRA", "br", "BRL"},
    {"BRUNEI", "BRUNEI DARUSSALAM", 96, "BN", "BRN", "BND"},
    {"BULGARIA", "BULGARIA", 100, "BG", "BGR", "bg", "BGN"},
    {"CAMERUN", "CAMEROON", 120, "CM", "CMR", "cm", "XAF"},
    {"CANADA", "CANADA", 124, "CA", "CAN", "ca", "CAD"},
    {"CIAD", "CHAD", 148, "TD", "TCD", "td", "XAF"},
    {"CILE", "CHILE", 152, "CL", "CHL", "cl", "CLP"},
    {"CINA", "CHINA", 156, "CN", "CHN", "cn", "CNY"},
    {"CITTA DEL VATICANO", "HOLY SEE", 336, "VA", "VAT", "va", "EUR"},
    {"COLOMBIA", "COLOMBIA", 170, "CO", "COL", "co", "COP"},
    {"COREA DEL SUD", "KOREA, SOUTH", 410, "KR", "KOR", "kr", "KRW"},
    {"COSTA D'AVORIO", "COTE D'IVORE", 384, "CI", "CIV", "ci", "XOF"},
    {"COSTA RICA", "COSTA RICA", 188, "CR", "CRI", "cr", "CRC"},
    {"CROAZIA", "CROATIA", 191, "HR", "HRV", "hr", "HRK"},
    {"CUBA", "CUBA", 192, "CU", "CUB", "cu", "CUP"},
    {"CIPRO", "CYPRUS", 196, "CY", "CYP", "cy", "EUR"},
    {"DANIMARCA", "DENMARK", 208, "DK", "DNK", "dk", "DKK"},
    {"ECUADOR", "ECUADOR", 218, "EC", "ECU", "ec", "USD"},
    {"EGITTO", "EGYPT", 818, "EG", "EGY", "eg", "EGP"},
    {"EL SALVADOR", "EL SALVADOR", 222, "SV", "SLV", "sv", "USD"},
    {"EMIRATI ARABI UNITI", "UNITED ARAB EMIRATES", 784,"AE","ARE","ae","AED"},
    {"ERITREA", "ERITREA", 232, "ER", "ERI", "er", "ERN"},
    {"ESTONIA", "ESTONIA", 233, "EE", "EST", "ee", "EEK"},
    {"ETIOPIA", "ETHIOPIA", 231, "ET", "ETH", "et", "ETB"},
    {"FILIPPINE", "PHILIPPINES", 608, "PH", "PHL", "ph", "PHP"},
    {"FINLANDIA", "FINLAND", 246, "FI", "FIN", "fi", "EUR"},
    {"FRANCIA", "FRANCE", 250, "FR", "FRA", "fr", "EUR"},
    {"GABON", "GABON", 266, "GA", "GAB", "ga", "XAF"},
    {"GAMBIA", "GAMBIA", 270, "GM", "GMB", "gm", "GMD"},
    {"GEORGIA", "GEORGIA", 268, "GE", "GEO", "ge", "GEL"},
    {"GERMANIA", "GERMANY", 276, "DE", "DEU", "de", "EUR"},
    {"GHANA", "GHANA", 288, "GH", "GHA", "gh", "GHC"},
    {"GIAMAICA", "JAMAICA", 388, "JM", "JAM", "jm", "JMD"},
    {"GIAPPONE", "JAPAN", 392, "JP", "JPN", "jp", "JPY"},
    {"GIBILTERRA", "GIBRALTAR", 292, "GI", "GIB", "gi", "GIP"},
    {"GIORDANIA", "JORDAN", 400, "JO", "JOR", "jo", "JOD"},
    {"GRECIA", "GREECE", 300, "GR", "GRC", "gr", "EUR"},
    {"GROENLANDIA", "GREENLAND", 304, "GL", "GRL", "gl", "DKK"},
    {"GUADALUPA", "GUADELOUPE", 312, "GP", "GLP", "gp", "EUR"},
    {"GUAM", "GUAM", 316, "GU", "GUM", "gu", "USD"},
    {"GUAYANA FRANCESE", "FRENCH GUIANA", 254, "GF", "GUF", "gf", "EUR"},
    {"GUINEA EQUATORIALE", "EQUATORIAL GUINEA", 226, "GQ", "GNQ", "gq", "XAF"},
    {"HONDURAS", "HONDURAS", 340, "HN", "HND", "hn", "HNL"},
    {"INDIA", "INDIA", 356, "IN", "IND", "in", "INR"},
    {"INDONESIA", "INDONESIA", 360, "ID", "IDN", "id", "IDR"},
    {"IRAN", "IRAN", 364, "IR", "IRN", "ir", "IRR"},
    {"IRAQ", "IRAQ", 368, "IQ", "IRQ", "iq", "IQD"},
    {"IRLANDA", "IRELAND", 372, "IE", "IRL", "ie", "EUR"},
    {"ISLANDA", "ICELAND", 352, "IS", "ISL", "is", "ISK"},
    {"ISOLA DI MAN", "THE ISLE OF MAN", 833, "IM", "IMN", "im", "IMP"},
    {"ISOLE ALAND", "ALAND ISLANDS", 248, "AX", "ALA", "ax", "EUR"},
    {"ISOLE BES", "CARIBBEAN NETHERLANDS", 535, "BQ", "BES", "bq", "USD"},
    {"ISOLE MARSHALL", "MARSHALL ISLANDS", 584, "MH", "MHL", "mh", "USD"},
    {"ISOLE VERGINI BRITANNICHE",
     "BRITISH ENGLISH ISLANDS", 92, "VG", "VGB", "vg", "USD"},
    {"ISRAELE", "ISRAEL", 376, "IL", "ISR", "il", "ILS"},
    {"ITALIA", "ITALY", 380, "IT", "ITA", "it", "EUR"},
    {"KAZAKISTAN", "KAZAKHSTAN", 398, "KZ", "KAZ", "kz", "KZT"},
    {"KENYA", "KENYA", 404, "KE", "KEN", "ke", "KES"},
    {"KUWAIT", "KUWAIT", 414, "KW", "KWT", "kw", "KWD"},
    {"LA RIUNIONE", "REUNION", 638, "RE", "REU", "re", "EUR"},
    {"LETTONIA", "LATVIA", 428, "LV", "LVA", "lv", "LVL"},
    {"LIBANO", "LEBANON", 422, "LB", "LBN", "lb", "LBP"},
    {"LIBERIA", "LIBERIA", 430, "LR", "LBb", "lr", "LRD"},
    {"LIBIA", "LYBIA", 434, "LY", "LBY", "ly", "LYD"},
    {"LIECHTENSTEIN", "LIECHTENSTEIN", 438, "LI", "LIE", "li", "CHF"},
    {"LITUANIA", "LITHUANIA", 440, "LT", "LTU", "lt", "LTL"},
    {"LUSSEMBURGO", "LUXEMBURG", 442, "LU", "LUX", "lu", "EUR"},
    {"MACEDONIA", "MACEDONIA", 807, "MK", "MKD", "mk", "MKD"},
    {"MAIOTTA", "MAYOTTE", 175, "YT", "MYT", "yt", "EUR"},
    {"MALDIVE", "MALDIVES", 462, "MV", "MDV", "mv", "MVR"},
    {"MALESIA", "MALAYSIS", 458, "MY", "MYS", "my", "MYR"},
    {"MALI", "MALI", 466, "ML", "MLI", "ml", "XOF"},
    {"MALTA", "MALTA", 470, "MT", "MLT", "mt", "EUR"},
    {"MAROCCO", "MOROCCO", 504, "MA", "MAR", "ma", "MAD"},
    {"MARTINICA", "MARTINIQUE", 474, "MQ", "MTQ", "mq", "EUR"},
    {"MAURITIUS", "MAURITIUS", 480, "MU", "MUS", "mu", "MUR"},
    {"MESSICO", "MEXICO", 484, "MX", "MEX", "mx", "MXN"},
    {"MICRONESIA", "MICRONESIA", 583, "FM", "FSM", "fm", "USD"},
    {"MOLDAVIA", "MOLDOVA", 498, "MD", "MDA", "md", "MDL"},
    {"MONGOLIA", "MONGOLIA", 496, "MN", "MNG", "mn", "MNT"},
    {"MONTENEGRO", "MONTENEGRO", 499, "ME", "MNE", "me", "EUR"},
    {"NEPAL", "NEPAL", 524, "NP", "NPL", "np", "NPR"},
    {"NIGER", "NIGER", 562, "NE", "NER", "ne", "XOF"},
    {"NIGERIA", "NIGERIA", 566, "NG", "NGA", "ng", "NGN"},
    {"NORVEGIA", "NORWAY", 578, "NO", "NOR", "no", "NOK"},
    {"NUOVA CALEDONIA","NEW CALEDONIA", 540, "NC", "NCL", "nc", "XPF"},
    {"NUOVA ZELANDA", "NEW ZELAND", 554, "NZ", "NZL", "nz", "NZD"},
    {"OMAN", "OMAN", 512, "OM", "OMN", "om", "OMR"},
    {"PAESI BASSI", "NETHERLANDS", 528, "NL", "NLD", "nl", "EUR"},
    {"PAKISTAN", "PAKISTAN", 586, "PK", "PAK", "pk", "PKR"},
    {"PALAU", "PALAU", 585, "PW", "PLW", "pw", "USD"},
    {"PANAMA", "PANAMA", 591, "PA", "PAN", "pa", "USD"},
    {"PARAGUAY", "PARAGUAY", 600, "PY", "PRY", "py", "PYG"},
    {"PERU", "PERU", 604, "PE", "PER", "pe", "PEN"},
    {"POLONIA", "POLAND", 616, "PL", "POL", "pl", "PLN"},
    {"PORTOGALLO", "PORTUGAL", 620, "PT", "PRT", "pt", "EUR"},
    {"PRINCIPATO DI MONACO", "MONACO", 492, "MC", "MCO", "mc", "EUR"},
    {"QATAR", "QATAR", 634, "QA", "AQT", "qa", "QAR"},
    {"REGNO UNITO", "UNITED KINDOM", 826, "GB", "GBR", "uk", "GBP"},
    {"REPUBBLICA CECA", "CZECH REPUBLIC", 203, "CZ", "CZE", "cz", "CZK"},
    {"REPUBBLICA CENTRAFRICANA",
     "CENTRAL AFRICAN REPUBLIC", 140, "CF", "CAF", "cf", "XAF"},
    {"REPUBBLICA DEL CONGO",
     "REPUBLIC OF THE CONGO", 178, "CG", "COG", "cg", "XAF"},
    {"ROMANIA", "ROMANIA", 642, "RO", "ROU", "ro", "ROL"},
    {"RUANDA", "RWANDA", 646, "RW", "RWA", "rw", "RWF"},
    {"RUSSIA", "RUSSIA", 643, "RU", "RUS", "ru", "RUB"},
    {"SAINT BARTHELEMY", "SAINT BARTHELEMY", 652, "BL", "BLM", "bl", "EUR"},
    {"SAINT MARTIN", "SAINT MARTIN", 663, "MF", "MAF", "mf", "EUR"},
    {"SAINT PIERRE E MIQUELON",
     "SAINT PIERRE AND MIQUELON", 666, "PM", "SPM", "pm", "EUR"},
    {"SAMOA AMERICANE", "AMERICAN SAMOA", 15, "AS", "ASM", "as", "USD"},
    {"SAN MARINO", "SAN MARINO", 674, "SM", "SMR", "sm", "EUR"},
    {"SENEGAL", "SENEGAL", 686, "SN", "SEN", "sn", "XOF"},
    {"SERBIA", "SERBIA", 688, "RS", "SRB", "rs", "RSD"},
    {"SEYCHELLES", "SEYCHELLES", 690, "SC", "SYC", "sc", "SCR"},
    {"SINGAPORE", "SINGAPORE", 702, "SG", "SGP", "sg", "SGD"},
    {"SIRIA", "SYRIA", 760, "SY", "SYR", "sy", "SYP"},
    {"SLOVACCHIA", "SLOVAKIA", 703, "SK", "SVK", "sk", "SKK"},
    {"SLOVENIA", "SLOVENIA", 705, "SI", "SVN", "si", "EUR"},
    {"SOMALIA", "SOMALIA", 706, "SO", "SOM", "so", "SOS"},
    {"SPAGNA", "SPAIN", 724, "ES", "ESP", "es", "EUR"},
    {"STATI UNITI", "UNITED STATES", 840, "US", "USA", "us", "USD"},
    {"SUDAFRICA", "SOUTH AFRICA", 710, "ZA", "ZAF", "za", "ZAR"},
    {"SUDAN", "SUDAN", 736, "SD", "SDN", "sd", "SDG"},
    {"SURINAME", "SURINAME", 740, "SR", "SUR", "sr", "SRD"},
    {"SVEZIA", "SWEDEN", 752, "SE", "SWE", "se", "SEK"},
    {"SVIZZERA", "SWITZERLAND", 756, "CH", "CHE", "ch", "CHF"},
    {"TAIWAN", "TAIWAN", 158, "TW", "TWN", "tw", "TWD"},
    {"TANZANIA", "TANZANIA", 834, "TZ", "TZA", "tz", "TZS"},
    {"TERRE AUSTRALI E ANTARTICHE FRANCESI",
     "FRENCH SOUTHERN AND ANTARCTIC LANDS", 260, "TF", "ATF", "tf", "EUR"},
    {"TERRITORIO BRITANNICO DELL'OCEANO INDIANO",
     "BRITISH INDIAN OCEAN TERRITORY", 86, "IO", "IOT", "io", "USD"},
    {"THAILANDIA", "THAILAND", 764, "TH", "THA", "th", "THB"},
    {"TIMOR EST", "EAST TIMOR", 626, "TL", "TLS", "tl", "USD"},
    {"TOGO", "TOGO", 768, "TG", "TGO", "tg", "XOF"},
    {"TUNISIA", "TUNISIA", 788, "TN", "TUN", "tn", "TND"},
    {"TURCHIA", "TURKEY", 792, "TR", "TUR", "tr", "TRY"},
    {"TURKS E CAICOS",
     "TURKS AND CAICOS ISLANDS", 796, "TC", "TCA", "tc", "USD"},
    {"UCRAINA", "UKRAINE", 804, "UA", "UKR", "ua", "UAH"},
    {"UGANDA", "UGANDA", 800, "UG", "UGA", "ug", "UGX"},
    {"UNGHERIA", "HUNGARY", 348, "HU", "HUN", "hu", "HUF"},
    {"URUGUAY", "URUGUAY", 858, "UY", "URY", "uy", "UYU"},
    {"UZBEKISTAN", "UZBEKISTAN", 242, "UZ", "UZB", "uz", "UZS"},
    {"VENEZUELA", "VENEZUELA", 862, "VE", "VEN", "ve", "VEB"},
    {"VIETNAM", "VIETNAM", 704, "VN", "VNM", "vn", "VND"},
    {"ZIMBABWE", "ZIMBABWE", 716, "ZW", "ZWE", "zw", "ZWL"}
  };

  return db;
}

}  // namespace details

/*********************************************************************
 * IMPLEMENTAZIONE
 *********************************************************************/
inline nazione::nazione(const std::string &cod)
  : cod_(trova(cod).iso3166_alpha2)
{
  if (cod_.empty())
    throw std::invalid_argument("Nazione sconosciuta");
}

inline nazione::nazione(unsigned cod) : cod_(trova(cod).iso3166_alpha2)
{
  if (cod_.empty())
    throw std::invalid_argument("Codice numerico ISO3166 scorretto");
}

///
/// Identifica le nazioni appartenenti alla UE.
///
/// \param[in] cod codice iso3166 alpha2 o nome della nazione di interesse
/// \return        `true` se la nazione fa parte della UE
///
inline bool nazione::ue(const std::string &cod)
{
  static const std::set<std::string> codes =
  {
    "AT", "BE", "BG", "CY", "CZ", "DE", "DK", "EE", "EL", "ES", "FI", "FR",
    "HR", "HU", "IE", "IT", "LT", "LU", "LV", "MT", "NL", "PL", "PT", "RO",
    "SE", "SK", "SI"
  };

  const std::string c2(cod.length() == 2 ? cod : trova(cod).iso3166_alpha2);

  return codes.find(c2) != codes.end();
}

///
/// Identifica le nazioni appartenenti al Regno Unito (United Kingdom).
///
/// \param[in] cod codice iso3166 alpha2 o nome della nazione di interesse
/// \return        `true` se la nazione fa parte della Regno Unito
///
/// Sotto la sovranita' del Regno Unito della Gran Bretagna ci sono varie
/// entita' territoriali che costituiscono i territori d'oltremare britannici.
///
inline bool nazione::uk(const std::string &cod)
{
  static const std::set<std::string> codes =
  {
    "AI", "BM", "IO", "KY", "FK", "GB", "GI", "GS", "MS", "PN", "SH", "TC",
    "VG"
  };

  const std::string c2(cod.length() == 2 ? cod : trova(cod).iso3166_alpha2);

  return codes.find(c2) != codes.end();
}

///
/// Trova i dati riguardanti la nazione identificata da `cod`
///
/// \param[in] cod codice iso3166 alpha2 o nome della nazione di interesse
/// \return        informazioni recuperate (puo' essere vuoto)
///
/// La ricerca di nomi di nazioni e' case insensitive.
///
inline nazione::info nazione::trova(const std::string &cod)
{
  static std::map<std::string, std::size_t> iso3166_alpha2_map;
  if (cod.length() == 2 && std::isupper(cod[0]) && std::isupper(cod[1]))
  {
    if (iso3166_alpha2_map.empty())
    {
      for (std::size_t i(0); i < details::db_nazioni().size(); ++i)
      {
        assert(details::db_nazioni()[i].iso3166_alpha2.length() == 2);
        iso3166_alpha2_map.emplace(details::db_nazioni()[i].iso3166_alpha2, i);
      }
    }

    const auto it(iso3166_alpha2_map.find(cod));
    return it == iso3166_alpha2_map.end() ? nazione::info()
                                          : details::db_nazioni()[it->second];
  }

  static std::map<std::string, std::size_t> nome_map;
  if (nome_map.empty())
  {
    for (std::size_t i(0); i < details::db_nazioni().size(); ++i)
      nome_map.emplace(details::db_nazioni()[i].nome, i);
  }

  const auto it(nome_map.find(toupper(cod)));
  return it == nome_map.end() ? nazione::info()
                              : details::db_nazioni()[it->second];
}

///
/// Trova i dati riguardanti la nazione identificata da `num`.
///
/// \param[in] num codice iso3166 numerico della nazione di interesse
/// \return        informazioni recuperate (puo' essere vuoto)
///
inline nazione::info nazione::trova(unsigned num)
{
  static std::map<unsigned, std::size_t> iso3166_num_map;
  if (iso3166_num_map.empty())
  {
    for (std::size_t i(0); i < details::db_nazioni().size(); ++i)
      iso3166_num_map.emplace(details::db_nazioni()[i].iso3166_num, i);
  }

  const auto it(iso3166_num_map.find(num));

  return it == iso3166_num_map.end() ? nazione::info()
                                     : details::db_nazioni()[it->second];
}

///
/// Verifica la validita' di un presunto codice ISO3166 alpha2.
///
/// \param[in] cod codice da verificare
/// \return        `true` se il codice e' valido
///
inline bool nazione::verifica_iso3166_alpha2(const std::string &cod)
{
  return trova(cod).iso3166_alpha2 == cod;
}

inline bool nazione::info::empty() const
{
  return nome.empty();
}

}  // namespace lib_commercio

#endif  // include guard
