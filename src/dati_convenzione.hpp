/**
 *  \file
 *  \remark This file is part of FELPA.
 *
 *  \copyright Copyright (C) 2015-2021 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#if !defined(FELPA_DATI_CONVENZIONE_HPP)
#define      FELPA_DATI_CONVENZIONE_HPP

#include "dati_documenti_correlati_type.hpp"

namespace felpa
{

class DatiConvenzione : public DatiDocumentiCorrelatiType
{
public:
  DatiConvenzione() : DatiDocumentiCorrelatiType("DatiConvenzione") {}

  using DatiDocumentiCorrelatiType::operator==;

};  // class DatiConvenzione

}  // namespace felpa

#endif  // include guard
