/**
 *  \file
 *  \remark This file is part of FELPA.
 *
 *  \copyright Copyright (C) 2018 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#if !defined(FELPA_ALIQUOTA_RITENUTA_HPP)
#define      FELPA_ALIQUOTA_RITENUTA_HPP

#include "fattura_elettronica_eccezioni.hpp"
#include "numero_type.hpp"

namespace felpa
{

///
/// Aliquota (espressa in percentuale %) della ritenuta.
///
class AliquotaRitenuta : public NumeroType
{
public:
  explicit AliquotaRitenuta(const std::string &p = "")
    : NumeroType("AliquotaRitenuta", 2, solo_positivo)
  {
    assign(p);
  }

  explicit AliquotaRitenuta(double p)
    : NumeroType("AliquotaRitenuta", 2, solo_positivo)
  {
    assign(p);
  }

  AliquotaRitenuta &operator=(const std::string &p)
  {
    assign(p);
    return *this;
  }

  AliquotaRitenuta &operator=(double p)
  {
    assign(p);
    return *this;
  }
};  // class AliquotaRitenuta

}  // namespace felpa

#endif  // include guard
