/**
 *  \file
 *  \remark This file is part of FELPA.
 *
 *  \copyright Copyright (C) 2015-2024 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#if !defined(FELPA_DETTAGLIO_LINEE_HPP)
#define      FELPA_DETTAGLIO_LINEE_HPP

#include <vector>

#include "aliquota_iva.hpp"
#include "altri_dati_gestionali.hpp"
#include "codice_articolo.hpp"
#include "data_inizio_periodo.hpp"
#include "data_fine_periodo.hpp"
#include "natura.hpp"
#include "prezzo_totale.hpp"
#include "prezzo_unitario.hpp"
#include "quantita.hpp"
#include "ritenuta.hpp"
#include "sconto_maggiorazione.hpp"
#include "tipo_cessione_prestazione.hpp"

namespace felpa
{

///
/// Ripetuto per ogni riga di dettaglio del documento.
///
class DettaglioLinee
{
public:
  DettaglioLinee() = default;

  [[nodiscard]] bool empty() const;

  [[nodiscard]] PrezzoUnitario prezzo_unitario_effettivo() const;

  [[nodiscard]] static const std::string &tag();

  // ********* Data member pubblici *********
  /// \warning
  /// OBBLIGATORIETA' sconto, premio, abbuono o spesa accessoria.
  TipoCessionePrestazione tipo_cessione_prestazione {};

  /// Eventuale codifica conosciuta. La voce e' consigliata.
  ///
  /// \note
  /// Attualmente non supportato: lo standard prevede che sia possibile
  /// specificare piu' di una tipologia di codifica per lo stesso articolo.
  CodiceArticolo codice_articolo {};

  /// \warning OBBLIGATORIETA': SI, sempre.
  std::string descrizione {};

  /// \warning
  /// OBBLIGATORIETA': SI, ma solo se il bene/servizio e' oggettivamente
  /// quantificabile.
  Quantita quantita {};

  /// \warning
  /// OBBLIGATORIETA': SI, ma solo se e' stata indicata una quantita'.
  std::string unita_misura {};

  /// \warning OBBLIGATORIETA': SI, sempre.
  PrezzoUnitario prezzo_unitario {};

  /// \note
  /// Lo standard prevede che questa voce possa essere replicata piu' volte
  /// per rappresentare sconti in cascata.
  /// Attualmente questa possibilita' non e' supportata.
  ScontoMaggiorazione sconto_maggiorazione {};

  /// \warning OBBLIGATORIETA': SI, sempre.
  PrezzoTotale prezzo_totale {};

  /// \warning OBBLIGATORIETA': SI, sempre.
  AliquotaIva aliquota_iva {};

  /// Indica se la linea della fattura si riferisce ad una cessione/prestazione
  /// soggetta a ritenuta.
  Ritenuta ritenuta {};

  /// \warning
  /// OBBLIGATORIETA': SI, se l'operazione non rientra tra le operazioni
  /// fiscalmente "imponibili" o nei casi di inversione contabile.
  Natura natura {};

  /// Data iniziale del periodo di riferimento cui si riferisce l'eventuale
  /// servizio prestato.
  DataInizioPeriodo data_inizio_periodo {};

  /// Data finale del periodo di riferimento cui si riferisce l'eventuale
  /// servizio prestato.
  DataFinePeriodo data_fine_periodo {};

  // Dati aggiuntivi utili alla gestione automatica della fattura elettronica.
  std::vector<AltriDatiGestionali> altri_dati_gestionali {};
};  // class DettaglioLinee

[[nodiscard]] bool operator==(const DettaglioLinee &, const DettaglioLinee &);

///
/// Calcola il prezzo totale per ogni linea della fattura (**se non gia'
/// compilato** e solo **in caso cessione diversa dall'omaggio**).
///
[[nodiscard]] DettaglioLinee completa(DettaglioLinee);

}  // namespace felpa

#endif  // include guard
