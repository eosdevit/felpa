/**
 *  \file
 *  \remark This file is part of FELPA.
 *
 *  \copyright Copyright (C) 2015-2024 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#if !defined(FELPA_DETTAGLI_HPP)
#define      FELPA_DETTAGLI_HPP

#include <string>

#include "third_party/tinyxml2/tinyxml2.h"

namespace felpa
{
class Allegato;
class AltriDatiGestionali;
class Anagrafica;
class CausalePagamento;
class CedentePrestatore;
class CessionarioCommittente;
class CodiceArticolo;
class CodiceDestinatario;
class CondizioniPagamento;
class ContattiTrasmittente;
class DataOraType;
class DataType;
class DatiAnagraficiCedente;
class DatiAnagraficiCessionario;
class DatiAnagraficiType;
class DatiAnagraficiVettore;
class DatiBeniServizi;
class DatiBollo;
class DatiDDT;
class DatiDocumentiCorrelatiType;
class DatiGenerali;
class DatiGeneraliDocumento;
class DatiPagamento;
class DatiRiepilogo;
class DatiRitenuta;
class DatiSAL;
class DatiTrasmissione;
class DatiTrasporto;
class DettaglioLinee;
class DettaglioPagamento;
class Divisa;
class EsigibilitaIva;
class FatturaElettronicaBody;
class FatturaElettronicaHeader;
class FormatoTrasmissione;
class IdFiscaleType;
class IndirizzoType;
class IscrizioneRea;
class ModalitaPagamento;
class Natura;
class NazioneType;
class NumeroType;
class RegimeFiscale;
class Ritenuta;
class ScontoMaggiorazione;
class SoggettoEmittente;
class TipoCessionePrestazione;
class TipoDocumento;
class TipoRitenuta;

namespace dettagli
{

// In generale le funzioni `carica` restituiscono un valore `true` se le
// informazioni richieste sono disponibile E conformi allo standard della
// fattura elettronica.
// Indipendentemente dalla conformita' (per esempio la mancanza di una voce
// obbligatoria) tutte le informazioni disponibili vengono lette.
// In caso di errore irreversibile viene sollevata un'eccezione.

bool carica(tinyxml2::XMLConstHandle &, Allegato *);
bool carica(tinyxml2::XMLConstHandle &, AltriDatiGestionali *);
bool carica(tinyxml2::XMLConstHandle &, Anagrafica *);
bool carica(tinyxml2::XMLConstHandle &, CausalePagamento *);
bool carica(tinyxml2::XMLConstHandle &, CedentePrestatore *);
bool carica(tinyxml2::XMLConstHandle &, CessionarioCommittente *);
bool carica(tinyxml2::XMLConstHandle &, CodiceArticolo *);
bool carica(tinyxml2::XMLConstHandle &, CodiceDestinatario *);
bool carica(tinyxml2::XMLConstHandle &, CondizioniPagamento *);
bool carica(tinyxml2::XMLConstHandle &, ContattiTrasmittente *);
bool carica(tinyxml2::XMLConstHandle &, DataOraType *);
bool carica(tinyxml2::XMLConstHandle &, DataType *);
bool carica(tinyxml2::XMLConstHandle &, DatiAnagraficiCedente *);
bool carica(tinyxml2::XMLConstHandle &, DatiAnagraficiCessionario *);
bool carica(tinyxml2::XMLConstHandle &, bool, DatiAnagraficiType *);
bool carica(tinyxml2::XMLConstHandle &, DatiAnagraficiVettore *);
bool carica(tinyxml2::XMLConstHandle &, DatiBeniServizi *);
bool carica(tinyxml2::XMLConstHandle &, DatiBollo *);
bool carica(tinyxml2::XMLConstHandle &, DatiDDT *);
bool carica(tinyxml2::XMLConstHandle &, DatiDocumentiCorrelatiType *);
bool carica(tinyxml2::XMLConstHandle &, DatiGenerali *);
bool carica(tinyxml2::XMLConstHandle &, DatiGeneraliDocumento *);
bool carica(tinyxml2::XMLConstHandle &, DatiPagamento *);
bool carica(tinyxml2::XMLConstHandle &, DatiRiepilogo *);
bool carica(tinyxml2::XMLConstHandle &, DatiRitenuta *);
bool carica(tinyxml2::XMLConstHandle &, DatiSAL *);
bool carica(tinyxml2::XMLConstHandle &, DatiTrasmissione *);
bool carica(tinyxml2::XMLConstHandle &, DatiTrasporto *);
bool carica(tinyxml2::XMLConstHandle &, DettaglioLinee *);
bool carica(tinyxml2::XMLConstHandle &, DettaglioPagamento *);
bool carica(tinyxml2::XMLConstHandle &, Divisa *);
bool carica(tinyxml2::XMLConstHandle &, EsigibilitaIva *);
bool carica(tinyxml2::XMLConstHandle &, FatturaElettronicaBody *);
bool carica(tinyxml2::XMLConstHandle &, FatturaElettronicaHeader *);
bool carica(tinyxml2::XMLConstHandle &, FormatoTrasmissione *);
bool carica(tinyxml2::XMLConstHandle &, IdFiscaleType *);
bool carica(tinyxml2::XMLConstHandle &, IndirizzoType *);
bool carica(tinyxml2::XMLConstHandle &, IscrizioneRea *);
bool carica(tinyxml2::XMLConstHandle &, ModalitaPagamento *);
bool carica(tinyxml2::XMLConstHandle &, Natura *);
bool carica(tinyxml2::XMLConstHandle &, NazioneType *);
bool carica(tinyxml2::XMLConstHandle &, NumeroType *);
bool carica(tinyxml2::XMLConstHandle &, RegimeFiscale *);
bool carica(tinyxml2::XMLConstHandle &, Ritenuta *);
bool carica(tinyxml2::XMLConstHandle &, ScontoMaggiorazione *);
bool carica(tinyxml2::XMLConstHandle &, SoggettoEmittente *);
bool carica(tinyxml2::XMLConstHandle &, TipoCessionePrestazione *);
bool carica(tinyxml2::XMLConstHandle &, TipoDocumento *);
bool carica(tinyxml2::XMLConstHandle &, TipoRitenuta *);

void registra(tinyxml2::XMLPrinter &, const Allegato &);
void registra(tinyxml2::XMLPrinter &, const AltriDatiGestionali &);
void registra(tinyxml2::XMLPrinter &, const Anagrafica &);
void registra(tinyxml2::XMLPrinter &, const CausalePagamento &);
void registra(tinyxml2::XMLPrinter &, const CedentePrestatore &);
void registra(tinyxml2::XMLPrinter &, const CessionarioCommittente &);
void registra(tinyxml2::XMLPrinter &, const CodiceArticolo &);
void registra(tinyxml2::XMLPrinter &, const CodiceDestinatario &);
void registra(tinyxml2::XMLPrinter &, const CondizioniPagamento &);
void registra(tinyxml2::XMLPrinter &, const ContattiTrasmittente &);
void registra(tinyxml2::XMLPrinter &, const DataOraType &);
void registra(tinyxml2::XMLPrinter &, const DataType &);
void registra(tinyxml2::XMLPrinter &, const DatiAnagraficiCedente &);
void registra(tinyxml2::XMLPrinter &, const DatiAnagraficiCessionario &);
void registra(tinyxml2::XMLPrinter &, const DatiAnagraficiType &, bool);
void registra(tinyxml2::XMLPrinter &, const DatiAnagraficiVettore &);
void registra(tinyxml2::XMLPrinter &, const DatiBeniServizi &);
void registra(tinyxml2::XMLPrinter &, const DatiBollo &);
void registra(tinyxml2::XMLPrinter &, const DatiDDT &);
void registra(tinyxml2::XMLPrinter &, const DatiDocumentiCorrelatiType &);
void registra(tinyxml2::XMLPrinter &, const DatiGenerali &);
void registra(tinyxml2::XMLPrinter &, const DatiGeneraliDocumento &);
void registra(tinyxml2::XMLPrinter &, const DatiPagamento &);
void registra(tinyxml2::XMLPrinter &, const DatiRiepilogo &);
void registra(tinyxml2::XMLPrinter &, const DatiRitenuta &);
void registra(tinyxml2::XMLPrinter &, const DatiSAL &);
void registra(tinyxml2::XMLPrinter &, const DatiTrasmissione &);
void registra(tinyxml2::XMLPrinter &, const DatiTrasporto &);
void registra(tinyxml2::XMLPrinter &, unsigned, const DettaglioLinee &);
void registra(tinyxml2::XMLPrinter &, const DettaglioPagamento &);
void registra(tinyxml2::XMLPrinter &, const Divisa &);
void registra(tinyxml2::XMLPrinter &, const EsigibilitaIva &);
void registra(tinyxml2::XMLPrinter &, const FatturaElettronicaBody &);
void registra(tinyxml2::XMLPrinter &, const FatturaElettronicaHeader &);
void registra(tinyxml2::XMLPrinter &, const FormatoTrasmissione &);
void registra(tinyxml2::XMLPrinter &, const IdFiscaleType &);
void registra(tinyxml2::XMLPrinter &, const IndirizzoType &);
void registra(tinyxml2::XMLPrinter &, const IscrizioneRea &);
void registra(tinyxml2::XMLPrinter &, const ModalitaPagamento &);
void registra(tinyxml2::XMLPrinter &, const Natura &);
void registra(tinyxml2::XMLPrinter &, const NazioneType &);
void registra(tinyxml2::XMLPrinter &, const NumeroType &);
void registra(tinyxml2::XMLPrinter &, const RegimeFiscale &);
void registra(tinyxml2::XMLPrinter &, const Ritenuta &);
void registra(tinyxml2::XMLPrinter &, const ScontoMaggiorazione &);
void registra(tinyxml2::XMLPrinter &, const SoggettoEmittente &);
void registra(tinyxml2::XMLPrinter &, const TipoCessionePrestazione &);
void registra(tinyxml2::XMLPrinter &, const TipoDocumento &);
void registra(tinyxml2::XMLPrinter &, const TipoRitenuta &);

inline void push_text(tinyxml2::XMLPrinter &p, const std::string &s)
{
  p.PushText(s.c_str());
}

}  // namespace dettagli
}  // namespace felpa

#endif  // include guard
