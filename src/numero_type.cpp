/**
 *  \file
 *  \remark This file is part of FELPA.
 *
 *  \copyright Copyright (C) 2015-2019 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#include <cassert>
#include <cmath>
#include <iomanip>
#include <limits>
#include <sstream>

#include "numero_type.hpp"
#include "fattura_elettronica_eccezioni.hpp"
#include "fattura_elettronica_dettagli.hpp"
#include "utility.hpp"

namespace felpa
{

bool NumeroType::valido(const std::string &n, neg_flag f)
{
  if (n.empty())
    return true;

  if (n.length() < 4 || n.length() > 21)
    return false;

  if (f == solo_positivo && n[0] == '-')
    return false;

  if (to_float_def(n, -1.0) < 0.0 && to_float_def(n, 1.0) > 0.0)
    return false;

  return true;
}

bool NumeroType::valido(double n, neg_flag f)
{
  if (f == solo_positivo && n < 0.0)
    return false;

  return true;
}

bool NumeroType::empty() const
{
  return n_.empty();
}

NumeroType::NumeroType(const std::string &t, unsigned p, neg_flag n)
  : tag_(t), precisione_(p), flag_negativo_(n)
{
  assert(precisione_ >= 2);
  assert(precisione_ <= 8);
  assert(!tag_.empty());

  assign("");
}

void NumeroType::assign(const std::string &n)
{
  if (!valido(n, flag_negativo_))
    throw eccezione::numero_cod(tag() + ": valore scorretto");

  n_ = n;

  if (!n_.empty())
    assign(to_double());
}

void NumeroType::assign(double n)
{
  if (!valido(n, flag_negativo_))
    throw eccezione::numero_cod(tag() + ": valore scorretto o mancante");

  if (n > -0.0000000001)
    n += 0.0000000001;
  else
    n -= 0.0000000001;

  std::ostringstream ss;
  ss << std::fixed << std::setprecision(precisione_) << n;

  n_ = ss.str();
}

double NumeroType::to_double() const
{
  return to_float_def(n_, std::numeric_limits<double>::quiet_NaN());
}

NumeroType::operator std::string () const
{
  std::string n1(n_);

  for (std::size_t i(0); i < n1.length(); ++i)
    if (n1[i] == ',')
    {
      n1[i] = '.';
      break;
    }

  return n1;
}

namespace dettagli
{

bool carica(tinyxml2::XMLConstHandle &ft, NumeroType *n)
{
  auto *h(ft.FirstChildElement(n->tag().c_str()).ToElement());
  if (!h)
    return false;

  auto *numero(h->GetText());
  if (!numero)
    return false;

  n->assign(numero);
  return true;
}

void registra(tinyxml2::XMLPrinter &ft, const NumeroType &n)
{
  if (!n.empty())
  {
    ft.OpenElement(n.tag().c_str());
    push_text(ft, n);
    ft.CloseElement();
  }
}

}  // namespace dettagli

}  // namespace felpa
