/**
 *  \file
 *  \remark This file is part of FELPA.
 *
 *  \copyright Copyright (C) 2018-2023 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#include <cassert>

#include "codice_destinatario.hpp"
#include "fattura_elettronica_eccezioni.hpp"
#include "fattura_elettronica_dettagli.hpp"

namespace felpa
{

const std::string &CodiceDestinatario::tag()
{
  static const std::string TAG("CodiceDestinatario");
  return TAG;
}

namespace dettagli
{

bool carica(tinyxml2::XMLConstHandle &ft, CodiceDestinatario *cd)
{
  auto *h(ft.FirstChildElement(CodiceDestinatario::tag().c_str()).ToElement());
  if (!h || !h->GetText())
    return false;

  *cd = h->GetText();
  return true;
}

void registra(tinyxml2::XMLPrinter &ft, const CodiceDestinatario &cd)
{
  ft.OpenElement(CodiceDestinatario::tag().c_str());
  push_text(ft, cd);
  ft.CloseElement();
}

}  // namespace dettagli

bool operator==(const CodiceDestinatario &c1, const CodiceDestinatario &c2)
{
  return static_cast<std::string>(c1) == static_cast<std::string>(c2);
}

bool CodiceDestinatario::valido(const std::string &id)
{
  return id.empty() || id.length() == 6 || id.length() == 7;
}

CodiceDestinatario::CodiceDestinatario(const std::string &id) : codice_(id)
{
  if (!valido(codice_))
    throw eccezione::cod_destinatario_errato(tag() + ": codice non valido");
}

CodiceDestinatario &CodiceDestinatario::operator=(const std::string &s)
{
  *this = CodiceDestinatario(s);
  return *this;
}

}  // namespace felpa
