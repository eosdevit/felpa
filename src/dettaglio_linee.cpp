/**
 *  \file
 *  \remark This file is part of FELPA.
 *
 *  \copyright Copyright (C) 2015-2024 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#include <cassert>
#include "dettaglio_linee.hpp"
#include "fattura_elettronica_dettagli.hpp"
#include "fattura_elettronica_eccezioni.hpp"
#include "utility.hpp"

namespace felpa
{

bool DettaglioLinee::empty() const
{
  return descrizione.empty();
}

PrezzoUnitario DettaglioLinee::prezzo_unitario_effettivo() const
{
  PrezzoUnitario pe(prezzo_unitario);

  if (!sconto_maggiorazione.empty())
  {
    if (auto perc = sconto_maggiorazione.percentuale;
        !perc.empty())  // variazione percentuale
    {
      if (sconto_maggiorazione.tipo == "SC")
        pe = pe.to_double() * (1.0 - perc.to_double() / 100.0);
      else
        pe = pe.to_double() * (1.0 + perc.to_double() / 100.0);
    }
    else if (auto importo = sconto_maggiorazione.importo;
             !importo.empty())  // variazione in valore assoluto
    {
      if (sconto_maggiorazione.tipo == "SC")
        pe = pe.to_double() - importo.to_double();
      else
        pe = pe.to_double() + importo.to_double();
    }
  }

  return pe;
}

DettaglioLinee completa(DettaglioLinee dl)
{
  // Controllo di sicurezza. Non vogliamo che venga ricalcolato il prezzo
  // totale se:
  // - e' gia' stato fornito dall'utente;
  // - nel caso di un omaggio (e' possibile che il prezzo totale sia nullo pur
  //   in presenza di un prezzo unitario non nullo).
  if (!dl.prezzo_totale.empty()
      || !dl.tipo_cessione_prestazione.empty())
    return dl;

/*
  const double pu(dl.prezzo_unitario.to_double());

  const double t1(pu * dl.quantita.to_double());

  PrezzoTotale t2(t1);
  if (!dl.sconto_maggiorazione.empty())
  {
    const auto perc(dl.sconto_maggiorazione.percentuale);
    const auto  importo(dl.sconto_maggiorazione.importo);

    if (dl.sconto_maggiorazione.tipo == "SC")
    {
      if (!perc.empty())
        t2 = t1 * (1.0 - perc.to_double() / 100.0);
      else if (!importo.empty())
        t2 = t1 - importo.to_double();
    }
    else  // maggiorazione
    {
      if (!perc.empty())
        t2 = t1 * (1.0 + perc.to_double() / 100.0);
      else if (!importo.empty())
        t2 = t1 + importo.to_double();
    }

    // Con i numeri in virgola mobile non si sa mai...
    if (t2.to_double() < 0.0)
      t2 = 0.0;
      }*/

  if (!dl.prezzo_unitario_effettivo().empty())
    dl.prezzo_totale = dl.prezzo_unitario_effettivo().to_double()
                       * dl.quantita.to_double();

  return dl;
}

bool operator==(const DettaglioLinee &lhs, const DettaglioLinee &rhs)
{
  return
    std::string(lhs.tipo_cessione_prestazione)
    == std::string(rhs.tipo_cessione_prestazione)
    && lhs.codice_articolo == rhs.codice_articolo
    && lhs.descrizione == rhs.descrizione
    && std::string(lhs.quantita) == std::string(rhs.quantita)
    && lhs.unita_misura == rhs.unita_misura
    && std::string(lhs.prezzo_unitario) == std::string(rhs.prezzo_unitario)
    && lhs.sconto_maggiorazione == rhs.sconto_maggiorazione
    && std::string(lhs.prezzo_totale) == std::string(rhs.prezzo_totale)
    && std::string(lhs.aliquota_iva) == std::string(rhs.aliquota_iva)
    && std::string(lhs.ritenuta) == std::string(rhs.ritenuta)
    && std::string(lhs.natura) == std::string(rhs.natura)
    && lhs.data_inizio_periodo == rhs.data_inizio_periodo
    && lhs.data_fine_periodo == rhs.data_fine_periodo
    && lhs.altri_dati_gestionali == rhs.altri_dati_gestionali;
}

const std::string &DettaglioLinee::tag()
{
  static const std::string TAG("DettaglioLinee");
  return TAG;
}

namespace dettagli
{

constexpr char DESCRIZIONE_TAG[] = "Descrizione";
constexpr char NUMERO_LINEA_TAG[] = "NumeroLinea";
constexpr char UNITA_MISURA_TAG[] = "UnitaMisura";

constexpr std::size_t DESCRIZIONE_SIZE = 1000;
constexpr std::size_t UNITA_MISURA_SIZE = 10;

bool carica(tinyxml2::XMLConstHandle &ft, DettaglioLinee *dl)
{
  tinyxml2::XMLConstHandle h(ft.FirstChildElement(dl->tag().c_str()));
  if (!h.ToElement())
    h = ft;

  carica(h, &dl->tipo_cessione_prestazione);

  carica(h, &dl->codice_articolo);

  if (auto *he = h.FirstChildElement(DESCRIZIONE_TAG).ToElement())
    dl->descrizione = coalesce(he->GetText()).substr(0, DESCRIZIONE_SIZE);

  carica(h, &dl->quantita);

  if (auto *he = h.FirstChildElement(UNITA_MISURA_TAG).ToElement())
    dl->unita_misura = coalesce(he->GetText()).substr(0, UNITA_MISURA_SIZE);

  const bool pu(carica(h, &dl->prezzo_unitario));

  carica(h, &dl->sconto_maggiorazione);

  const bool pt(carica(h, &dl->prezzo_totale));

  const bool ai(carica(h, &dl->aliquota_iva));

  carica(h, &dl->ritenuta);

  carica(h, &dl->data_inizio_periodo);
  carica(h, &dl->data_fine_periodo);

  carica(h, &dl->natura);

  dl->altri_dati_gestionali.clear();
  const char *dg_tag(AltriDatiGestionali::tag().c_str());
  for (auto *he(h.FirstChildElement(dg_tag).ToElement());
       he;
       he = he->NextSiblingElement(dg_tag))
  {
    tinyxml2::XMLConstHandle heh(he);
    AltriDatiGestionali adg;
    if (carica(heh, &adg))
      dl->altri_dati_gestionali.push_back(adg);
  }

  return dl->descrizione.length() && pu && pt && ai;
}

void registra(tinyxml2::XMLPrinter &ft, unsigned i, const DettaglioLinee &dl)
{
  ft.OpenElement(dl.tag().c_str());

  if (i == 0)
    throw eccezione::dett_linee_numero_mancante(dl.tag()
                                                + ": numero linea mancante");
  ft.OpenElement(NUMERO_LINEA_TAG);
  push_text(ft, std::to_string(i));
  ft.CloseElement();

  if (!dl.tipo_cessione_prestazione.empty())
    registra(ft, dl.tipo_cessione_prestazione);

  if (!dl.codice_articolo.empty())
    registra(ft, dl.codice_articolo);

  if (dl.descrizione.empty())
    throw eccezione::dett_linee_dex_mancante(dl.tag()
                                             + ": descrizione mancante");
  ft.OpenElement(DESCRIZIONE_TAG);
  push_text(ft, dl.descrizione.substr(0, DESCRIZIONE_SIZE));
  ft.CloseElement();

  registra(ft, dl.quantita);

  const auto um(dl.unita_misura.substr(0, UNITA_MISURA_SIZE));
  if (!um.empty())
  {
    ft.OpenElement(UNITA_MISURA_TAG);
    push_text(ft, um);
    ft.CloseElement();
  }

  if (dl.prezzo_unitario.empty())
    throw eccezione::dett_linee_pu_mancante(dl.tag()
                                            + ": prezzo unitario mancante");
  registra(ft, dl.prezzo_unitario);

  if (!dl.sconto_maggiorazione.empty())
    registra(ft, dl.sconto_maggiorazione);

  DettaglioLinee dlc(completa(dl));

  registra(ft, dlc.prezzo_totale);

  registra(ft, dl.aliquota_iva);

  registra(ft, dl.ritenuta);

  if (dl.aliquota_iva.empty() && !dl.natura.empty())
    throw eccezione::dett_linee_conflitto_natura(
      dl.tag() + ": esenzione IVA specificata per voce non esente");

  if (!dl.data_inizio_periodo.empty())
    registra(ft, dl.data_inizio_periodo);

  if (!dl.data_fine_periodo.empty())
    registra(ft, dl.data_fine_periodo);

  if (!dl.natura.empty())
    registra(ft, dl.natura);

  for (const auto &d : dl.altri_dati_gestionali)
    registra(ft, d);

  ft.CloseElement();
}

}  // namespace dettagli

}  // namespace felpa
