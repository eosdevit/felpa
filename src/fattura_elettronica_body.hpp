/**
 *  \file
 *  \remark This file is part of FELPA.
 *
 *  \copyright Copyright (C) 2015-2024 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#if !defined(FELPA_FATTURA_ELETTRONICA_BODY_HPP)
#define      FELPA_FATTURA_ELETTRONICA_BODY_HPP

#include "allegato.hpp"
#include "dati_generali.hpp"
#include "dati_beni_servizi.hpp"
#include "dati_pagamento.hpp"

namespace felpa
{

///
/// Costituisce il corpo della fattura e ne rappresenta il contenuto specifico.
/// Deve essere presente una sola volta nel caso di fattura singola, mentre nel
/// caso di lotto di fatture deve essere ripetuto per ogni fattura.
///
class FatturaElettronicaBody
{
public:
  explicit FatturaElettronicaBody(EsigibilitaIva);

  [[nodiscard]] bool empty() const;

  [[nodiscard]] static const std::string &tag();

  // ********* Data member pubblici *********
  /// Contiene i dati generali del documento fattura e dei documenti ad essa
  /// correlati.
  /// \warning OBBLIGATORIETA': SI, sempre.
  DatiGenerali dati_generali {};

  /// Contiene i dati relativi all'operazione cui la fattura si riferisce: la
  /// cessione di beni o la prestazione di servizi.
  /// \warning OBBLIGATORIETA': SI, sempre.
  DatiBeniServizi dati_beni_servizi;

  /// Informazioni relative al pagamento in termini di condizioni, modalita e
  /// termini.
  /// Puo' essere replicato piu' volte: in questo modo e' possibile
  /// rappresentare, per lo stesso documento, eventuali condizioni diverse di
  /// pagamento (attualmente non supportato).
  DatiPagamento dati_pagamento {};

  /// Dati relativi ad eventuali allegati.
  std::vector<Allegato> allegati {};
};  // class FatturaElettronicaBody

///
/// Completa i totali numerici delle sezioni `DatiBeniServizi` e
/// `DatiGenerali.DatiGeneraliDocumento` (`ImportoTotaleDocumento`).
///
[[nodiscard]] FatturaElettronicaBody completa(FatturaElettronicaBody);

[[nodiscard]] bool operator==(const FatturaElettronicaBody &,
                              const FatturaElettronicaBody &);

}  // namespace felpa

#endif  // include guard
