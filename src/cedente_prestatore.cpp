/**
 *  \file
 *  \remark This file is part of FELPA.
 *
 *  \copyright Copyright (C) 2015-2024 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#include <cassert>

#include "cedente_prestatore.hpp"
#include "fattura_elettronica_eccezioni.hpp"
#include "fattura_elettronica_dettagli.hpp"
#include "utility.hpp"

namespace felpa
{

const std::string &CedentePrestatore::tag()
{
  static const std::string TAG("CedentePrestatore");
  return TAG;
}

CedentePrestatore::CedentePrestatore() : dati_anagrafici(), sede(),
                                         stabile_organizzazione(),
                                         iscrizione_rea(),
                                         riferimento_amministrazione()
{
}

bool CedentePrestatore::empty() const
{
  return *this == CedentePrestatore();
}

bool operator==(const CedentePrestatore &lhs, const CedentePrestatore &rhs)
{
  return lhs.dati_anagrafici == rhs.dati_anagrafici
         && lhs.sede == rhs.sede
         && lhs.stabile_organizzazione == rhs.stabile_organizzazione
         && lhs.iscrizione_rea == rhs.iscrizione_rea
         && lhs.riferimento_amministrazione == rhs.riferimento_amministrazione;
}

CedentePrestatore completa(CedentePrestatore cp)
{
  const bool italia(cp.dati_anagrafici.id_fiscale_iva.id_paese.italia());

  if (!italia)
  {
    if (cp.dati_anagrafici.regime_fiscale == RegimeFiscale("RF01"))
      cp.dati_anagrafici.regime_fiscale = "RF18";

    if (cp.sede.cap.empty())
      cp.sede.cap = "00000";
  }

  return cp;
}

namespace dettagli
{

constexpr std::size_t RIFERIMENTO_AMMINISTRAZIONE_SIZE = 20;
constexpr char RIFERIMENTO_AMMINISTRAZIONE_TAG[] = "RiferimentoAmministrazione";

bool carica(tinyxml2::XMLConstHandle &ft, CedentePrestatore *cp)
{
  auto h(ft.FirstChildElement(cp->tag().c_str()));
  if (!h.ToElement())
    return false;

  const bool da(carica(h, &cp->dati_anagrafici));
  const bool se(carica(h, &cp->sede));

  carica(h, &cp->stabile_organizzazione);
  carica(h, &cp->iscrizione_rea);

  if (auto *he =
      h.FirstChildElement(RIFERIMENTO_AMMINISTRAZIONE_TAG).ToElement())
    cp->riferimento_amministrazione =
      coalesce(he->GetText()).substr(0, RIFERIMENTO_AMMINISTRAZIONE_SIZE);

  return da && se;
}

void registra(tinyxml2::XMLPrinter &ft, const CedentePrestatore &cp)
{
  ft.OpenElement(cp.tag().c_str());

  registra(ft, cp.dati_anagrafici);
  registra(ft, cp.sede);

  if (!cp.stabile_organizzazione.empty())
    registra(ft, cp.stabile_organizzazione);

  registra(ft, cp.iscrizione_rea);

  if (!cp.riferimento_amministrazione.empty())
  {
    ft.OpenElement(RIFERIMENTO_AMMINISTRAZIONE_TAG);
    push_text(
      ft,
      cp.riferimento_amministrazione.substr(0,
                                            RIFERIMENTO_AMMINISTRAZIONE_SIZE));
    ft.CloseElement();
  }

  ft.CloseElement();
}

}  // namespace dettagli

}  // namespace felpa
