/**
 *  \file
 *  \remark This file is part of FELPA.
 *
 *  \copyright Copyright (C) 2015 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#if !defined(FELPA_CESSIONARIO_COMMITTENTE_HPP)
#define      FELPA_CESSIONARIO_COMMITTENTE_HPP

#include <string>

#include "dati_anagrafici_cessionario.hpp"
#include "sede.hpp"
#include "stabile_organizzazione.hpp"

namespace felpa
{

///
/// Dati relativi al cessionario o committente del bene/servizio oggetto di
/// fatturazione.
///
class CessionarioCommittente
{
public:
  static const std::string &tag();

  CessionarioCommittente();

  bool empty() const;

  // ********* Data members pubblici *********
  DatiAnagraficiCessionario dati_anagrafici;
  Sede sede;
};  // class CessionarioCommittente

bool operator==(const CessionarioCommittente &,
                const CessionarioCommittente &);

}  // namespace felpa

#endif  // include guard
