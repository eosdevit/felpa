/**
 *  \file
 *  \remark This file is part of FELPA.
 *
 *  \copyright Copyright (C) 2019-2024 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#if !defined(FELPA_DATI_BOLLO_HPP)
#define      FELPA_DATI_BOLLO_HPP

#include "importo_bollo.hpp"

namespace felpa
{

class DatiBollo
{
public:
  DatiBollo() = default;

  [[nodiscard]] bool empty() const;

  [[nodiscard]] static const std::string &tag();

  // ********* Data member pubblici *********

  /// Indica l'assolvimento dell'imposta di bolla ai sensi del decreto MEF 17
  /// giugno 2014 (bollo virtuale).
  std::string bollo_virtuale {};

  /// Importo dell'imposta di bollo.
  /// \remark
  /// Ai sensi dell'art. 8 del DPR n.642/1972 nei rapporti con lo Stato, o con
  /// enti parificati per legge allo Stato, agli effetti tributari, l'imposta
  /// di bollo e' a carico del fornitore e pertanto l'importo corrispondente
  /// non deve essere incluso nel campo `ImponibileImporto`.
  ImportoBollo importo_bollo {};

  static constexpr char NO[] = "NO";
  static constexpr char SI[] = "SI";
};  // class DatiBollo

[[nodiscard]] bool operator==(const DatiBollo &, const DatiBollo &);

}  // namespace felpa

#endif  // include guard
