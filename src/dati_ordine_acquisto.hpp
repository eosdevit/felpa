/**
 *  \file
 *  \remark This file is part of FELPA.
 *
 *  \copyright Copyright (C) 2015-2021 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#if !defined(FELPA_DATI_ORDINE_ACQUISTO_HPP)
#define      FELPA_DATI_ORDINE_ACQUISTO_HPP

#include "dati_documenti_correlati_type.hpp"

namespace felpa
{

///
/// Utilizzato per dare evidenza del fatto che la fattura e' emessa a fronte di
/// un ordine di acquisto al quale fa riferimento.
///
/// La valorizzazione avviene o dietro richiesta del cessionario/committente che
/// ha necessita' di "legare" la fattura all'ordine di acquisto
/// precedentemente emesso, ovvero quando si deve dare evidenza dei codici CIG
/// e CUP legati ad un ordine di acquisto.
///
/// Occorre considerare che l'art.25 del DL 66/2014, convertito nella Legge 23
/// giugno 2014 n. 89, prevede in alcuni casi l'indicazione obbligatoria di CIG
/// e/o CUP in fattura, la cui assenza ha come conseguenza il mancato pagamento
/// da parte dell'amministrazione.
///
class DatiOrdineAcquisto : public DatiDocumentiCorrelatiType
{
public:
  DatiOrdineAcquisto() : DatiDocumentiCorrelatiType("DatiOrdineAcquisto") {}

  using DatiDocumentiCorrelatiType::operator==;
};  // class DatiOrdineAcquisto

}  // namespace felpa

#endif  // Include guard
