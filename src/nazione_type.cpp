/**
 *  \file
 *  \remark This file is part of FELPA.
 *
 *  \copyright Copyright (C) 2015 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#include <cassert>

#include "nazione_type.hpp"
#include "fattura_elettronica_eccezioni.hpp"
#include "fattura_elettronica_dettagli.hpp"

#include "lib_commercio/nazione.hpp"

namespace felpa
{

bool NazioneType::valido(const std::string &id)
{
  return lib_commercio::nazione::verifica_iso3166_alpha2(id);
}

NazioneType::NazioneType(const std::string &t, const std::string &id) : tag_(t)
{
  assert(!tag_.empty());

  assign(id);
}

void NazioneType::assign(const std::string &s)
{
  if (!valido(s))
    throw eccezione::nazione_cod_scorretto(tag() +  ": codice non valido");
  codice_ = s;
  assert(codice_.length() == 2);
}

bool NazioneType::italia() const
{
  return codice_ == "IT";
}

bool NazioneType::ue() const
{
  return lib_commercio::nazione::ue(codice_);
}

namespace dettagli
{

bool carica(tinyxml2::XMLConstHandle &ft, NazioneType *id)
{
  auto *h(ft.FirstChildElement(id->tag().c_str()).ToElement());
  if (!h)
    return false;

  auto *cod(h->GetText());
  if (!cod)
    return false;

  id->assign(cod);
  return true;
}

void registra(tinyxml2::XMLPrinter &ft, const NazioneType &id)
{
  ft.OpenElement(id.tag().c_str());
  push_text(ft, id);
  ft.CloseElement();
}

}  // namespace dettagli

}  // namespace felpa
