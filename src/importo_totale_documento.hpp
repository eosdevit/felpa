/**
 *  \file
 *  \remark This file is part of FELPA.
 *
 *  \copyright Copyright (C) 2015-2018 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#if !defined(FELPA_IMPORTO_TOTALE_DOCUMENTO_HPP)
#define      FELPA_IMPORTO_TOTALE_DOCUMENTO_HPP

#include "numero_type.hpp"

namespace felpa
{

///
/// Serve per dare evidenza dell'ammontare totale del documento, diminuito
/// dell'eventuale sconto o aumentato dell'eventale maggiorazione e comprensivo
/// di imposta a debito del cessionario / committente.
///
/// Contiene un valore numerico composto da un intero e da due decimali. I
/// decimali, separati dall'intero con il carattere punto ("."), vanno sempre
/// indicati anche se pari a zero (es.: 2500.00).
///
/// \note
/// Il sistema non effettua alcun controllo di corrispondenza tra il valore del
/// campo `ImportoTotaleDocumento` e la somma dei valori contenuti nel campo
/// `PrezzoTotale` a livello di linea di dettaglio.
///
class ImportoTotaleDocumento : public NumeroType
{
public:
  explicit ImportoTotaleDocumento(const std::string & = "");
  explicit ImportoTotaleDocumento(double);

  ImportoTotaleDocumento &operator=(const std::string &);
  ImportoTotaleDocumento &operator=(double);
};  // class ImportoTotaleDocumento

}  // namespace felpa

#endif  // include guard
