/**
 *  \file
 *  \remark This file is part of FELPA.
 *
 *  \copyright Copyright (C) 2015-2024 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#include <cassert>

#include "dati_generali_documento.hpp"
#include "fattura_elettronica_dettagli.hpp"
#include "fattura_elettronica_eccezioni.hpp"
#include "utility.hpp"

namespace felpa
{

const std::string &DatiGeneraliDocumento::tag()
{
  static const std::string TAG("DatiGeneraliDocumento");
  return TAG;
}

bool DatiGeneraliDocumento::empty() const
{
  return data.empty() || numero.empty();
}

bool operator==(const DatiGeneraliDocumento &lhs,
                const DatiGeneraliDocumento &rhs)
{
  return
    std::string(lhs.tipo_documento) == std::string(rhs.tipo_documento)
    && std::string(lhs.divisa) == std::string(rhs.divisa)
    && std::string(lhs.data) == std::string(rhs.data)
    && std::string(lhs.numero) == std::string(rhs.numero)
    && lhs.dati_ritenuta == rhs.dati_ritenuta
    && lhs.dati_bollo == rhs.dati_bollo
    && lhs.sconto_maggiorazione == rhs.sconto_maggiorazione
    && std::string(lhs.importo_totale_documento)
       == std::string(rhs.importo_totale_documento)
    && lhs.causale == rhs.causale;
}

namespace dettagli
{

constexpr char CAUSALE_TAG[] = "Causale";
constexpr char NUMERO_TAG[] = "Numero";

constexpr std::size_t CAUSALE_SIZE = 200;
constexpr std::size_t NUMERO_SIZE = 20;

bool carica(tinyxml2::XMLConstHandle &ft, DatiGeneraliDocumento *dd)
{
  auto h(ft.FirstChildElement(dd->tag().c_str()));
  if (!h.ToElement())
    return false;

  const bool tid(carica(h, &dd->tipo_documento));
  const bool div(carica(h, &dd->divisa));
  const bool dat(carica(h, &dd->data));

  if (auto *he = h.FirstChildElement(NUMERO_TAG).ToElement())
    dd->numero = coalesce(he->GetText()).substr(0, NUMERO_SIZE);

  dd->dati_ritenuta.clear();
  const char *dr_tag(DatiRitenuta::tag().c_str());
  for (auto *he(h.FirstChildElement(dr_tag).ToElement());
       he;
       he = he->NextSiblingElement(dr_tag))
  {
    tinyxml2::XMLConstHandle heh(he);
    DatiRitenuta dett_dr;
    if (carica(heh, &dett_dr))
      dd->dati_ritenuta.push_back(dett_dr);
  }

  carica(h, &dd->dati_bollo);

  dd->sconto_maggiorazione.clear();
  const char *sm_tag(ScontoMaggiorazione::tag().c_str());
  for (auto *he(h.FirstChildElement(sm_tag).ToElement());
       he;
       he = he->NextSiblingElement(sm_tag))
  {
    tinyxml2::XMLConstHandle heh(he);
    ScontoMaggiorazione dett_sm;
    if (carica(heh, &dett_sm))
      dd->sconto_maggiorazione.push_back(dett_sm);
  }

  carica(h, &dd->importo_totale_documento);

  dd->causale = {};
  for (auto *he(h.FirstChildElement(CAUSALE_TAG).ToElement());
       he;
       he = he->NextSiblingElement(CAUSALE_TAG))
    dd->causale += coalesce(he->GetText());

 return tid && div && dat && !dd->numero.empty();
}

void registra(tinyxml2::XMLPrinter &ft, const DatiGeneraliDocumento &dd)
{
  ft.OpenElement(dd.tag().c_str());

  registra(ft, dd.tipo_documento);
  registra(ft, dd.divisa);

  if (dd.data.empty())
    throw eccezione::data_doc_mancante(
      DatiGeneraliDocumento::tag() + ": data mancante");

  registra(ft, dd.data);

  if (dd.numero.empty())
    throw eccezione::numero_doc_mancante(std::string(NUMERO_TAG)
                                         + ": numero mancante");
  ft.OpenElement(NUMERO_TAG);
  push_text(ft, dd.numero.substr(0, NUMERO_SIZE));
  ft.CloseElement();

  for (const auto &r : dd.dati_ritenuta)
    registra(ft, r);

  registra(ft, dd.dati_bollo);

  for (const auto &s : dd.sconto_maggiorazione)
    registra(ft, s);

  if (!dd.importo_totale_documento.empty())
    registra(ft, dd.importo_totale_documento);

  for (unsigned pos(0);
       pos < dd.causale.length()
       && !dd.causale.substr(pos, CAUSALE_SIZE).empty();
       pos += CAUSALE_SIZE)
  {
    ft.OpenElement(CAUSALE_TAG);
    push_text(ft, dd.causale.substr(pos, CAUSALE_SIZE));
    ft.CloseElement();
  }

  ft.CloseElement();
}

}  // namespace dettagli

}  // namespace felpa
