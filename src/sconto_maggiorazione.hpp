/**
 *  \file
 *  \remark This file is part of FELPA.
 *
 *  \copyright Copyright (C) 2015-2024 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#if !defined(FELPA_SCONTO_MAGGIORAZIONE_HPP)
#define      FELPA_SCONTO_MAGGIORAZIONE_HPP

#include <string>

#include "percentuale.hpp"
#include "importo.hpp"

namespace felpa
{

///
/// Utilizzato per specificare sconti o maggiorazioni applicate dal
/// cedente/prestatore.
///
class ScontoMaggiorazione
{
public:
  ScontoMaggiorazione() = default;

  [[nodiscard]] bool empty() const;

  [[nodiscard]] static const std::string &tag();

  static constexpr char SCONTO[] = "SC";
  static constexpr char MAGGIORAZIONE[] = "MG";

  // ********* Data member pubblici *********
  /// Serve per indicare se si tratta di uno sconto o di una maggiorazione
  /// applicata sul corrispettivo del bene/servizio.
  std::string tipo {};

  /// \warning
  /// OBBLIGATORIETA': SI, se non viene valorizzato il campo Importo.
  Percentuale percentuale {};

  /// \warning
  /// OBBLIGATORIETA': SI, se non viene valorizzato il campo Percentuale.
  Importo importo {};
};  // class ScontoMaggiorazione

[[nodiscard]] bool operator==(const ScontoMaggiorazione &,
                              const ScontoMaggiorazione &);

}  // namespace felpa

#endif  // include guard
