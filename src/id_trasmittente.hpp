/**
 *  \file
 *  \remark This file is part of FELPA.
 *
 *  \copyright Copyright (C) 2015-2020 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#if !defined(FELPA_ID_TRASMITTENTE_HPP)
#define      FELPA_ID_TRASMITTENTE_HPP

#include "id_fiscale_type.hpp"

namespace felpa
{

///
/// Serve per identificare il soggetto che effettua la trasmissione del file al
/// Sistema di Interscambio.
///
/// Deve contenere, secondo lo standard ISO 3166 - 1 alpha - 2 code, il codice
/// della nazione che ha attribuito l'identificativo fiscale al soggetto e, nel
/// caso in cui:
/// * `IdPaese == "IT"` (soggetto residente in Italia) il codice fiscale del
///   trasmittente
/// * negli altri casi l'identificativo fiscale che al trasmittente e' stato
///   attribuito dal paese di appartenenza.
///
/// \warning OBBLIGATORIETA': SI, sempre.
///
class IdTrasmittente : public IdFiscaleType
{
public:
  IdTrasmittente() : IdFiscaleType("IdTrasmittente") {}

  using IdFiscaleType::operator==;
};  // class IdTrasmittente

}  // namespace felpa

#endif  // include guard
