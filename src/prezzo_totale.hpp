/**
 *  \file
 *  \remark This file is part of FELPA.
 *
 *  \copyright Copyright (C) 2015-2018 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#if !defined(FELPA_PREZZO_TOTALE_HPP)
#define      FELPA_PREZZO_TOTALE_HPP

#include "numero_type.hpp"

namespace felpa
{

///
/// Indica il quantum imponibile rappresentato dal valore di base
/// dell'operazione diminuito di eventuali sconti ed aumentato di eventuali
/// maggiorazioni (art.21, comma 2, lett. c del DPR 633/1972).
///
/// Il campo deve contenere il prezzo totale (scontato/maggiorato) del
/// bene/servizio. Il sistema controlla che tale valore corrisponda al
/// prodotto del `PrezzoUnitario` diminuito/aumentato degli eventuali
/// sconti/maggiorazioni, per la `Quantita` se presente.
///
/// Il valore e' rappresentato da un intero e da decimali che vanno da un
/// minimo di due ad un massimo di otto cifre. I decimali, separati dall'intero
/// con il carattere punto (`.`), vanno sempre indicati anche se pari a zero.
///
/// Il controllo relativo al PrezzoTotale e' il `423`. Va osservato che esiste
/// una tolleranza di 1 centesimo di euro (se la differenza fra il valore
/// attesso e quello effettivo e' inferiore a 0.01 il controllo si ritiene
/// superato. In questo modo l'arrotondamento alla seconda cifra decimale
/// viene accettato e permane una equivalenza fra fattura cartacea e fattura
/// elettronica).
///
class PrezzoTotale : public NumeroType
{
public:
  explicit PrezzoTotale(const std::string & = "");
  explicit PrezzoTotale(double);

  PrezzoTotale &operator=(const std::string &);
  PrezzoTotale &operator=(double);
};  // class PrezzoTotale

}  // namespace felpa

#endif  // include guard
