/**
 *  \file
 *  \remark This file is part of FELPA.
 *
 *  \copyright Copyright (C) 2019-2020 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#if !defined(FELPA_ALLEGATI_HPP)
#define      FELPA_ALLEGATI_HPP

#include <string>

namespace felpa
{

///
/// Dati relativi ad eventuali allegati.
///
/// E' opportuno allegare documenti alla fattura elettronica, previo accordo
/// il destinatario del documento, quando i dati aggiuntivi:
/// - non trovano collocazione nei campi del tracciato XML;
/// - pur potendo trovare collocazione in detti campi  si ritiene maggiormente
///   utile o conveniente trasmetterli attraverso un altro file.
///
/// \warning
/// Se l'allegato contiene uno o piu' dati gia' presenti nel tracciato XML, i
/// primi non possono essere in alcun modo sostitutivi, qualora diversi, dei
/// secondi che restano quelli ufficialmente validi.
///
class Allegato
{
public:
  static const std::string &tag();

  Allegato();
  explicit Allegato(const std::string &);

  bool empty() const;

  // ********* Data member pubblici *********
  /// Nome del documento allegato alla fattura elettronica.
  /// Non e' stabilito alcun criterio particolare di valorizzazione.
  /// \warning OBBLIGATORIETA': SI (se si allega un file).
  std::string nome_attachment;

  /// Algoritmo utilizzato per comprimere l'allegato.
  /// Non e' stabilito alcun criterio particolare di valorizzazione.
  /// \warning OBBLIGATORIETA': solo se il file allegato e' compresso.
  std::string algoritmo_compressione;

  /// Formato dell'allegato.
  /// Non e' stabilito alcun criterio particolare di valorizzazione.
  std::string formato_attachment;

  /// Descrizione del documento allegato alla fattura elettronica.
  /// Non e' stabilito alcun criterio particolare di valorizzazione.
  std::string descrizione_attachment;

  /// Documento allegato alla fattura elettronica (`formato xs:base64Binary`).
  /// La dimensione massima non e' fissata a priori, ma deve necessariamente
  /// tenere conto dei vincoli dimensionali previsti pre la fattura
  /// elettronica.
  /// \warning OBBLIGATORIETA': SI (se si allega un file).
  std::string attachment;
};  // class Allegati

bool operator==(const Allegato &, const Allegato &);

}  // namespace felpa

#endif  // include guard
