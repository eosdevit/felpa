/**
 *  \file
 *  \remark This file is part of FELPA.
 *
 *  \copyright Copyright (C) 2015-2022 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#include <cassert>
#include <set>

#include "tipo_documento.hpp"
#include "fattura_elettronica_dettagli.hpp"
#include "fattura_elettronica_eccezioni.hpp"

namespace felpa
{

const std::string &TipoDocumento::tag()
{
  static const std::string TAG("TipoDocumento");
  return TAG;
}

namespace dettagli
{

const std::set<std::string> tipo_documento(
{
  "TD01",  // fattura
  "TD02",  // acconto/Anticipo su fattura
  "TD03",  // acconto/Anticipo su parcella
  "TD04",  // nota di Credito
  "TD05",  // nota di Debito
  "TD06",  // parcella
  "TD07",  // fattura semplificata
  "TD08",  // nota di credito semplificata
  "TD10",  // fattura per acquisto intracomunitario beni
  "TD11",  // fattura per acquisto intracomunitario servizi
  "TD12",  // documento riepilogativo (DPR 695/1996)
  "TD16",  // integrazione fattura reverse charge interno
  "TD17",  // integrazione autofattura per acquisto servizi dall'estero
  "TD18",  // integrazione per acquisto di beni intracomunitari
  "TD19",  // integrazione autofattura acquisto di beni ex art.17 c.2 DPR633/72
  "TD20",  // autofattura per regolarizzazione e integrazione  delle fatture
           // (art.6 c.8 e 9bis d.lgs. 471/97 o art.46 c. 5 DL 331/93)
  "TD21",  // autofattura per splafonamento
  "TD22",  // estrazione beni da deposito IVA
  "TD23",  // estrazione beni da deposito IVA con versamento dell'IVA
  "TD24",  // fattura differita di cui all'art.21 comma 4 lett. A
  "TD25",  // fattura differita di cui all'art.21 comma 4 terzo periodo lett. B
  "TD26",  // cessione beni ammortizzabili / passaggi (art.36 dpr 633/72)
  "TD27",  // fattura per autoconsumo o per cessioni gratuite senza rivalsa
  "TD28",  // acquisti da San Marino con IVA (fattura cartacea)
  "TD29"   // comunicazione per omessa o irregolare fatturazione (art. 6, comma
           // 8, D.Lgs. 471/97)
});


bool carica(tinyxml2::XMLConstHandle &ft, TipoDocumento *td)
{
  auto *h(ft.FirstChildElement(TipoDocumento::tag().c_str()).ToElement());
  if (!h || !h->GetText())
    return false;
  *td = h->GetText();

  return true;
}

void registra(tinyxml2::XMLPrinter &ft, const TipoDocumento &td)
{
  ft.OpenElement(TipoDocumento::tag().c_str());
  push_text(ft, td);
  ft.CloseElement();
}

}  // namespace dettagli

bool TipoDocumento::valido(const std::string &id)
{
  return dettagli::tipo_documento.find(id) != dettagli::tipo_documento.end();
}

TipoDocumento::TipoDocumento(const std::string &c) : codice_(c)
{
  if (!valido(codice_))
    throw eccezione::tipo_doc_cod_scorretto(tag() + ": codice scorretto");

  assert(codice_.length() == 4);
}

TipoDocumento &TipoDocumento::operator=(const std::string &c)
{
  return *this = TipoDocumento(c);
}

bool operator==(const TipoDocumento &lhs, const TipoDocumento &rhs)
{
  return std::string(lhs) == std::string(rhs);
}

}  // namespace felpa
