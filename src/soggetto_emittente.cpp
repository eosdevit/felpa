/**
 *  \file
 *  \remark This file is part of FELPA.
 *
 *  \copyright Copyright (C) 2022 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#include <cassert>

#include "soggetto_emittente.hpp"
#include "fattura_elettronica_dettagli.hpp"
#include "fattura_elettronica_eccezioni.hpp"

namespace
{

[[nodiscard]] bool valido(const std::string &id)
{
  return id == ""        // vuoto (fattura emessa da cedente/prestatore)
         || id == "CC"   // cessionario/committente
         || id == "TZ";  // soggetto terzo
}

}  // unnamed namespace

namespace felpa
{

const std::string &SoggettoEmittente::tag()
{
  static const std::string TAG("SoggettoEmittente");
  return TAG;
}

SoggettoEmittente::SoggettoEmittente(const std::string &s) : codice_(s)
{
  if (!valido(s))
    throw eccezione::soggetto_emittente_scorretto(tag()
                                                  +  ": codice non valido");
}

SoggettoEmittente &SoggettoEmittente::operator=(const std::string &s)
{
  *this = SoggettoEmittente(s);
  return *this;
}

bool SoggettoEmittente::empty() const
{
  return codice_.empty();
}

bool operator==(const SoggettoEmittente &lhs, const SoggettoEmittente &rhs)
{
  return static_cast<std::string>(lhs) == static_cast<std::string>(rhs);
}

namespace dettagli
{

bool carica(tinyxml2::XMLConstHandle &ft, SoggettoEmittente *se)
{
  auto *h(ft.FirstChildElement(se->tag().c_str()).ToElement());
  if (!h)
    return false;

  auto *cod(h->GetText());
  if (!cod)
    return false;

  *se = cod;
  return true;
}

void registra(tinyxml2::XMLPrinter &ft, const SoggettoEmittente &se)
{
  if (!se.empty())
  {
    ft.OpenElement(se.tag().c_str());
    push_text(ft, se);
    ft.CloseElement();
  }
}

}  // namespace dettagli

}  // namespace felpa
