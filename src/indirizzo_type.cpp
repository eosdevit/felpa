/**
 *  \file
 *  \remark This file is part of FELPA.
 *
 *  \copyright Copyright (C) 2015-2021 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#include <cassert>

#include "indirizzo_type.hpp"
#include "fattura_elettronica_eccezioni.hpp"
#include "fattura_elettronica_dettagli.hpp"
#include "utility.hpp"

namespace felpa
{

IndirizzoType::IndirizzoType(const std::string &t) : indirizzo(),
                                                     numero_civico(), cap(),
                                                     comune(), provincia(),
                                                     nazione(), tag_(t)
{
  assert(!tag_.empty());
}

bool IndirizzoType::operator==(const IndirizzoType &rhs) const
{
  return indirizzo == rhs.indirizzo
         && numero_civico == rhs.numero_civico
         && cap == rhs.cap
         && comune == rhs.comune
         && provincia == rhs.provincia
         && nazione == rhs.nazione;
}

bool IndirizzoType::empty() const
{
  return *this == IndirizzoType("DUMMY");
}

namespace dettagli
{

constexpr std::size_t CAP_LENGTH = 5;
constexpr std::size_t COMUNE_SIZE = 60;
constexpr std::size_t INDIRIZZO_SIZE = 60;
constexpr std::size_t NUMERO_CIVICO_SIZE = 8;
constexpr std::size_t PROVINCIA_LENGTH = 2;

constexpr char CAP_TAG[] = "CAP";
constexpr char COMUNE_TAG[] = "Comune";
constexpr char INDIRIZZO_TAG[] = "Indirizzo";
constexpr char NUMERO_CIVICO_TAG[] = "NumeroCivico";
constexpr char PROVINCIA_TAG[] = "Provincia";

bool carica(tinyxml2::XMLConstHandle &ft, IndirizzoType *id)
{
  tinyxml2::XMLConstHandle h(ft.FirstChildElement(id->tag().c_str()));
  if (!h.ToElement())
    return false;

  if (auto *he = h.FirstChildElement(INDIRIZZO_TAG).ToElement())
    id->indirizzo = coalesce(he->GetText());

  if (auto *he = h.FirstChildElement(NUMERO_CIVICO_TAG).ToElement())
    id->numero_civico = coalesce(he->GetText());

  if (auto *he = h.FirstChildElement(CAP_TAG).ToElement())
    id->cap = coalesce(he->GetText());

  if (auto *he = h.FirstChildElement(COMUNE_TAG).ToElement())
    id->comune = coalesce(he->GetText());

  if (auto *he = h.FirstChildElement(PROVINCIA_TAG).ToElement())
    id->provincia = coalesce(he->GetText());

  const bool na(carica(h, &id->nazione));

  return id->indirizzo.length() && na;
}

void registra(tinyxml2::XMLPrinter &ft, const IndirizzoType &id)
{
  ft.OpenElement(id.tag().c_str());

  if (id.indirizzo.empty())
    throw eccezione::indirizzo_mancante(id.tag() + ": indirizzo mancante");
  ft.OpenElement(INDIRIZZO_TAG);
  push_text(ft, id.indirizzo.substr(0, INDIRIZZO_SIZE));
  ft.CloseElement();

  if (!id.numero_civico.empty())
  {
    if (felpa::ends_with(id.indirizzo, id.numero_civico))
      throw eccezione::indirizzo_nr_2(
        id.tag() + ": numero civico specificato due volte");

    ft.OpenElement(NUMERO_CIVICO_TAG);
    push_text(ft, id.numero_civico.substr(0, NUMERO_CIVICO_SIZE));
    ft.CloseElement();
  }

  std::string cap(id.cap);
  if (!id.nazione.italia())
    cap = "00000";
  if (cap.length() != CAP_LENGTH)
    throw eccezione::indirizzo_cap_scorretto(id.tag() + ": CAP scorretto");
  ft.OpenElement(CAP_TAG);
  push_text(ft, cap);
  ft.CloseElement();

  if (id.comune.empty())
    throw eccezione::indirizzo_comune_mancante(id.tag() + ": comune mancante");
  ft.OpenElement(COMUNE_TAG);
  push_text(ft, id.comune.substr(0, COMUNE_SIZE));
  ft.CloseElement();

  if (!id.provincia.empty())
  {
    if (id.provincia.length() != PROVINCIA_LENGTH)
      throw eccezione::indirizzo_formato_provincia(
        id.tag() + ": formato provincia scorretto");

    ft.OpenElement(PROVINCIA_TAG);
    push_text(ft, id.provincia);
    ft.CloseElement();
  }

  registra(ft, id.nazione);

  ft.CloseElement();
}

}  // namespace dettagli

}  // namespace felpa
