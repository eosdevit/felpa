/**
 *  \file
 *  \remark This file is part of FELPA.
 *
 *  \copyright Copyright (C) 2015-2021 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#include <cassert>
#include <set>

#include "tipo_cessione_prestazione.hpp"
#include "fattura_elettronica_dettagli.hpp"
#include "fattura_elettronica_eccezioni.hpp"

namespace felpa
{

const std::string &TipoCessionePrestazione::tag()
{
  static const std::string TAG("TipoCessionePrestazione");
  return TAG;
}

namespace dettagli
{

static const std::set<std::string> tipo_cessione(
{
  "",    // VUOTO
  "SC",  // Sconto
  "PR",  // Premio
  "AB",  // Abbuono
  "AC"   // Spesa accessoria
});

bool carica(tinyxml2::XMLConstHandle &ft, TipoCessionePrestazione *tcp)
{
  auto *h(ft.FirstChildElement(tcp->tag().c_str()).ToElement());
  if (!h || !h->GetText())
    return false;

  *tcp = h->GetText();
  return true;
}

void registra(tinyxml2::XMLPrinter &ft, const TipoCessionePrestazione &tcp)
{
  if (!tcp.empty())
  {
    ft.OpenElement(TipoCessionePrestazione::tag().c_str());
    push_text(ft, tcp);
    ft.CloseElement();
  }
}

}  // namespace dettagli

bool TipoCessionePrestazione::valido(const std::string &id)
{
  return dettagli::tipo_cessione.find(id) != dettagli::tipo_cessione.end();
}

TipoCessionePrestazione::TipoCessionePrestazione(const std::string &c)
  : codice_(c)
{
  if (!valido(codice_))
    throw eccezione::tipo_cessione_cod_scorretto(tag() + ": codice scorretto");

  assert(codice_.empty() || codice_.length() == 2);
}

TipoCessionePrestazione &TipoCessionePrestazione::operator=(
  const std::string &c)
{
  *this = TipoCessionePrestazione(c);
  return *this;
}

}  // namespace felpa
