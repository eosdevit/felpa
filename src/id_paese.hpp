/**
 *  \file
 *  \remark This file is part of FELPA.
 *
 *  \copyright Copyright (C) 2015-2024 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#if !defined(FELPA_ID_PAESE_HPP)
#define      FELPA_ID_PAESE_HPP

#include "nazione_type.hpp"

namespace felpa
{

///
/// Serve ai fini fiscali per identificare, insieme al campo IdCodice, il
/// soggetto che ha effettuato la cessione del bene o la prestazione del
/// servizio oggetto di fatturazione.
///
/// Contiene, secondo lo standard ISO 3166-1 alpha-2 code, il codice della
/// nazione che ha attributito l'identificativo fiscale al soggetto.
///
/// \warning OBBLIGATORIETA': SI, sempre.
///
class IdPaese : public NazioneType
{
public:
  explicit IdPaese(const std::string &s = "IT") : NazioneType("IdPaese", s) {}

  IdPaese &operator=(const std::string &s)
  {
    assign(s);
    return *this;
  }

  [[nodiscard]] bool operator==(const NazioneType &rhs) const
  { return NazioneType::operator==(rhs); }
};  // class IdPaese

}  // namespace felpa

#endif  // include guard
