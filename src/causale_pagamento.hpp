/**
 *  \file
 *  \remark This file is part of FELPA.
 *
 *  \copyright Copyright (C) 2018 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#if !defined(FELPA_CAUSALE_PAGAMENTO_HPP)
#define      FELPA_CAUSALE_PAGAMENTO_HPP

#include <string>

namespace felpa
{

///
/// Codice della causale del pagamento.
///
/// Il codice corrisponde a quello utilizzato per la compilazione del modello
/// 770S.
///
class CausalePagamento
{
public:
  explicit CausalePagamento(const std::string & = "");

  operator std::string () const { return codice_; }

  CausalePagamento &operator=(const std::string &);

  bool empty() const;

  static const std::string &tag();

private:
  // Funzioni private di supporto.
  bool valido(const std::string &);

  // Data member privati.
  std::string codice_;
};  // class CausalePagamento

}  // namespace felpa

#endif  // include guard
