/**
 *  \file
 *  \remark This file is part of FELPA.
 *
 *  \copyright Copyright (C) 2015 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#include <cassert>

#include "cessionario_committente.hpp"
#include "fattura_elettronica_eccezioni.hpp"
#include "fattura_elettronica_dettagli.hpp"

namespace felpa
{

const std::string &CessionarioCommittente::tag()
{
  static const std::string TAG("CessionarioCommittente");
  return TAG;
}

CessionarioCommittente::CessionarioCommittente() : dati_anagrafici(), sede()
{
}

bool CessionarioCommittente::empty() const
{
  return *this == CessionarioCommittente();
}

bool operator==(const CessionarioCommittente &lhs,
                const CessionarioCommittente &rhs)
{
  return lhs.dati_anagrafici == rhs.dati_anagrafici
         && lhs.sede == rhs.sede;
}

namespace dettagli
{

bool carica(tinyxml2::XMLConstHandle &ft, CessionarioCommittente *cc)
{
  tinyxml2::XMLConstHandle h(ft.FirstChildElement(cc->tag().c_str()));
  if (!h.ToElement())
    return false;

  const bool da(carica(h, &cc->dati_anagrafici));
  const bool se(carica(h, &cc->sede));

  return da && se;
}

void registra(tinyxml2::XMLPrinter &ft, const CessionarioCommittente &cc)
{
  ft.OpenElement(cc.tag().c_str());

  registra(ft, cc.dati_anagrafici);
  registra(ft, cc.sede);

  ft.CloseElement();
}

}  // namespace dettagli

}  // namespace felpa
