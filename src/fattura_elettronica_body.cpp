/**
 *  \file
 *  \remark This file is part of FELPA.
 *
 *  \copyright Copyright (C) 2015-2024 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#include <algorithm>

#include "fattura_elettronica_body.hpp"
#include "fattura_elettronica_dettagli.hpp"
#include "utility.hpp"

namespace felpa
{

FatturaElettronicaBody::FatturaElettronicaBody(EsigibilitaIva ei)
  : dati_beni_servizi(ei)
{
}

bool FatturaElettronicaBody::empty() const
{
  return dati_generali.empty() || dati_beni_servizi.empty();
}

bool operator==(const FatturaElettronicaBody &lhs,
                const FatturaElettronicaBody &rhs)
{
  return lhs.dati_generali == rhs.dati_generali
         && lhs.dati_beni_servizi == rhs.dati_beni_servizi
         && lhs.dati_pagamento == rhs.dati_pagamento
         && lhs.allegati == rhs.allegati;
}

const std::string &FatturaElettronicaBody::tag()
{
  static const std::string TAG("FatturaElettronicaBody");
  return TAG;
}

namespace dettagli
{

bool carica(tinyxml2::XMLConstHandle &ft, FatturaElettronicaBody *b)
{
  auto h(ft.FirstChildElement(b->tag().c_str()));
  if (!h.ToElement())
    return false;

  const bool dg(carica(h, &b->dati_generali));
  const bool db(carica(h, &b->dati_beni_servizi));
  const bool dp(carica(h, &b->dati_pagamento));

  const std::string a_tag(Allegato::tag());
  b->allegati.clear();
  for (auto *he(h.FirstChildElement(a_tag.c_str()).ToElement());
       he;
       he = he->NextSiblingElement(a_tag.c_str()))
  {
    tinyxml2::XMLConstHandle heh(he);
    if (Allegato tmp; carica(heh, &tmp))
      b->allegati.push_back(tmp);
  }

  return dg && db && dp;
}

void registra(tinyxml2::XMLPrinter &ft, const FatturaElettronicaBody &b)
{
  ft.OpenElement(b.tag().c_str());

  const FatturaElettronicaBody bc(completa(b));

  registra(ft, bc.dati_generali);
  registra(ft, bc.dati_beni_servizi);
  registra(ft, bc.dati_pagamento);

  for (const auto &a : bc.allegati)
    registra(ft, a);

  ft.CloseElement();
}

}  // namespace dettagli

FatturaElettronicaBody completa(FatturaElettronicaBody feb)
{
  feb.dati_beni_servizi =
    completa(feb.dati_beni_servizi,
             feb.dati_beni_servizi.default_esigibilita_iva);

  ImportoTotaleDocumento &totale(
    feb.dati_generali.dati_generali_documento.importo_totale_documento);

  if (totale.empty())
  {
    totale = "0.00";
    for (const auto &j : feb.dati_beni_servizi.dati_riepilogo)
      totale = totale.to_double() +
               j.imponibile_importo.to_double() +
               j.imposta.to_double();
  }

  // Normalizzazione in caso si `signed zero`. Questo accorgimento evita anche
  // che venga generata, sporadicamente, una nota di credito con importo nullo
  // anziche' una fattura.
  if (zero(totale.to_double()))
    totale = "0.00";

  // Il calcolo automatico assume una ritenuta applicata al 100%
  // dell'imponibile (la situazione piu' frequente). In generale e' pero'
  // possibile che la ritenuta sia applicata ad una percentuale dell'imponibile
  // totale.
  auto &dr(feb.dati_generali.dati_generali_documento.dati_ritenuta);
  if (auto ir = std::find_if(dr.begin(), dr.end(),
                             [](const auto &r)
                             {
                               return !r.empty() && r.importo_ritenuta.empty();
                             });
      ir != dr.end())
  {
    auto &dati_ritenuta(*ir);

    double imponibile(0.0);
    for (const auto &j : feb.dati_beni_servizi.dettaglio_linee)
      if (j.ritenuta)
        imponibile += j.prezzo_totale.to_double();

    dati_ritenuta.importo_ritenuta =
      imponibile
      * (dati_ritenuta.aliquota_ritenuta.to_double() / 100.0);
  }

  const auto &tipo(feb.dati_generali.dati_generali_documento.tipo_documento);

  if (totale.to_double() < 0.0
      && (tipo == TipoDocumento("TD01") || tipo == TipoDocumento("TD02")
          || tipo == TipoDocumento("TD03")))
    feb.dati_generali.dati_generali_documento.tipo_documento = "TD04";
  else if (!feb.dati_generali.dati_ddt.empty())
    feb.dati_generali.dati_generali_documento.tipo_documento = "TD24";

  return feb;
}

}  // namespace felpa
