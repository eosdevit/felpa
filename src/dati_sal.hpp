/**
 *  \file
 *  \remark This file is part of FELPA.
 *
 *  \copyright Copyright (C) 2024 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#if !defined(FELPA_DATI_SAL_HPP)
#define      FELPA_DATI_SAL_HPP

#include <string>

namespace felpa
{

///
/// Serve per identificare la fase del lavoro svolto nell’ambito dello stato di
/// avanzamento lavori.
///
class DatiSAL
{
public:
  DatiSAL() = default;

  [[nodiscard]] bool empty() const;

  void clear();

  [[nodiscard]] static const std::string &tag();

  // ********* Data member pubblici *********
  /// Contiene il numero progressivo della fase di avanzamento lavori che si
  /// sta fatturando.
  int riferimento_fase {0};
};  // class DatiSQL

[[nodiscard]] bool operator==(const DatiSAL &, const DatiSAL &);

}  // namespace felpa

#endif  // include guard
