/**
 *  \file
 *  \remark This file is part of FELPA.
 *
 *  \copyright Copyright (C) 2015-2024 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#include <algorithm>
#include <cassert>

#include "dati_beni_servizi.hpp"
#include "fattura_elettronica_dettagli.hpp"
#include "fattura_elettronica_eccezioni.hpp"
#include "utility.hpp"

namespace felpa
{

DatiBeniServizi::DatiBeniServizi(EsigibilitaIva ei)
  : default_esigibilita_iva(ei), dettaglio_linee(), dati_riepilogo(),
    riferimenti_normativi_()
{
}

bool DatiBeniServizi::empty() const
{
  return dettaglio_linee.empty();
}

void DatiBeniServizi::aggiungi_riferimento_normativo(
  const Natura &n, const std::string &rif)
{
  if (!n.empty() && !rif.empty())
    riferimenti_normativi_[std::string(n)] = rif;
}

///
/// \param[in] n codice esenzione natura iva
/// \return      riferimento normativo per l'esenzione iva
///
/// \warning
/// La libreria FELPA assume che, nell'ambito della stessa fattura, per ogni
/// codice `n` esista un unico riferimento normativo. In generale, seppur
/// estremamente raro, questo non e' vero.
///
std::string DatiBeniServizi::riferimento_normativo(const Natura &n) const
{
  for (const auto &dr : dati_riepilogo)
    if (zero(dr.aliquota_iva.to_double()) && dr.natura == n)
      return dr.riferimento_normativo;

  return "";
}

bool operator==(const DatiBeniServizi &lhs, const DatiBeniServizi &rhs)
{
  return lhs.dettaglio_linee == rhs.dettaglio_linee
         && lhs.dati_riepilogo == rhs.dati_riepilogo;
}

const std::string &DatiBeniServizi::tag()
{
  static const std::string TAG("DatiBeniServizi");
  return TAG;
}

DatiBeniServizi completa(DatiBeniServizi dbs, EsigibilitaIva ei)
{
  using mappa = std::map<std::string, DatiRiepilogo>;
  using value_type = mappa::value_type;

  mappa totali;

  const auto make_key([](const AliquotaIva &iva, const Natura &n)
                      {
                        return std::string(iva) + std::string(n);
                      });

  for (auto &d : dbs.dettaglio_linee)
  {
    d = completa(d);

    const auto key(make_key(d.aliquota_iva, d.natura));

    auto it(totali.find(key));
    if (it == totali.end())
    {
      DatiRiepilogo dr(ei);
      dr.aliquota_iva = d.aliquota_iva;
      dr.natura       = d.natura;

      if (!d.natura.empty())
      {
        const auto rif(dbs.riferimenti_normativi_.find(d.natura));
        if (rif != dbs.riferimenti_normativi_.end())
          dr.riferimento_normativo = rif->second;
      }

      it = totali.insert(value_type(key, dr)).first;
    }

    if (it->second.imponibile_importo.empty())
      it->second.imponibile_importo = "0.00";

    it->second.imponibile_importo = it->second.imponibile_importo.to_double()
                                    + d.prezzo_totale.to_double();

    // Non bisogna calcolare l'IVA totale effettuando la somma dei singoli
    // importi iva:
    //
    // it->second.imposta =
    //   it->second.imposta.to_double()
    //   + d.prezzo_totale.to_double() * d.aliquota_iva.to_double() / 100.0;
    //
    // L'IVA verra' calcolata in seguito a partire dalla somma degli
    // imponibili.
  }

  for (const auto &j : totali)
  {
    auto i(std::find_if(dbs.dati_riepilogo.begin(), dbs.dati_riepilogo.end(),
                        [&](const DatiRiepilogo &d)
                        {
                          auto key(make_key(d.aliquota_iva, d.natura));
                          return key == j.first;
                        }));
    if (i == dbs.dati_riepilogo.end())  // aliquota assente nel riepilogo
    {
      dbs.dati_riepilogo.push_back(j.second);
      i = std::prev(dbs.dati_riepilogo.end());
    }

    assert(std::string(i->aliquota_iva)==std::string(j.second.aliquota_iva));
    assert(std::string(i->natura) == std::string(j.second.natura));
    i->imponibile_importo = j.second.imponibile_importo;
    i->imposta = i->imponibile_importo.to_double()
                 * j.second.aliquota_iva.to_double() / 100.0;
  }

  return dbs;
}

namespace dettagli
{

bool carica(tinyxml2::XMLConstHandle &ft, DatiBeniServizi *dbs)
{
  auto h(ft.FirstChildElement(dbs->tag().c_str()));
  if (!h.ToElement())
    return false;

  bool dl(true);
  const std::string dl_tag(DettaglioLinee::tag());
  dbs->dettaglio_linee.clear();
  for (auto *he(h.FirstChildElement(dl_tag.c_str()).ToElement());
       he;
       he = he->NextSiblingElement(dl_tag.c_str()))
  {
    tinyxml2::XMLConstHandle heh(he);
    if (DettaglioLinee tmp; carica(heh, &tmp))
      dbs->dettaglio_linee.push_back(tmp);
    else
      dl = false;
  }
  dl = dl && dbs->dettaglio_linee.size();

  bool dr(true);
  const EsigibilitaIva dummy_ei(EsigibilitaIva("I"));  // verra' sovrascritta
  const std::string dr_tag(DatiRiepilogo::tag());
  dbs->dati_riepilogo.clear();
  for (auto *he(h.FirstChildElement(dr_tag.c_str()).ToElement());
       he;
       he = he->NextSiblingElement(dr_tag.c_str()))
  {
    tinyxml2::XMLConstHandle heh(he);
    if (DatiRiepilogo tmp(dummy_ei); carica(heh, &tmp))
      dbs->dati_riepilogo.push_back(tmp);
    else
      dr = false;
  }
  dr = dr && dbs->dati_riepilogo.size();

  return dl && dr;
}

void registra(tinyxml2::XMLPrinter &ft, const DatiBeniServizi &dbs)
{
  ft.OpenElement(dbs.tag().c_str());

  if (dbs.dettaglio_linee.empty())
    throw eccezione::dett_linee_vuoto(dbs.tag()
                                      + ": fattura senza linee dettaglio");

  for (unsigned i(0); i < dbs.dettaglio_linee.size(); ++i)
    registra(ft, i + 1, dbs.dettaglio_linee[i]);

  const DatiBeniServizi dbsc(completa(dbs, dbs.default_esigibilita_iva));

  for (unsigned i(0); i < dbsc.dati_riepilogo.size(); ++i)
    registra(ft, dbsc.dati_riepilogo[i]);

  ft.CloseElement();
}

}  // namespace dettagli

}  // namespace felpa
