/**
 *  \file
 *  \remark This file is part of FELPA.
 *
 *  \copyright Copyright (C) 2015-2021 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#if !defined(FELPA_CONTATTI_TRASMITTENTE_HPP)
#define      FELPA_CONTATTI_TRASMITTENTE_HPP

#include <string>

namespace felpa
{

///
/// Informazioni aggiuntive utili per contattare un soggetto.
///
class ContattiTrasmittente
{
public:
  explicit ContattiTrasmittente(const std::string & = "",
                                const std::string & = "");

  bool empty() const;

  static const std::string &tag();

  // ********* Data member pubblici *********
  std::string telefono;
  std::string email;
};  // class ContattiTrasmittente


bool operator==(const ContattiTrasmittente &, const ContattiTrasmittente &);

}  // namespace felpa

#endif  // include guard
