/**
 *  \file
 *  \remark This file is part of FELPA.
 *
 *  \copyright Copyright (C) 2015 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#if !defined(FELPA_REGIME_FISCALE_HPP)
#define      FELPA_REGIME_FISCALE_HPP

#include <string>

#include "id_trasmittente.hpp"

namespace felpa
{

///
/// Indispensabile per il calcolo della base imponibile.
///
/// Deve contenere uno dei codici previsti nella lista valori associata; il
/// codice identifica, sulla base del settore commerciale o della situazione
/// reddituale, il regime fiscale in cui opera il soggetto.
///
/// \warning OBBLIGATORIETA': SI, sempre.
///
class RegimeFiscale
{
public:
  explicit RegimeFiscale(const std::string & = "RF01");

  [[nodiscard]] operator std::string () const { return codice_; }
  RegimeFiscale &operator=(const std::string &);

  [[nodiscard]] bool operator==(const RegimeFiscale &rhs) const
  { return codice_ == rhs.codice_; }

  [[nodiscard]] static const std::string &tag();

private:
  // Funzioni private di supporto.
  [[nodiscard]] static bool valido(const std::string &);

  // Data member pubblici.
  std::string codice_;
};  // class RegimeFiscale

}  // namespace felpa

#endif  // include guard
