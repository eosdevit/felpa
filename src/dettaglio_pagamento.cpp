/**
 *  \file
 *  \remark This file is part of FELPA.
 *
 *  \copyright Copyright (C) 2015-2024 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#include <cassert>

#include "dettaglio_pagamento.hpp"
#include "fattura_elettronica_eccezioni.hpp"
#include "fattura_elettronica_dettagli.hpp"

#include "utility.hpp"

namespace felpa
{

const std::string &DettaglioPagamento::tag()
{
  static const std::string TAG("DettaglioPagamento");
  return TAG;
}

bool DettaglioPagamento::empty() const
{
  return modalita_pagamento.empty() || importo_pagamento.empty();
}

bool operator==(const DettaglioPagamento &lhs, const DettaglioPagamento &rhs)
{
  return lhs.beneficiario == rhs.beneficiario
         && std::string(lhs.modalita_pagamento)
            == std::string(rhs.modalita_pagamento)
         && lhs.data_scadenza_pagamento == rhs.data_scadenza_pagamento
         && std::string(lhs.importo_pagamento)
            == std::string(rhs.importo_pagamento)
         && lhs.istituto_finanziario == rhs.istituto_finanziario
         && lhs.iban == rhs.iban
         && lhs.codice_pagamento == rhs.codice_pagamento;
}

namespace dettagli
{

constexpr std::size_t BENEFICIARIO_SIZE         = 200;
constexpr std::size_t ISTITUTO_FINANZIARIO_SIZE =  80;

constexpr char BENEFICIARIO_TAG[] = "Beneficiario";
constexpr char CODICE_PAGAMENTO_TAG[] = "CodicePagamento";
constexpr char IBAN_TAG[] = "IBAN";
constexpr char ISTITUTO_FINANZIARIO_TAG[] = "IstitutoFinanziario";

bool carica(tinyxml2::XMLConstHandle &ft, DettaglioPagamento *dp)
{
  tinyxml2::XMLConstHandle h(ft.FirstChildElement(dp->tag().c_str()));
  if (!h.ToElement())
    h = ft;

  if (auto *he = h.FirstChildElement(BENEFICIARIO_TAG).ToElement())
    dp->beneficiario = coalesce(he->GetText()).substr(0, BENEFICIARIO_SIZE);

  const bool mp(carica(h, &dp->modalita_pagamento));

  carica(h, &dp->data_scadenza_pagamento);

  const bool ip(carica(h, &dp->importo_pagamento));

  if (auto *he = h.FirstChildElement(ISTITUTO_FINANZIARIO_TAG).ToElement())
    dp->istituto_finanziario =
      coalesce(he->GetText()).substr(0, ISTITUTO_FINANZIARIO_SIZE);

  if (auto *he = h.FirstChildElement(IBAN_TAG).ToElement())
    dp->iban = coalesce(he->GetText());

  if (auto *he = h.FirstChildElement(CODICE_PAGAMENTO_TAG).ToElement())
    dp->codice_pagamento = coalesce(he->GetText());

  return mp && ip;
}

void registra(tinyxml2::XMLPrinter &ft, const DettaglioPagamento &d)
{
  ft.OpenElement(DettaglioPagamento::tag().c_str());

  if (!d.beneficiario.empty())
  {
    ft.OpenElement(BENEFICIARIO_TAG);
    push_text(ft, d.beneficiario.substr(0, BENEFICIARIO_SIZE));
    ft.CloseElement();
  }

  if (d.modalita_pagamento.empty())
    throw eccezione::dett_pagamento_modalita_mancante(
      DettaglioPagamento::tag() + ": modalita' pagamento mancante");
  registra(ft, d.modalita_pagamento);

  if (!d.data_scadenza_pagamento.empty())
    registra(ft, d.data_scadenza_pagamento);

  if (d.importo_pagamento.empty())
    throw eccezione::dett_pagamento_importo_mancante(DettaglioPagamento::tag()
                                                     + ": importo mancante");
  registra(ft, d.importo_pagamento);

  if (!d.istituto_finanziario.empty())
  {
    ft.OpenElement(ISTITUTO_FINANZIARIO_TAG);
    push_text(ft, d.istituto_finanziario.substr(0, ISTITUTO_FINANZIARIO_SIZE));
    ft.CloseElement();
  }

  if (!d.iban.empty() && richiede_iban(d.modalita_pagamento))
  {
    ft.OpenElement(IBAN_TAG);
    push_text(ft, d.iban);
    ft.CloseElement();
  }

  if (!d.codice_pagamento.empty())
  {
    ft.OpenElement(CODICE_PAGAMENTO_TAG);
    push_text(ft, d.codice_pagamento);
    ft.CloseElement();
  }

  ft.CloseElement();
}

}  // namespace dettagli

}  // namespace felpa
