/**
 *  \file
 *  \remark This file is part of FELPA.
 *
 *  \copyright Copyright (C) 2015 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#include "fattura_elettronica_header.hpp"
#include "fattura_elettronica_dettagli.hpp"
#include "fattura_elettronica_eccezioni.hpp"

namespace felpa
{

const std::string &FatturaElettronicaHeader::tag()
{
  static const std::string TAG("FatturaElettronicaHeader");
  return TAG;
}

FatturaElettronicaHeader::FatturaElettronicaHeader(formato_fattura formato)
  : dati_trasmissione(formato), cedente_prestatore(), cessionario_committente(),
    soggetto_emittente()
{
}

bool FatturaElettronicaHeader::empty() const
{
  return dati_trasmissione.empty()
         && cedente_prestatore.empty()
         && cessionario_committente.empty()
         && soggetto_emittente.empty();
}

bool operator==(const FatturaElettronicaHeader &lhs,
                const FatturaElettronicaHeader &rhs)
{
  return lhs.dati_trasmissione == rhs.dati_trasmissione
         && lhs.cedente_prestatore == rhs.cedente_prestatore
         && lhs.cessionario_committente == rhs.cessionario_committente
         && lhs.soggetto_emittente == rhs.soggetto_emittente;
}

namespace dettagli
{

bool carica(tinyxml2::XMLConstHandle &ft, FatturaElettronicaHeader *fh)
{
  auto h(ft.FirstChildElement(fh->tag().c_str()));
  if (!h.ToElement())
    return false;

  const bool dt(carica(h, &fh->dati_trasmissione));
  const bool cp(carica(h, &fh->cedente_prestatore));
  const bool cc(carica(h, &fh->cessionario_committente));

  carica(h, &fh->soggetto_emittente);

  return dt && cp && cc;
}

void registra(tinyxml2::XMLPrinter &ft, const FatturaElettronicaHeader &h)
{
  ft.OpenElement(h.tag().c_str());

  if (h.dati_trasmissione.codice_destinatario.estero()
      && h.cessionario_committente.dati_anagrafici.id_fiscale_iva.id_paese
         .italia())
  {
    throw eccezione::cod_destinatario_estero_per_italia(
      h.tag()
      +  ": codice destinatario XXXXXXX utilizzato per cliente italiano");
  }

  // Verifica 00476.
  const auto &cc_paese(h.cessionario_committente.dati_anagrafici.id_fiscale_iva
                        .id_paese);
  const auto &cp_paese(h.cedente_prestatore.dati_anagrafici.id_fiscale_iva
                        .id_paese);

  if (!cc_paese.italia() && !cp_paese.italia())
    throw eccezione::tutti_esteri(
      h.tag()
      + ": non e' ammessa una fattura riportante contemporaneamente nel paese "
        "dell'identificativo fiscale del cedente/prestatore e del "
        "cessionario/committente un valore diverso da IT");

  registra(ft, h.dati_trasmissione);
  registra(ft, h.cedente_prestatore);
  registra(ft, h.cessionario_committente);
  registra(ft, h.soggetto_emittente);

  ft.CloseElement();
}

}  // namespace dettagli

}  // namespace felpa
