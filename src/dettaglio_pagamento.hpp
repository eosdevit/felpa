/*
 *  \remark This file is part of FELPA.
 *
 *  \copyright Copyright (C) 2015-2024 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#if !defined(FELPA_DETTAGLIO_PAGAMENTO_HPP)
#define      FELPA_DETTAGLIO_PAGAMENTO_HPP

#include "modalita_pagamento.hpp"
#include "data_scadenza_pagamento.hpp"
#include "importo_pagamento.hpp"

namespace felpa
{

class DettaglioPagamento
{
public:
  DettaglioPagamento() = default;

  [[nodiscard]] bool empty() const;

  [[nodiscard]] static const std::string &tag();

  // ********* Data member pubblici *********
  /// Estremi anagrafici del beneficiario del pagamento (utilizzabile se si
  /// intende indicare un beneficiario diverso dal cedente/prestatore).
  std::string beneficiario {};

  ModalitaPagamento modalita_pagamento {};
  DataScadenzaPagamento data_scadenza_pagamento {};
  ImportoPagamento importo_pagamento {};

  /// Serve ad identificare l'istituto di credito destinatario del pagamento,
  /// (qualora la modalita' di pagamento ne presupponga l'indicazione).
  std::string istituto_finanziario {};

  /// Serve ad identificare l'International Bank Account Number, standard
  /// internazionale utilizzato riferirsi alle utenze bancarie.
  ///
  /// \warning
  /// OBBLIGATORIETA': solo se la modalita' di pagamento ne richiede
  /// l'indicazione.
  std::string iban {};

  // Codice da utilizzare per la riconciliazione degli incassi da parte del
  // cedente/prestatore.
  std::string codice_pagamento {};
};  // class DettaglioPagamento

[[nodiscard]] bool operator==(const DettaglioPagamento &,
                              const DettaglioPagamento &);

}  // namespace felpa

#endif  // include guard
