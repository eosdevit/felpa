/**
 *  \file
 *  \remark This file is part of FELPA.
 *
 *  \copyright Copyright (C) 2024 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#include <cassert>

#include "dati_sal.hpp"
#include "fattura_elettronica_dettagli.hpp"
#include "fattura_elettronica_eccezioni.hpp"
#include "utility.hpp"

namespace felpa
{

bool DatiSAL::empty() const
{
  return !riferimento_fase;
}

void DatiSAL::clear()
{
  *this = DatiSAL();
}

bool operator==(const DatiSAL &lhs, const DatiSAL &rhs)
{
  return lhs.riferimento_fase == rhs.riferimento_fase;
}

const std::string &DatiSAL::tag()
{
  static const std::string TAG("DatiSAL");
  return TAG;
}

namespace dettagli
{

constexpr char RIFERIMENTO_FASE_TAG[] = "RiferimentoFase";

bool carica(tinyxml2::XMLConstHandle &ft, DatiSAL *sal)
{
  tinyxml2::XMLConstHandle h(ft.FirstChildElement(sal->tag().c_str()));
  if (!h.ToElement())
    h = ft;

  if (auto *he = h.FirstChildElement(RIFERIMENTO_FASE_TAG).ToElement())
    sal->riferimento_fase = std::stoi(he->GetText());
  else
    sal->riferimento_fase = 0;

  return !sal->empty();
}

void registra(tinyxml2::XMLPrinter &ft, const DatiSAL &sal)
{
  if (sal.empty())
    return;

  ft.OpenElement(sal.tag().c_str());

  if (sal.riferimento_fase <= 0 || sal.riferimento_fase > 999)
    throw eccezione::riferimento_sal_errato(sal.tag());

  ft.OpenElement(RIFERIMENTO_FASE_TAG);
  push_text(ft, std::to_string(sal.riferimento_fase));
  ft.CloseElement();

  ft.CloseElement();
}

}  // namespace dettagli

}  // namespace felpa
