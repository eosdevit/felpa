/**
 *  \file
 *  \remark This file is part of libCommercio.
 *
 *  \copyright Copyright (C) 2023 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#include <limits>
#include <stdexcept>
#include <string>

#include "utils.hpp"

#if !defined(LIB_COMMERCIO_CODICE_FISCALE_HPP)
#define      LIB_COMMERCIO_CODICE_FISCALE_HPP

namespace lib_commercio
{

/*********************************************************************
 * CODICE FISCALE
 *********************************************************************/
///
/// Costituisce lo strumento di identificazione delle persone fisiche.
///
/// Il codice fiscale delle persone fisiche e' costituito da un'espressione
/// alfanumerica di sedici caratteri.
///
/// I primi quindici caratteri sono indicativi dei dati anagrafici di ciascun
/// soggetto secondo l'ordine seguente:
/// - tre caratteri alfabetici per il cognome;
/// - tre caratteri alfabetici per il nome;
/// - due caratteri numerici per l'anno di nascita;
/// - un carattere alfabetico per il mese di nascita;
/// - due caratteri numerici per il giorno di nascita ed il sesso;
/// - quattro caratteri, di cui uno alfabetico e tre numerici per il comune
///   italiano o per lo Stato estero di nascita;
/// - il sedicesimo carattere, alfabetico, ha funzione di controllo.
///
class codice_fiscale
{
public:
  codice_fiscale(const std::string & = "");

  struct componenti
  {
    std::string cognome {};
    std::string nome    {};
    unsigned anno       {std::numeric_limits<decltype(anno)>::max()};
    unsigned mese       {std::numeric_limits<decltype(mese)>::max()};
    unsigned giorno     {std::numeric_limits<decltype(giorno)>::max()};
    char sesso          {'?'};
    std::string comune  {};
    char controllo      {'?'};
  };

  operator std::string() const { return cf_; }
  [[nodiscard]] componenti scomponi() const { return scomponi(cf_); }

  [[nodiscard]] static char char_controllo(const std::string &);
  [[nodiscard]] static componenti scomponi(const std::string &);
  [[nodiscard]] static std::string sigla_cognome(const std::string &);
  [[nodiscard]] static std::string sigla_nome(const std::string &);
  [[nodiscard]] static bool verifica(const std::string &);

#if defined(__BORLANDC__)
  codice_fiscale(const String &);
  [[nodiscard]] static bool verifica(const String &);
#endif

private:
  std::string cf_;
}; // class codice_fiscale

[[nodiscard]] bool operator==(const codice_fiscale &, const codice_fiscale &);
[[nodiscard]] bool operator<(const codice_fiscale &, const codice_fiscale &);


/*********************************************************************
 * DETTAGLI DI IMPLEMENTAZIONE
 *********************************************************************/
namespace details
{

[[nodiscard]] inline std::pair<std::string, std::string> consonanti_vocali(
  const std::string &s)
{
  std::pair<std::string, std::string> ret;

  // Estrazione vocali e consonanti.
  for (auto c : s)
    if (std::isalpha(c))
    {
      c = std::toupper(c);

      if (c == 'A' || c == 'E' || c == 'I' || c == 'O' || c == 'U')
        ret.second.push_back(c);
      else  // consonante
        ret.first.push_back(c);
    }

  return ret;
}

}  // namespace details


/*********************************************************************
 * IMPLEMENTAZIONE
 *********************************************************************/
inline codice_fiscale::codice_fiscale(const std::string &cf)
  : cf_(erase_spaces(cf))
{
  if (!verifica(cf))
    throw std::invalid_argument("Codice fiscale scorretto");
}

///
/// Calcola il carattere di controllo del codice fiscale.
///
/// \param[in] cf contiene i primi 15 caratteri di un codice fiscale
/// \return       carattere di controllo o '?' in caso di errore
///
inline char codice_fiscale::char_controllo(const std::string &cf)
{
  if (cf.length() < 15)
    return '?';

  unsigned s(0);
  for (std::size_t i(1); i <= 13; i += 2)
    s += chr_to_ui(cf[i]);

  static const unsigned setdisp[] =
  {
     1, 0, 5, 7, 9, 13, 15, 17, 19, 21, 2, 4, 18, 20, 11, 3, 6, 8, 12, 14, 16,
     10, 22, 25, 24, 23
  };

  for (std::size_t i(0); i <= 14; i += 2)
    s += setdisp[chr_to_ui(cf[i])];

  return 'A' + s % 26;
}

///
/// Estrae le informazioni contenute nel codice fiscale.
///
/// \param[in] cf un codice fiscale
/// \return       componenti del codice fiscale (data di nascita, sesso...)
///
/// \note
/// Nel caso il codice fiscale sia troppo corto, viene restituita una struttura
/// vuota.
///
inline codice_fiscale::componenti codice_fiscale::scomponi(
  const std::string &cf)
{
  static const std::string mesi = {"ABCDEHLMPRST"};

  componenti ret;

  if (cf.length() < 15)
    return ret;

  // Estrazione delle sigle associate al cognome ed al nome.
  ret.cognome = cf.substr(0, 3);
  ret.nome = cf.substr(3, 3);

  // Estrazione dell'anno.
  ret.anno = chr_to_ui(cf[6]) * 10 + chr_to_ui(cf[7]);

  // Estrazione del mese.
  if (const auto i = mesi.find(std::toupper(cf[8])); i != std::string::npos)
    ret.mese = i + 1;

  // Estrazione del giorno / sesso.
  const auto g(chr_to_ui(cf[9]) * 10 + chr_to_ui(cf[10]));
  if (g <= 31)
  {
    ret.giorno = g;
    ret.sesso = 'M';
  }
  else if (41 <= g && g <= 71)
  {
    ret.giorno = g - 40;
    ret.sesso = 'F';
  }

  // Estrazione codice comune
  ret.comune = cf.substr(11, 4);

  ret.controllo = char_controllo(cf);

  return ret;
}

///
/// Verifica la correttezza formale di un codice fiscale.
///
/// \param[in] cf codice fiscale
/// \return       `true` se il codice fiscale e' corretto
///
/// La funzione si assicura che che lettere/cifre siano al posto giusto ed in
/// numero sufficiente e che il carattere di controllo sia valido.
///
inline bool codice_fiscale::verifica(const std::string &cf)
{
  static const unsigned giorni[12] =
  {31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

  if (cf.length() != 16)
    return false;

  // Controllo del pattern:
  // [A-Z]{6}[0-9LMNPQRSTUV]{2}[ABCDEHLMPRST]{1}[0-9LMNPQRSTUV]{2}[A-Z]{1}[0-9LMNPQRSTUV]{3}[A-Z]{1}
  // che tiene conto delle sostituzioni valide delle cifre con lettere nel caso
  // di omocodie.
  if (!std::isalpha(cf[0]) || !std::isalpha(cf[1]) || !std::isalpha(cf[2])
      || !std::isalpha(cf[3]) || !std::isalpha(cf[4]) || !std::isalpha(cf[5])
      || !std::isalpha(cf[11]) || !std::isalpha(cf[15]))
    return false;

  static const std::string tipo1("ABCDEHLMPRST");
  if (tipo1.find(cf[8]) == std::string::npos)
    return false;

  static const std::string tipo2("0123456789LMNPQRSTUV");
  if (tipo2.find(cf[6]) == std::string::npos
      || tipo2.find(cf[7]) == std::string::npos
      || tipo2.find(cf[9]) == std::string::npos
      || tipo2.find(cf[10]) == std::string::npos
      || tipo2.find(cf[12]) == std::string::npos
      || tipo2.find(cf[13]) == std::string::npos
      || tipo2.find(cf[14]) == std::string::npos)
    return false;

  if (char_controllo(cf) != cf.back())
    return false;

  componenti ccf(scomponi(cf));

  return 1 <= ccf.mese && ccf.mese <= 12
         && ccf.giorno <= giorni[ccf.mese - 1]
         && ccf.sesso != '?';
}

///
/// Calcola, partendo dal cognome, i tre carratteri riassuntivi.
///
/// \param[in] cognome cognome per esteso
/// \return            sigla identificativa del cognome
///
/// Se l'input e' nullo restituisce la stringa "XXX".
///
inline std::string codice_fiscale::sigla_cognome(const std::string &cognome)
{
  std::string ret("XXX");

  if (!cognome.empty())
  {
    // Estrazione vocali e consonanti.
    auto [con, voc] = details::consonanti_vocali(cognome);

    const auto max(ret.length());

    // Si inseriscono fino ad un massimo di tre consonanti.
    auto last(std::copy_n(con.begin(), std::min(max, con.length()),
                          ret.begin()));

    // Se c'e' spazio residuo, si inseriscono le vocali disponibili sino ad un
    // massimo di tre.
    const auto residuo(static_cast<decltype(max)>(std::distance(last,
                                                                ret.end())));
    std::copy_n(voc.begin(),
                std::min({max, voc.length(), residuo}),
                last);
  }

  return ret;
}

///
/// Calcola, partendo dal nome, i tre carratteri riassuntivi.
///
/// \param[in] nome nome per esteso
/// \return         sigla identificativa del nome
///
/// Se l'input e' nullo restituisce la stringa "XXX".
///
inline std::string codice_fiscale::sigla_nome(const std::string &nome)
{
  std::string ret("XXX");

  if (nome.empty())
    return ret;

  // Estrazione vocali e consonanti.
  auto [con, voc] = details::consonanti_vocali(nome);

  if (con.length() > 3) // caso particolare: nome con piu' di tre consonanti

  {
    ret[0] = con[0];
    ret[1] = con[2];
    ret[2] = con[3];
  }
  else  // caso generale
  {
    const auto max(ret.length());

    // Si inseriscono fino ad un massimo di tre consonanti.
    auto last(std::copy_n(con.begin(), std::min(max, con.length()),
                          ret.begin()));

    // Se c'e' spazio residuo, si inseriscono le vocali disponibili sino ad un
    // massimo di tre.
    const auto residuo(static_cast<decltype(max)>(std::distance(last,
                                                                ret.end())));
    std::copy_n(voc.begin(),
                std::min({max, voc.length(), residuo}),
                last);
  }

  return ret;
}

inline bool operator==(const codice_fiscale &lhs, const codice_fiscale &rhs)
{
  return static_cast<std::string>(lhs) == static_cast<std::string>(rhs);
}

inline bool operator<(const codice_fiscale &lhs, const codice_fiscale &rhs)
{
  return static_cast<std::string>(lhs) < static_cast<std::string>(rhs);
}

#if defined(__BORLANDC__)
inline codice_fiscale::codice_fiscale(const String &cf)
  : codice_fiscale(details::to_stdstring(cf))
{
}

inline bool codice_fiscale::verifica(const String &cf)
{
  return verifica(details::to_stdstring(cf));
}
#endif

}  // namespace lib_commercio

#endif  // include guard
