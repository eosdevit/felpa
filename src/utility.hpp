/**
 *  \file
 *  \remark This file is part of FELPA.
 *
 *  \copyright Copyright (C) 2015-2024 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#if !defined(FELPA_UTILITY_HPP)
#define      FELPA_UTILITY_HPP

#include <cstdlib>
#include <fstream>
#include <string>
#include <vector>

namespace felpa
{

class serie_e_numero
{
public:
  explicit serie_e_numero(const std::string &);

  [[nodiscard]] const std::string &serie() const { return serie_; }
  [[nodiscard]] const std::string &numero() const { return numero_; }

private:
  [[nodiscard]] static std::string rimuovi_separatori(std::string);

  std::string serie_ {};
  std::string numero_ {};
};

[[nodiscard]] std::string base_conversion(std::int64_t, int);
[[nodiscard]] std::vector<unsigned char> base64_decode(const std::string &);
[[nodiscard]] std::string base64_encode(const std::vector<unsigned char> &);
[[nodiscard]] std::string base64_encode(const std::string &);
[[nodiscard]] std::string coalesce(const char [], const std::string & = {});
[[nodiscard]] std::string extract_filename(std::string);
int extract_p7m_content(const std::string &, const std::string &);
[[nodiscard]] bool ends_with(const std::string &, const std::string &);
[[nodiscard]] double to_float_def(const std::string &, double);
[[nodiscard]] bool zero(double);

}  // namespace felpa

#endif  // include guard
