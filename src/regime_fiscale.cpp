/**
 *  \file
 *  \remark This file is part of FELPA.
 *
 *  \copyright Copyright (C) 2015 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#include <cassert>
#include <set>

#include "regime_fiscale.hpp"
#include "fattura_elettronica_eccezioni.hpp"
#include "fattura_elettronica_dettagli.hpp"

namespace felpa
{

const std::string &RegimeFiscale::tag()
{
  static const std::string TAG("RegimeFiscale");
  return TAG;
}

namespace dettagli
{

const std::set<std::string> regime_fiscale(
{
  "RF01",  // Ordinario
  "RF02",  // Contribuenti minimi
  "RF03",  // Nuove iniziative produttive
  "RF04",  // Agricoltura ed attivita' connesse e pesca
  "RF05",  // Vendita sali e tabacchi
  "RF06",  // Commercio dei fiammiferi
  "RF07",  // Editoria
  "RF08",  // Gestione dei servizi di telefonia pubblica
  "RF09",  // Rivendita di documenti di trasporto pubblico e di sosta
  "RF10",  // Intrattenimenti, giochi ed altre attivita' di cui al DPR 640/72
  "RF11",  // Agenzie di viaggi e turismo
  "RF12",  // Agriturismo
  "RF13",  // Vendite a domicilio
  "RF14",  // Rivendita di beni usati di oggetti d'arte, d'antiquariato
  "RF15",  // Agenzie di vendite all'asta di oggetti d'arte
  "RF16",  // IVA per cassa PA
  "RF17",  // IVA per cassa art 32-bis
  "RF18",  // Altro
  "RF19",  // Forfettario
  "RF20"   // Regime transfrontaliero di franchigia IVA (direttiva UE2020/285)
});

bool carica(tinyxml2::XMLConstHandle &ft, RegimeFiscale *rf)
{
  auto *h(ft.FirstChildElement(RegimeFiscale::tag().c_str()).ToElement());
  if (!h || !h->GetText())
    return false;

  *rf = h->GetText();
  return true;
}

void registra(tinyxml2::XMLPrinter &ft, const RegimeFiscale &rf)
{
  ft.OpenElement(RegimeFiscale::tag().c_str());
  push_text(ft, rf);
  ft.CloseElement();
}

}  // namespace dettagli

bool RegimeFiscale::valido(const std::string &id)
{
  return dettagli::regime_fiscale.find(id) != dettagli::regime_fiscale.end();
}

RegimeFiscale::RegimeFiscale(const std::string &id) : codice_(id)
{
  if (!valido(codice_))
    throw eccezione::cod_regime_fiscale(tag() + ": codice non valido");

  assert(codice_.length() == 4);
}

RegimeFiscale &RegimeFiscale::operator=(const std::string &s)
{
  return *this = RegimeFiscale(s);
}

}  // namespace felpa
