/**
 *  \file
 *  \remark This file is part of FELPA.
 *
 *  \copyright Copyright (C) 2015-2020 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#if !defined(FELPA_DATA_ORA_TYPE_HPP)
#define      FELPA_DATA_ORA_TYPE_HPP

#include <string>

namespace felpa
{

///
/// Contiene una data in formato ISO 8601:2004 con precisione
/// YYYY-MM-DDTHH:MM:SS.
///
class DataOraType
{
public:
  const std::string &tag() const { return tag_; }

  operator std::string () const { return data_ora_; }

  bool empty() const { return data_ora_.empty(); }

  void assign(const std::string &);

protected:
  explicit DataOraType(const std::string &, const std::string & = "");

private:
  // Funzioni private di supporto.
  static bool valida(const std::string &);

  // Data member privati.
  std::string tag_;
  std::string data_ora_;
};  // class DataOraType

}  // namespace felpa

#endif  // include guard
