/**
 *  \file
 *  \remark This file is part of FELPA.
 *
 *  \copyright Copyright (C) 2015-2024 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#include <cassert>
#include <set>

#include "natura.hpp"
#include "fattura_elettronica_eccezioni.hpp"
#include "fattura_elettronica_dettagli.hpp"

namespace felpa
{

const std::string &Natura::tag()
{
  static const std::string TAG("Natura");
  return TAG;
}

bool Natura::reverse_charge() const
{
  return codice_.length() == 4
         && codice_[0] == 'N' && codice_[1] == '6' && codice_[2] == '.';
}

bool operator==(const Natura &lhs, const Natura &rhs)
{
  return std::string(lhs) == std::string(rhs);
}

namespace dettagli
{

const std::set<std::string> natura(
{
  "",
  "N1",    // escluse ex art. 15
//"N2",    // NON PIU' VALIDO (non soggette)
  "N2.1",  // non soggette IVA ai sensi degli art. da 7 a 7septies DPR 633/72
  "N2.2",  // non soggette - altri casi
//"N3",    // NON PIU' VALIDO (non imponibili)
  "N3.1",  // non imp. - esportazioni
  "N3.2",  // non imp. - cessioni intracomunitarie
  "N3.3",  // non imp. - cessioni verso San Marino
  "N3.4",  // non imp. - oper. assimilate alle cessioni all'esportazione
  "N3.5",  // non imp. - a seguito di dichiarazioni d'intento
  "N3.6",  // non imp. - altre oper. che non concorrono al plafond
  "N4",    // esenti
//"N5",    // regime del margine / IVA non esposta in fattura
//"N6",    // NON PIU' VALIDO (inversione contabile)
  "N6.1",  // inversione contabile - cessione di rottami / materiali recupero
  "N6.2",  // inversione contabile - cessione di oro e argento puro
  "N6.3",  // inversione contabile - subappalto nel settore edile
  "N6.4",  // inversione contabile - cessione di fabbricati
  "N6.5",  // inversione contabile - cessione di telefoni cellulari
  "N6.6",  // inversione contabile - cessione di prodotti elettronici
  "N6.7",  // inversione contabile - prestazioni comparto edile e connessi
  "N6.8",  // inversione contabile - oper. settore energetico
  "N6.9",  // inversione contabile - altri casi
  "N7"     // IVA assolta in altro stato UE (prestazioni di servizi di telecom.
           // tele-radiodiffusione ed elettronici ex art. 7-octies, comma 1,
           // lett. a, b art. 74-sexies DPR 633/72)
});

bool carica(tinyxml2::XMLConstHandle &ft, Natura *n)
{
  auto h(ft.FirstChildElement(Natura::tag().c_str()));
  if (!h.ToElement())
    return false;

  const auto *cod(h.ToElement()->GetText());
  if (!cod)
    return false;

  *n = std::string(cod);
  return true;
}

void registra(tinyxml2::XMLPrinter &ft, const Natura &n)
{
  if (!n.empty())
  {
    ft.OpenElement(Natura::tag().c_str());
    push_text(ft, n);
    ft.CloseElement();
  }
}

}  // namespace dettagli

bool Natura::valido(const std::string &id)
{
  return dettagli::natura.find(id) != dettagli::natura.end();
}

Natura::Natura(const std::string &id) : codice_(id)
{
  if (!valido(codice_))
    throw eccezione::natura_cod_scorretto(Natura::tag()
                                          + ": codice non valido");
}

Natura &Natura::operator=(const std::string &s)
{
  return *this = Natura(s);
}

}  // namespace felpa
