/**
 *  \file
 *  \remark This file is part of libCommercio.
 *
 *  \copyright Copyright (C) 2023 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#include "lib_commercio/codice_fiscale.hpp"
#include "lib_commercio/codice_sdi.hpp"
#include "lib_commercio/iban.hpp"
#include "lib_commercio/nazione.hpp"
#include "lib_commercio/partita_iva.hpp"
#include "lib_commercio/provincia.hpp"
#include "lib_commercio/regione.hpp"
#include "lib_commercio/vat.hpp"
