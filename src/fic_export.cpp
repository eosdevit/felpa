/**
 *  \file
 *  \remark This file is part of FELPA.
 *
 *  \copyright Copyright (C) 2024 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#include <cassert>

#include "fic_export.hpp"
#include "fattura_elettronica.hpp"

#include "third_party/json/json.hpp"

namespace felpa::fic
{

using json = nlohmann::json;

namespace
{

[[nodiscard]] json::object_t fic_entity(const FatturaElettronica &f)
{
  std::string name;
  std::string vat_number;
  std::string tax_code;
  std::string address_street;
  std::string address_postal_code;
  std::string address_city;
  std::string address_province;
  std::string country_iso;

  if (f.acquisto())  // controparte CedentePrestatore
  {
    name =
      f.header.cedente_prestatore.dati_anagrafici.anagrafica.denominazione;
    vat_number =
      f.header.cedente_prestatore.dati_anagrafici.id_fiscale_iva.id_codice;
    tax_code =
      f.header.cedente_prestatore.dati_anagrafici.codice_fiscale;
    address_street =
      f.header.cedente_prestatore.sede.indirizzo;
    address_postal_code =
      f.header.cedente_prestatore.sede.cap;
    address_city = f.header.cedente_prestatore.sede.comune;
    address_province = f.header.cedente_prestatore.sede.provincia;
    country_iso = f.header.cedente_prestatore.sede.nazione;
  }
  else  // controparte CessionarioCommittente
  {
    name =
      f.header.cessionario_committente.dati_anagrafici.anagrafica.denominazione;
    vat_number =
      f.header.cessionario_committente.dati_anagrafici.id_fiscale_iva.id_codice;
    tax_code =
      f.header.cessionario_committente.dati_anagrafici.codice_fiscale;
    address_street =
      f.header.cessionario_committente.sede.indirizzo;
    address_postal_code =
      f.header.cessionario_committente.sede.cap;
    address_city = f.header.cessionario_committente.sede.comune;
    address_province = f.header.cessionario_committente.sede.provincia;
    country_iso = f.header.cessionario_committente.sede.nazione;
  }

  return {
           {"name", name},
           {"vat_number", vat_number},
           {"tax_code", tax_code},
           {"address_street", address_street},
           {"address_postal_code", address_postal_code},
           {"address_city", address_city},
           {"address_province", address_province},
           {"country_iso", country_iso},
           {"ei_code",
            std::string(f.header.dati_trasmissione.codice_destinatario)},
           {"certified_mail",
            std::string(f.header.dati_trasmissione.pec_destinatario)}
         };
}

[[nodiscard]] json::array_t fic_items_list(const FatturaElettronica &f)
{
  auto j(json::array());

  for (const auto &d : f.body.dati_beni_servizi.dettaglio_linee)
  {
    const std::string rif(
      f.body.dati_beni_servizi.riferimento_normativo(d.natura));

    json::object_t o =
      {
        {"code", d.codice_articolo.codice_valore},
        {"name", d.descrizione},
        {"net_price", d.prezzo_unitario.to_double()},
        {"measure", d.unita_misura},
        {"qty", d.quantita.to_double()},
        {"vat", {{"id", vat_id(d.aliquota_iva.to_double(), rif)}}},
        {"apply_withholding_tax", bool(d.ritenuta)}
      };

    if (d.sconto_maggiorazione.tipo == "SC"
        && d.sconto_maggiorazione.percentuale.to_double() > 0.0)
    o["discount"] = d.sconto_maggiorazione.percentuale.to_double();

    if (!d.altri_dati_gestionali.empty())
    {
      auto j_adg(json::array());;
      for (const auto &adg : d.altri_dati_gestionali)
      {
        json::object_t o_adg =
          {
            {"TipoDato", adg.tipo_dato},
            {"RiferimentoTesto", adg.riferimento_testo},
            {"RiferimentoData", adg.riferimento_data}
          };

        j_adg += o_adg;
      }

      o["ei_raw"][DettaglioLinee::tag()][AltriDatiGestionali::tag()] = j_adg;
    }

    j += o;
  }

  return j;
}

[[nodiscard]] std::string fic_type(const FatturaElettronica &f)
{
  const auto &td(f.body.dati_generali.dati_generali_documento
                 .tipo_documento);

  return td == TipoDocumento("TD01") ? "invoice" :
         td == TipoDocumento("TD04") ? "credit_note" :
         f.acquisto() ? "self_supplier_invoice" :
         "";
}

[[nodiscard]] json fic_body_ei_raw(const FatturaElettronica &f)
{
  json dg;

  struct doc_info
  {
    const DatiDocumentiCorrelatiType *dati;
    std::size_t size;
  };

  const auto coppia([](const auto &doc)
  {
    return doc_info{doc.data(), doc.size()};
  });

  const std::vector doc_correlati =
  {
    coppia(f.body.dati_generali.dati_ordine_acquisto),
    coppia(f.body.dati_generali.dati_contratto),
    coppia(f.body.dati_generali.dati_convenzione),
    coppia(f.body.dati_generali.dati_fatture_collegate)
  };

  for (const auto &dc : doc_correlati)
  {
    auto ja(json::array());

    for (std::size_t i(0); i < dc.size; ++i)
      ja +=
      {
        {"IdDocumento", dc.dati->id_documento},
        {"Data", dc.dati->data},
        {"CodiceCUP", dc.dati->codice_cup},
        {"CodiceCIG", dc.dati->codice_cig},
        {"CodiceCommessaConvenzione", dc.dati->codice_commessa_convenzione},
        {"NumItem", dc.dati->num_item}
      };

    if (!ja.empty())
      dg[dc.dati->tag()] = ja;
  }

  {
    const auto &dati_ddt(f.body.dati_generali.dati_ddt);
    auto ja(json::array());
    for (const auto &ddt : dati_ddt)
      ja +=
      {
        {"NumeroDDT", ddt.numero_ddt},
        {"DataDDT", ddt.data_ddt},
        {"RiferimentoNumeroLinea", json(ddt.riferimento_numero_linea)}
      };

    if (!ja.empty())
      dg[dati_ddt.front().tag()] = ja;
  }

  {
    const auto &dati_sal(f.body.dati_generali.dati_sal);
    auto ja(json::array());
    for (const auto &sal : dati_sal)
      ja +=
      {
        {"RiferimentoFase", sal.riferimento_fase}
      };

    if (!ja.empty())
      dg[dati_sal.front().tag()] = ja;
  }

  return {{DatiGenerali::tag(), dg}};
}

void fic_pagamento(class FatturaElettronica &f, json &j)
{
  if (!f.body.dati_pagamento.condizioni_pagamento.empty())
    j["ei_raw"][FatturaElettronicaBody::tag()][DatiPagamento::tag()]
     [CondizioniPagamento::tag()] = f.body.dati_pagamento.condizioni_pagamento;

  auto ja(json::array());
  const auto &dettaglio_pagamento(f.body.dati_pagamento.dettaglio_pagamento);
  for (const auto &dp : dettaglio_pagamento)
  {
    json::object_t o = {{"amount", dp.importo_pagamento.to_double()}};

    if (dp.data_scadenza_pagamento.empty())
      o["due_date"] = f.body.dati_generali.dati_generali_documento.data;
    else
      o["due_date"] = dp.data_scadenza_pagamento;

    if (!dp.modalita_pagamento.empty())
      o["ei_raw"][DettaglioPagamento::tag()][ModalitaPagamento::tag()] =
       std::string(dp.modalita_pagamento);

    if (!dp.iban.empty())
      o["ei_raw"][DettaglioPagamento::tag()]["IBAN"] = dp.iban;

    ja += o;
  }

  if (ja.empty())
  {
    const auto &dgd(f.body.dati_generali.dati_generali_documento);
    const json::object_t o =
    {
      {"amount", dgd.importo_totale_documento.to_double()},
      {"due_date", dgd.data}
    };

    ja += o;
  }

  j["payments_list"] = ja;
}

void fic_withholding_tax(class FatturaElettronica &f, json &j)
{
  if (f.body.dati_generali.dati_generali_documento.dati_ritenuta.empty())
    return;

  double imponibile_con_ritenuta(0.0);
  for (const auto &dl : f.body.dati_beni_servizi.dettaglio_linee)
    if (dl.ritenuta)
      imponibile_con_ritenuta += dl.prezzo_totale.to_double();

  assert(imponibile_con_ritenuta > 0.0);

  const auto &dr(f.body.dati_generali.dati_generali_documento.dati_ritenuta[0]);
  const Percentuale withholding_tax_taxable(
    dr.importo_ritenuta.to_double() * 100.0
    / (imponibile_con_ritenuta * dr.aliquota_ritenuta.to_double() / 100.0));

  j["ei_withholding_tax_causal"] = dr.causale_pagamento;
  j["ei_other_withholding_tax_type"] = dr.tipo_ritenuta;
  j["withholding_tax"] = dr.aliquota_ritenuta.to_double();
  j["withholding_tax_taxable"] = withholding_tax_taxable.to_double();
}

[[nodiscard]] json fic_ei_data(const FatturaElettronica &f)
{
  return
  {
    {
      "vat_kind",
      std::string(f.body.dati_beni_servizi.default_esigibilita_iva)
    },
    {"payment_method", "MP01"}  // costituisce una sorta di valore di default
                                // se in dettaglio_pagamento manca la modalita'
  };

  // La procedura descritta in "Invoice Creation - Step Three: E-Invoice"
  // (https://developers.fattureincloud.it/docs/guides/invoice-creation/#three-step-three-e-invoice)
  // per quanto riguarda le note di credito, suggerisce:
  //
  //   std::string invoice_number, invoice_date;
  //   if (fic_type(f) == "credit_note"
  //       && !f.body.dati_generali.dati_fatture_collegate.empty())
  //   {
  //     const auto &df(f.body.dati_generali.dati_fatture_collegate[0]);
  //     invoice_number = df.id_documento;
  //     invoice_date = df.data;
  //   }
  //
  //   if (!invoice_number.empty())
  //   {
  //     ret += json::object_t::value_type("invoice_number", invoice_number);
  //     ret += json::object_t::value_type("invoice_date", invoice_date);
  //   }
  //
  // Nella libreria abbiamo scelto di inserire direttamente i dati delle
  // fatture collegate nell'array "DatiFattureCollegate" (parte dell'oggetto
  // "ei_raw" : "FatturaElettronicaBody" : "DatiGenerali").
  // Vedi `fic_body_ei_raw`.
}

}  // namespace

///
/// \param[in] aliquota valore numerico dell'aliquota IVA (da `0` a `100`)
/// \param[in] rif      contiene informazioni per l'identificazione del
/// \return             codice numerico iva su FIC (utilizzato, in particolare,
///                     nel caso di aliquota `0`)
///
/// Assume la tabella di default utilizzata su Fatture in Cloud. Alla stessa
/// natura possono corrispondere codice differenti.
///
[[nodiscard]] unsigned vat_id(double aliquota, const std::string &rif)
{
  assert(0.0 <= aliquota && aliquota <= 100.0);

  if (!rif.empty())
  {
    const std::string fic_id("[FIC ");
    if (const auto pos(rif.find(fic_id)); pos != std::string::npos)
      if (const auto id_end(rif.find("]", pos)); id_end != std::string::npos)
      {
        const auto id_start(pos + fic_id.size());

        const std::string str_id(rif.substr(id_start, id_end - id_start));
        if (const unsigned id(std::stoul(str_id)); id)
          return id;
      }
  }

  if (zero(aliquota - 22.0))
    return 0;
  if (zero(aliquota - 21.0))
    return 1;
  if (zero(aliquota - 20.0))
    return 2;
  if (zero(aliquota - 10.0))
    return 3;
  if (zero(aliquota - 8.0))
    return 29;
  if (zero(aliquota - 4.0))
    return 4;
  if (zero(aliquota - 23.0))
    return 40;
  if (zero(aliquota - 24.0))
    return 41;
  if (zero(aliquota - 5.0))
    return 54;

  // Valore arbitrario fra quelli che prevedono iva a zero. Nel caso di
  // aliquota `0` bisogna indicare nel riferimento il corretto ID su FIC.
  if (zero(aliquota))
    return 7;

  // In caso di disperazione si utilizza l'IVA al 22%.
  return 0;
}

std::string export_json(class ::felpa::FatturaElettronica &f)
{
  json j_base;
  j_base["data"]["type"] = fic_type(f);

  json &j(j_base["data"]);

  j["entity"] = fic_entity(f);

  j["date"] = f.body.dati_generali.dati_generali_documento.data;

  if (const serie_e_numero sen(f.body.dati_generali.dati_generali_documento.
                               numero);
      !sen.numero().empty())
  {
    j["number"] = std::stoul(sen.numero());
    j["numeration"] = sen.serie();
  }

  j["visible_subject"] = f.body.dati_generali.dati_generali_documento.causale;

  j["currency"] = {{"id", "EUR"}};

  j["e_invoice"] = true;

  j["items_list"] = fic_items_list(f);

  j["ei_data"] = fic_ei_data(f);

  j["ei_raw"][FatturaElettronicaBody::tag()] = fic_body_ei_raw(f);

  fic_withholding_tax(f, j);

  if (!f.body.dati_generali.dati_generali_documento.dati_bollo.bollo_virtuale
      .empty())
    j["stamp_duty"] = f.body.dati_generali.dati_generali_documento.dati_bollo
                      .importo_bollo.to_double();

  if (const auto &sm(f.body.dati_generali.dati_generali_documento
                     .sconto_maggiorazione);
      !sm.empty() && sm.front().tipo == ScontoMaggiorazione::SCONTO)
    j["amount_due_discount"] = sm.front().importo.to_double();

  fic_pagamento(f, j);

  // Mancanti:
  // riferimento amministrazione
  // allegati

  return j_base.dump();
}

}  // namespace felpa::fic
