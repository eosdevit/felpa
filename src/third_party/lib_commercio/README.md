# [LibCommercio](https://bitbucket.org/morinim/lib_commercio)

## Caratteristiche ##

*Header only library*, sviluppata in C++, per semplificare la verifica, memorizzazione, compilazione di codici in uso nelle transazioni commerciali:
- codice fiscale;
- codice SDI / destinatario;
- IBAN;
- nazione;
- partita iva;
- provincia;
- regione;
- VAT.

Il software e' distribuito con licenza d'uso [Mozilla Public License v2.0](https://www.mozilla.org/MPL/2.0/).

## Requisiti

Compilatore compatibile con lo standard C++17.

Al momento gli ambienti di sviluppo verificati sono:

* G++
* CLANG++
* C++Builder Rio / Sydney / Alexandria

## Uso

E' possibile includere l'header principale:

```c++
#include "lib_commercio.hpp"
```

o solo quelli di interesse:

```c++
#include "lib_commercio/vat.hpp"
```

## Osservazioni

LibCommercio e' *UTF-8 pure*: la libreria considera tutte le stringhe in ingresso ed in uscita codificate in formato UTF-8.

Per fare un esempio con il C++Builder in ambiente Windows:

```
UnicodeString str(OpenDialog1->FileName);
UTF8String    ustr(str);

std::string   s(ustr.c_str(), ustr.Length())  // questa e' la stringa da usare
```

Il tutto funziona poiche' UTF-8 e' compatibile con i codici ASCII a 7 bit. Se il valore di un byte e' superiore a 127, significa che ha inizio un carattere multi-byte (a seconda del valore del primo byte si puo' capire da quanti byte e' composto il carattere, da 2 a 4).

Dal momento che l'UTF-8 e' basato sui char-byte e le stringhe sono *0-terminated*, si possono utilizzare le funzioni della libreria standard per gran parte delle necessita'.

La cosa piu' importante da ricordare e' che il conteggio dei caratteri potrebbe differire da quello dei byte (una funzioni come `strlen()` restituisce il numero dei byte non dei caratteri).