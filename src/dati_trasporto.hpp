/**
 *  \file
 *  \remark This file is part of FELPA.
 *
 *  \copyright Copyright (C) 2015-2021 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#if !defined(FELPA_DATI_TRASPORTO_HPP)
#define      FELPA_DATI_TRASPORTO_HPP

#include "dati_anagrafici_vettore.hpp"
#include "data_ora_consegna.hpp"

namespace felpa
{

class DatiTrasporto
{
public:
  DatiTrasporto();

  bool empty() const;

  static const std::string &tag();

  // ********* Data member pubblici *********
  DatiAnagraficiVettore dati_anagrafici_vettore;
  std::string causale_trasporto;
  unsigned numero_colli;
  DataOraConsegna data_ora_consegna;
};  // class DatiTrasporto

bool operator==(const DatiTrasporto &, const DatiTrasporto &);

}  // namespace felpa

#endif  // include guard
