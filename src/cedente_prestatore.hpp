/**
 *  \file
 *  \remark This file is part of FELPA.
 *
 *  \copyright Copyright (C) 2015-2024 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#if !defined(FELPA_CEDENTE_PRESTATORE_HPP)
#define      FELPA_CEDENTE_PRESTATORE_HPP

#include <string>

#include "dati_anagrafici_cedente.hpp"
#include "iscrizione_rea.hpp"
#include "sede.hpp"
#include "stabile_organizzazione.hpp"

namespace felpa
{

///
/// Contiene i dati relativi al cedente/prestatore del bene/servizio oggetto di
/// fatturazione.
///
class CedentePrestatore
{
public:
  CedentePrestatore();

  [[nodiscard]] bool empty() const;

  [[nodiscard]] static const std::string &tag();

  // ********* Data members pubblici *********
  DatiAnagraficiCedente dati_anagrafici;

  /// Serve per individuare la residenza od il domicilio del
  /// cedente/prestatore.
  ///
  /// \warning OBBLIGATORIETA': SI, sempre.
  Sede sede;

  StabileOrganizzazione stabile_organizzazione;

  /// \warning
  /// OBBLIGATORIETA': nei soli casi di societa' soggette al vincolo
  /// dell'iscrizione nel registro delle imprese ai sensi dell'art. 2250 del
  /// codice civile.
  IscrizioneRea iscrizione_rea;

  /// Eventuale riferimento utile al destinatario per automatizzare la gestione
  /// amministrativa dell'operazione in fattura.
  std::string riferimento_amministrazione;
};  // class CedentePrestatore

[[nodiscard]] bool operator==(const CedentePrestatore &,
                              const CedentePrestatore &);

[[nodiscard]] CedentePrestatore completa(CedentePrestatore);

}  // namespace felpa

#endif  // include guard
