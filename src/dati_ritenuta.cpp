/**
 *  \file
 *  \remark This file is part of FELPA.
 *
 *  \copyright Copyright (C) 2018-2024 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#include <cassert>

#include "dati_ritenuta.hpp"
#include "fattura_elettronica_dettagli.hpp"
#include "fattura_elettronica_eccezioni.hpp"

namespace felpa
{

const std::string &DatiRitenuta::tag()
{
  static const std::string TAG("DatiRitenuta");
  return TAG;
}

namespace dettagli
{

bool carica(tinyxml2::XMLConstHandle &ft, DatiRitenuta *dr)
{
  tinyxml2::XMLConstHandle h(ft.FirstChildElement(dr->tag().c_str()));
  if (!h.ToElement())
    h = ft;

  const bool e1(carica(h, &dr->tipo_ritenuta));
  const bool e2(carica(h, &dr->importo_ritenuta));
  const bool e3(carica(h, &dr->aliquota_ritenuta));
  const bool e4(carica(h, &dr->causale_pagamento));

  return e1 && e2 && e3 && e4;
}

void registra(tinyxml2::XMLPrinter &ft, const DatiRitenuta &dr)
{
  if (dr.empty())
    return;

  ft.OpenElement(DatiRitenuta::tag().c_str());

  registra(ft, dr.tipo_ritenuta);
  registra(ft, dr.importo_ritenuta);
  registra(ft, dr.aliquota_ritenuta);
  registra(ft, dr.causale_pagamento);

  ft.CloseElement();
}

}  // namespace dettagli

bool DatiRitenuta::empty() const
{
  return tipo_ritenuta.empty();
}

bool operator==(const DatiRitenuta &lhs, const DatiRitenuta &rhs)
{
  return
    std::string(lhs.tipo_ritenuta) == std::string(rhs.tipo_ritenuta)
    && std::string(lhs.importo_ritenuta) == std::string(rhs.importo_ritenuta)
    && std::string(lhs.aliquota_ritenuta) == std::string(rhs.aliquota_ritenuta)
    && std::string(lhs.causale_pagamento) == std::string(rhs.causale_pagamento);
}

}  // namespace felpa
