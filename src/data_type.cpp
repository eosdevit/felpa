/**
 *  \file
 *  \remark This file is part of FELPA.
 *
 *  \copyright Copyright (C) 2015-2021 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#include <cassert>

#include "data_type.hpp"
#include "fattura_elettronica_dettagli.hpp"
#include "fattura_elettronica_eccezioni.hpp"

namespace felpa
{

namespace dettagli
{

bool carica(tinyxml2::XMLConstHandle &ft, DataType *d)
{
  auto *h(ft.FirstChildElement(d->tag().c_str()).ToElement());
  if (!h || !h->GetText())
    return false;

  d->assign(h->GetText());
  return true;
}

void registra(tinyxml2::XMLPrinter &ft, const DataType &d)
{
  if (!d.empty())
  {
    ft.OpenElement(d.tag().c_str());
    push_text(ft, d);
    ft.CloseElement();
  }
}

}  // namespace dettagli

bool DataType::valida(const std::string &d)
{
  if (d.empty())
    return true;

  return d.length() == 10
         && std::isdigit(d[0]) && std::isdigit(d[1])
         && std::isdigit(d[2]) && std::isdigit(d[3])
         && d[4] == '-'
         && std::isdigit(d[5]) && std::isdigit(d[6])
         && d[7] == '-'
         && std::isdigit(d[8]) && std::isdigit(d[9]);
}

DataType::DataType(const std::string &t, const std::string &d) : tag_(t),
                                                                 data_()
{
  assert(!tag_.empty());
  assign(d);
}

void DataType::assign(const std::string &d)
{
  if (!valida(d))
    throw eccezione::formato_data(tag() + ": formato data scorretto");

  data_ = d;
  assert(data_.empty() || data_.length() == 10);
}

}  // namespace felpa
