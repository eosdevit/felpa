/**
 *  \file
 *  \remark This file is part of FELPA.
 *
 *  \copyright Copyright (C) 2018-2024 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#if !defined(FELPA_DATI_RITENUTA_HPP)
#define      FELPA_DATI_RITENUTA_HPP

#include "tipo_ritenuta.hpp"
#include "importo_ritenuta.hpp"
#include "aliquota_ritenuta.hpp"
#include "causale_pagamento.hpp"

namespace felpa
{

class DatiRitenuta
{
public:
  DatiRitenuta() = default;

  [[nodiscard]] bool empty() const;

  [[nodiscard]] static const std::string &tag();

  // ********* Data member pubblici *********

  /// Tipologia di ritenuta (persone fisiche o persone giuridiche).
  TipoRitenuta tipo_ritenuta {};

  ImportoRitenuta importo_ritenuta {};

  AliquotaRitenuta aliquota_ritenuta {};

  /// Codice della causale del pagamento. Il codice corrisponde a quello
  /// utilizzato per la compilazione del modello 770S.
  CausalePagamento causale_pagamento {};
};  // class DatiRitenuta

[[nodiscard]] bool operator==(const DatiRitenuta &, const DatiRitenuta &);

}  // namespace felpa

#endif  // include guard
