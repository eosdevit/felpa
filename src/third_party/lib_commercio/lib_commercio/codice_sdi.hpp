/**
 *  \file
 *  \remark This file is part of libCommercio.
 *
 *  \copyright Copyright (C) 2023 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#include <cassert>
#include <string>

#include "utils.hpp"

#if !defined(LIB_COMMERCIO_CODICE_SDI_HPP)
#define      LIB_COMMERCIO_CODICE_SDI_HPP

namespace lib_commercio
{

/*********************************************************************
 * CODICE SDI (Servizio Di Interscambio)
 *********************************************************************/
///
/// Il codice SDI, cosi' chiamato perche' il suo utilizzo e' legato al Sistema
/// di Interscambio telematico dell'Agenzia delle Entrate, e' un codice
/// alfanumerico composto da 7 caratteri, che permette al Sistema di
/// Interscambio di recapitare al corretto indirizzo digitale la fattura
/// elettronica emessa.
/// Il codice, chiamato più correttamente codice destinatario, identifica
/// infatti il servizio web o il software utilizzato dal destinatario della
/// fattura e permette di ricevere direttamente sul proprio gestionale di
/// fatturazione elettronica le fatture ricevute.
///
class codice_sdi
{
public:
  codice_sdi(const std::string & = "");

  operator std::string() const { return sdi_; }

  enum codice_verifica {errato, sconosciuto, formalmente_valido, corretto};

  [[nodiscard]] bool ente_pubblico() const;
  [[nodiscard]] const std::string &gestore() const;

  [[nodiscard]] static codice_verifica verifica(const std::string &);
  [[nodiscard]] static const std::string &gestore(const std::string &);

#if defined(__BORLANDC__)
  codice_sdi(const String &);
  [[nodiscard]] static bool verifica(const String &);
#endif

private:
  std::string sdi_;
}; // class codice_sdi

[[nodiscard]] bool operator==(const codice_sdi &, const codice_sdi &);
[[nodiscard]] bool operator<(const codice_sdi &, const codice_sdi &);


/*********************************************************************
 * IMPLEMENTAZIONE
 *********************************************************************/
inline codice_sdi::codice_sdi(const std::string &cod)
  : sdi_(toupper(erase_spaces(cod)))
{
  if (!verifica(sdi_))
    throw std::invalid_argument("Codice destinatario scorretto");
}

///
/// Identifica il gestore associato ad un codice destinatario.
///
/// \param[in] cod codice destinatario
/// \return        ragione sociale del gestore del codice `cod` se disponibile,
///                una stringa vuota in caso contrario.
///
inline const std::string &codice_sdi::gestore(const std::string &cod)
{
  const static std::map<std::string, std::string> cod_gestore =
  {
    {"0000000", "AGENZIA DELLE ENTRATE"},
    {"04CC85E", "SOFTA SRL"},
    {"0G6TBBX", "TECH EDGE"},
    {"0KDMVIB", "SMART VET"},
    {"10ZKECO", "LIBERO SIFATTURA"},
    {"23R5HA7", "-"},
    {"2HE9UNC", "-"},
    {"2LCMINU", "ENTE AUTONOMO VOLTURNO"},
    {"2R4GTO8", "UFFICIO TRIBUTARIO DI SAN MARINO"},
    {"3000002", "BANCA IFIS"},
    {"38P86EY", "MEDIATICA"},
    {"3G3OPYL", "VAR GROUP SPA"},
    {"3RB98ZT", "STUDIOFARMA SRL"},
    {"3ZJY534", "ARCHIVIUM"},
    {"4000002", "BANCA IFIS"},
    {"43OS0AD", "-"},
    {"4ADX8V9", "MEDIAINX"},
    {"4IYRYKK", "-"},
    {"4UHNMQ0", "-"},
    {"51TILC8", "-"},
    {"596NUAX", "NADIRSOFTWARE"},
    {"5KQRP7D", "-"},
    {"5P3UNVR", "GRUPPO CMT"},
    {"5RU082D", "-"},
    {"5RUO82D", "FATTURA PA"},
    {"5RUO8ZD", "-"},
    {"5W4A8J1", "COLDIRETTI"},
    {"61GKEYB", "-"},
    {"624GNVM", "-"},
    {"66OZKW1", "INFOCAMERE"},
    {"6EWHWGT", "-"},
    {"6EWHWLT", "FTPA"},
    {"6JXPS2J", "CLOUDFINANCE"},
    {"6RB0OU9", "CIA"},
    {"7035UR5", "UNIONE AGRICOLTORIE E COLTIVATORI DIRETTI SUDTIROLESI"},
    {"73JC40C", "-"},
    {"7G984OP", "SISTEMIC"},
    {"7HE8RN5", "CENTRO SOFTWARE"},
    {"8CQGKGJ", "B CONSOLE"},
    {"8GRE4CU", "-"},
    {"8KI81S6", "EDIGEST"},
    {"94QLT54", "-"},
    {"9GHPHLV", "-"},
    {"9KX6LE7", "-"},
    {"9ME3HSM", "-"},
    {"9MTMAR3", "-"},
    {"9RN6ROS", "-"},
    {"9ROR5K4", "-"},
    {"A4707H7", "ABLETECH"},
    {"ACTKBBZ", "ARCHIVIST"},
    {"AGZS6TU", "-"},
    {"AO3AEUZ", "MISTRAL"},
    {"ARBTDQ3", "-"},
    {"AU7YEU4", "CONSORZIO CIAT"},
    {"B2K7HJZ", "-"},
    {"B66HAMY", "CONFARTIGIANATO"},
    {"BA6ET11", "BUFFETTI"},
    {"BLY9JDQ", "J-SOFTWARE"},
    {"BMQCL4W", "SOFTWARE SEMPLICE"},
    {"BMWX7J9", "-"},
    {"C1QQYZR", "ALTO TREVIGIANO SERVIZI"},
    {"C3UCBRB", "-"},
    {"C3UCNRB", "-"},
    {"C4AL1U8", "-"},
    {"C5J14RS", "-"},
    {"CAUTIS0", "-"},
    {"CC2G1TV", "REGINVOICE"},
    {"CCU1MJ3", "-"},
    {"CEORGIG", "SKYNET"},
    {"CLZ9MPC", "-"},
    {"DG0M2PE", "-"},
    {"DG30ACJ", "-"},
    {"DHDLG27", "-"},
    {"DUDU0GE", "ORISLINE"},
    {"DXEBYTP", "ASSIST INFORMATICA SRL"},
    {"E06UCUD", "UNIMATICA"},
    {"E2VWRNU", "EXTREME SOFTWARE"},
    {"E4X9PNC", "-"},
    {"EH1R83N", "LINEA UFFICIO"},
    {"ER3XO7K", "-"},
    {"FEDYLOG", "-"},
    {"G1XGCBG", "DANISOFT"},
    {"G4AI1U8", "READY PRO"},
    {"G7Q6SPJ", "ENTAKSI"},
    {"G9H7JRW", "-"},
    {"G9HZJRW", "BESIDETECH"},
    {"G9YK3BM", "SEDICO SERVIZI"},
    {"GR2P7ZP", "MULTIWIRE"},
    {"GYTYMI8", "-"},
    {"H348Q01", "MySond"},
    {"HHBD9AK", "Ediel"},
    {"HHBNB2R", "Clapps"},
    {"HQSIB42", "Tokem srl"},
    {"HQSJ6TR", "-"},
    {"HUE516M", "Agyo"},
    {"I22N9XP", "-"},
    {"I347Y6N", "MyFiscalCloud"},
    {"I6VXTJA", "IEO Informatica"},
    {"ISHDUAE", "Subito fattura"},
    {"ITH9EQH", "Software GestLeader"},
    {"J5BX4DY", "-"},
    {"J6URRTW", "DocEasy"},
    {"JC7P1DW", "Zerogestionale.it"},
    {"JHBM40P", "Multidialogo"},
    {"JX8OYTO", "-"},
    {"K0ROACV", "Acut"},
    {"K1L1030", "-"},
    {"KBRM7PS", "Ondata cloud"},
    {"KGVVJ2H", "YouDox"},
    {"KJSRCTG", "QuickMastro"},
    {"KRRH689", "-"},
    {"KRRH6B9", "ARUBA"},
    {"KRRH6BG", "-"},
    {"KRRHOB9", "-"},
    {"KUPCRMI", "Digithera Srl"},
    {"L8WH30T", "-"},
    {"LKYVDU9", "-"},
    {"LX4UQQ5", "Trust Technology"},
    {"M5ITOJA", "Var Group"},
    {"M5UXCR1", "Agyo"},
    {"M5UXCRA", "-"},
    {"M62SGNV", "Gestionale Amica"},
    {"MJ1OYNU", "Ifin Sistemi"},
    {"MJEGRSK", "Nebu"},
    {"MRCC2DY", "Memory"},
    {"MSUXCR1", "K Link Solutions"},
    {"MTD8HRW", "Gsp"},
    {"MZO2A0U", "Credemtel"},
    {"MZO2AOU", "Credemtel"},
    {"N3HJJJI", "Gespac srl"},
    {"N92GLON", "Fattura Elettronica APP"},
    {"N9KM26R", "Kalyos"},
    {"NKNH5UQ", "IDOCTORS"},
    {"NVXARUR", "-"},
    {"O62QHVQ", "-"},
    {"O8L2VB7", "Olivetti"},
    {"OB9MX5S", "-"},
    {"OCCDHSV", "Fattura 1 click"},
    {"OHYG9HP", "Rai"},
    {"OLCJ82K", "Olivetti"},
    {"OUD4SW5", "-"},
    {"P43TKPP", "Metel"},
    {"P4IUPYH", "Mustweb Srl"},
    {"P62QHVQ", "Seac"},
    {"P83CKOC", "Puntanet"},
    {"PAXCCYU", "ANDXOR"},
    {"PD4ZURU", "-"},
    {"PPX7BLB", "Idea software"},
    {"PR4AG6C", "-"},
    {"PWC9LEM", "-"},
    {"PXQYICS", "Studioboost"},
    {"PZIJH2V", "Register"},
    {"Q4AM8JN", "-"},
    {"Q7YVJK9", "-"},
    {"QCNN53Y", "-"},
    {"QD4YHQC", "Net4market"},
    {"QDZCM9N", "Wincar"},
    {"QLDR2VY", "Profarma"},
    {"QULXG4S", "FattureGB"},
    {"QVDR2VY", "-"},
    {"QYISEC3", "Omicron Sistemi"},
    {"R0NR49R", "-"},
    {"R3KOGSP", "-"},
    {"RGBDW7A", "Rai Radiotelevisione Italiana"},
    {"RN5Y3PI", "Federfarma"},
    {"RNMN7NC", "PegasoGest"},
    {"ROINDUX", "Fattutto"},
    {"RTVLCR1", "Siweb"},
    {"RV0QRBC", "-"},
    {"RVLIKN1", "-"},
    {"RWB54P8", "Intesa San Paolo"},
    {"RWDS1D5", "-"},
    {"RWYUTBX", "-"},
    {"RXMENAR", "-"},
    {"RYRNP0U", "FerServizi"},
    {"RZTDA93", "-"},
    {"SA0PL6Q", "SataNet"},
    {"SKUA8Y6", "Italsoft"},
    {"SN4CSRI", "Ready Pro"},
    {"SNT102H", "Spsoft"},
    {"SR77I32", "-"},
    {"SU1UTOG", "Mister software"},
    {"SU9YNJA", "Rdv network"},
    {"SUBM70N", "Zucchetti"},
    {"SUBM70W", "-"},
    {"SUMN70N", "-"},
    {"SZLUBAI", "Fattura24"},
    {"T04ZH3R", "Legal Solution"},
    {"T04ZHR3", "WebClient"},
    {"T1YN8TL", "-"},
    {"T7V19W4", "-"},
    {"T9K4ZHO", "Datev Koinos"},
    {"T9U85V9", "-"},
    {"TA0WO4S", "-"},
    {"TPICRCA", "TRUST INVOICE"},
    {"TRS30H9", "-"},
    {"TRS3OH9", "Ksg"},
    {"TRTSWMZ", "Trust Technologies"},
    {"TT0B2J8", "Uno erp"},
    {"TULURSB", "Modula3"},
    {"TV195KG", "-"},
    {"U0UWT4Y", "-"},
    {"U21NEXA", "-"},
    {"UCN4I0G", "-"},
    {"UE2LXTM", "Finson Express"},
    {"UNI0W8G", "Edigest"},
    {"UO5NS3A", "-"},
    {"UOUWT4Y", "-"},
    {"URCROKA", "-"},
    {"URSWIEX", "Digithera Srl"},
    {"URT8N9K", "-"},
    {"USA39RA", "Day Ristoservice"},
    {"USAL8PD", "-"},
    {"USAL8PV", "Sistemi"},
    {"VDBYKVW", "-"},
    {"VHXVZM7", "-"},
    {"W4KYJ8V", "Metodo"},
    {"W7YUJK9", "-"},
    {"W7YV5K9", "-"},
    {"W7YVJK9", "Wolters Kluwer"},
    {"W7YVTK9", "-"},
    {"W7YWJK9", "-"},
    {"W840XLE", "-"},
    {"WH2KO8I", "FatturaPaPerTutti"},
    {"WHP7LTE", "FattApp"},
    {"WNK4HCP", "CoffeWeb"},
    {"WNR31TD", "-"},
    {"WP75E2Q", "-"},
    {"WP76E2Q", "-"},
    {"WP7SE2Q", "ARCHIVIA.ONLINE"},
    {"WTHLGAQ", "YouInvoice"},
    {"WY7PJ6K", "FatturaOnClick"},
    {"X2PH38J", "Bluenext"},
    {"X46AXNR", "Ok Copy Italia"},
    {"XIT6IP5", "Olsa informatica"},
    {"XL13LG4", "Infocert"},
    {"XMXAUP4", "LaPam"},
    {"XRHAYHP", "-"},
    {"XVBJ9YM", "-"},
    {"XWJKNZD", "Faber System"},
    {"Y1UP231", "-"},
    {"Y4BUAV4", "RM-Tech"},
    {"Y799V0R", "-"},
    {"YMUS82V", "-"},
    {"YRXHCLN", "InTouch"},
    {"YU23YVQ", "-"},
    {"ZCK6XHR", "Savino Solution srl"},
    {"ZE7RB0G", "-"},
    {"ZRBGBN7", "-"},
    {"ZS100U1", "-"}
  };

  if (const auto it(cod_gestore.find(cod)); it != cod_gestore.end())
    return it->second;

  const static std::string empty;
  return empty;
}

inline const std::string &codice_sdi::gestore() const
{
  return gestore(sdi_);
}

///
/// Verifica la correttezza formale di un codice destinatario.
///
/// \param[in] cod codice destinatario
/// \return        `errato` nel caso di codice sicuramente scorretto;
///                `formalmente_valido` per i codici nel corretto formato ma
///                non censiti e `corretto` per un codice sicuramente corretto
///
inline codice_sdi::codice_verifica codice_sdi::verifica(const std::string &cod)
{
  if (cod.length() < 6 || cod.length() > 7)
    return errato;

  if (!std::all_of(cod.begin(), cod.end(),
                   [](auto c) { return std::isdigit(c) || std::isupper(c); }))
    return errato;

  if (cod.length() == 6)
    return formalmente_valido;

  assert(cod.length() == 7);

  return gestore(cod).empty() ? formalmente_valido : corretto;
}

///
/// Verifica se il codice destinatario e' associato ad un ente pubblico.
///
/// \return `true` se il codice destinatario e' associato ad un ente pubblico
///
inline bool codice_sdi::ente_pubblico() const
{
  return sdi_.length() == 6;
}

inline bool operator==(const codice_sdi &lhs, const codice_sdi &rhs)
{
  return static_cast<std::string>(lhs) == static_cast<std::string>(rhs);
}

inline bool operator<(const codice_sdi &lhs, const codice_sdi &rhs)
{
  return static_cast<std::string>(lhs) < static_cast<std::string>(rhs);
}

#if defined(__BORLANDC__)
inline codice_sdi::codice_sdi(const String &cf)
  : codice_sdi(details::to_stdstring(cf))
{
}

inline bool codice_sdi::verifica(const String &cf)
{
  return verifica(details::to_stdstring(cf));
}
#endif

}  // namespace lib_commercio

#endif  // include guard
