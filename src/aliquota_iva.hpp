/**
 *  \file
 *  \remark This file is part of FELPA.
 *
 *  \copyright Copyright (C) 2015-2018 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#if !defined(FELPA_ALIQUOTA_IVA_HPP)
#define      FELPA_ALIQUOTA_IVA_HPP

#include "fattura_elettronica_eccezioni.hpp"
#include "numero_type.hpp"

namespace felpa
{

class AliquotaIva : public NumeroType
{
public:
  explicit AliquotaIva(const std::string &p = "22.00")
    : NumeroType("AliquotaIVA", 2, solo_positivo)
  {
    assign(p);
  }

  explicit AliquotaIva(double p) : NumeroType("AliquotaIVA", 2, solo_positivo)
  {
    assign(p);
  }

  AliquotaIva &operator=(const std::string &p)
  {
    return *this = AliquotaIva(p);
  }

  AliquotaIva &operator=(double p)
  {
    return *this = AliquotaIva(p);
  }
};  // class AliquotaIva

}  // namespace felpa

#endif  // include guard
