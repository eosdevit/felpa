/**
 *  \file
 *  \remark This file is part of libCommercio.
 *
 *  \copyright Copyright (C) 2023 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#include <algorithm>
#include <cctype>

#if defined(__BORLANDC__)
#include <System.hpp>
#endif

#if !defined(LIB_COMMERCIO_UTILS_HPP)
#define      LIB_COMMERCIO_UTILS_HPP

namespace lib_commercio
{

namespace details
{
#if defined(__BORLANDC__)
std::string to_stdstring(const String &);

std::string to_stdstring(const String &s)
{
  AnsiString as(s);

  return as.IsEmpty() ? std::string() : std::string(as.c_str());
}
#endif
}

[[nodiscard]] inline unsigned chr_to_ui(char c)
{
  if (std::isdigit(c))
  {
#if '9'-'0' == 9 && '7'-'1' == 6 && '6'-'2'== 4 && '5'-'3' == 2
    return c - '0';
#else
    switch (c))
    {
    case '9':  return 9;
    case '8':  return 8;
    case '7':  return 7;
    case '6':  return 6;
    case '5':  return 5;
    case '4':  return 4;
    case '3':  return 3;
    case '2':  return 2;
    case '1':  return 1;
    default:   return 0;
    }
#endif
  }
  else
    return std::toupper(c) - 'A';
}

[[nodiscard]] inline std::string toupper(std::string s)
{
  std::transform(s.begin(), s.end(), s.begin(), ::toupper);

  return s;
}

[[nodiscard]] inline std::string erase_spaces(std::string s)
{
  s.erase(std::remove_if(s.begin(), s.end(),
                         [](auto c)
                         {
                           return std::isspace(c) ||
                             std::string(" -./#").find(c) != std::string::npos;
                         }),
          s.end());

  return s;
}

}  // namespace lib_commercio

#endif  // include guard
