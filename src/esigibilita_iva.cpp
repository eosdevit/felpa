/**
 *  \file
 *  \remark This file is part of FELPA.
 *
 *  \copyright Copyright (C) 2015-2021 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#include <cassert>
#include <set>

#include "esigibilita_iva.hpp"
#include "fattura_elettronica_eccezioni.hpp"
#include "fattura_elettronica_dettagli.hpp"

namespace felpa
{

const std::string &EsigibilitaIva::tag()
{
  static const std::string TAG("EsigibilitaIVA");
  return TAG;
}

namespace dettagli
{

bool carica(tinyxml2::XMLConstHandle &ft, EsigibilitaIva *ei)
{
  auto *h(ft.FirstChildElement(ei->tag().c_str()).ToElement());
  if (!h || !h->GetText())
    return false;

  *ei = h->GetText();
  return true;
}

void registra(tinyxml2::XMLPrinter &ft, const EsigibilitaIva &ei)
{
  ft.OpenElement(EsigibilitaIva::tag().c_str());
  push_text(ft, ei);
  ft.CloseElement();
}

}  // namespace dettagli

bool EsigibilitaIva::valido(const std::string &id)
{
  return id == "I" || id == "D" || id == "S";;
}

///
/// \param[in] id `I`: immediata, `D`: differita, `S`: split payment
///
EsigibilitaIva::EsigibilitaIva(const std::string &id) : codice_(id)
{
  if (!valido(codice_))
    throw eccezione::cod_esig_iva(tag() + ": codice non valido");

  assert(codice_.length() == 1);
}

EsigibilitaIva &EsigibilitaIva::operator=(const std::string &s)
{
  return *this = EsigibilitaIva(s);
}

}  // namespace felpa
