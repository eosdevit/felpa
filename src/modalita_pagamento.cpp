/**
 *  \file
 *  \remark This file is part of FELPA.
 *
 *  \copyright Copyright (C) 2015-2024 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#include <cassert>
#include <set>

#include "modalita_pagamento.hpp"
#include "fattura_elettronica_eccezioni.hpp"
#include "fattura_elettronica_dettagli.hpp"

namespace felpa
{

const std::string &ModalitaPagamento::tag()
{
  static const std::string TAG("ModalitaPagamento");
  return TAG;
}

namespace dettagli
{

const std::set<std::string> modalita_pagamento(
{
  "",
  "MP01",  // contanti
  "MP02",  // assegno
  "MP03",  // assegno circolare
  "MP04",  // contanti presso Tesoreria
  "MP05",  // bonifico
  "MP06",  // vaglia cambiario
  "MP07",  // bollettino bancario
  "MP08",  // carta di pagamento
  "MP09",  // RID
  "MP10",  // RID utenze
  "MP11",  // RID veloce
  "MP12",  // riba
  "MP13",  // MAV
  "MP14",  // quietanza erario stato
  "MP15",  // giroconto su conti di contabilita' speciale
  "MP16",  // domiciliazione bancaria
  "MP17",  // domiciliazione postale
  "MP18",  // bollettino di c/c postale
  "MP19",  // SEPA Direct Debit
  "MP20",  // SEPA Direct Debit CORE
  "MP21",  // SEPA Direct Debit B2B
  "MP22",  // trattenuta su somme gia' riscosse
  "MP23"   // PagoPA
});

bool carica(tinyxml2::XMLConstHandle &ft, ModalitaPagamento *mp)
{
  auto *h(ft.FirstChildElement(ModalitaPagamento::tag().c_str()).ToElement());
  if (!h || !h->GetText())
    return false;

  *mp = h->GetText();
  return true;
}

void registra(tinyxml2::XMLPrinter &ft, const ModalitaPagamento &mp)
{
  if (!mp.empty())
  {
    ft.OpenElement(ModalitaPagamento::tag().c_str());
    push_text(ft, mp);
    ft.CloseElement();
  }
}

}  // namespace dettagli

bool richiede_iban(ModalitaPagamento mp)
{
  const std::string c(mp);
  return c == "MP05";
}

bool ModalitaPagamento::valido(const std::string &id)
{
  return dettagli::modalita_pagamento.find(id) !=
         dettagli::modalita_pagamento.end();
}

ModalitaPagamento::ModalitaPagamento(const std::string &id) : codice_(id)
{
  if (!valido(codice_))
    throw eccezione::mod_pagamento_cod_scorretto(tag()+ ": codice non valido");

  assert(codice_.empty() || codice_.length() == 4);
}

ModalitaPagamento &ModalitaPagamento::operator=(const std::string &s)
{
  *this = ModalitaPagamento(s);
  return *this;
}

}  // namespace felpa
