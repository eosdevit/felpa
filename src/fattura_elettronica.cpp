/**
 *  \file
 *  \remark This file is part of FELPA.
 *
 *  \copyright Copyright (C) 2015 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#include <cassert>

#include "fattura_elettronica.hpp"
#include "fattura_elettronica_dettagli.hpp"
#include "fattura_elettronica_eccezioni.hpp"

namespace felpa
{

FatturaElettronica::FatturaElettronica(const EsigibilitaIva &esig_iva,
                                       formato_fattura ver)
  : versione(FormatoTrasmissione(ver)), header(ver), body(esig_iva)
{
}

FatturaElettronica::FatturaElettronica()
  : FatturaElettronica(EsigibilitaIva("I"), formato_fattura::FPR)
{
}

FatturaElettronica::FatturaElettronica(const std::string &fn)
  : FatturaElettronica()
{
  carica(fn);
}

std::string FatturaElettronica::filename() const
{
  return std::string(header.dati_trasmissione.id_trasmittente.id_paese) +
         header.cedente_prestatore.dati_anagrafici.codice_fiscale +
         "_" +
         header.dati_trasmissione.progressivo_invio +
         ".xml";
}

bool FatturaElettronica::acquisto() const
{
  const std::string td(body.dati_generali.dati_generali_documento
                       .tipo_documento);

  return td == "TD17" || td == "TD18" || td == "TD19" || td == "TD28";
}

bool FatturaElettronica::carica(const std::string &n)
{
  if (n.empty())
    throw eccezione::felpa("Nome del file da leggere mancante");

  tinyxml2::XMLDocument ft;
  auto risultato(ft.LoadFile(n.c_str()));
  if (risultato != tinyxml2::XML_SUCCESS)
    throw eccezione::felpa("Formato XML non valido (errore "
                           + std::to_string(risultato) + ")");

  tinyxml2::XMLConstHandle handle(ft.RootElement());

  // Alcune fatture elettroniche sono "innestate" dentro l'elemento
  // `<P:FatturaElettronica>` o `<p:FatturaElettronica>`.
  const auto h(handle
               .FirstChildElement(FatturaElettronicaHeader::tag()
                                  .c_str()));
  if (!h.ToElement())
    handle = handle.FirstChildElement();

  const bool he(dettagli::carica(handle, &header));
  const bool bo(dettagli::carica(handle, &body));

  return he && bo;
}

void FatturaElettronica::registra(std::string n) const
{
  if (n.empty())
    n = filename();

  auto *file(std::fopen(n.c_str(), "w"));

  if (!file)
    throw eccezione::registrazione("Impossibile registrare la fattura");

  tinyxml2::XMLPrinter ft(file);

  ft.PushDeclaration("xml version=\"1.0\" encoding=\"UTF-8\" ");
  ft.OpenElement("p:FatturaElettronica");
  ft.PushAttribute("versione", versione.c_str());
  ft.PushAttribute(
    "xsi:schemaLocation",
    "http://ivaservizi.agenziaentrate.gov.it/docs/xsd/fatture/v1.2 http://www.fatturapa.gov.it/export/fatturazione/sdi/fatturapa/v1.2/Schema_del_file_xml_FatturaPA_versione_1.2.xsd");
  ft.PushAttribute(
    "xmlns:xsi",
    "http://www.w3.org/2001/XMLSchema-instance");
  ft.PushAttribute(
    "xmlns:p",
    "http://ivaservizi.agenziaentrate.gov.it/docs/xsd/fatture/v1.2");
  ft.PushAttribute(
    "xmlns:ds",
    "http://www.w3.org/2000/09/xmldsig#");

  const auto &anagrafici_cp(header.cedente_prestatore.dati_anagrafici);
  const auto &paese_cp(anagrafici_cp.id_fiscale_iva.id_paese);
  const std::string tipo_doc(body.dati_generali.dati_generali_documento
                                 .tipo_documento);
  if (paese_cp.italia())
  {
    if (acquisto())
      throw eccezione::tipo_doc_cod_scorretto(
        "Tipo documento non compatibile con cedente/prestatore italiano");
  }
  else  // cedente prestatore estero
  {
    if (tipo_doc == "TD18" && !paese_cp.ue())
      throw eccezione::tipo_doc_cod_scorretto(
        "Tipo documento non compatibile con cedente/prestatore extra UE");

    if (tipo_doc == "TD29")
      throw eccezione::tipo_doc_cod_scorretto(
        "Tipo documento non compatibile con cedente/prestatore estero");
  }

  if (const auto &anagrafici_cc(header.cessionario_committente
                                      .dati_anagrafici);
      anagrafici_cp.id_fiscale_iva == anagrafici_cc.id_fiscale_iva
      || anagrafici_cp.codice_fiscale == anagrafici_cc.codice_fiscale)
  {
    const std::set<std::string> td00471 =
    {
      "TD01", "TD02", "TD03", "TD06", "TD07", "TD16", "TD17", "TD18", "TD19",
      "TD20"  "TD24", "TD25", "TD28", "TD29"
    };

    if (td00471.find(tipo_doc) != td00471.end())
      throw eccezione::stesso_cedente_cessionario(
      "Il cedente/prestatore non puo' essere uguale al cessionario/committente "
      "(i valori del tipo documento TD01, TD02, TD03, TD06, TD16, TD17, TD18, "
      "TD19, TD20, TD24, TD25,TD28 e TD29 (fatture ordinarie) e TD07 (fatture "
      "semplificate) non ammettono l'indicazione in fattura dello stesso "
      "soggetto sia come cedente che come cessionario");
  }

  dettagli::registra(ft, header);
  dettagli::registra(ft, body);

  ft.CloseElement();

  std::fclose(file);
}

}  // namespace felpa
