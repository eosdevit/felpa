/**
 *  \file
 *  \remark This file is part of FELPA.
 *
 *  \copyright Copyright (C) 2018-2020 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#if !defined(FELPA_TIPO_RITENUTA_HPP)
#define      FELPA_TIPO_RITENUTA_HPP

#include <string>

namespace felpa
{

///
/// Tipologia di ritenuta (persone fisiche, persone giuridiche, contributo
/// previdenziale).
///
class TipoRitenuta
{
public:
  explicit TipoRitenuta(const std::string & = "");

  operator std::string () const { return codice_; }

  TipoRitenuta &operator=(const std::string &);

  bool empty() const;

  static const std::string &tag();

  static const std::string PERSONE_FISICHE;
  static const std::string PERSONE_GIURIDICHE;
  static const std::string CONTRIBUTO_INPS;
  static const std::string CONTRIBUTO_ENASARCO;
  static const std::string CONTRIBUTO_ENPAM;
  static const std::string ALTRO_PREVIDENZIALE;

private:
  // Funzioni private di supporto.
  bool valido(const std::string &);

  // Data member privati.
  std::string codice_;
};  // class TipoRitenuta

}  // namespace felpa

#endif  // include guard
