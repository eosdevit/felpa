/**
 *  \file
 *  \remark This file is part of FELPA.
 *
 *  \copyright Copyright (C) 2015-2021 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#if !defined(FELPA_STABILE_ORGANIZZAZIONE_HPP)
#define      FELPA_STABILE_ORGANIZZAZIONE_HPP

#include "indirizzo_type.hpp"

namespace felpa
{

///
/// Utilizzato quando il cedente/prestatore e' un soggetto non residente ed
/// effettua una transazione tramite un'organizzazione residente sul territorio
/// nazionale.
///
class StabileOrganizzazione : public IndirizzoType
{
public:
  StabileOrganizzazione() : IndirizzoType("StabileOrganizzazione") {}

  bool empty() const
  {
    return indirizzo.empty() && numero_civico.empty() && cap.empty() &&
           comune.empty() && provincia.empty();
  }

  bool operator==(const StabileOrganizzazione &rhs) const
  {
    return IndirizzoType::operator==(rhs);
  }
};  // class StabileOrganizzazione

}  // namespace felpa

#endif  // Include guard
