/**
 *  \file
 *  \remark This file is part of FELPA.
 *
 *  \copyright Copyright (C) 2015-2021 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#if !defined(FELPA_DATI_RICEZIONE_HPP)
#define      FELPA_DATI_RICEZIONE_HPP

#include "dati_documenti_correlati_type.hpp"

namespace felpa
{

class DatiRicezione : public DatiDocumentiCorrelatiType
{
public:
  DatiRicezione() : DatiDocumentiCorrelatiType("DatiRicezione") {}

  using DatiDocumentiCorrelatiType::operator==;
};  // class DatiRicezione

}  // namespace felpa

#endif  // include guard
