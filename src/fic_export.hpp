/**
 *  \file
 *  \remark This file is part of FELPA.
 *
 *  \copyright Copyright (C) 2024 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#include <string>

#if !defined(FELPA_FIC_EXPORT_HPP)
#define      FELPA_FIC_EXPORT_HPP

namespace felpa
{

class FatturaElettronica;

namespace fic
{
[[nodiscard]] std::string export_json(class FatturaElettronica &);
[[nodiscard]] unsigned vat_id(double, const std::string & = "");
}  // namespace fic

}  // namespace felpa

#endif  // include guard
