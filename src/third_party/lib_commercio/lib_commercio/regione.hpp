/**
 *  \file
 *  \remark This file is part of libCommercio.
 *
 *  \copyright Copyright (C) 2023 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#include <cassert>
#include <map>
#include <regex>
#include <set>
#include <stdexcept>
#include <string>
#include <vector>

#include "utils.hpp"

#if !defined(LIB_COMMERCIO_REGIONE_HPP)
#define      LIB_COMMERCIO_REGIONE_HPP

namespace lib_commercio
{

///
/// Codici relativi alle regioni italiane.
///
class regione
{
public:
  explicit regione(const std::string &);

  struct info
  {
    /// Sebbene le funzioni di ricerca supportino le varianti con gli spazi
    /// ("Trentino Alto Adige", "Emilia Romagna", "Friuli Venezia Giulia"), la
    /// Costituzione prevede (Art. 131) il trattino nel nome per evidenziare
    /// distinte unita' territoriali.
    std::string nome {};

    /// Il codice ISTAT e' un identificativo numerico assegnato a fini
    /// statistici alle unita' amministrative italiane (esistenti o soppresse)
    /// da parte dell'Istituto nazionale di statistica. Il codice fu istituito
    /// nel 1966.
    unsigned istat {};

    [[nodiscard]] bool empty() const;
  };

  [[nodiscard]] static info trova(const std::string &);
  [[nodiscard]] static info trova(unsigned);

  [[nodiscard]] static bool verifica_nome(const std::string &);

private:
  std::string nome_;
};


/*********************************************************************
 * DETTAGLI DI IMPLEMENTAZIONE
 *********************************************************************/
namespace details
{
[[nodiscard]] inline const std::vector<regione::info> &db_regioni()
{
  static const std::vector<regione::info> db =
  {
    {"PIEMONTE", 1},
    {"VALLE D'AOSTA", 2},
    {"LOMBARDIA", 3},
    {"TRENTINO-ALTO ADIGE", 4},
    {"VENETO", 5},
    {"FRIULI-VENEZIA GIULIA", 6},
    {"LIGURIA", 7},
    {"EMILIA-ROMAGNA", 8},
    {"TOSCANA", 9},
    {"UMBRIA", 10},
    {"MARCHE", 11},
    {"LAZIO", 12},
    {"ABRUZZO", 13},
    {"MOLISE", 14},
    {"CAMPANIA", 15},
    {"PUGLIA", 16},
    {"BASILICATA", 17},
    {"CALABRIA", 18},
    {"SICILIA", 19},
    {"SARDEGNA", 20}
  };

  return db;
}

}  // namespace details

/*********************************************************************
 * IMPLEMENTAZIONE
 *********************************************************************/
///
/// Inizializza la regione.
///
/// \param[in] nome nome della regione
///
/// Il nome e' case insensitive e supporta le varianti con trattini e/o
/// spazi (per esempio "Trentino-Alto Adige" e "TRENTINO ALTO ADIGE").
///
inline regione::regione(const std::string &nome)
  : nome_(trova(nome).nome)
{
  if (nome_.empty())
    throw std::invalid_argument("Regione sconosciuta");
}

///
/// Trova i dati riguardanti la regione identificata da `nome`.
///
/// \param[in] nome nome della regione di interesse
/// \return         informazioni recuperate (puo' essere vuoto)
///
/// La ricerca e' case insensitive e supporta le varianti con trattini e/o
/// spazi (per esempio "Trentino-Alto Adige" e "TRENTINO ALTO ADIGE").
///
inline regione::info regione::trova(const std::string &nome)
{
  static std::map<std::string, std::size_t> nome_map;
  if (nome_map.empty())
    for (std::size_t i(0); i < details::db_regioni().size(); ++i)
      nome_map.emplace(details::db_regioni()[i].nome, i);

  const std::string un(toupper(nome));

  if (const auto it(nome_map.find(un)); it != nome_map.end())
    return details::db_regioni()[it->second];

  if (const std::size_t primo_spazio(un.find(' '));
      primo_spazio != std::string::npos)
  {
    std::string un1(un);
    un1[primo_spazio] = '-';

    if (const auto it(nome_map.find(un1)); it != nome_map.end())
      return details::db_regioni()[it->second];
  }

  if (const std::size_t primo_trattino(un.find('-'));
      primo_trattino != std::string::npos)
  {
    std::string un1(un);
    un1[primo_trattino] = ' ';

    if (const auto it(nome_map.find(un1)); it != nome_map.end())
      return details::db_regioni()[it->second];
  }

  return regione::info();
}

///
/// Trova i dati riguardanti la regione identificata da `num`.
///
/// \param[in] num codice ISTAT della regione di interesse
/// \return        informazioni recuperate (puo' essere vuoto)
///
inline regione::info regione::trova(unsigned num)
{
  static std::map<unsigned, std::size_t> istat_map;
  if (istat_map.empty())
    for (std::size_t i(0); i < details::db_regioni().size(); ++i)
      istat_map.emplace(details::db_regioni()[i].istat, i);

  const auto it(istat_map.find(num));

  return it == istat_map.end() ? regione::info()
                               : details::db_regioni()[it->second];
}

///
/// Verifica la validita' di un presunto nome regione.
///
/// \param[in] nome nome da verificare
/// \return         `true` se il codice e' valido
///
/// La verifica e' case insensitive e supporta le varianti con trattini e/o
/// spazi (per esempio "Trentino-Alto Adige" e "TRENTINO ALTO ADIGE").
///
inline bool regione::verifica_nome(const std::string &nome)
{
  return !trova(nome).nome.empty();
}

inline bool regione::info::empty() const
{
  return nome.empty();
}

}  // namespace lib_commercio

#endif  // include guard
