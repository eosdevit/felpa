/**
 *  \file
 *  \remark This file is part of FELPA.
 *
 *  \copyright Copyright (C) 2015-2021 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#include <cassert>

#include "dati_anagrafici_vettore.hpp"
#include "fattura_elettronica_eccezioni.hpp"
#include "fattura_elettronica_dettagli.hpp"
#include "utility.hpp"

namespace felpa
{

DatiAnagraficiVettore::DatiAnagraficiVettore()
  : DatiAnagraficiType("DatiAnagraficiVettore"), numero_licenza_guida()
{
}

bool DatiAnagraficiVettore::operator==(const DatiAnagraficiVettore &rhs) const
{
  return DatiAnagraficiType::operator==(rhs)
         && numero_licenza_guida == rhs.numero_licenza_guida;
}

namespace dettagli
{

constexpr char NUMERO_LICENZA_GUIDA_TAG[] = "NumeroLicenzaGuida";

constexpr std::size_t CODICE_FISCALE_SIZE = 20;

bool carica(tinyxml2::XMLConstHandle &ft, DatiAnagraficiVettore *dav)
{
  tinyxml2::XMLConstHandle h(ft.FirstChildElement(dav->tag().c_str()));
  if (!h.ToElement())
    return false;

  const bool da(carica(h, false, dav));

  if (auto *he = h.FirstChildElement(NUMERO_LICENZA_GUIDA_TAG).ToElement())
    dav->numero_licenza_guida =
      coalesce(he->GetText()).substr(0, CODICE_FISCALE_SIZE);

  return da;
}

void registra(tinyxml2::XMLPrinter &ft, const DatiAnagraficiVettore &dav)
{
  ft.OpenElement(dav.tag().c_str());

  registra(ft, dav, false);

  if (!dav.numero_licenza_guida.empty())
  {
    ft.OpenElement(NUMERO_LICENZA_GUIDA_TAG);
    push_text(ft, dav.numero_licenza_guida.substr(0, CODICE_FISCALE_SIZE));
    ft.CloseElement();
  }

  ft.CloseElement();
}

}  // namespace dettagli

}  // namespace felpa
