/**
 *  \file
 *  \remark This file is part of FELPA.
 *
 *  \copyright Copyright (C) 2015 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#if !defined(FELPA_FATTURA_ELETTRONICA_HEADER_HPP)
#define      FELPA_FATTURA_ELETTRONICA_HEADER_HPP

#include "dati_trasmissione.hpp"
#include "cedente_prestatore.hpp"
#include "cessionario_committente.hpp"
#include "soggetto_emittente.hpp"

namespace felpa
{

///
/// Costituisce la parte di *testata* del documento fattura e contiene i dati
/// delle parti che intervengono nell'operazione di cessione del bene o
/// prestazione del servizio, nonche' i dati necessari al Sistema di
/// Interscambio per identificare il soggetto che trasmette il file in
/// modalita' elettronica ed il destinatario al quale il file deve essere
/// recapitato.
///
class FatturaElettronicaHeader
{
public:
  explicit FatturaElettronicaHeader(formato_fattura);

  [[nodiscard]] bool empty() const;

  [[nodiscard]] static const std::string &tag();

  DatiTrasmissione             dati_trasmissione;
  CedentePrestatore           cedente_prestatore;
  CessionarioCommittente cessionario_committente;
  SoggettoEmittente           soggetto_emittente;
};  // class FatturaElettronicaHeader

[[nodiscard]] bool operator==(const FatturaElettronicaHeader &,
                              const FatturaElettronicaHeader &);

}  // namespace felpa

#endif  // include guard
