/**
 *  \file
 *  \remark This file is part of FELPA.
 *
 *  \copyright Copyright (C) 2015-2024 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#if !defined(FELPA_PERCENTUALE_HPP)
#define      FELPA_PERCENTUALE_HPP

#include <algorithm>

#include "numero_type.hpp"

namespace felpa
{

class Percentuale : public NumeroType
{
public:
  explicit Percentuale(const std::string &p = "")
    : NumeroType("Percentuale", 2, solo_positivo)
  {
    assign(p);

    if (!empty())
      *this = Percentuale(to_double());
  }

  explicit Percentuale(double p) : NumeroType("Percentuale", 2, solo_positivo)
  {
    p = std::clamp(p, 0.0, 100.0);

    assign(p);
  }

  Percentuale &operator=(const std::string &p)
  {
    return *this = Percentuale(p);
  }

  Percentuale &operator=(double p)
  {
    return *this = Percentuale(p);
  }
};  // class Percentuale

}  // namespace felpa

#endif  // include guard
