/**
 *  \file
 *  \remark This file is part of FELPA.
 *
 *  \copyright Copyright (C) 2015-2024 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN
#include "third_party/doctest/doctest.h"
#include "third_party/json/json.hpp"

#include <algorithm>
#include <iostream>
#include <filesystem>
#include <random>

#include "fattura_elettronica.hpp"

const std::string iban_valido("IT66C0100503382000000218020");
const std::string piva_non_valida("01234567890");
const std::string piva_valida("02188520544");
const std::string piva_valida2("00468990015");
const std::string piva_valida3("11359591002");

std::string clear_formatting(const std::string &in)
{
  std::istringstream ss(in);

  const auto trim([](const std::string &s)
  {
    auto front = std::find_if_not(s.begin(), s.end(),
                                  [](char c) { return std::isspace(c); });

    // The search is limited in the reverse direction to the last non-space
    // value found in the search in the forward direction.
    auto back = std::find_if_not(s.rbegin(),
                                 std::reverse_iterator<decltype(front)>(front),
                                 [](char c)
                                 {
                                   return std::isspace(c);
                                 }).base();

    return std::string(front, back);
  });

  std::string ret;
  for (std::string line; std::getline(ss, line); )
    ret += trim(line);

  return ret;
}

template<class IMPORTO> bool arrotondamento_2_decimali()
{
  IMPORTO v;

  for (int i(0); i < 100000; ++i)
  {
    auto vd(static_cast<double>(i) / 1000.0);

    v = vd;
    v = v.to_double() * 1000.0;

    int ri((i + (i > 0 ? 5 : -5)) / 10 * 10);

    if (static_cast<int>(v.to_double()) != ri)
      return false;
  }

  return true;
}

template<class C> bool carica(const tinyxml2::XMLPrinter &p, C *c)
{
  tinyxml2::XMLDocument doc;
  doc.Parse(p.CStr());
  tinyxml2::XMLConstHandle h(&doc);

  return felpa::dettagli::carica(h, c);
}

// Generatori di oggetti definiti nella libreria FELPA. In base ad un indice
// numerico restituiscono un oggetto valido per il debug.
template<class C> C oggetto_valido(unsigned = 0);
template<class C> std::string xml_oggetto_valido(unsigned = 0);
template<class C> std::string xml_oggetto_valido(unsigned, unsigned);

template<> felpa::Anagrafica oggetto_valido(unsigned id)
{
  felpa::Anagrafica a;

  switch (id)
  {
  case 0:
    a.denominazione = "MARIO ROSSI SRL";
    a.cod_eori = "ABCDEFGHILMNOPQ";
    break;

  case 1:
    a.nome = "MARIO";
    a.cognome = "ROSSI";
    break;
  }

  return a;
}

template<> std::string xml_oggetto_valido<felpa::Anagrafica>(unsigned id)
{
  std::string xml;

  switch (id)
  {
  case 0:
    xml = "<Anagrafica>"
          "<Denominazione>MARIO ROSSI SRL</Denominazione>"
          "<CodEORI>ABCDEFGHILMNOPQ</CodEORI>"
          "</Anagrafica>";
    break;

  case 1:
    xml = "<Anagrafica>"
          "<Nome>MARIO</Nome>"
          "<Cognome>ROSSI</Cognome>"
          "</Anagrafica>";
    break;
  }

  return xml;
}

template<> felpa::IdFiscaleIva oggetto_valido(unsigned id)
{
  felpa::IdFiscaleIva ifi;

  switch (id)
  {
  case 0:
    ifi.id_codice = piva_valida;
    break;
  case 1:
    ifi.id_codice = piva_valida3;
    break;
  case 2:
    ifi.id_codice = piva_valida2;
    break;
  }

  return ifi;
}

template<> std::string xml_oggetto_valido<felpa::IdFiscaleIva>(unsigned id)
{
  std::string xml;

  switch (id)
  {
  case 0:
    xml = "<IdFiscaleIVA>"
          "<IdPaese>IT</IdPaese>"
          "<IdCodice>" +  piva_valida + "</IdCodice>"
          "</IdFiscaleIVA>";
    break;
  case 1:
    xml = "<IdFiscaleIVA>"
          "<IdPaese>IT</IdPaese>"
          "<IdCodice>" +  piva_valida3 + "</IdCodice>"
          "</IdFiscaleIVA>";
    break;
  case 2:
    xml = "<IdFiscaleIVA>"
          "<IdPaese>IT</IdPaese>"
          "<IdCodice>" +  piva_valida2 + "</IdCodice>"
          "</IdFiscaleIVA>";
    break;
  }

  return xml;
}

template<> felpa::DatiSAL oggetto_valido(unsigned id)
{
  felpa::DatiSAL sal;

  switch (id)
  {
  case 0:
    sal.riferimento_fase = 1;
    break;

  case 1:
    sal.riferimento_fase = 999;
    break;
  }

  return sal;
}

template<> std::string xml_oggetto_valido<felpa::DatiSAL>(unsigned id)
{
  std::string xml;

  switch (id)
  {
  case 0:
    xml = "<DatiSAL>"
          "<RiferimentoFase>1</RiferimentoFase>"
          "</DatiSAL>";
    break;

  case 1:
    xml = "<DatiSAL>"
          "<RiferimentoFase>999</RiferimentoFase>"
          "</DatiSAL>";
    break;
  }

  return xml;
}

template<> felpa::ScontoMaggiorazione oggetto_valido(unsigned id)
{
  felpa::ScontoMaggiorazione s;

  switch (id)
  {
  case 0:
    s.tipo = "SC";
    s.importo = 0.5;
    break;

  case 1:
    s.tipo = "SC";
    s.percentuale = 8.0;
    break;
  }

  return s;
}

template<> std::string xml_oggetto_valido<felpa::ScontoMaggiorazione>(
  unsigned id)
{
  std::string xml;

  switch (id)
  {
  case 0:
    xml = "<ScontoMaggiorazione>"
          "<Tipo>SC</Tipo>"
          "<Importo>0.50</Importo>"
          "</ScontoMaggiorazione>";
    break;

  case 1:
    xml = "<ScontoMaggiorazione>"
          "<Tipo>SC</Tipo>"
          "<Percentuale>8.00</Percentuale>"
          "</ScontoMaggiorazione>";
    break;
  }

  return xml;
}

template<> felpa::Sede oggetto_valido(unsigned id)
{
  felpa::Sede s;

  switch (id)
  {
  case 0:
    s.cap = "56127";
    s.comune = "PISA";
    s.indirizzo = "VIA STRADA";
    s.numero_civico = "1";
    s.provincia = "PI";
    break;

  case 1:
    s.cap = "07100";
    s.comune = "SASSARI";
    s.indirizzo = "VIALE ROMA 543";
    s.provincia = "SS";
    break;

  case 2:
    s.indirizzo = "VIA TORINO 38-B";
    s.cap = "00145";
    s.comune = "ROMA";
    s.provincia = "RM";
    break;
  }

  return s;
}

template<> std::string xml_oggetto_valido<felpa::Sede>(unsigned id)
{
  std::string xml;

  switch (id)
  {
  case 0:
    xml = "<Sede>"
          "<Indirizzo>VIA STRADA</Indirizzo>"
          "<NumeroCivico>1</NumeroCivico>"
          "<CAP>56127</CAP>"
          "<Comune>PISA</Comune>"
          "<Provincia>PI</Provincia>"
          "<Nazione>IT</Nazione>"
          "</Sede>";
    break;

  case 1:
    xml = "<Sede>"
          "<Indirizzo>VIALE ROMA 543</Indirizzo>"
          "<CAP>07100</CAP>"
          "<Comune>SASSARI</Comune>"
          "<Provincia>SS</Provincia>"
          "<Nazione>IT</Nazione>"
          "</Sede>";
    break;

  case 2:
    xml = "<Sede>"
          "<Indirizzo>VIA TORINO 38-B</Indirizzo>"
          "<CAP>00145</CAP>"
          "<Comune>ROMA</Comune>"
          "<Provincia>RM</Provincia>"
          "<Nazione>IT</Nazione>"
          "</Sede>";
    break;
  }

  return xml;
}

template<> felpa::AltriDatiGestionali oggetto_valido(unsigned id)
{
  felpa::AltriDatiGestionali a;

  switch (id)
  {
  case 0:
    a.tipo_dato = "ECOBONUS";
    a.riferimento_testo = "Art. 121, DL 34 del 2020";
    break;

  case 1:
    a.tipo_dato = "TARGA";
    a.riferimento_testo = "AB123CD";
    break;

  case 2:
    a.tipo_dato = "SCONTRINO";
    a.riferimento_testo = "S1234";
    a.riferimento_numero = "10.00";
    a.riferimento_data = "2021-03-18";
    break;
  }

  return a;
}

template<> std::string xml_oggetto_valido<felpa::AltriDatiGestionali>(
  unsigned id)
{
  std::string xml;

  switch (id)
  {
  case 0:
    xml = "<AltriDatiGestionali>"
          "<TipoDato>ECOBONUS</TipoDato>"
          "<RiferimentoTesto>Art. 121, DL 34 del 2020</RiferimentoTesto>"
          "</AltriDatiGestionali>";
    break;

  case 1:
    xml = "<AltriDatiGestionali>"
          "<TipoDato>TARGA</TipoDato>"
          "<RiferimentoTesto>AB123CD</RiferimentoTesto>"
          "</AltriDatiGestionali>";
    break;

  case 2:
    xml = "<AltriDatiGestionali>"
          "<TipoDato>SCONTRINO</TipoDato>"
          "<RiferimentoTesto>S1234</RiferimentoTesto>"
          "<RiferimentoNumero>10.00</RiferimentoNumero>"
          "<RiferimentoData>2021-03-18</RiferimentoData>"
          "</AltriDatiGestionali>";
    break;
  }

  return xml;
}

template<> felpa::DettaglioPagamento oggetto_valido(unsigned id)
{
  felpa::DettaglioPagamento dp;

  switch (id)
  {
  case 0:
    dp.modalita_pagamento = "MP01";
    dp.data_scadenza_pagamento = "2015-01-30";
    dp.importo_pagamento = 25.0;
    break;

  case 1:
    dp.beneficiario = "MARIO ROSSI";
    dp.data_scadenza_pagamento = "2021-12-01";
    dp.istituto_finanziario = "BANCA POPOLARE DEL RE";
    dp.iban = iban_valido;
    dp.modalita_pagamento = "MP01";
    dp.importo_pagamento = 100.0;
    break;

  case 2:
    dp.beneficiario = "MARIO ROSSI";
    dp.istituto_finanziario = "BANCA POPOLARE DEL RE";
    dp.modalita_pagamento = "MP02";
    dp.data_scadenza_pagamento = "2021-01-20";
    dp.importo_pagamento = 75.0;
    break;
  }

  return dp;
}

template<> std::string xml_oggetto_valido<felpa::DettaglioPagamento>(
  unsigned id)
{
  std::string xml;

  switch (id)
  {
  case 0:
    xml = "<DettaglioPagamento>"
          "<ModalitaPagamento>MP01</ModalitaPagamento>"
          "<DataScadenzaPagamento>2015-01-30</DataScadenzaPagamento>"
          "<ImportoPagamento>25.00</ImportoPagamento>"
          "</DettaglioPagamento>";
    break;

  case 1:
    xml = "<DettaglioPagamento>"
          "<Beneficiario>MARIO ROSSI</Beneficiario>"
          "<ModalitaPagamento>MP01</ModalitaPagamento>"
          "<DataScadenzaPagamento>2021-12-01</DataScadenzaPagamento>"
          "<ImportoPagamento>100.00</ImportoPagamento>"
          "<IstitutoFinanziario>BANCA POPOLARE DEL RE</IstitutoFinanziario>"
          "</DettaglioPagamento>";
    // IBAN non richiesto e quindi non registrato anche se presente.
    break;

  case 2:
    xml = "<DettaglioPagamento>"
          "<Beneficiario>MARIO ROSSI</Beneficiario>"
          "<ModalitaPagamento>MP02</ModalitaPagamento>"
          "<DataScadenzaPagamento>2021-01-20</DataScadenzaPagamento>"
          "<ImportoPagamento>75.00</ImportoPagamento>"
          "<IstitutoFinanziario>BANCA POPOLARE DEL RE</IstitutoFinanziario>"
          "</DettaglioPagamento>";
    break;
  }

  return xml;
}

template<> felpa::DatiPagamento oggetto_valido(unsigned id)
{
  felpa::DatiPagamento dp;

  switch (id)
  {
  case 0:
    dp.condizioni_pagamento = "TP01";
    dp.dettaglio_pagamento =
    {
      oggetto_valido<felpa::DettaglioPagamento>(0),
      oggetto_valido<felpa::DettaglioPagamento>(2)
    };
    break;

  case 1:
    dp.condizioni_pagamento = "TP01";
    dp.dettaglio_pagamento = {oggetto_valido<felpa::DettaglioPagamento>(0)};
    break;
  }

  return dp;
}

template<> std::string xml_oggetto_valido<felpa::DatiPagamento>(
  unsigned id)
{
  std::string xml;

  switch (id)
  {
  case 0:
    xml = "<DatiPagamento>"
          "<CondizioniPagamento>TP01</CondizioniPagamento>"
          + xml_oggetto_valido<felpa::DettaglioPagamento>(0)
          + xml_oggetto_valido<felpa::DettaglioPagamento>(2)
          + "</DatiPagamento>";
    break;

  case 1:
    xml = "<DatiPagamento>"
          "<CondizioniPagamento>TP01</CondizioniPagamento>"
          + xml_oggetto_valido<felpa::DettaglioPagamento>(0)
          + "</DatiPagamento>";
    break;
  }

  return xml;
}

template<> felpa::DatiContratto oggetto_valido(unsigned id)
{
  felpa::DatiContratto dc;

  switch (id)
  {
  case 0:
    dc.riferimento_numero_linea.insert(1);
    dc.id_documento = "123";
    dc.data = "2012-09-01";
    dc.num_item = "5";
    dc.codice_cup = "123abc";
    dc.codice_cig = "456def";
    break;

  case 1:
  case 2:
  case 3:
  case 4:
    dc.id_documento = "C" + std::to_string(2345 + id);
    dc.riferimento_numero_linea = {2 + id, 3 + id , 4 + id, 5 + id};
    dc.data = "2021-01-02";
    dc.num_item = std::to_string(2222 + id);
    dc.codice_commessa_convenzione = "ABS99";
    dc.codice_cup = "C11G06000000002";
    dc.codice_cig = "B" + std::to_string(13 + id);
    break;
  }

  return dc;
}

template<> std::string xml_oggetto_valido<felpa::DatiContratto>(
  unsigned id)
{
  std::string xml;

  switch (id)
  {
  case 0:
    xml = "<DatiContratto>"
          "<RiferimentoNumeroLinea>1</RiferimentoNumeroLinea>"
          "<IdDocumento>123</IdDocumento>"
          "<Data>2012-09-01</Data>"
          "<NumItem>5</NumItem>"
          "<CodiceCUP>123abc</CodiceCUP>"
          "<CodiceCIG>456def</CodiceCIG>"
          "</DatiContratto>";
    break;

  case 1:
  case 2:
  case 3:
  case 4:
    xml =
    "<DatiContratto>"
    "<RiferimentoNumeroLinea>"+std::to_string(2+id)+"</RiferimentoNumeroLinea>"
    "<RiferimentoNumeroLinea>"+std::to_string(3+id)+"</RiferimentoNumeroLinea>"
    "<RiferimentoNumeroLinea>"+std::to_string(4+id)+"</RiferimentoNumeroLinea>"
    "<RiferimentoNumeroLinea>"+std::to_string(5+id)+"</RiferimentoNumeroLinea>"
    "<IdDocumento>C" + std::to_string(2345 + id) + "</IdDocumento>"
    "<Data>2021-01-02</Data>"
    "<NumItem>" + std::to_string(2222 + id) + "</NumItem>"
    "<CodiceCommessaConvenzione>ABS99</CodiceCommessaConvenzione>"
    "<CodiceCUP>C11G06000000002</CodiceCUP>"
    "<CodiceCIG>B" + std::to_string(13 + id) + "</CodiceCIG>"
    "</DatiContratto>";
    break;
  }

  return xml;
}

template<> felpa::DatiConvenzione oggetto_valido(unsigned id)
{
  felpa::DatiConvenzione dc;

  switch (id)
  {
  case 0:
    dc.riferimento_numero_linea.insert(1);
    dc.id_documento = "123";
    dc.num_item = "5";
    dc.codice_cup = "123abc";
    dc.codice_cig = "456def";
    break;

  case 1:
  case 2:
  case 3:
  case 4:
    dc.id_documento = "CV" + std::to_string(3456 + id);
    dc.riferimento_numero_linea = {3 + id, 4 + id , 5 + id, 6 + id};
    dc.data = "2021-01-03";
    dc.num_item = std::to_string(9876 + id);
    dc.codice_commessa_convenzione = "007";
    dc.codice_cup = "C11G06000000003";
    dc.codice_cig = "Z" + std::to_string(9 + id);
    break;
  }

  return dc;
}

template<> std::string xml_oggetto_valido<felpa::DatiConvenzione>(
  unsigned id)
{
  std::string xml;

  switch (id)
  {
  case 0:
    xml = "<DatiConvenzione>"
          "<RiferimentoNumeroLinea>1</RiferimentoNumeroLinea>"
          "<IdDocumento>123</IdDocumento>"
          "<NumItem>5</NumItem>"
          "<CodiceCUP>123abc</CodiceCUP>"
          "<CodiceCIG>456def</CodiceCIG>"
          "</DatiConvenzione>";
    break;

  case 1:
  case 2:
  case 3:
  case 4:
    xml =
    "<DatiConvenzione>"
    "<RiferimentoNumeroLinea>"+std::to_string(3+id)+"</RiferimentoNumeroLinea>"
    "<RiferimentoNumeroLinea>"+std::to_string(4+id)+"</RiferimentoNumeroLinea>"
    "<RiferimentoNumeroLinea>"+std::to_string(5+id)+"</RiferimentoNumeroLinea>"
    "<RiferimentoNumeroLinea>"+std::to_string(6+id)+"</RiferimentoNumeroLinea>"
    "<IdDocumento>CV" + std::to_string(3456 + id) + "</IdDocumento>"
    "<Data>2021-01-03</Data>"
    "<NumItem>" + std::to_string(9876 + id) + "</NumItem>"
    "<CodiceCommessaConvenzione>007</CodiceCommessaConvenzione>"
    "<CodiceCUP>C11G06000000003</CodiceCUP>"
    "<CodiceCIG>Z" + std::to_string(9 + id) + "</CodiceCIG>"
    "</DatiConvenzione>";
  }

  return xml;
}

template<> felpa::DatiFattureCollegate oggetto_valido(unsigned id)
{
  felpa::DatiFattureCollegate d;

  switch (id)
  {
  case 0:
  case 1:
  case 2:
  case 3:
  case 4:
    d.id_documento = "F" + std::to_string(100 + id);
    d.riferimento_numero_linea = {1 + id, 2 + id , 3 + id, 4 + id};
    d.data = "2021-02-01";
    d.num_item = std::to_string(1 + id);
    d.codice_commessa_convenzione = "B52";
    d.codice_cup = "C11G06000000007";
    d.codice_cig = "X" + std::to_string(1 + id);
    break;
  }

  return d;
}

template<> std::string xml_oggetto_valido<felpa::DatiFattureCollegate>(
  unsigned id)
{
  std::string xml;

  switch (id)
  {
  case 0:
  case 1:
  case 2:
  case 3:
  case 4:
    xml =
    "<DatiFattureCollegate>"
    "<RiferimentoNumeroLinea>"+std::to_string(1+id)+"</RiferimentoNumeroLinea>"
    "<RiferimentoNumeroLinea>"+std::to_string(2+id)+"</RiferimentoNumeroLinea>"
    "<RiferimentoNumeroLinea>"+std::to_string(3+id)+"</RiferimentoNumeroLinea>"
    "<RiferimentoNumeroLinea>"+std::to_string(4+id)+"</RiferimentoNumeroLinea>"
    "<IdDocumento>F" + std::to_string(100 + id) + "</IdDocumento>"
    "<Data>2021-02-01</Data>"
    "<NumItem>" + std::to_string(1 + id) + "</NumItem>"
    "<CodiceCommessaConvenzione>B52</CodiceCommessaConvenzione>"
    "<CodiceCUP>C11G06000000007</CodiceCUP>"
    "<CodiceCIG>X" + std::to_string(1 + id) + "</CodiceCIG>"
    "</DatiFattureCollegate>";
    break;
  }

  return xml;
}

template<> felpa::DatiDDT oggetto_valido(unsigned id)
{
  felpa::DatiDDT d;

  d.numero_ddt               = "K" + std::to_string(1234 + id);
  d.data_ddt                 = "2018-12-18";
  d.riferimento_numero_linea = {1 + id, 2 + id};

  return d;
}

template<> std::string xml_oggetto_valido<felpa::DatiDDT>(
  unsigned id)
{
  return
    "<DatiDDT>"
    "<NumeroDDT>K" + std::to_string(1234 + id) + "</NumeroDDT>"
    "<DataDDT>2018-12-18</DataDDT>"
    "<RiferimentoNumeroLinea>"+std::to_string(1+id)+"</RiferimentoNumeroLinea>"
    "<RiferimentoNumeroLinea>"+std::to_string(2+id)+"</RiferimentoNumeroLinea>"
    "</DatiDDT>";
}

template<> felpa::DatiOrdineAcquisto oggetto_valido(unsigned id)
{
  felpa::DatiOrdineAcquisto doa;

  switch (id)
  {
  case 0:
    doa.id_documento = "66685";
    doa.riferimento_numero_linea = {1};
    doa.num_item = "1";
    doa.codice_cup = "123abc";
    doa.codice_cig = "456def";
    break;

  case 1:
  case 2:
  case 3:
  case 4:
    doa.id_documento = "O" + std::to_string(1234 + id);
    doa.riferimento_numero_linea = {1 + id, 2 + id , 3 + id, 4 + id};
    doa.data = "2021-01-01";
    doa.num_item = std::to_string(1111 + id);
    doa.codice_commessa_convenzione = "CAI23";
    doa.codice_cup = "C11G06000000001";
    doa.codice_cig = "A" + std::to_string(12 + id);
  }

  return doa;
}

template<> std::string xml_oggetto_valido<felpa::DatiOrdineAcquisto>(
  unsigned id)
{
  std::string xml;

  switch (id)
  {
  case 0:
    xml = "<DatiOrdineAcquisto>"
          "<RiferimentoNumeroLinea>1</RiferimentoNumeroLinea>"
          "<IdDocumento>66685</IdDocumento>"
          "<NumItem>1</NumItem>"
          "<CodiceCUP>123abc</CodiceCUP>"
          "<CodiceCIG>456def</CodiceCIG>"
          "</DatiOrdineAcquisto>";
    break;

  case 1:
  case 2:
  case 3:
  case 4:
    xml =
    "<DatiOrdineAcquisto>"
    "<RiferimentoNumeroLinea>"+std::to_string(1+id)+"</RiferimentoNumeroLinea>"
    "<RiferimentoNumeroLinea>"+std::to_string(2+id)+"</RiferimentoNumeroLinea>"
    "<RiferimentoNumeroLinea>"+std::to_string(3+id)+"</RiferimentoNumeroLinea>"
    "<RiferimentoNumeroLinea>"+std::to_string(4+id)+"</RiferimentoNumeroLinea>"
    "<IdDocumento>O" + std::to_string(1234 + id) + "</IdDocumento>"
    "<Data>2021-01-01</Data>"
    "<NumItem>" + std::to_string(1111 + id) + "</NumItem>"
    "<CodiceCommessaConvenzione>CAI23</CodiceCommessaConvenzione>"
    "<CodiceCUP>C11G06000000001</CodiceCUP>"
    "<CodiceCIG>A" + std::to_string(12 + id) + "</CodiceCIG>"
    "</DatiOrdineAcquisto>";
    break;
  }

  return xml;
}

template<> felpa::DatiRicezione oggetto_valido(unsigned id)
{
  felpa::DatiRicezione dr;

  switch (id)
  {
  case 0:
  case 1:
  case 2:
  case 3:
  case 4:
    dr.id_documento = "R" + std::to_string(3456 + id);
    dr.riferimento_numero_linea = {1 + id, 2 + id , 3 + id};
    dr.data = "2021-01-04";
    dr.num_item = std::to_string(1 + id);
    dr.codice_commessa_convenzione = "XYZ";
    dr.codice_cup = "C11G06000000004";
    dr.codice_cig = "W" + std::to_string(1 + id);
    break;
  }

  return dr;
}

template<> std::string xml_oggetto_valido<felpa::DatiRicezione>(
  unsigned id)
{
  std::string xml;

  switch (id)
  {
  case 0:
  case 1:
  case 2:
  case 3:
  case 4:
    xml =
    "<DatiRicezione>"
    "<RiferimentoNumeroLinea>"+std::to_string(1+id)+"</RiferimentoNumeroLinea>"
    "<RiferimentoNumeroLinea>"+std::to_string(2+id)+"</RiferimentoNumeroLinea>"
    "<RiferimentoNumeroLinea>"+std::to_string(3+id)+"</RiferimentoNumeroLinea>"
    "<IdDocumento>R" + std::to_string(3456 + id) + "</IdDocumento>"
    "<Data>2021-01-04</Data>"
    "<NumItem>" + std::to_string(1 + id) + "</NumItem>"
    "<CodiceCommessaConvenzione>XYZ</CodiceCommessaConvenzione>"
    "<CodiceCUP>C11G06000000004</CodiceCUP>"
    "<CodiceCIG>W" + std::to_string(1 + id) + "</CodiceCIG>"
    "</DatiRicezione>";
    break;
  }

  return xml;
}

template<> felpa::DatiRitenuta oggetto_valido(unsigned id)
{
  felpa::DatiRitenuta dr;

  switch (id)
  {
  case 0:
    dr.tipo_ritenuta = felpa::TipoRitenuta::PERSONE_FISICHE;
    dr.importo_ritenuta = 10.00;
    dr.aliquota_ritenuta = 20.0;
    dr.causale_pagamento = "B";
    break;
  case 1:
    dr.tipo_ritenuta = felpa::TipoRitenuta::PERSONE_FISICHE;
    dr.importo_ritenuta = 123.00;
    dr.aliquota_ritenuta = "20.0";
    dr.causale_pagamento = "A";
    break;
  case 2:
    dr.tipo_ritenuta = felpa::TipoRitenuta::PERSONE_FISICHE;
    dr.importo_ritenuta = 4.00;
    dr.aliquota_ritenuta = "20.0";
    dr.causale_pagamento = "A";
    break;
  }

  return dr;
}

template<> std::string xml_oggetto_valido<felpa::DatiRitenuta>(unsigned id)
{
  std::string xml;

  switch (id)
  {
  case 0:
    xml = "<DatiRitenuta>"
          "<TipoRitenuta>RT01</TipoRitenuta>"
          "<ImportoRitenuta>10.00</ImportoRitenuta>"
          "<AliquotaRitenuta>20.00</AliquotaRitenuta>"
          "<CausalePagamento>B</CausalePagamento>"
          "</DatiRitenuta>";
    break;
  case 1:
    xml = "<DatiRitenuta>"
          "<TipoRitenuta>RT01</TipoRitenuta>"
          "<ImportoRitenuta>123.00</ImportoRitenuta>"
          "<AliquotaRitenuta>20.00</AliquotaRitenuta>"
          "<CausalePagamento>A</CausalePagamento>"
          "</DatiRitenuta>";
    break;
  case 2:
    xml = "<DatiRitenuta>"
          "<TipoRitenuta>RT01</TipoRitenuta>"
          "<ImportoRitenuta>4.00</ImportoRitenuta>"
          "<AliquotaRitenuta>20.00</AliquotaRitenuta>"
          "<CausalePagamento>A</CausalePagamento>"
          "</DatiRitenuta>";
    break;
  }

  return xml;
}

template<> felpa::DatiAnagraficiCedente oggetto_valido(unsigned id)
{
  felpa::DatiAnagraficiCedente dac;

  switch (id)
  {
  case 0:
    dac.id_fiscale_iva.id_codice = piva_valida;
    dac.anagrafica.denominazione = "SOCIETA' ALPHA SRL";
    dac.regime_fiscale = "RF19";
    break;

  case 1:
    dac.id_fiscale_iva.id_codice = piva_valida;
    dac.anagrafica.denominazione = "SOCIETA' BETA SAS";
    dac.albo_professionale = "Albo Avvocati";
    dac.provincia_albo = "PI";
    dac.numero_iscrizione_albo = "12345";
    dac.regime_fiscale = "RF19";
    break;
  }

  return dac;
}

template<> std::string xml_oggetto_valido<felpa::DatiAnagraficiCedente>(
  unsigned id)
{
  std::string xml;

  switch (id)
  {
  case 0:
    xml =
    "<DatiAnagrafici>"
    + xml_oggetto_valido<felpa::IdFiscaleIva>(0) +
    "<Anagrafica>"
    "<Denominazione>SOCIETA' ALPHA SRL</Denominazione>"
    "</Anagrafica>"
    "<RegimeFiscale>RF19</RegimeFiscale>"
    "</DatiAnagrafici>";
    break;

  case 1:
    xml =
    "<DatiAnagrafici>"
    + xml_oggetto_valido<felpa::IdFiscaleIva>(0) +
    "<Anagrafica>"
    "<Denominazione>SOCIETA' BETA SAS</Denominazione>"
    "</Anagrafica>"
    "<AlboProfessionale>Albo Avvocati</AlboProfessionale>"
    "<ProvinciaAlbo>PI</ProvinciaAlbo>"
    "<NumeroIscrizioneAlbo>12345</NumeroIscrizioneAlbo>"
    "<RegimeFiscale>RF19</RegimeFiscale>"
    "</DatiAnagrafici>";
    break;
  }

  return xml;
}

template<> felpa::DatiAnagraficiVettore oggetto_valido(unsigned id)
{
  felpa::DatiAnagraficiVettore dav;

  switch (id)
  {
  case 0:
    dav.id_fiscale_iva.id_codice = piva_valida;
    dav.anagrafica.denominazione = "CHL" + std::to_string(1 + id);
    dav.codice_fiscale = piva_valida;
    dav.numero_licenza_guida = "PI" + std::to_string(123456 + id);
    break;

  case 1:
    dav.id_fiscale_iva.id_codice = piva_valida3;
    dav.anagrafica.denominazione = "Trasporto spa";
    break;
  }

  return dav;
}

template<> std::string xml_oggetto_valido<felpa::DatiAnagraficiVettore>(
  unsigned id)
{
  std::string xml;

  switch (id)
  {
  case 0:
    xml =
    "<DatiAnagraficiVettore>"
    + xml_oggetto_valido<felpa::IdFiscaleIva>(0) +
    "<CodiceFiscale>" +  piva_valida + "</CodiceFiscale>"
    "<Anagrafica>"
    "<Denominazione>CHL1</Denominazione>"
    "</Anagrafica>"
    "<NumeroLicenzaGuida>PI123456</NumeroLicenzaGuida>"
    "</DatiAnagraficiVettore>";
    break;

  case 1:
    xml = "<DatiAnagraficiVettore>"
          + xml_oggetto_valido<felpa::IdFiscaleIva>(1) +
          "<Anagrafica>"
          "<Denominazione>Trasporto spa</Denominazione>"
          "</Anagrafica>"
          "</DatiAnagraficiVettore>";
    break;
  }

  return xml;
}

template<> felpa::DatiTrasporto oggetto_valido(unsigned id)
{
  felpa::DatiTrasporto d;

  d.dati_anagrafici_vettore = oggetto_valido<felpa::DatiAnagraficiVettore>(id);

  switch (id)
  {
  case 0:
    d.causale_trasporto = "Vendita";
    d.numero_colli = 3;
    d.data_ora_consegna = "2021-01-21T21:21:21";
    break;

  case 1:
    d.data_ora_consegna = "2012-10-22T16:46:12.000+02:00";
    break;
  }

  return d;
}

template<> std::string xml_oggetto_valido<felpa::DatiTrasporto>(unsigned id)
{
  std::string xml;

  switch (id)
  {
  case 0:
    xml = "<DatiTrasporto>"
          + xml_oggetto_valido<felpa::DatiAnagraficiVettore>(id)
          + "<CausaleTrasporto>Vendita</CausaleTrasporto>"
          "<NumeroColli>3</NumeroColli>"
          "<DataOraConsegna>2021-01-21T21:21:21</DataOraConsegna>"
          "</DatiTrasporto>";
    break;

  case 1:
    xml = "<DatiTrasporto>"
          + xml_oggetto_valido<felpa::DatiAnagraficiVettore>(id)
          + "<DataOraConsegna>2012-10-22T16:46:12.000+02:00</DataOraConsegna>"
          "</DatiTrasporto>";
    break;
  }

  return xml;
}

template<> felpa::CessionarioCommittente oggetto_valido(unsigned id)
{
  felpa::CessionarioCommittente cc;

  switch (id)
  {
  case 0:
    cc.dati_anagrafici.id_fiscale_iva.id_codice = piva_valida;
    cc.dati_anagrafici.anagrafica.denominazione = "SOCIETA' ALPHA SRL";
    cc.sede = oggetto_valido<felpa::Sede>(1);
    break;

  case 1:
    cc.dati_anagrafici.codice_fiscale = piva_valida2;
    cc.dati_anagrafici.anagrafica.denominazione = "AMMINISTRAZIONE BETA";
    cc.sede = oggetto_valido<felpa::Sede>(2);
    break;
  }

  return cc;
}

template<> std::string xml_oggetto_valido<felpa::CessionarioCommittente>(
  unsigned id)
{
  std::string xml;

  switch (id)
  {
  case 0:
    xml = "<CessionarioCommittente>"
          "<DatiAnagrafici>"
          + xml_oggetto_valido<felpa::IdFiscaleIva>(0) +
          "<Anagrafica>"
          "<Denominazione>SOCIETA' ALPHA SRL</Denominazione>"
          "</Anagrafica>"
          "</DatiAnagrafici>"
          + xml_oggetto_valido<felpa::Sede>(1)
          + "</CessionarioCommittente>";
    break;

  case 1:
    xml = "<CessionarioCommittente>"
          "<DatiAnagrafici>"
          "<CodiceFiscale>" + piva_valida2 + "</CodiceFiscale>"
          "<Anagrafica>"
          "<Denominazione>AMMINISTRAZIONE BETA</Denominazione>"
          "</Anagrafica>"
          "</DatiAnagrafici>"
          + xml_oggetto_valido<felpa::Sede>(2)
          + "</CessionarioCommittente>";
    break;
  }

  return xml;
}


template<> felpa::CedentePrestatore oggetto_valido(unsigned id)
{
  felpa::CedentePrestatore cp;

  switch (id)
  {
  case 0:
    cp.dati_anagrafici = oggetto_valido<felpa::DatiAnagraficiCedente>(0);
    cp.sede = oggetto_valido<felpa::Sede>(1);
    break;
  }

  return cp;
}

template<> std::string xml_oggetto_valido<felpa::CedentePrestatore>(
  unsigned id)
{
  std::string xml;

  switch (id)
  {
  case 0:
    xml = "<CedentePrestatore>"
          + xml_oggetto_valido<felpa::DatiAnagraficiCedente>(0)
          + xml_oggetto_valido<felpa::Sede>(1)
          + "</CedentePrestatore>";
    break;
  }

  return xml;
}

template<> felpa::DatiGeneraliDocumento oggetto_valido(unsigned id)
{
  felpa::DatiGeneraliDocumento dg;

  switch (id)
  {
  case 0:
    dg.tipo_documento = "TD02";
    dg.divisa = "EUR";
    dg.data = "2021-01-30";
    dg.numero = "E123";

    dg.dati_ritenuta = { oggetto_valido<felpa::DatiRitenuta>(id) };

    dg.dati_bollo.bollo_virtuale = felpa::DatiBollo::SI;
    dg.dati_bollo.importo_bollo = 2.00;

    dg.importo_totale_documento = 66.0;
    dg.causale = "CAUSALE";
    break;

  case 1:
    dg.data = "2014-12-18";
    dg.numero = "123";

    dg.dati_ritenuta = { oggetto_valido<felpa::DatiRitenuta>(id) };

    dg.causale =
    "LA FATTURA FA RIFERIMENTO AD UNA OPERAZIONE AAAA BBBBBBBBBBBBBBBBBB CCC "
    "DDDDDDDDDDDDDDD E FFFFFFFFFFFFFFFFFFFF GGGGGGGGGG HHHHHHH II "
    "LLLLLLLLLLLLLLLLL MMM NNNNN OO PPPPPPPPPPP QQQQ RRRR SSSSSSSSSSSSSS"
    "SEGUE DESCRIZIONE CAUSALE NEL CASO IN CUI NON SIANO STATI SUFFICIENTI "
    "200 CARATTERI AAAAAAAAAAA BBBBBBBBBBBBBBBBB";
    break;

  case 2:
    dg.data = "2014-12-18";
    dg.numero = "123";

    dg.dati_ritenuta = { oggetto_valido<felpa::DatiRitenuta>(id) };
    dg.importo_totale_documento = 30.5;

    dg.causale =
    "LA FATTURA FA RIFERIMENTO AD UNA OPERAZIONE AAAA BBBBBBBBBBBBBBBBBB CCC "
    "DDDDDDDDDDDDDDD E FFFFFFFFFFFFFFFFFFFF GGGGGGGGGG HHHHHHH II "
    "LLLLLLLLLLLLLLLLL MMM NNNNN OO PPPPPPPPPPP QQQQ RRRR SSSSSSSSSSSSSS"
    "SEGUE DESCRIZIONE CAUSALE NEL CASO IN CUI NON SIANO STATI SUFFICIENTI "
    "200 CARATTERI AAAAAAAAAAA BBBBBBBBBBBBBBBBB";
    break;

  case 3:
    dg.tipo_documento = "TD01";
    dg.divisa = "EUR";
    dg.data = "2021-03-18";
    dg.numero = "E.9875";

    dg.importo_totale_documento = 1000.0;
    dg.causale = "EFFICIENTAMENTO ENERGETICO";

    dg.sconto_maggiorazione = { oggetto_valido<felpa::ScontoMaggiorazione>(0) };
    break;
  }

  return dg;
}

template<> std::string xml_oggetto_valido<felpa::DatiGeneraliDocumento>(
  unsigned id)
{
  std::string xml;

  switch (id)
  {
  case 0:
    xml = "<DatiGeneraliDocumento>"
          "<TipoDocumento>TD02</TipoDocumento>"
          "<Divisa>EUR</Divisa>"
          "<Data>2021-01-30</Data>"
          "<Numero>E123</Numero>"
          + xml_oggetto_valido<felpa::DatiRitenuta>(id)
          + "<DatiBollo>"
          "<BolloVirtuale>SI</BolloVirtuale>"
          "<ImportoBollo>2.00</ImportoBollo>"
          "</DatiBollo>"
          "<ImportoTotaleDocumento>66.00</ImportoTotaleDocumento>"
          "<Causale>CAUSALE</Causale>"
          "</DatiGeneraliDocumento>";
    break;

  case 1:
    xml = "<DatiGeneraliDocumento>"
          "<TipoDocumento>TD01</TipoDocumento>"
          "<Divisa>EUR</Divisa>"
          "<Data>2014-12-18</Data>"
          "<Numero>123</Numero>"
          + xml_oggetto_valido<felpa::DatiRitenuta>(id)
          + "<Causale>"
          "LA FATTURA FA RIFERIMENTO AD UNA OPERAZIONE AAAA "
          "BBBBBBBBBBBBBBBBBB CCC DDDDDDDDDDDDDDD E FFFFFFFFFFFFFFFFFFFF "
          "GGGGGGGGGG HHHHHHH II LLLLLLLLLLLLLLLLL MMM NNNNN OO PPPPPPPPPPP "
          "QQQQ RRRR SSSSSSSSSSSSSS"
          "</Causale>"
          "<Causale>"
          "SEGUE DESCRIZIONE CAUSALE NEL CASO IN CUI "
          "NON SIANO STATI SUFFICIENTI 200 CARATTERI AAAAAAAAAAA "
          "BBBBBBBBBBBBBBBBB"
          "</Causale>"
          "</DatiGeneraliDocumento>";
    break;

  case 2:
    xml = "<DatiGeneraliDocumento>"
          "<TipoDocumento>TD01</TipoDocumento>"
          "<Divisa>EUR</Divisa>"
          "<Data>2014-12-18</Data>"
          "<Numero>123</Numero>"
          + xml_oggetto_valido<felpa::DatiRitenuta>(id)
          + "<ImportoTotaleDocumento>30.50</ImportoTotaleDocumento>"
          "<Causale>"
          "LA FATTURA FA RIFERIMENTO AD UNA OPERAZIONE AAAA "
          "BBBBBBBBBBBBBBBBBB CCC DDDDDDDDDDDDDDD E FFFFFFFFFFFFFFFFFFFF "
          "GGGGGGGGGG HHHHHHH II LLLLLLLLLLLLLLLLL MMM NNNNN OO PPPPPPPPPPP "
          "QQQQ RRRR SSSSSSSSSSSSSS"
          "</Causale>"
          "<Causale>"
          "SEGUE DESCRIZIONE CAUSALE NEL CASO IN CUI "
          "NON SIANO STATI SUFFICIENTI 200 CARATTERI AAAAAAAAAAA "
          "BBBBBBBBBBBBBBBBB"
          "</Causale>"
          "</DatiGeneraliDocumento>";
    break;

  case 3:
    xml = "<DatiGeneraliDocumento>"
          "<TipoDocumento>TD01</TipoDocumento>"
          "<Divisa>EUR</Divisa>"
          "<Data>2021-03-18</Data>"
          "<Numero>E.9875</Numero>"
          + xml_oggetto_valido<felpa::ScontoMaggiorazione>(0)
          + "<ImportoTotaleDocumento>1000.00</ImportoTotaleDocumento>"
          "<Causale>EFFICIENTAMENTO ENERGETICO</Causale>"
          "</DatiGeneraliDocumento>";
    break;

  }

  return xml;
}

template<> felpa::DatiGenerali oggetto_valido(unsigned id)
{
  using namespace felpa;

  DatiGenerali dg;

  switch (id)
  {
  case 0:
  case 1:
  case 2:
    dg.dati_generali_documento = oggetto_valido<DatiGeneraliDocumento>(id);
    dg.dati_ordine_acquisto =
    {
      oggetto_valido<DatiOrdineAcquisto>(id),
      oggetto_valido<DatiOrdineAcquisto>(id+1)
    };
    dg.dati_contratto =
    {
      oggetto_valido<DatiContratto>(id),
      oggetto_valido<DatiContratto>(id+1)
    };
    dg.dati_convenzione =
    {
      oggetto_valido<DatiConvenzione>(id),
      oggetto_valido<DatiConvenzione>(id+1)
    };
    dg.dati_ricezione =
    {
      oggetto_valido<DatiRicezione>(id),
      oggetto_valido<DatiRicezione>(id+1)
    };
    dg.dati_fatture_collegate =
    {
      oggetto_valido<DatiFattureCollegate>(id),
      oggetto_valido<DatiFattureCollegate>(id+1)
    };
    dg.dati_ddt =
    {
      oggetto_valido<DatiDDT>(id),
      oggetto_valido<DatiDDT>(id+1)
    };
    dg.dati_trasporto = oggetto_valido<DatiTrasporto>(id);
    break;

  case 3:
    dg.dati_generali_documento = oggetto_valido<DatiGeneraliDocumento>(2);
    dg.dati_generali_documento.dati_ritenuta[0].importo_ritenuta = "4.00";
    dg.dati_generali_documento.importo_totale_documento = "30.50";
    dg.dati_ordine_acquisto = { oggetto_valido<DatiOrdineAcquisto>(0) };
    dg.dati_contratto = { oggetto_valido<DatiContratto>(0) };
    dg.dati_convenzione = { oggetto_valido<DatiConvenzione>(0) };
    dg.dati_trasporto = { oggetto_valido<DatiTrasporto>(1) };
    break;
  }

  return dg;
}

template<> std::string xml_oggetto_valido<felpa::DatiGenerali>(
  unsigned id)
{
  std::string xml;

  switch (id)
  {
  case 0:
  case 1:
  case 2:
    xml = "<DatiGenerali>"
          + xml_oggetto_valido<felpa::DatiGeneraliDocumento>(id)
          + xml_oggetto_valido<felpa::DatiOrdineAcquisto>(id)
          + xml_oggetto_valido<felpa::DatiOrdineAcquisto>(id + 1)
          + xml_oggetto_valido<felpa::DatiContratto>(id)
          + xml_oggetto_valido<felpa::DatiContratto>(id + 1)
          + xml_oggetto_valido<felpa::DatiConvenzione>(id)
          + xml_oggetto_valido<felpa::DatiConvenzione>(id + 1)
          + xml_oggetto_valido<felpa::DatiRicezione>(id)
          + xml_oggetto_valido<felpa::DatiRicezione>(id + 1)
          + xml_oggetto_valido<felpa::DatiFattureCollegate>(id)
          + xml_oggetto_valido<felpa::DatiFattureCollegate>(id + 1)
          + xml_oggetto_valido<felpa::DatiDDT>(id)
          + xml_oggetto_valido<felpa::DatiDDT>(id + 1)
          + xml_oggetto_valido<felpa::DatiTrasporto>(id)
          + "</DatiGenerali>";
    break;

  case 3:
    xml = "<DatiGenerali>"
          + xml_oggetto_valido<felpa::DatiGeneraliDocumento>(2)
          + xml_oggetto_valido<felpa::DatiOrdineAcquisto>(0)
          + xml_oggetto_valido<felpa::DatiContratto>(0)
          + xml_oggetto_valido<felpa::DatiConvenzione>(0)
          + xml_oggetto_valido<felpa::DatiTrasporto>(1)
          + "</DatiGenerali>";
    break;
  }

  return xml;
}

template<> felpa::DettaglioLinee oggetto_valido(unsigned id)
{
  felpa::DettaglioLinee dl;

  switch (id)
  {
  case 0:
    dl.descrizione = "DESCRIZIONE";
    dl.quantita = 2.0;
    dl.prezzo_unitario = 1.7475;
    break;
  case 1:
    dl.descrizione =
    "LA DESCRIZIONE DELLA FORNITURA PUO' SUPERARE I CENTO CARATTERI CHE "
    "RAPPRESENTAVANO IL PRECEDENTE LIMITE DIMENSIONALE. TALE LIMITE NELLA "
    "NUOVA VERSIONE E' STATO PORTATO A MILLE CARATTERI";
    dl.quantita = 5.0;
    dl.prezzo_unitario = 1.0;
    break;
  case 2:
    dl.descrizione = "FORNITURE VARIE PER UFFICIO";
    dl.quantita = 10.0;
    dl.prezzo_unitario = 2.0;
    dl.ritenuta = felpa::Ritenuta::SI;
    dl.data_inizio_periodo = "2014-12-01";
    dl.data_fine_periodo = "2014-12-16";
    break;
  case 3:
    dl.descrizione = "VOCE 1";
    dl.quantita = 1.0;
    dl.prezzo_unitario = 1.0;
    dl.aliquota_iva = 0.0;
    dl.natura = "N1";
    break;
  case 4:
    dl.descrizione = "VOCE 2";
    dl.quantita = 1.0;
    dl.prezzo_unitario = 2.0;
    dl.aliquota_iva = 0.0;
    dl.natura = "N2.1";
    break;
  case 5:
    dl.descrizione = "Lavori di manutenzione straordinaria, cappotto termico";
    dl.quantita = 1.0;
    dl.prezzo_unitario = 279712.27;
    dl.aliquota_iva = 10.0;
    dl.altri_dati_gestionali = {oggetto_valido<felpa::AltriDatiGestionali>(0)};
    break;
  }

  return dl;
}

template<> std::string xml_oggetto_valido<felpa::DettaglioLinee>(
  unsigned id, unsigned linea)
{
  std::string xml;

  switch (id)
  {
  case 0:
    xml = "<DettaglioLinee>"
          "<NumeroLinea>" + std::to_string(linea) + "</NumeroLinea>"
          "<Descrizione>DESCRIZIONE</Descrizione>"
          "<Quantita>2.000000</Quantita>"
          "<PrezzoUnitario>1.7475</PrezzoUnitario>"
          "<PrezzoTotale>3.50</PrezzoTotale>"
          "<AliquotaIVA>22.00</AliquotaIVA>"
          "</DettaglioLinee>";
    break;
  case 1:
    xml = "<DettaglioLinee>"
          "<NumeroLinea>" + std::to_string(linea) + "</NumeroLinea>"
          "<Descrizione>"
          "LA DESCRIZIONE DELLA FORNITURA PUO' SUPERARE I CENTO CARATTERI CHE "
          "RAPPRESENTAVANO IL PRECEDENTE LIMITE DIMENSIONALE. TALE LIMITE "
          "NELLA NUOVA VERSIONE E' STATO PORTATO A MILLE CARATTERI"
          "</Descrizione>"
          "<Quantita>5.000000</Quantita>"
          "<PrezzoUnitario>1.0000</PrezzoUnitario>"
          "<PrezzoTotale>5.00</PrezzoTotale>"
          "<AliquotaIVA>22.00</AliquotaIVA>"
          "</DettaglioLinee>";
    break;
  case 2:
    xml = "<DettaglioLinee>"
          "<NumeroLinea>" + std::to_string(linea) + "</NumeroLinea>"
          "<Descrizione>FORNITURE VARIE PER UFFICIO</Descrizione>"
          "<Quantita>10.000000</Quantita>"
          "<PrezzoUnitario>2.0000</PrezzoUnitario>"
          "<PrezzoTotale>20.00</PrezzoTotale>"
          "<AliquotaIVA>22.00</AliquotaIVA>"
          "<Ritenuta>SI</Ritenuta>"
          "<DataInizioPeriodo>2014-12-01</DataInizioPeriodo>"
          "<DataFinePeriodo>2014-12-16</DataFinePeriodo>"
          "</DettaglioLinee>";
    break;
  case 3:
    xml = "<DettaglioLinee>"
          "<NumeroLinea>" + std::to_string(linea) + "</NumeroLinea>"
          "<Descrizione>VOCE 1</Descrizione>"
          "<Quantita>1.000000</Quantita>"
          "<PrezzoUnitario>1.0000</PrezzoUnitario>"
          "<PrezzoTotale>1.00</PrezzoTotale>"
          "<AliquotaIVA>0.00</AliquotaIVA>"
          "<Natura>N1</Natura>"
          "</DettaglioLinee>";
    break;
  case 4:
    xml = "<DettaglioLinee>"
          "<NumeroLinea>" + std::to_string(linea) + "</NumeroLinea>"
          "<Descrizione>VOCE 2</Descrizione>"
          "<Quantita>1.000000</Quantita>"
          "<PrezzoUnitario>2.0000</PrezzoUnitario>"
          "<PrezzoTotale>2.00</PrezzoTotale>"
          "<AliquotaIVA>0.00</AliquotaIVA>"
          "<Natura>N2.1</Natura>"
          "</DettaglioLinee>";
    break;
  case 5:
    xml = "<DettaglioLinee>"
          "<NumeroLinea>" + std::to_string(linea) + "</NumeroLinea>"
          "<Descrizione>"
          "Lavori di manutenzione straordinaria, cappotto termico"
          "</Descrizione>"
          "<Quantita>1.000000</Quantita>"
          "<PrezzoUnitario>279712.2700</PrezzoUnitario>"
          "<PrezzoTotale>279712.27</PrezzoTotale>"
          "<AliquotaIVA>10.00</AliquotaIVA>"
          + xml_oggetto_valido<felpa::AltriDatiGestionali>(0) +
          "</DettaglioLinee>";
    break;
  }

  return xml;
}

template<> felpa::DatiBeniServizi oggetto_valido(unsigned id)
{
  felpa::DatiBeniServizi dbs(felpa::EsigibilitaIva("I"));

  switch (id)
  {
  case 0:
    dbs = felpa::DatiBeniServizi(felpa::EsigibilitaIva("S"));

    dbs.dettaglio_linee =
    {
      oggetto_valido<felpa::DettaglioLinee>(1),
      oggetto_valido<felpa::DettaglioLinee>(2)
    };

    dbs = completa(dbs);
    break;

  case 1:
    dbs.dettaglio_linee =
    {
      oggetto_valido<felpa::DettaglioLinee>(3),
      oggetto_valido<felpa::DettaglioLinee>(4)
    };

    dbs.aggiungi_riferimento_normativo(dbs.dettaglio_linee[0].natura,
                                       "RIFERIMENTO 1");
    dbs.aggiungi_riferimento_normativo(dbs.dettaglio_linee[1].natura,
                                       "RIFERIMENTO 2");

    dbs = completa(dbs);
    break;

  case 2:
    dbs.dettaglio_linee =
    {
      oggetto_valido<felpa::DettaglioLinee>(1),
      oggetto_valido<felpa::DettaglioLinee>(2)
    };

    dbs = completa(dbs);
    break;

  case 3:
    dbs.dettaglio_linee =
    {
      oggetto_valido<felpa::DettaglioLinee>(3),
      oggetto_valido<felpa::DettaglioLinee>(4)
    };

    dbs.aggiungi_riferimento_normativo(dbs.dettaglio_linee[0].natura,
                                       "Esenti [FIC 7]");
    dbs.aggiungi_riferimento_normativo(dbs.dettaglio_linee[1].natura,
                                       "Non Imp. Art.72 [FIC 28]");

    dbs = completa(dbs);
    break;
  }

  return dbs;
}

template<> std::string xml_oggetto_valido<felpa::DatiBeniServizi>(unsigned id)
{
  const auto dr(oggetto_valido<felpa::DatiBeniServizi>(id).dati_riepilogo);

  tinyxml2::XMLPrinter p;
  for (const auto &i : dr)
    felpa::dettagli::registra(p, i);
  const std::string xml_dr(clear_formatting(p.CStr()));

  std::string xml;
  switch (id)
  {
  case 0:
    xml = "<DatiBeniServizi>"
          + xml_oggetto_valido<felpa::DettaglioLinee>(1, 1)
          + xml_oggetto_valido<felpa::DettaglioLinee>(2, 2)
          + xml_dr
          + "</DatiBeniServizi>";
    break;

  case 1:
    xml = "<DatiBeniServizi>"
          + xml_oggetto_valido<felpa::DettaglioLinee>(3, 1)
          + xml_oggetto_valido<felpa::DettaglioLinee>(4, 2)
          + xml_dr
          + "</DatiBeniServizi>";
    break;

  case 2:
    xml = "<DatiBeniServizi>"
          + xml_oggetto_valido<felpa::DettaglioLinee>(1, 1)
          + xml_oggetto_valido<felpa::DettaglioLinee>(2, 2)
          + xml_dr
          + "</DatiBeniServizi>";
    break;

  case 3:
    xml = "<DatiBeniServizi>"
          + xml_oggetto_valido<felpa::DettaglioLinee>(3, 1)
          + xml_oggetto_valido<felpa::DettaglioLinee>(4, 2)
          + xml_dr
          + "</DatiBeniServizi>";
    break;
  }

  return xml;
}

template<> felpa::DatiTrasmissione oggetto_valido(unsigned id)
{
  using namespace felpa;

  DatiTrasmissione d;

  switch (id)
  {
  case 0:
    d.formato_trasmissione = felpa::formato_fattura::FPR;
    d.id_trasmittente.id_codice = piva_valida;
    d.progressivo_invio = "0001";
    d.codice_destinatario = "AAAAAAA";
    d.contatti_trasmittente.email = "prova@prova.it";
    break;

  case 1:
    d.formato_trasmissione = felpa::formato_fattura::FPA;
    d.id_trasmittente.id_codice = piva_valida;
    d.progressivo_invio = "0002";
    d.codice_destinatario = "AAAAAA";
    d.contatti_trasmittente.email = "prova@prova.gov.it";
    break;
  }

  return d;
}

template<> std::string xml_oggetto_valido<felpa::DatiTrasmissione>(
  unsigned id)
{
  std::string xml;

  switch (id)
  {
  case 0:
    xml = "<DatiTrasmissione>"
          "<IdTrasmittente>"
          "<IdPaese>IT</IdPaese>"
          "<IdCodice>" + piva_valida + "</IdCodice>"
          "</IdTrasmittente>"
          "<ProgressivoInvio>0001</ProgressivoInvio>"
          "<FormatoTrasmissione>FPR12</FormatoTrasmissione>"
          "<CodiceDestinatario>AAAAAAA</CodiceDestinatario>"
          "<ContattiTrasmittente>"
          "<Email>prova@prova.it</Email>"
          "</ContattiTrasmittente>"
          "</DatiTrasmissione>";
    break;

  case 1:
    xml = "<DatiTrasmissione>"
          "<IdTrasmittente>"
          "<IdPaese>IT</IdPaese>"
          "<IdCodice>" + piva_valida + "</IdCodice>"
          "</IdTrasmittente>"
          "<ProgressivoInvio>0002</ProgressivoInvio>"
          "<FormatoTrasmissione>FPA12</FormatoTrasmissione>"
          "<CodiceDestinatario>AAAAAA</CodiceDestinatario>"
          "<ContattiTrasmittente>"
          "<Email>prova@prova.gov.it</Email>"
          "</ContattiTrasmittente>"
          "</DatiTrasmissione>";
    break;
  }

  return xml;
}

template<> felpa::FatturaElettronicaHeader oggetto_valido(unsigned id)
{
  using namespace felpa;

  FatturaElettronicaHeader h(formato_fattura::FPR);

  switch (id)
  {
  case 0:
    h.dati_trasmissione = oggetto_valido<DatiTrasmissione>(0);
    h.cedente_prestatore = oggetto_valido<CedentePrestatore>(0);
    h.cessionario_committente = oggetto_valido<CessionarioCommittente>(0);
    break;

  case 1:
    h.dati_trasmissione = oggetto_valido<DatiTrasmissione>(1);
    h.cedente_prestatore = oggetto_valido<CedentePrestatore>(0);
    h.cessionario_committente = oggetto_valido<CessionarioCommittente>(1);
    break;
  }

  return h;
}

template<> std::string xml_oggetto_valido<felpa::FatturaElettronicaHeader>(
  unsigned id)
{
  std::string xml;

  switch (id)
  {
  case 0:
    xml = "<FatturaElettronicaHeader>"
          + xml_oggetto_valido<felpa::DatiTrasmissione>(0)
          + xml_oggetto_valido<felpa::CedentePrestatore>(0)
          + xml_oggetto_valido<felpa::CessionarioCommittente>(0)
          + "</FatturaElettronicaHeader>";
    break;

  case 1:
    xml = "<FatturaElettronicaHeader>"
          + xml_oggetto_valido<felpa::DatiTrasmissione>(1)
          + xml_oggetto_valido<felpa::CedentePrestatore>(0)
          + xml_oggetto_valido<felpa::CessionarioCommittente>(1)
          + "</FatturaElettronicaHeader>";
    break;
  }

  return xml;
}
template<> felpa::FatturaElettronicaBody oggetto_valido(unsigned id)
{
  using namespace felpa;

  FatturaElettronicaBody b(EsigibilitaIva("I"));

  switch (id)
  {
  case 0:
    b.dati_generali = oggetto_valido<DatiGenerali>(3);
    b.dati_beni_servizi = oggetto_valido<DatiBeniServizi>(2);
    b.dati_pagamento = oggetto_valido<DatiPagamento>(1);

    // Calcolo automatico.
    b.dati_generali.dati_generali_documento.dati_ritenuta[0].importo_ritenuta
      = "";
    b.dati_generali.dati_generali_documento.importo_totale_documento = "";
    b = completa(b);
    break;
  }

  return b;
}

template<> std::string xml_oggetto_valido<felpa::FatturaElettronicaBody>(
  unsigned id)
{
  std::string xml;

  switch (id)
  {
  case 0:
    xml = "<FatturaElettronicaBody>"
          + xml_oggetto_valido<felpa::DatiGenerali>(3)
          + xml_oggetto_valido<felpa::DatiBeniServizi>(2)
          + xml_oggetto_valido<felpa::DatiPagamento>(1)
          + "</FatturaElettronicaBody>";
    break;
  }

  return xml;
}

TEST_SUITE("FELPA UTILITY")
{

TEST_CASE("Estrazione serie/numero")
{
  using felpa::serie_e_numero;

  {
    const serie_e_numero id("1234");
    CHECK(id.serie().empty());
    CHECK(id.numero() == "1234");
  }

  {
    const serie_e_numero id("2019/FE53735/368");
    CHECK(id.serie() == "2019FE53735");
    CHECK(id.numero() == "368");
  }

  {
    const serie_e_numero id("IT20-AEUI-5346061");
    CHECK(id.serie() == "IT20AEUI");
    CHECK(id.numero() == "5346061");
  }

  {
    const serie_e_numero id("1234/22");
    CHECK(id.serie() == "1234");
    CHECK(id.numero() == "22");
  }

  {
    const serie_e_numero id("E123");
    CHECK(id.serie() == "E");
    CHECK(id.numero() == "123");
  }

  {
    const serie_e_numero id("123E");
    CHECK(id.serie() == "E");
    CHECK(id.numero() == "123");
  }

  {
    const serie_e_numero id("A123B");
    CHECK(id.serie() == "AB");
    CHECK(id.numero() == "123");
  }

  {
    const serie_e_numero id("189/00");
    CHECK(id.serie() == "00");
    CHECK(id.numero() == "189");
  }
}

TEST_CASE("Conversione di base")
{
  using felpa::base_conversion;

  CHECK(base_conversion(1048575, 30) == "18P2F");
  CHECK(base_conversion(  99999, 30) == "03L39");
  CHECK(base_conversion(   3124, 30) == "003E4");
  CHECK(base_conversion(    521, 30) == "000HB");
  CHECK(base_conversion(     16, 30) == "0000G");
  CHECK(base_conversion(     15, 16) == "0000F");
  CHECK(base_conversion(      0, 16) == "00000");

  CHECK(base_conversion(1048575, 16) == "FFFFF");
  CHECK(base_conversion(  99999, 16) == "1869F");
  CHECK(base_conversion(   3124, 16) == "00C34");
  CHECK(base_conversion(    521, 16) == "00209");
  CHECK(base_conversion(     16, 16) == "00010");
  CHECK(base_conversion(     15, 16) == "0000F");
  CHECK(base_conversion(      0, 16) == "00000");

  CHECK(base_conversion(99999, 10) == "99999");
  CHECK(base_conversion( 3124, 10) == "03124");
  CHECK(base_conversion(  521, 10) == "00521");
  CHECK(base_conversion(   16, 10) == "00016");
  CHECK(base_conversion(   15, 10) == "00015");
  CHECK(base_conversion(    0, 10) == "00000");

  CHECK(base_conversion(3124, 5) == "44444");
  CHECK(base_conversion( 521, 5) == "04041");
  CHECK(base_conversion(  16, 5) == "00031");
  CHECK(base_conversion(  15, 5) == "00030");
  CHECK(base_conversion(   0, 5) == "00000");

  CHECK(base_conversion(31, 2) == "11111");
  CHECK(base_conversion(16, 2) == "10000");
  CHECK(base_conversion(15, 2) == "01111");
  CHECK(base_conversion( 3, 2) == "00011");
  CHECK(base_conversion( 2, 2) == "00010");
  CHECK(base_conversion( 1, 2) == "00001");
  CHECK(base_conversion( 0, 2) == "00000");
}

TEST_CASE("Estrazione nome file")
{
  using felpa::extract_filename;

  CHECK(extract_filename("c:\\prova\\path\\windows\\lungo\\d.pdf") == "d.pdf");
  CHECK(extract_filename("/home/usera/documenti/doc.pdf") == "doc.pdf");
  CHECK(extract_filename("immagine.jpg") == "immagine.jpg");
}

TEST_CASE("Codifica/decodifica base64")
{
  using felpa::base64_decode;
  using felpa::base64_encode;

  const auto rand_val(
    []()
    {
      static std::default_random_engine rng;
      static std::uniform_int_distribution<> dist(0, 255);

      return dist(rng);
    });


  const auto random_vector(
    [&](std::size_t length)
    {
      std::vector<unsigned char> str(length, 0);
      std::generate_n(str.begin(), length, rand_val);
      return str;
    });

  for (unsigned i(0); i < 2000; ++i)
  {
    const auto rv(random_vector(i));
    CHECK(base64_decode(base64_encode(rv)) == rv);
  }
}

TEST_CASE("Estrazione xml firmato (p7m)")
{
  using felpa::extract_p7m_content;

  const std::string infile("IT00000000000_AAAAA.xml.p7m");
  const std::string outfile("test.xml");

  CHECK(extract_p7m_content(infile, outfile) == 0);

  std::ifstream f(outfile.c_str());
  CHECK(f.good());
}

}  // TEST_SUITE("FELPA UTILITY")

TEST_SUITE("FELPA")
{

TEST_CASE("CondizioniPagamento")
{
  using namespace felpa;

  CondizioniPagamento cp;
  CHECK(cp.empty());
  CHECK(CondizioniPagamento::tag() == "CondizioniPagamento");

  CHECK_THROWS_AS(cp = "TP99", eccezione::cond_pag_cod_scorretto);

  CHECK_NOTHROW(cp = "TP01");
  CHECK(!cp.empty());
  CHECK(!cp.completo());

  CHECK_NOTHROW(cp = "TP02");
  CHECK(cp.completo());

  tinyxml2::XMLPrinter p;
  CHECK_NOTHROW(dettagli::registra(p, cp));
  CHECK(clear_formatting(p.CStr())
        == "<" + cp.tag() + ">"
           "TP02"
           "</" + cp.tag() + ">");

  CondizioniPagamento cp1;
  CHECK(carica(p, &cp1));
  CHECK(std::string(cp1) == std::string(cp));
}

TEST_CASE("DataOraConsegna")
{
  using namespace felpa;

  DataOraConsegna cdo;
  CHECK(cdo.empty());
  CHECK(cdo.tag() == "DataOraConsegna");

  CHECK_THROWS_AS(cdo = "01/12/2020", eccezione::formato_data_ora);
  CHECK_THROWS_AS(cdo = "01-12-2020", eccezione::formato_data_ora);
  CHECK_THROWS_AS(cdo = "2020-12-01", eccezione::formato_data_ora);

  CHECK_NOTHROW(cdo = "2020-12-01T16:30");

  const std::string scdo("2020-12-01T16:30:00");
  CHECK(!cdo.empty());
  CHECK_NOTHROW(cdo = scdo);

  tinyxml2::XMLPrinter p;
  CHECK_NOTHROW(dettagli::registra(p, cdo));
  CHECK(clear_formatting(p.CStr())
        == "<" + cdo.tag() + ">"
           + scdo +
           "</" + cdo.tag() + ">");

  DataOraConsegna cdo1;
  CHECK(carica(p, &cdo1));
  CHECK(std::string(cdo1) == std::string(cdo));
}

TEST_CASE("DataScadenzaPagamento")
{
  using namespace felpa;

  DataScadenzaPagamento dsp;
  CHECK(dsp.empty());
  CHECK(dsp.tag() == "DataScadenzaPagamento");
  CHECK(dsp == dsp);

  CHECK_THROWS_AS(dsp = "01/12/2020", eccezione::formato_data);
  CHECK_THROWS_AS(dsp = "01-12-2020", eccezione::formato_data);

  const std::string sdsp("2020-12-01");
  CHECK(!sdsp.empty());
  CHECK_NOTHROW(dsp = sdsp);

  tinyxml2::XMLPrinter p;
  CHECK_NOTHROW(dettagli::registra(p, dsp));
  CHECK(clear_formatting(p.CStr())
        == "<" + dsp.tag() + ">"
           + sdsp +
           "</" + dsp.tag() + ">");

  DataScadenzaPagamento dsp1;
  CHECK(carica(p, &dsp1));
  CHECK(std::string(dsp1) == std::string(dsp));
}

TEST_CASE("ContattiTrasmittente")
{
  using namespace felpa;

  ContattiTrasmittente ct;
  CHECK(ct.tag() == "ContattiTrasmittente");
  CHECK(ct.empty());
  CHECK(ct == ct);

  ct.email = "prova@prova.it";
  ct.telefono = "0123456789";
  CHECK(!ct.empty());

  tinyxml2::XMLPrinter p;
  CHECK_NOTHROW(dettagli::registra(p, ct));
  CHECK(clear_formatting(p.CStr())
        == "<" + ContattiTrasmittente::tag() + ">"
           "<Telefono>" + ct.telefono + "</Telefono>"
           "<Email>" + ct.email + "</Email>"
           "</" + ContattiTrasmittente::tag() + ">");

  ContattiTrasmittente ct1;
  CHECK(carica(p, &ct1));
  CHECK(ct1 == ct);
}

TEST_CASE("SoggettoEmittente")
{
  using namespace felpa;

  SoggettoEmittente se;
  CHECK(se.empty());
  CHECK(se.tag() == "SoggettoEmittente");

  CHECK_THROWS_AS(SoggettoEmittente("ZZ"),
                  eccezione::soggetto_emittente_scorretto);
  CHECK_THROWS_AS(se = "ZZ", eccezione::soggetto_emittente_scorretto);

  CHECK(std::string(se = "TZ") == "TZ");
  CHECK(std::string(se = "CC") == "CC");

  tinyxml2::XMLPrinter p;
  CHECK_NOTHROW(dettagli::registra(p, se));
  CHECK(clear_formatting(p.CStr())
        == "<SoggettoEmittente>CC</SoggettoEmittente>");

  SoggettoEmittente se1;
  CHECK(carica(p, &se1));
  CHECK(se1 == se);
}

TEST_CASE("IdPaese")
{
  using namespace felpa;

  IdPaese ip;
  CHECK(std::string(ip) == "IT");
  CHECK(ip.tag() == "IdPaese");
  CHECK(ip.italia());

  CHECK_THROWS_AS(ip = "ZZ", eccezione::nazione_cod_scorretto);

  ip = "ES";
  CHECK(std::string(ip) == "ES");
  CHECK(ip.tag() == "IdPaese");
  CHECK(!ip.italia());

  ip = "US";
  CHECK(std::string(ip) == "US");
  CHECK(!ip.italia());

  CHECK_THROWS_AS(IdPaese("ZZ"), eccezione::nazione_cod_scorretto);

  tinyxml2::XMLPrinter p;
  CHECK_NOTHROW(dettagli::registra(p, ip));
  CHECK(clear_formatting(p.CStr()) == "<IdPaese>US</IdPaese>");

  IdPaese ip1;
  CHECK(carica(p, &ip1));
  CHECK(ip1 == ip);
}

TEST_CASE("Nazione")
{
  using namespace felpa;

  Nazione n;
  CHECK(std::string(n) == "IT");
  CHECK(n.tag() == "Nazione");

  CHECK_THROWS_AS(n = "ZZ", eccezione::nazione_cod_scorretto);

  n = "ES";
  CHECK(std::string(n) == "ES");
  CHECK(n.tag() == "Nazione");

  CHECK_THROWS_AS(Nazione("ZZ"), eccezione::nazione_cod_scorretto);

  tinyxml2::XMLPrinter p;
  CHECK_NOTHROW(dettagli::registra(p, n));
  CHECK(clear_formatting(p.CStr()) == "<Nazione>ES</Nazione>");

  Nazione n1;
  CHECK(carica(p, &n1));
  CHECK(n1 == n);
}

TEST_CASE("Divisa")
{
  using namespace felpa;

  Divisa d;
  CHECK(std::string(d) == "EUR");

  CHECK_THROWS_AS(d = "XXXX", eccezione::divisa_cod_scorretto);

  d = "GBP";
  CHECK(std::string(d) == "GBP");

  CHECK_THROWS_AS(Divisa("XXXX"), eccezione::divisa_cod_scorretto);

  d = "EUR";

  tinyxml2::XMLPrinter p;
  CHECK_NOTHROW(dettagli::registra(p, d));
  CHECK(clear_formatting(p.CStr()) == "<Divisa>EUR</Divisa>");

  Divisa d1;
  CHECK(carica(p, &d1));
  CHECK(d1 == d);
}

TEST_CASE("Allegato")
{
  using namespace felpa;

  Allegato a;

  CHECK(a.empty());

  tinyxml2::XMLPrinter p;
  CHECK_NOTHROW(dettagli::registra(p, a));
  CHECK(clear_formatting(p.CStr()) == "");

  const std::vector<unsigned char> data =
  {
    'P', 'r', 'o', 'v', 'a', ' ', 'f', 'i', 'l', 'e', ' ', 'd', 'i',
    't', 'e', 's', 't', 'o', ' ', 'a', 'l', 'l', 'e', 'g', 'a', 't', 'o', '.'
  };

  a.nome_attachment = "prova.txt";
  a.algoritmo_compressione = "ZIP";
  a.attachment = base64_encode(data);
  a.descrizione_attachment = "DEX";
  a.formato_attachment = "ZIP";

  CHECK(base64_decode(a.attachment) == data);

  CHECK(!a.empty());

  CHECK_NOTHROW(dettagli::registra(p, a));
  CHECK(clear_formatting(p.CStr())
        == "<" + Allegato::tag() + ">"
           "<NomeAttachment>" + a.nome_attachment + "</NomeAttachment>"
           "<AlgoritmoCompressione>" + a.algoritmo_compressione + "</AlgoritmoCompressione>"
           "<FormatoAttachment>" + a.formato_attachment + "</FormatoAttachment>"
           "<DescrizioneAttachment>" + a.descrizione_attachment + "</DescrizioneAttachment>"
           "<Attachment>" + a.attachment + "</Attachment>"
           "</" + Allegato::tag() + ">");

  Allegato a1;
  CHECK(carica(p, &a1));
  CHECK(a1 == a);

  CHECK_NOTHROW(dettagli::registra(p, a1));
}

TEST_CASE("TipoDocumento")
{
  using namespace felpa;

  TipoDocumento td;
  CHECK(td.tag() == "TipoDocumento");
  CHECK(std::string(td) == "TD01");

  CHECK_THROWS_AS(td = "XXXX", eccezione::tipo_doc_cod_scorretto);

  td = "TD02";
  CHECK(std::string(td) == "TD02");

  CHECK_THROWS_AS(TipoDocumento("XXXX"), eccezione::tipo_doc_cod_scorretto);

  td = "TD03";

  tinyxml2::XMLPrinter p;
  CHECK_NOTHROW(dettagli::registra(p, td));
  CHECK(clear_formatting(p.CStr()) == "<TipoDocumento>TD03</TipoDocumento>");

  TipoDocumento td1;
  CHECK(carica(p, &td1));
  CHECK(td1 == td);
}

TEST_CASE("ModalitaPagamento")
{
  using namespace felpa;

  ModalitaPagamento mp;
  CHECK(ModalitaPagamento::tag() == "ModalitaPagamento");
  CHECK(mp.empty());

  tinyxml2::XMLPrinter p;
  CHECK_NOTHROW(dettagli::registra(p, mp));
  CHECK(clear_formatting(p.CStr()) == "");

  mp = "MP01";
  CHECK(!mp.empty());

  tinyxml2::XMLPrinter p1;
  CHECK_NOTHROW(dettagli::registra(p1, mp));
  CHECK(clear_formatting(p1.CStr())
        == "<ModalitaPagamento>MP01</ModalitaPagamento>");

  ModalitaPagamento mp1;
  CHECK(carica(p1, &mp1));
  CHECK(std::string(mp1) == std::string(mp));

  CHECK_NOTHROW(dettagli::registra(p1, mp1));
}

TEST_CASE("IdFiscaleIVA")
{
  using namespace felpa;

  {
    IdFiscaleIva ifi;
    CHECK(ifi.tag() == "IdFiscaleIVA");
    CHECK(ifi.empty());

    tinyxml2::XMLPrinter p;
    CHECK_THROWS_AS(dettagli::registra(p, ifi),
                    eccezione::id_fiscale_codice);
  }

  for (unsigned i(0); !oggetto_valido<IdFiscaleIva>(i).empty(); ++i)
  {
    auto ifi(oggetto_valido<IdFiscaleIva>(i));
    CHECK(!ifi.empty());

    tinyxml2::XMLPrinter p;
    CHECK_NOTHROW(dettagli::registra(p, ifi));
    CHECK(clear_formatting(p.CStr())
          == xml_oggetto_valido<IdFiscaleIva>(i));

    IdFiscaleIva ifi1;
    CHECK(carica(p, &ifi1));
    CHECK(ifi1 == ifi);

    CHECK_NOTHROW(dettagli::registra(p, ifi1));
  }
}

TEST_CASE("TipoCessionePrestazione")
{
  using namespace felpa;

  TipoCessionePrestazione tcp;
  CHECK(TipoCessionePrestazione::tag() == "TipoCessionePrestazione");
  CHECK(tcp.empty());

  tinyxml2::XMLPrinter p;
  CHECK_NOTHROW(dettagli::registra(p, tcp));
  CHECK(clear_formatting(p.CStr()) == "");

  CHECK_THROWS_AS(tcp = "XX", eccezione::tipo_cessione_cod_scorretto);

  CHECK_NOTHROW(tcp = "SC");
  CHECK_NOTHROW(tcp = "PR");
  CHECK_NOTHROW(tcp = "AB");
  CHECK_NOTHROW(tcp = "AC");
  CHECK(!tcp.empty());

  tinyxml2::XMLPrinter p1;
  CHECK_NOTHROW(dettagli::registra(p1, tcp));
  CHECK(clear_formatting(p1.CStr())
        == "<TipoCessionePrestazione>AC</TipoCessionePrestazione>");

  TipoCessionePrestazione tcp1;
  CHECK(carica(p1, &tcp1));
  CHECK(std::string(tcp1) == std::string(tcp));
}

TEST_CASE("Ritenuta")
{
  using namespace felpa;

  Ritenuta r;
  CHECK(Ritenuta::tag() == "Ritenuta");
  CHECK(std::string(r) == Ritenuta::NO);
  CHECK(!r);

  tinyxml2::XMLPrinter p;
  CHECK_NOTHROW(dettagli::registra(p, r));
  CHECK(clear_formatting(p.CStr()) == "");

  r = Ritenuta::SI;

  CHECK(std::string(r) == Ritenuta::SI);
  CHECK(r);

  tinyxml2::XMLPrinter p1;
  CHECK_NOTHROW(dettagli::registra(p1, r));
  CHECK(clear_formatting(p1.CStr()) == "<Ritenuta>SI</Ritenuta>");

  Ritenuta r1;
  CHECK(carica(p1, &r1));
  CHECK(std::string(r1) == std::string(r));
}

TEST_CASE("TipoRitenuta")
{
  using namespace felpa;

  TipoRitenuta r;
  CHECK(TipoRitenuta::tag() == "TipoRitenuta");
  CHECK(r.empty());

  tinyxml2::XMLPrinter p;
  CHECK_THROWS_AS(dettagli::registra(p, r), eccezione::tipo_ritenuta_vuoto);

  CHECK_THROWS_AS(r = "XXXX", eccezione::tipo_ritenuta);

  const std::string tr("RT01");
  r = tr;
  CHECK(std::string(r) == tr);
  CHECK(!r.empty());

  CHECK_NOTHROW(dettagli::registra(p, r));
  CHECK(clear_formatting(p.CStr()) == "<TipoRitenuta>"+tr+"</TipoRitenuta>");

  TipoRitenuta r1;
  CHECK(carica(p, &r1));
  CHECK(std::string(r1) == std::string(r));
}

TEST_CASE("ImportoRitenuta")
{
  using namespace felpa;

  ImportoRitenuta v;
  CHECK(v.tag() == "ImportoRitenuta");
  CHECK(v.empty());

  CHECK(v.empty());
  CHECK(std::string(v) == "");

  v = 32.175;
  CHECK(std::string(v) == "32.18");
  CHECK(v.to_double() == doctest::Approx(32.18));
  CHECK(!v.empty());

  v = 3.705;
  CHECK(std::string(v) == "3.71");
  CHECK(v.to_double() == doctest::Approx(3.71));

  v = -20.0;
  CHECK(std::string(v) == "-20.00");
  CHECK(v.to_double() == doctest::Approx(-20.0));

  v = "315.275";
  CHECK(std::string(v) == "315.28");
  CHECK(v.to_double() == doctest::Approx(315.28));

  tinyxml2::XMLPrinter p;
  CHECK_NOTHROW(dettagli::registra(p, v));
  CHECK(clear_formatting(p.CStr())
        == "<ImportoRitenuta>315.28</ImportoRitenuta>");

  ImportoRitenuta v1;
  CHECK(carica(p, &v1));
  CHECK(std::string(v1) == std::string(v));

  CHECK(arrotondamento_2_decimali<ImportoRitenuta>());
}

TEST_CASE("ImportoBollo")
{
  using namespace felpa;

  ImportoBollo v;
  CHECK(v.tag() == "ImportoBollo");

  CHECK(v.empty());
  CHECK(std::string(v) == "");

  v = 32.175;
  CHECK(std::string(v) == "32.18");
  CHECK(v.to_double() == doctest::Approx(32.18));
  CHECK(!v.empty());

  v = 3.705;
  CHECK(std::string(v) == "3.71");
  CHECK(v.to_double() == doctest::Approx(3.71));

  v = "315.275";
  CHECK(std::string(v) == "315.28");
  CHECK(v.to_double() == doctest::Approx(315.28));

  tinyxml2::XMLPrinter p;
  CHECK_NOTHROW(dettagli::registra(p, v));
  CHECK(clear_formatting(p.CStr())
        == "<ImportoBollo>315.28</ImportoBollo>");

  ImportoBollo v1;
  CHECK(carica(p, &v1));
  CHECK(std::string(v1) == std::string(v));

  CHECK(arrotondamento_2_decimali<ImportoBollo>());
}

TEST_CASE("CausalePagamento")
{
  using namespace felpa;

  CausalePagamento c;
  CHECK(CausalePagamento::tag() == "CausalePagamento");
  CHECK(c.empty());

  tinyxml2::XMLPrinter p;
  CHECK_THROWS_AS(dettagli::registra(p, c), eccezione::causale_pagamento_vuota);

  CHECK_THROWS_AS(c = "XXXX", eccezione::cod_causale_pagamento);

  const std::string ca("A");
  c = ca;
  CHECK(std::string(c) == ca);
  CHECK(!c.empty());

  CHECK_NOTHROW(dettagli::registra(p, c));
  CHECK(clear_formatting(p.CStr())
                         == "<CausalePagamento>" + ca + "</CausalePagamento>");

  CausalePagamento c1;
  CHECK(carica(p, &c1));
  CHECK(std::string(c1) == std::string(c));
}

TEST_CASE("DatiRiepilogo")
{
  using namespace felpa;

  DatiRiepilogo dr(EsigibilitaIva("S"));
  CHECK(dr.tag() == "DatiRiepilogo");
  CHECK(dr.empty());
  CHECK(dr == dr);

  {
    tinyxml2::XMLPrinter p;

    dr.aliquota_iva = "0.00";

    CHECK_THROWS_AS(dettagli::registra(p, dr),
                    eccezione::imponibile_importo_mancante);

    dr.esigibilita_iva = "S";
    dr.natura = "N6.1";
    dr.imponibile_importo = "1000.00";

    CHECK_THROWS_AS(dettagli::registra(p, dr), eccezione::split_and_reverse);
  }

  dr.aliquota_iva = "0.00";
  dr.natura = "N1";
  dr.imponibile_importo = "1000.00";
  dr.imposta = "0.00";
  dr.esigibilita_iva = "I";
  dr.riferimento_normativo = "r1";
  CHECK(!dr.empty());

  {
    tinyxml2::XMLPrinter p;
    CHECK_NOTHROW(dettagli::registra(p, dr));
    CHECK(clear_formatting(p.CStr())
                           == "<DatiRiepilogo>"
                              "<AliquotaIVA>0.00</AliquotaIVA>"
                              "<Natura>N1</Natura>"
                              "<ImponibileImporto>1000.00</ImponibileImporto>"
                              "<Imposta>0.00</Imposta>"
                              "<RiferimentoNormativo>r1</RiferimentoNormativo>"
                              "</DatiRiepilogo>");
  }

  dr.aliquota_iva = "22.00";
  dr.natura = "";
  dr.imponibile_importo = "1000.00";
  dr.imposta = "220.00";
  dr.riferimento_normativo = "";

  tinyxml2::XMLPrinter p;
  CHECK_NOTHROW(dettagli::registra(p, dr));
  CHECK(clear_formatting(p.CStr())
                         == "<DatiRiepilogo>"
                            "<AliquotaIVA>22.00</AliquotaIVA>"
                            "<ImponibileImporto>1000.00</ImponibileImporto>"
                            "<Imposta>220.00</Imposta>"
                            "<EsigibilitaIVA>I</EsigibilitaIVA>"
                            "</DatiRiepilogo>");

  DatiRiepilogo dr1(EsigibilitaIva("I"));
  CHECK(carica(p, &dr1));
  CHECK(dr1 == dr);
}

TEST_CASE("DatiRitenuta")
{
  using namespace felpa;

  DatiRitenuta dr;
  CHECK(DatiRitenuta::tag() == "DatiRitenuta");
  CHECK(dr.empty());
  CHECK(dr == dr);

  {
    tinyxml2::XMLPrinter p;
    CHECK(clear_formatting(p.CStr()) == "");
  }

  for (unsigned i(0); !oggetto_valido<DatiRitenuta>(i).empty(); ++i)
  {
    dr = oggetto_valido<DatiRitenuta>(i);
    CHECK(!dr.empty());

    tinyxml2::XMLPrinter p;
    CHECK_NOTHROW(dettagli::registra(p, dr));
    CHECK(clear_formatting(p.CStr()) == xml_oggetto_valido<DatiRitenuta>(i));

    DatiRitenuta dr1;
    CHECK(carica(p, &dr1));
    CHECK(dr1 == dr);

    CHECK_NOTHROW(dettagli::registra(p, dr1));
  }
}

TEST_CASE("DatiBollo")
{
  using namespace felpa;

  DatiBollo db;
  CHECK(db.tag() == "DatiBollo");
  CHECK(db.empty());
  CHECK(db == db);

  db.bollo_virtuale = DatiBollo::NO;
  CHECK(db.empty());
  db.bollo_virtuale = "si";
  CHECK(db.empty());

  tinyxml2::XMLPrinter p;
  CHECK(clear_formatting(p.CStr()) == "");

  db.bollo_virtuale = DatiBollo::SI;
  CHECK(!db.empty());

  db.importo_bollo = 13.00;

  CHECK_NOTHROW(dettagli::registra(p, db));
  CHECK(clear_formatting(p.CStr())
        == "<DatiBollo>"
           "<BolloVirtuale>SI</BolloVirtuale>"
           "<ImportoBollo>13.00</ImportoBollo>"
           "</DatiBollo>");

  DatiBollo db1;
  CHECK(carica(p, &db1));
  CHECK(db1 == db);
}

TEST_CASE("CodiceArticolo")
{
  using namespace felpa;

  tinyxml2::XMLPrinter p;

  CodiceArticolo ca;

  SUBCASE("VUOTO")
  {
    CHECK(ca.empty());
    CHECK(ca.tag() == "CodiceArticolo");
    CHECK(ca == ca);

    CHECK_THROWS_AS(dettagli::registra(p, ca), eccezione::cod_tipo_mancante);
  }

  SUBCASE("PARZIALE")
  {
    ca.codice_tipo = "CUSTOM";
    CHECK(!ca.empty());

    CHECK_THROWS_AS(dettagli::registra(p, ca), eccezione::cod_valore_mancante);
  }

  SUBCASE("CUSTOM")
  {
    ca.codice_tipo = "CUSTOM";
    ca.codice_valore = "MAN1";
    CHECK(!ca.empty());

    CHECK_NOTHROW(dettagli::registra(p, ca));
    CHECK(clear_formatting(p.CStr())
          == "<" + ca.tag() + ">"
             "<CodiceTipo>" + ca.codice_tipo + "</CodiceTipo>"
             "<CodiceValore>" + ca.codice_valore + "</CodiceValore>"
             "</" + ca.tag() + ">");

    CodiceArticolo ca1;
    CHECK(carica(p, &ca1));
    CHECK(ca1 == ca);
  }

  SUBCASE("CARBURANTE")
  {
    ca.codice_tipo = "CARB";
    ca.codice_valore = "benzina";

    CHECK_THROWS_AS(dettagli::registra(p, ca),
                    eccezione::cod_tipo_cod_valore_incompatibili);

    ca.codice_valore = "27101944";

    CHECK_NOTHROW(dettagli::registra(p, ca));
    CHECK(clear_formatting(p.CStr())
          == "<" + ca.tag() + ">"
             "<CodiceTipo>" + ca.codice_tipo + "</CodiceTipo>"
             "<CodiceValore>" + ca.codice_valore + "</CodiceValore>"
             "</" + ca.tag() + ">");

    CodiceArticolo ca1;
    CHECK(carica(p, &ca1));
    CHECK(ca1 == ca);
  }
}

TEST_CASE_TEMPLATE("DatiDocumentiCorrelatiType", T,
                   felpa::DatiContratto, felpa::DatiConvenzione,
                   felpa::DatiFattureCollegate, felpa::DatiOrdineAcquisto,
                   felpa::DatiRicezione)
{
  using namespace felpa;

  T o;

  CHECK(o.empty());
  CHECK(o == o);

  {
    tinyxml2::XMLPrinter p;
    CHECK_THROWS_AS(dettagli::registra(p, o),
                    eccezione::dati_doc_corr_id_doc_mancante);
  }

  o.id_documento = "XXXX";
  CHECK(!o.empty());

  {
    o.riferimento_numero_linea.insert(12000);
    tinyxml2::XMLPrinter p;
    CHECK_THROWS_AS(dettagli::registra(p, o),
                    eccezione::dati_doc_corr_linea_scorretta);
  }

  for (unsigned i(0); !oggetto_valido<T>(i).empty(); ++i)
  {
    o = oggetto_valido<T>(i);

    tinyxml2::XMLPrinter p;
    CHECK_NOTHROW(dettagli::registra(p, o));

    CHECK(clear_formatting(p.CStr()) == xml_oggetto_valido<T>(i));

    T o1(o);
    CHECK(carica(p, &o1));
    CHECK(o1 == o);

    CHECK_NOTHROW(dettagli::registra(p, o1));
  }
}

TEST_CASE("DatiGeneraliDocumento")
{
  using namespace felpa;

  {
    DatiGeneraliDocumento dg;
    CHECK(dg.tag() == "DatiGeneraliDocumento");
    CHECK(dg.empty());
    CHECK(dg == dg);
  }

  {
    auto dg(oggetto_valido<DatiGeneraliDocumento>());
    dg.data = "";

    tinyxml2::XMLPrinter p;
    CHECK_THROWS_AS(dettagli::registra(p, dg), eccezione::data_doc_mancante);
  }

  {
    auto dg(oggetto_valido<DatiGeneraliDocumento>());
    dg.numero = "";

    tinyxml2::XMLPrinter p;
    CHECK_THROWS_AS(dettagli::registra(p, dg), eccezione::numero_doc_mancante);
  }

  for (unsigned i(0); !oggetto_valido<DatiGeneraliDocumento>(i).empty(); ++i)
  {
    const auto dg(oggetto_valido<DatiGeneraliDocumento>(i));

    tinyxml2::XMLPrinter p;
    CHECK_NOTHROW(dettagli::registra(p, dg));
    CHECK(clear_formatting(p.CStr())
          == xml_oggetto_valido<DatiGeneraliDocumento>(i));

    DatiGeneraliDocumento dg1;
    CHECK(carica(p, &dg1));
    CHECK(dg1 == dg);

    CHECK_NOTHROW(dettagli::registra(p, dg1));
  }
}

TEST_CASE("DatiGenerali")
{
  using namespace felpa;

  {
    DatiGenerali dg;
    CHECK(dg.tag() == "DatiGenerali");
    CHECK(dg.empty());
    CHECK(dg == dg);
  }

  for (unsigned i(0); !oggetto_valido<DatiGenerali>(i).empty(); ++i)
  {
    const auto dg(oggetto_valido<DatiGenerali>(i));

    tinyxml2::XMLPrinter p;
    CHECK_NOTHROW(dettagli::registra(p, dg));
    CHECK(clear_formatting(p.CStr())
          == xml_oggetto_valido<DatiGenerali>(i));

    DatiGenerali dg1;
    CHECK(carica(p, &dg1));
    CHECK(dg1 == dg);

    CHECK_NOTHROW(dettagli::registra(p, dg1));
  }
}

TEST_CASE("IdTrasmittente")
{
  using namespace felpa;

  IdTrasmittente it;
  CHECK(it.empty());
  CHECK(it.tag() == "IdTrasmittente");
  CHECK(it == it);

  tinyxml2::XMLPrinter p;
  CHECK_THROWS_AS(dettagli::registra(p, it), eccezione::felpa);

  it.id_codice = piva_non_valida;
  CHECK_THROWS_AS(dettagli::registra(p, it), eccezione::id_fiscale_codice);

  tinyxml2::XMLPrinter p1;
  it.id_codice = piva_valida;
  CHECK(!it.empty());
  CHECK_NOTHROW(dettagli::registra(p1, it));
  CHECK(clear_formatting(p1.CStr())
        == "<IdTrasmittente>"
           "<IdPaese>IT</IdPaese>"
           "<IdCodice>" + it.id_codice + "</IdCodice>"
           "</IdTrasmittente>");

  IdTrasmittente it1;
  CHECK(carica(p1, &it1));
  CHECK(it1 == it);
}

TEST_CASE("IscrizioneRea")
{
  using namespace felpa;

  IscrizioneRea ir;
  CHECK(ir.tag() == "IscrizioneREA");
  CHECK(ir.empty());
  CHECK(ir == ir);

  CHECK(ir.stato_liquidazione == "LN");
  CHECK(ir.capitale_sociale.empty());
  CHECK(std::string(ir.capitale_sociale) == "");
  CHECK(ir.socio_unico.empty());

  tinyxml2::XMLPrinter p;

  IscrizioneRea compilato;
  compilato.ufficio = "PI";
  compilato.numero_rea = "1234";
  CHECK(!compilato.empty());
  CHECK_NOTHROW(dettagli::registra(p, compilato));
  CHECK(clear_formatting(p.CStr())
        == "<IscrizioneREA>"
           "<Ufficio>" + compilato.ufficio + "</Ufficio>"
           "<NumeroREA>" + compilato.numero_rea + "</NumeroREA>"
           "<StatoLiquidazione>LN</StatoLiquidazione>"
           "</IscrizioneREA>");

  IscrizioneRea compilato1;
  CHECK(carica(p, &compilato1));
  CHECK(compilato1 == compilato);

  ir = compilato;
  ir.ufficio = "";
  CHECK_THROWS_AS(dettagli::registra(p, ir),
                  eccezione::iscrizione_rea_cod_ufficio_mancante);

  ir = compilato;
  ir.ufficio = "PISA";
  CHECK_THROWS_AS(dettagli::registra(p, ir),
                  eccezione::iscrizione_rea_cod_ufficio_scorretto);

  ir = compilato;
  ir.socio_unico = "SI";
  CHECK_THROWS_AS(dettagli::registra(p, ir),
                  eccezione::iscrizione_rea_socio_unico_scorretto);
}

TEST_CASE("FormatoTrasmissione")
{
  using namespace felpa;

  FormatoTrasmissione f1(formato_fattura::FPR);
  CHECK(f1 == f1);
  CHECK(f1.privato());
  CHECK(!f1.pubblico());
  CHECK(f1.tag() == "FormatoTrasmissione");

  FormatoTrasmissione f2(f1);
  CHECK(f1 == f2);
  f2 = formato_fattura::FPA;
  CHECK(!f2.privato());
  CHECK(f2.pubblico());

  tinyxml2::XMLPrinter p;

  CHECK_NOTHROW(dettagli::registra(p, f1));
  CHECK(clear_formatting(p.CStr())
        == "<FormatoTrasmissione>"
           + std::string(f1) +
           "</FormatoTrasmissione>");

  FormatoTrasmissione f3(f1);
  CHECK(carica(p, &f3));
  CHECK(f3 == f1);

  FormatoTrasmissione f4(formato_fattura::FPA);
  CHECK(!(f1 == f4));
}

TEST_CASE("CodiceDestinatario")
{
  using namespace felpa;

  CodiceDestinatario c;
  CHECK(c.tag() == "CodiceDestinatario");
  CHECK(c.empty());
  CHECK(c == c);

  c = "XXXXXXX";
  CHECK(!c.empty());
  CHECK(c.estero());
  CHECK(!c.pubblico());
  CHECK(c.privato());

  c = "B1V1D9";
  CHECK(!c.empty());
  CHECK(!c.estero());
  CHECK(c.pubblico());
  CHECK(!c.privato());

  c = "KRRH6B9";
  CHECK(!c.empty());
  CHECK(!c.estero());
  CHECK(!c.pubblico());
  CHECK(c.privato());

  tinyxml2::XMLPrinter p;

  CHECK_NOTHROW(dettagli::registra(p, c));
  CHECK(clear_formatting(p.CStr())
        == "<CodiceDestinatario>"
           + std::string(c) +
           "</CodiceDestinatario>");

  CodiceDestinatario c1(c);
  CHECK(carica(p, &c1));
  CHECK(c1 == c);
}

TEST_CASE("DatiTrasporto")
{
  using namespace felpa;

  DatiTrasporto dt;

  CHECK(dt.tag() == "DatiTrasporto");
  CHECK(dt.empty());
  CHECK(dt == dt);

  {
    tinyxml2::XMLPrinter p;
    CHECK_NOTHROW(dettagli::registra(p, dt));
    CHECK(clear_formatting(p.CStr()) == "");
  }

  for (unsigned i(0); !oggetto_valido<DatiTrasporto>(i).empty(); ++i)
  {
    dt = oggetto_valido<DatiTrasporto>(i);

    tinyxml2::XMLPrinter p;
    CHECK_NOTHROW(dettagli::registra(p, dt));

    CHECK(clear_formatting(p.CStr()) == xml_oggetto_valido<DatiTrasporto>(i));

    DatiTrasporto dt1(dt);
    CHECK(carica(p, &dt1));
    CHECK(dt1 == dt);

    CHECK_NOTHROW(dettagli::registra(p, dt1));
  }
}

TEST_CASE("DatiTrasmissione")
{
  using namespace felpa;

  {
    DatiTrasmissione dt(formato_fattura::FPA);

    tinyxml2::XMLPrinter p;
    CHECK_THROWS_AS(dettagli::registra(p, dt), eccezione::felpa);

    dt.codice_destinatario = "AAAAAA";
    CHECK_THROWS_AS(dettagli::registra(p, dt), eccezione::felpa);

    dt.id_trasmittente.id_codice = piva_valida;
    dt.codice_destinatario = "AAAAAA";
    dt.progressivo_invio = "";
    CHECK_THROWS_AS(dettagli::registra(p, dt), eccezione::prog_invio_mancante);

    dt.progressivo_invio = "0001";
    dt.codice_destinatario = "AAAAAAA";
    CHECK_THROWS_AS(dettagli::registra(p, dt),
                    eccezione::cod_destinatario_errato);
  }

  for (unsigned i(0); !oggetto_valido<DatiTrasmissione>(i).empty(); ++i)
  {
    auto dt(oggetto_valido<DatiTrasmissione>(i));

    tinyxml2::XMLPrinter p;
    CHECK_NOTHROW(dettagli::registra(p, dt));
    CHECK(clear_formatting(p.CStr())
          == xml_oggetto_valido<DatiTrasmissione>(i));

    DatiTrasmissione dt1;
    CHECK(carica(p, &dt1));
    CHECK(dt1 == dt);

    if (dt.formato_trasmissione.privato())
      dt.codice_destinatario = "AAAAAA";
    else
      dt.codice_destinatario = "AAAAAAA";
    CHECK_THROWS_AS(dettagli::registra(p, dt),
    eccezione::cod_destinatario_errato);
  }
}

TEST_CASE("IdTrasmittente")
{
  using namespace felpa;

  IdTrasmittente ii;
  CHECK(ii.tag() == "IdTrasmittente");
  CHECK(ii.empty());
  CHECK(ii == ii);

  tinyxml2::XMLPrinter p;
  CHECK_THROWS_AS(dettagli::registra(p, ii), eccezione::felpa);

  tinyxml2::XMLPrinter p1;
  ii.id_codice = piva_valida;
  CHECK(!ii.empty());

  CHECK_NOTHROW(dettagli::registra(p1, ii));
  CHECK(clear_formatting(p1.CStr())
        == "<IdTrasmittente>"
           "<IdPaese>IT</IdPaese>"
           "<IdCodice>" + ii.id_codice + "</IdCodice>"
           "</IdTrasmittente>");

  IdTrasmittente ii1;
  CHECK(carica(p1, &ii1));
  CHECK(ii1 == ii);
}

TEST_CASE("Anagrafica")
{
  using namespace felpa;

  Anagrafica srl(oggetto_valido<Anagrafica>(0));
  Anagrafica mario(oggetto_valido<Anagrafica>(1));

  tinyxml2::XMLPrinter p;

  // Nessun valore specificato!
  {
    Anagrafica a;
    CHECK(a == a);

    CHECK_THROWS_AS(dettagli::registra(p, a), eccezione::felpa);
  }

  Anagrafica a(srl);
  a.cod_eori = "ABCDEF";
  CHECK_THROWS_AS(dettagli::registra(p, a), eccezione::anagrafica_cod_eori);

  a.cod_eori = "ABCDEFGHILMNOPQRSTUVZ";
  CHECK_THROWS_AS(dettagli::registra(p, a), eccezione::anagrafica_cod_eori);

  a = mario;
  a.cognome = "";
  CHECK_THROWS_AS(dettagli::registra(p, a),
                  eccezione::anagrafica_cognome_mancante);

  a = mario;
  a.nome = "";
  CHECK_THROWS_AS(dettagli::registra(p, a),
                  eccezione::anagrafica_nome_mancante);

  a = mario;
  a.denominazione = srl.denominazione;
  CHECK_THROWS_AS(dettagli::registra(p, a), eccezione::anagrafica_3);

  a = mario;
  a.cod_eori = "A";
  CHECK_THROWS_AS(dettagli::registra(p, a), eccezione::anagrafica_cod_eori);

  tinyxml2::XMLPrinter p_srl;
  CHECK_NOTHROW(dettagli::registra(p_srl, srl));
  Anagrafica srl1;
  CHECK(carica(p_srl, &srl1));
  CHECK(srl1 == srl);

  tinyxml2::XMLPrinter p_mario;
  CHECK_NOTHROW(dettagli::registra(p_mario, mario));
  Anagrafica mario1;
  CHECK(carica(p_mario, &mario1));
  CHECK(mario1 == mario);
}

TEST_CASE("RegimeFiscale")
{
  using namespace felpa;

  RegimeFiscale rf;
  CHECK(std::string(rf) == "RF01");

  CHECK_THROWS_AS(rf = "PROVA", eccezione::cod_regime_fiscale);

  rf = "RF19";
  tinyxml2::XMLPrinter p;
  CHECK_NOTHROW(dettagli::registra(p, rf));
  CHECK(clear_formatting(p.CStr()) == "<RegimeFiscale>"
                                      + std::string(rf)
                                      + "</RegimeFiscale>");

  RegimeFiscale rf1;
  CHECK(carica(p, &rf1));
  CHECK(std::string(rf1) == std::string(rf));
}

TEST_CASE("Sede")
{
  using namespace felpa;

  CHECK(Sede().tag() == "Sede");

  // Mancano tutti i dati!
  {
    tinyxml2::XMLPrinter p;
    CHECK_THROWS_AS(dettagli::registra(p, Sede()), eccezione::felpa);
  }

  // Tutti i dati necessari inseriti, nessuna eccezione
  for (unsigned i(0); !oggetto_valido<Sede>(i).empty(); ++i)
  {
    Sede s(oggetto_valido<Sede>(i));

    tinyxml2::XMLPrinter p;
    CHECK_NOTHROW(dettagli::registra(p, s));
    CHECK(clear_formatting(p.CStr()) == xml_oggetto_valido<Sede>(i));

    Sede s1;
    CHECK(carica(p, &s1));
    CHECK(s1 == s);
  }

  const Sede completo(oggetto_valido<Sede>(0));
  tinyxml2::XMLPrinter p;

  Sede s(completo);
  s.indirizzo = "";
  CHECK_THROWS_AS(dettagli::registra(p, s), eccezione::indirizzo_mancante);

  s = completo;
  s.cap = "1234";
  CHECK_THROWS_AS(dettagli::registra(p, s),
                  eccezione::indirizzo_cap_scorretto);

  s = completo;
  s.comune = "";
  CHECK_THROWS_AS(dettagli::registra(p, s),
                  eccezione::indirizzo_comune_mancante);

  s = completo;
  s.provincia = "PISA";
  CHECK_THROWS_AS(dettagli::registra(p, s),
                  eccezione::indirizzo_formato_provincia);

  s = completo;
  s.indirizzo = "VIA STRADA, 1";
  CHECK_THROWS_AS(dettagli::registra(p, s), eccezione::indirizzo_nr_2);
}

TEST_CASE("DatiAnagraficiVettore")
{
  using namespace felpa;

  DatiAnagraficiVettore dav;
  CHECK(dav.empty());
  CHECK(dav.tag() == "DatiAnagraficiVettore");
  CHECK(dav == dav);

  for (unsigned i(0); !oggetto_valido<DatiAnagraficiVettore>(i).empty(); ++i)
  {
    dav = oggetto_valido<DatiAnagraficiVettore>(i);

    tinyxml2::XMLPrinter p;
    CHECK_NOTHROW(dettagli::registra(p, dav));

    CHECK(clear_formatting(p.CStr())
          == xml_oggetto_valido<DatiAnagraficiVettore>(i));

    DatiAnagraficiVettore dav1;
    CHECK(carica(p, &dav1));
    CHECK(dav1 == dav);

    CHECK_NOTHROW(dettagli::registra(p, dav1));
  }
}

TEST_CASE("DatiAnagraficiCedente")
{
  using namespace felpa;

  DatiAnagraficiCedente dac;
  CHECK(dac.empty());
  CHECK(dac.tag() == "DatiAnagrafici");
  CHECK(dac == dac);

  // Tentativo di registrazione con oggetto vuoto.
  {
    tinyxml2::XMLPrinter p;
    CHECK_THROWS_AS(dettagli::registra(p, DatiAnagraficiCedente()),
                    eccezione::felpa);
  }

  for (unsigned i(0); !oggetto_valido<DatiAnagraficiCedente>(i).empty(); ++i)
  {
    dac = oggetto_valido<DatiAnagraficiCedente>(i);

    tinyxml2::XMLPrinter p;
    CHECK_NOTHROW(dettagli::registra(p, dac));
    CHECK(clear_formatting(p.CStr())
          == xml_oggetto_valido<DatiAnagraficiCedente>(i));

    DatiAnagraficiCedente dac1;
    CHECK(carica(p, &dac1));
    CHECK(dac1 == dac);

    CHECK_NOTHROW(dettagli::registra(p, dac1));
  }
}

TEST_CASE("CedentePrestatore")
{
  using namespace felpa;

  CHECK(CedentePrestatore().tag() == "CedentePrestatore");

  {
    tinyxml2::XMLPrinter p;
    // Tentativo di registrazione con oggetto vuoto.
    CHECK_THROWS_AS(dettagli::registra(p, CedentePrestatore()),
                    eccezione::felpa);
  }

  for (unsigned i(0); !oggetto_valido<CedentePrestatore>(i).empty(); ++i)
  {
    auto cp(oggetto_valido<CedentePrestatore>(i));
    CHECK(!cp.empty());

    tinyxml2::XMLPrinter p;

    CHECK_NOTHROW(dettagli::registra(p, cp));
    CHECK(clear_formatting(p.CStr())
          == xml_oggetto_valido<CedentePrestatore>(i));

    CedentePrestatore cp1;
    CHECK(carica(p, &cp1));
    CHECK(cp1 == cp);

    tinyxml2::XMLPrinter p1;
    CHECK_NOTHROW(dettagli::registra(p, cp1));

    cp.sede = Sede();
    CHECK_THROWS_AS(dettagli::registra(p, cp), eccezione::felpa);

    cp = cp1;
    cp.dati_anagrafici = DatiAnagraficiCedente();
    CHECK_THROWS_AS(dettagli::registra(p, cp), eccezione::felpa);
  }
}

TEST_CASE("CessionarioCommittente")
{
  using namespace felpa;

  CHECK(CessionarioCommittente().tag() == "CessionarioCommittente");

  {
    // Tentativo di registrazione con oggetto vuoto.
    tinyxml2::XMLPrinter p;
    CHECK_THROWS_AS(dettagli::registra(p, CessionarioCommittente()),
                    eccezione::felpa);
  }

  for (unsigned i(0); !oggetto_valido<CessionarioCommittente>(i).empty(); ++i)
  {
    auto cc(oggetto_valido<CessionarioCommittente>(i));
    CHECK(!cc.empty());

    tinyxml2::XMLPrinter p;

    CHECK_NOTHROW(dettagli::registra(p, cc));
    CHECK(clear_formatting(p.CStr())
          == xml_oggetto_valido<CessionarioCommittente>(i));

    CessionarioCommittente cc1;
    CHECK(carica(p, &cc1));
    CHECK(cc1 == cc);

    tinyxml2::XMLPrinter p1;
    CHECK_NOTHROW(dettagli::registra(p, cc1));

    cc.sede = Sede();
    CHECK_THROWS_AS(dettagli::registra(p, cc), eccezione::felpa);

    cc = cc1;
    cc.dati_anagrafici = DatiAnagraficiCessionario();
    CHECK_THROWS_AS(dettagli::registra(p, cc), eccezione::felpa);
  }
}

TEST_CASE("EsigibilitaIva")
{
  using namespace felpa;

  EsigibilitaIva ei("I");
  CHECK(ei.tag() == "EsigibilitaIVA");

  CHECK_THROWS_AS(ei = "K", eccezione::cod_esig_iva);
  CHECK_NOTHROW(ei = "S");
  CHECK_NOTHROW(ei = "D");

  tinyxml2::XMLPrinter p;
  CHECK_NOTHROW(dettagli::registra(p, ei));
  CHECK(clear_formatting(p.CStr()) == "<EsigibilitaIVA>D</EsigibilitaIVA>");

  EsigibilitaIva ei1("I");
  CHECK(carica(p, &ei1));
  CHECK(std::string(ei1) == std::string(ei));
}

TEST_CASE("Data")
{
  using namespace felpa;

  Data d;
  CHECK(d.empty());
  CHECK(d.tag() == "Data");

  CHECK_THROWS_AS(d = "28/07/1973", eccezione::formato_data);

  CHECK_NOTHROW(d = "1973-07-28");
  CHECK(!d.empty());

  tinyxml2::XMLPrinter p;
  CHECK_NOTHROW(dettagli::registra(p, d));
  CHECK(clear_formatting(p.CStr()) == "<Data>1973-07-28</Data>");

  Data d1;
  CHECK(carica(p, &d1));
  CHECK(std::string(d1) == std::string(d));
}

TEST_CASE("Importo")
{
  using namespace felpa;

  Importo v;
  CHECK(v.tag() == "Importo");

  CHECK(v.empty());
  CHECK(std::string(v) == "");

  v = 32.175;
  CHECK(std::string(v) == "32.18");
  CHECK(v.to_double() == doctest::Approx(32.18));

  v = 3.705;
  CHECK(std::string(v) == "3.71");
  CHECK(v.to_double() == doctest::Approx(3.71));

  v = "315.275";
  CHECK(std::string(v) == "315.28");
  CHECK(v.to_double() == doctest::Approx(315.28));

  tinyxml2::XMLPrinter p;
  CHECK_NOTHROW(dettagli::registra(p, v));
  CHECK(clear_formatting(p.CStr()) == "<Importo>315.28</Importo>");

  CHECK(arrotondamento_2_decimali<Importo>());

  Importo v1;
  CHECK(carica(p, &v1));
  CHECK(std::string(v1) == std::string(v));
}

TEST_CASE("Natura")
{
  using namespace felpa;

  Natura n;
  CHECK(n.tag() == "Natura");
  CHECK(n.empty());

  CHECK_THROWS_AS(n = "XX", eccezione::natura_cod_scorretto);
  CHECK_THROWS_AS(n = "N2", eccezione::natura_cod_scorretto);

  CHECK_NOTHROW(n = "N1");

  tinyxml2::XMLPrinter p;
  CHECK_NOTHROW(dettagli::registra(p, n));
  CHECK(clear_formatting(p.CStr()) == "<Natura>N1</Natura>");

  Natura n1;
  CHECK(carica(p, &n1));
  CHECK(std::string(n1) == std::string(n));
}

TEST_CASE("AliquotaIVA")
{
  using namespace felpa;

  AliquotaIva a;
  CHECK(a.tag() == "AliquotaIVA");
  CHECK(!a.empty());
  CHECK(std::string(a) == "22.00");
  CHECK(a.to_double() == doctest::Approx(22.0));

  a = 10.0;
  CHECK(std::string(a) == "10.00");
  CHECK(a.to_double() == doctest::Approx(10.0));

  tinyxml2::XMLPrinter p;
  CHECK_NOTHROW(dettagli::registra(p, a));
  CHECK(clear_formatting(p.CStr()) == "<AliquotaIVA>10.00</AliquotaIVA>");

  AliquotaIva a1;
  CHECK(carica(p, &a1));
  CHECK(std::string(a1) == std::string(a));
}

TEST_CASE("Percentuale")
{
  using namespace felpa;

  Percentuale p;
  CHECK(p.tag() == "Percentuale");

  CHECK(p.empty());
  CHECK(std::string(p) == "");

  p = 32.175;
  CHECK(std::string(p) == "32.18");
  CHECK(p.to_double() == doctest::Approx(32.18));

  p = 3.705;
  CHECK(std::string(p) == "3.71");
  CHECK(p.to_double() == doctest::Approx(3.71));

  tinyxml2::XMLPrinter pr;
  CHECK_NOTHROW(dettagli::registra(pr, p));
  CHECK(clear_formatting(pr.CStr()) == "<Percentuale>3.71</Percentuale>");

  Percentuale p1;
  CHECK(carica(pr, &p1));
  CHECK(std::string(p1) == std::string(p));
}

TEST_CASE("ScontoMaggiorazione")
{
  using namespace felpa;

  CHECK(ScontoMaggiorazione().tag() == "ScontoMaggiorazione");
  CHECK(ScontoMaggiorazione().empty());

  ScontoMaggiorazione smp;
  smp.tipo = "SC";
  CHECK(smp.empty());
  smp.percentuale = 8.0;
  CHECK(!smp.empty());
  CHECK(!smp.percentuale.empty());
  CHECK(smp.importo.empty());

  ScontoMaggiorazione smi;
  CHECK_NOTHROW(smi = smp);
  smi.importo = "0.50";
  smi.percentuale = Percentuale();
  CHECK(!smi.empty());
  CHECK(smi.percentuale.empty());
  CHECK(!smi.importo.empty());

  {
    tinyxml2::XMLPrinter p;
    ScontoMaggiorazione sm(smi);
    sm.tipo = "XX";
    CHECK_THROWS_AS(dettagli::registra(p, sm),
                    eccezione::sconto_mag_cod_tipo_scorretto);
  }

  {
    tinyxml2::XMLPrinter p;
    ScontoMaggiorazione sm(smi);
    sm.percentuale = 8.0;
    CHECK_THROWS_AS(dettagli::registra(p, sm), eccezione::sconto_mag_2);
  }

  for (unsigned i(0); !oggetto_valido<ScontoMaggiorazione>(i).empty(); ++i)
  {
    auto sm(oggetto_valido<ScontoMaggiorazione>(i));

    tinyxml2::XMLPrinter p;
    CHECK_NOTHROW(dettagli::registra(p, sm));
    CHECK(clear_formatting(p.CStr())
          == xml_oggetto_valido<ScontoMaggiorazione>(i));

    ScontoMaggiorazione sm1;
    CHECK(carica(p, &sm1));
    CHECK(sm1 == sm);
  }
}

TEST_CASE("AltriDatiGestionali")
{
  using namespace felpa;

  CHECK(AltriDatiGestionali().tag() == "AltriDatiGestionali");
  CHECK(AltriDatiGestionali().empty());
  CHECK(AltriDatiGestionali() == AltriDatiGestionali());

  for (unsigned i(0); !oggetto_valido<AltriDatiGestionali>(i).empty(); ++i)
  {
    auto adg(oggetto_valido<AltriDatiGestionali>(i));

    tinyxml2::XMLPrinter p;
    CHECK_NOTHROW(dettagli::registra(p, adg));
    CHECK(clear_formatting(p.CStr())
          == xml_oggetto_valido<AltriDatiGestionali>(i));

    AltriDatiGestionali adg1;
    CHECK(carica(p, &adg1));
    CHECK(adg1 == adg);

    adg.tipo_dato = "";
    CHECK_THROWS_AS(dettagli::registra(p, adg),
                    eccezione::altri_dati_gestionali_tipo_dato);
  }
}

TEST_CASE("DettaglioPagamento")
{
  using namespace felpa;

  DettaglioPagamento dp;
  CHECK(dp.tag() == "DettaglioPagamento");
  CHECK(dp.empty());
  CHECK(dp == dp);

  {
    tinyxml2::XMLPrinter p;
    CHECK_THROWS_AS(dettagli::registra(p, dp), eccezione::felpa);

    dp = oggetto_valido<DettaglioPagamento>(1);
    dp.modalita_pagamento = "";
    CHECK_THROWS_AS(dettagli::registra(p, dp),
                    eccezione::dett_pagamento_modalita_mancante);

    dp = oggetto_valido<DettaglioPagamento>(1);
    dp.importo_pagamento = "";
    CHECK_THROWS_AS(dettagli::registra(p, dp),
                    eccezione::dett_pagamento_importo_mancante);
  }

  for (unsigned i(0); !oggetto_valido<DettaglioPagamento>(i).empty(); ++i)
  {
    dp = oggetto_valido<DettaglioPagamento>(i);

    tinyxml2::XMLPrinter p;
    CHECK_NOTHROW(dettagli::registra(p, dp));
    CHECK(clear_formatting(p.CStr())
          == xml_oggetto_valido<DettaglioPagamento>(i));

    DettaglioPagamento dp1;
    CHECK(carica(p, &dp1));

    if (std::string(dp.modalita_pagamento) == "MP01" && !dp.iban.empty())
    {
      CHECK(!(dp == dp1));
      dp1.iban = dp.iban;
    }

    CHECK(dp == dp1);
  }
}

TEST_CASE("DatiPagamento")
{
  using namespace felpa;

  DatiPagamento dp;
  CHECK(dp.tag() == "DatiPagamento");
  CHECK(dp.empty());
  CHECK(dp == dp);

  {
    tinyxml2::XMLPrinter p;
    CHECK_NOTHROW(dettagli::registra(p, dp));
    CHECK(clear_formatting(p.CStr()) == "");
  }

  for (unsigned i(0); !oggetto_valido<DatiPagamento>(i).empty(); ++i)
  {
    dp = oggetto_valido<DatiPagamento>(i);

    tinyxml2::XMLPrinter p;
    CHECK_NOTHROW(dettagli::registra(p, dp));
    CHECK(clear_formatting(p.CStr())
          == xml_oggetto_valido<DatiPagamento>(i));

    DatiPagamento dp1;
    CHECK(carica(p, &dp1));
    CHECK(dp1 == dp);
    CHECK_NOTHROW(dettagli::registra(p, dp1));
  }
}

TEST_CASE("Quantita")
{
  using namespace felpa;

  Quantita q;
  CHECK(q.tag() == "Quantita");

  CHECK(std::string(q) == "1.000000");
  CHECK(q.to_double() == doctest::Approx(1.0));

  CHECK_THROWS_AS(q = "prova", eccezione::numero_cod);
  CHECK_THROWS_AS(q = "-1.0", eccezione::numero_cod);

  CHECK_NOTHROW(q = 1234.5678);
  CHECK(q.to_double() == doctest::Approx(1234.5678));
  CHECK(std::string(q) =="1234.567800");

  CHECK_NOTHROW(q = 1234.5678678);
  CHECK(q.to_double() == doctest::Approx(1234.567868));
  CHECK(std::string(q) =="1234.567868");

  CHECK_NOTHROW(q = 0.298475);
  CHECK(q.to_double() == doctest::Approx(0.298475));
  CHECK(std::string(q) =="0.298475");

  CHECK_NOTHROW(q = 0.2999995);
  CHECK(q.to_double() == doctest::Approx(0.3));
  CHECK(std::string(q) =="0.300000");

  CHECK_NOTHROW(q = 0.2700005);
  CHECK(q.to_double() == doctest::Approx(0.270001));
  CHECK(std::string(q) == "0.270001");

  CHECK_NOTHROW(q = "2.0000");
  CHECK(q.to_double() == doctest::Approx(2.0));
  CHECK(std::string(q) == "2.000000");

  tinyxml2::XMLPrinter p1;
  CHECK_NOTHROW(dettagli::registra(p1, q));
  CHECK(clear_formatting(p1.CStr()) == "<Quantita>2.000000</Quantita>");

  CHECK_NOTHROW(q = "");
  CHECK(std::string(q) == "");

  tinyxml2::XMLPrinter p2;
  CHECK_NOTHROW(dettagli::registra(p2, q));
  CHECK(clear_formatting(p2.CStr()) == "");
}

TEST_CASE("DatiDDT")
{
  using namespace felpa;

  DatiDDT d;
  CHECK(d.tag() == "DatiDDT");
  CHECK(d.empty());
  CHECK(d == d);

  // Tentativo di registrazione con oggetto vuoto.
  {
    tinyxml2::XMLPrinter p;
    CHECK_NOTHROW(dettagli::registra(p, d));
    CHECK(clear_formatting(p.CStr()) == "");
  }

  // Verifica eccezioni
  {
    tinyxml2::XMLPrinter p;
    d.numero_ddt = "K1234";
    CHECK_THROWS_AS(dettagli::registra(p, d), eccezione::data_doc_mancante);
  }

  d = oggetto_valido<DatiDDT>();

  tinyxml2::XMLPrinter p;
  CHECK_NOTHROW(dettagli::registra(p, d));

  CHECK(clear_formatting(p.CStr()) == xml_oggetto_valido<DatiDDT>());

  DatiDDT d1;
  CHECK(carica(p, &d1));
  CHECK(d1 == d);

  CHECK_NOTHROW(dettagli::registra(p, d1));
}

TEST_CASE("DettaglioLinee")
{
  using namespace felpa;

  DettaglioLinee dl;
  CHECK(dl.tag() == "DettaglioLinee");
  CHECK(dl.empty());
  CHECK(dl == dl);

  // Tentativo di registrazione con oggetto vuoto.
  {
    tinyxml2::XMLPrinter p;
    CHECK_THROWS_AS(dettagli::registra(p, 1, dl), eccezione::felpa);
  }

  DettaglioLinee min_ok(oggetto_valido<DettaglioLinee>());

  // Tentativo di registrazione senza linea.
  {
    tinyxml2::XMLPrinter p;
    dl = min_ok;
    CHECK_THROWS_AS(dettagli::registra(p, 0, dl),
                    eccezione::dett_linee_numero_mancante);
  }

  // Tentativo di registrazione senza descrizione.
  {
    tinyxml2::XMLPrinter p;
    dl = min_ok;
    dl.descrizione = "";
    CHECK_THROWS_AS(dettagli::registra(p, 1, dl),
                    eccezione::dett_linee_dex_mancante);
  }

  // Tentativo di registrazione senza prezzo unitario.
  {
    tinyxml2::XMLPrinter p;
    dl = min_ok;
    dl.prezzo_unitario = "";
    CHECK_THROWS_AS(dettagli::registra(p, 1, dl),
                    eccezione::dett_linee_pu_mancante);
  }

  // Tentativo di registrazione senza prezzo unitario.
  {
    tinyxml2::XMLPrinter p;
    dl = min_ok;
    dl.aliquota_iva = "";
    dl.natura = "N1";
    CHECK_THROWS_AS(dettagli::registra(p, 1, dl),
                    eccezione::dett_linee_conflitto_natura);
  }

  dl = completa(min_ok);
  CHECK(std::string(dl.prezzo_totale) == "3.50");
  CHECK(dl.prezzo_totale.to_double() == doctest::Approx(3.5));
  CHECK(std::string(dl.ritenuta) == Ritenuta::NO);

  dl = min_ok;
  dl.sconto_maggiorazione.tipo = "SC";
  dl.sconto_maggiorazione.percentuale = 8.0;
  dl = completa(dl);
  CHECK(std::string(dl.prezzo_totale) == "3.22");
  CHECK(dl.prezzo_totale.to_double() == doctest::Approx(3.22));

  dl = min_ok;
  dl.sconto_maggiorazione.tipo = "SC";
  dl.sconto_maggiorazione.importo = "0.50";
  dl = completa(dl);
  CHECK(std::string(dl.prezzo_totale) == "2.50");
  CHECK(dl.prezzo_totale.to_double() == doctest::Approx(2.5));

  for (unsigned i(0); !oggetto_valido<DettaglioLinee>(i).empty(); ++i)
  {
    const auto d(oggetto_valido<DettaglioLinee>(i));

    tinyxml2::XMLPrinter p;
    CHECK_NOTHROW(dettagli::registra(p, i + 1, d));
    CHECK(clear_formatting(p.CStr())
          == xml_oggetto_valido<DettaglioLinee>(i, i + 1));

    DettaglioLinee d1;
    CHECK(carica(p, &d1));
    CHECK_NOTHROW(dettagli::registra(p, i + 1, d1));

    CHECK(!(d1 == d));
    CHECK(d1 == completa(d));

    const PrezzoTotale ptc(d1.prezzo_unitario_effettivo().to_double()
                           * d1.quantita.to_double());
    CHECK(std::string(ptc) == std::string(d1.prezzo_totale));
  }
}

TEST_CASE("DatiBeniServizi")
{
  using namespace felpa;


  // Tentativo di registrazione con oggetto vuoto.
  {
    DatiBeniServizi dbs(EsigibilitaIva("S"));
    tinyxml2::XMLPrinter p;
    CHECK_THROWS_AS(dettagli::registra(p, dbs), eccezione::dett_linee_vuoto);
  }

  DatiBeniServizi dbs1(oggetto_valido<DatiBeniServizi>(0));

  // Riepilogo contenente una sola aliquota iva.
  CHECK(dbs1.dati_riepilogo.size() == 1);

  // Calcolo automatico del prezzo totale per ogni linea.
  CHECK(std::string(dbs1.dettaglio_linee[0].prezzo_totale) == "5.00");
  CHECK(dbs1.dettaglio_linee[0].prezzo_totale.to_double() == 5.0);
  CHECK(std::string(dbs1.dettaglio_linee[1].prezzo_totale) == "20.00");
  CHECK(dbs1.dettaglio_linee[1].prezzo_totale.to_double()
        == doctest::Approx(20.0));

  // Impostazione automatica iva default.
  CHECK(std::string(dbs1.dettaglio_linee[0].aliquota_iva) == "22.00");
  CHECK(dbs1.dettaglio_linee[0].aliquota_iva.to_double()
        == doctest::Approx(22.0));

  CHECK(std::string(dbs1.dettaglio_linee[1].aliquota_iva) == "22.00");
  CHECK(dbs1.dettaglio_linee[1].aliquota_iva.to_double()
        == doctest::Approx(22.0));

  // Calcolo imponibile totale.
  CHECK(std::string(dbs1.dati_riepilogo[0].imponibile_importo) == "25.00");
  CHECK(dbs1.dati_riepilogo[0].imponibile_importo.to_double()
        == doctest::Approx(25.0));

  // Calcolo imposta totale.
  CHECK(std::string(dbs1.dati_riepilogo[0].imposta) == "5.50");
  CHECK(dbs1.dati_riepilogo[0].imposta.to_double() == doctest::Approx(5.5));

  // ****************************************

  DatiBeniServizi dbs2(oggetto_valido<DatiBeniServizi>(1));

  CHECK(std::string(dbs2.dati_riepilogo[0].natura) == "N1");
  CHECK(dbs2.dati_riepilogo[0].riferimento_normativo == "RIFERIMENTO 1");

  CHECK(std::string(dbs2.dati_riepilogo[1].natura) == "N2.1");
  CHECK(dbs2.dati_riepilogo[1].riferimento_normativo == "RIFERIMENTO 2");

  CHECK(dbs2.riferimento_normativo(Natura("N1"))
        == dbs2.dati_riepilogo[0].riferimento_normativo);
  CHECK(dbs2.riferimento_normativo(Natura("N2.1"))
        == dbs2.dati_riepilogo[1].riferimento_normativo);
  CHECK(dbs2.riferimento_normativo(Natura("N6.9")) == "");

  // ****************************************

  for (unsigned i(0); !oggetto_valido<DatiBeniServizi>(i).empty(); ++i)
  {
    const auto d(oggetto_valido<DatiBeniServizi>(i));

    tinyxml2::XMLPrinter p;
    CHECK_NOTHROW(dettagli::registra(p, d));
    CHECK(clear_formatting(p.CStr()) == xml_oggetto_valido<DatiBeniServizi>(i));

    DatiBeniServizi d1(dbs1);
    CHECK(carica(p, &d1));
    CHECK_NOTHROW(dettagli::registra(p, d1));

    //CHECK(d1 == d);
  }
}

TEST_CASE("FatturaElettronicaBody")
{
  using namespace felpa;

  FatturaElettronicaBody b(EsigibilitaIva("I"));

  CHECK(b.tag() == "FatturaElettronicaBody");
  CHECK(b.empty());
  CHECK(b == b);

  // Tentativo di registrazione con oggetto vuoto.
  {
    tinyxml2::XMLPrinter p;
    CHECK_THROWS_AS(dettagli::registra(p, b), eccezione::felpa);
  }

  for (unsigned i(0); !oggetto_valido<FatturaElettronicaBody>(i).empty(); ++i)
  {
    const auto b(oggetto_valido<FatturaElettronicaBody>(i));

    tinyxml2::XMLPrinter p;
    CHECK_NOTHROW(dettagli::registra(p, b));
    CHECK(clear_formatting(p.CStr())
          == xml_oggetto_valido<FatturaElettronicaBody>(i));

    FatturaElettronicaBody b1(EsigibilitaIva("I"));
    CHECK(carica(p, &b1));
    CHECK_NOTHROW(dettagli::registra(p, b1));

    CHECK(b1 == b);
  }
}

TEST_CASE("FatturaElettronicaHeader")
{
  using namespace felpa;

  FatturaElettronicaHeader h(formato_fattura::FPR);

  CHECK(h.tag() == "FatturaElettronicaHeader");
  CHECK(h == h);

  // Tentativo di registrazione con oggetto vuoto.
  {
    tinyxml2::XMLPrinter p;
    CHECK_THROWS_AS(dettagli::registra(p, h), eccezione::felpa);
  }

  for (unsigned i(0); !oggetto_valido<FatturaElettronicaHeader>(i).empty(); ++i)
  {
    const auto h(oggetto_valido<FatturaElettronicaHeader>(i));

    tinyxml2::XMLPrinter p;
    CHECK_NOTHROW(dettagli::registra(p, h));
    CHECK(clear_formatting(p.CStr())
          == xml_oggetto_valido<FatturaElettronicaHeader>(i));

    FatturaElettronicaHeader h1(formato_fattura::FPR);
    CHECK(carica(p, &h1));
    CHECK(h1 == h);

    CHECK_NOTHROW(dettagli::registra(p, h1));
  }

  SUBCASE("Errore 00476")
  {
    auto h(oggetto_valido<FatturaElettronicaHeader>(0));
    h.cessionario_committente.dati_anagrafici.id_fiscale_iva.id_paese = "US";
    h.cedente_prestatore.dati_anagrafici.id_fiscale_iva.id_paese = "US";

    tinyxml2::XMLPrinter p;
    CHECK_THROWS_AS(dettagli::registra(p, h), eccezione::tutti_esteri);
  }
}

TEST_CASE("FatturaElettronica")
{
  using namespace felpa;

  FatturaElettronica f;

  f.header = oggetto_valido<FatturaElettronicaHeader>(1);
  f.body = oggetto_valido<FatturaElettronicaBody>(0);

  const auto fn(f.filename());

  SUBCASE("Base")
  {
    CHECK(
      std::string(f.body.dati_generali.dati_generali_documento
                  .importo_totale_documento)
      == "30.50");

    CHECK(
      std::string(f.body.dati_generali.dati_generali_documento.dati_ritenuta
                  .front().importo_ritenuta)
      == "4.00");

    CHECK_NOTHROW(f.registra(fn));
    CHECK_NOTHROW(f.carica(fn));
  }

  SUBCASE("TD18")
  {
    f.body.dati_generali.dati_generali_documento.tipo_documento = "TD18";
    CHECK_THROWS_AS(f.registra(fn), eccezione::tipo_doc_cod_scorretto);

    f.header.cedente_prestatore.dati_anagrafici.id_fiscale_iva.id_paese = "US";
    CHECK_THROWS_AS(f.registra(fn), eccezione::tipo_doc_cod_scorretto);
  }

  SUBCASE("TD29")
  {
    f.header.cedente_prestatore.dati_anagrafici.id_fiscale_iva.id_paese = "US";
    f.body.dati_generali.dati_generali_documento.tipo_documento = "TD29";
    CHECK_THROWS_AS(f.registra(fn), eccezione::tipo_doc_cod_scorretto);
  }

  SUBCASE("00471")
  {
    f.header.cedente_prestatore.dati_anagrafici.id_fiscale_iva =
      f.header.cessionario_committente.dati_anagrafici.id_fiscale_iva;
    CHECK_THROWS_AS(f.registra(fn), eccezione::stesso_cedente_cessionario);
  }
}

TEST_CASE("Caricamento")
{
  using namespace felpa;
  namespace fs = std::filesystem;

  const fs::path path("../res/");

  for (const auto &f : fs::directory_iterator(path))
  {
    const auto fn(f.path().filename().string());

    if (f.is_regular_file() && f.path().extension() == ".xml")
    {
      FatturaElettronica fe;
      CHECK_NOTHROW(fe.carica(fn));
    }
  }
}

TEST_CASE("DatiSAL")
{
  using namespace felpa;

  CHECK(DatiSAL().tag() == "DatiSAL");
  CHECK(DatiSAL().empty());
  CHECK(DatiSAL() == DatiSAL());

  tinyxml2::XMLPrinter p1;
  const DatiSAL empty;
  CHECK_NOTHROW(dettagli::registra(p1, empty));
  std::cout << clear_formatting(p1.CStr()) << std::endl;
  CHECK(clear_formatting(p1.CStr()).empty());

  for (unsigned i(0); !oggetto_valido<DatiSAL>(i).empty(); ++i)
  {
    auto sal(oggetto_valido<DatiSAL>(i));

    tinyxml2::XMLPrinter p;
    CHECK_NOTHROW(dettagli::registra(p, sal));
    CHECK(clear_formatting(p.CStr())
          == xml_oggetto_valido<DatiSAL>(i));

    DatiSAL sal1;
    CHECK(carica(p, &sal1));
    CHECK(sal1 == sal);

    sal.riferimento_fase = 1000;
    CHECK_THROWS_AS(dettagli::registra(p, sal),
                    eccezione::riferimento_sal_errato);
  }
}

TEST_CASE("fic_export")
{
  using namespace felpa;
  using json = nlohmann::json;

  FatturaElettronica f;
  f.header = oggetto_valido<FatturaElettronicaHeader>(1);
  f.body = oggetto_valido<FatturaElettronicaBody>(0);

  json j_all;
  CHECK_NOTHROW(j_all = json::parse(fic::export_json(f)));
  json &j(j_all["data"]);

  CHECK(j["entity"]["name"]
        == f.header.cessionario_committente.dati_anagrafici.anagrafica
           .denominazione);

  CHECK(j["withholding_tax"]
        == doctest::Approx(f.body.dati_generali.dati_generali_documento
                           .dati_ritenuta.front().aliquota_ritenuta
                           .to_double()));
  CHECK(j["withholding_tax_taxable"] == doctest::Approx(100.));

  CHECK(j["ei_raw"][FatturaElettronicaBody::tag()][DatiPagamento::tag()]
         [CondizioniPagamento::tag()] == "TP01");

  //std::cout << j.dump(2) << std::endl;
}

TEST_CASE("fic_vat_id")
{
  using namespace felpa;

  DatiBeniServizi dbs(oggetto_valido<DatiBeniServizi>(3));

  CHECK(std::string(dbs.dati_riepilogo[0].natura) == "N1");
  CHECK(dbs.dati_riepilogo[0].riferimento_normativo == "Esenti [FIC 7]");

  CHECK(std::string(dbs.dati_riepilogo[1].natura) == "N2.1");
  CHECK(dbs.dati_riepilogo[1].riferimento_normativo
        == "Non Imp. Art.72 [FIC 28]");

  CHECK(dbs.riferimento_normativo(Natura("N1"))
        == dbs.dati_riepilogo[0].riferimento_normativo);
  CHECK(dbs.riferimento_normativo(Natura("N2.1"))
        == dbs.dati_riepilogo[1].riferimento_normativo);

  CHECK(fic::vat_id(0.0, dbs.riferimento_normativo(Natura("N1"))) == 7);
  CHECK(fic::vat_id(0.0, dbs.riferimento_normativo(Natura("N2.1"))) == 28);

  CHECK(fic::vat_id(22.0) == 0);
  CHECK(fic::vat_id(10.0) == 3);
  CHECK(fic::vat_id(4.0) == 4);
}

}  // TEST_SUITE("FELPA")
