/**
 *  \file
 *  \remark This file is part of FELPA.
 *
 *  \copyright Copyright (C) 2015-2022 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#include <cassert>

#include "dati_anagrafici_type.hpp"
#include "fattura_elettronica_eccezioni.hpp"
#include "fattura_elettronica_dettagli.hpp"
#include "utility.hpp"

namespace felpa
{

DatiAnagraficiType::DatiAnagraficiType(const std::string &t)
  : id_fiscale_iva(), codice_fiscale(), anagrafica(), tag_(t)
{
  assert(!tag_.empty());
}

bool DatiAnagraficiType::operator==(const DatiAnagraficiType &rhs) const
{
  return id_fiscale_iva == rhs.id_fiscale_iva
         && codice_fiscale == rhs.codice_fiscale
         && anagrafica == rhs.anagrafica;
}

namespace dettagli
{

constexpr char CODICE_FISCALE_TAG[] = "CodiceFiscale";

constexpr std::size_t CODICE_FISCALE_SIZE = 16;

bool carica(tinyxml2::XMLConstHandle &ft, bool t, DatiAnagraficiType *da)
{
  tinyxml2::XMLConstHandle h(t ? ft.FirstChildElement(da->tag().c_str()) : ft);
  if (!h.ToElement())
    return false;

  const bool ifi(carica(h, &da->id_fiscale_iva));

  if (auto *he = h.FirstChildElement(CODICE_FISCALE_TAG).ToElement())
    da->codice_fiscale = coalesce(he->GetText());

  const bool an(carica(h, &da->anagrafica));

  return (ifi || da->codice_fiscale.length()) && an;
}

void registra(tinyxml2::XMLPrinter &ft, const DatiAnagraficiType &da, bool t)
{
  if (t)
    ft.OpenElement(da.tag().c_str());

  if (!da.id_fiscale_iva.empty())
    registra(ft, da.id_fiscale_iva);

  if (!da.codice_fiscale.empty())
  {
    ft.OpenElement(CODICE_FISCALE_TAG);
    push_text(ft, da.codice_fiscale.substr(0, CODICE_FISCALE_SIZE));
    ft.CloseElement();
  }

  registra(ft, da.anagrafica);

  if (t)
    ft.CloseElement();
}

}  // namespace dettagli

}  // namespace felpa
