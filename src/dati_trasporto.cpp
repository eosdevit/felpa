/**
 *  \file
 *  \remark This file is part of FELPA.
 *
 *  \copyright Copyright (C) 2015-2021 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#include <cassert>

#include "dati_trasporto.hpp"
#include "fattura_elettronica_dettagli.hpp"
#include "fattura_elettronica_eccezioni.hpp"
#include "utility.hpp"

namespace felpa
{

const std::string &DatiTrasporto::tag()
{
  static const std::string TAG("DatiTrasporto");
  return TAG;
}

DatiTrasporto::DatiTrasporto()
  : dati_anagrafici_vettore(), causale_trasporto(), numero_colli(0),
    data_ora_consegna()
{
}

bool DatiTrasporto::empty() const
{
  return *this == DatiTrasporto();
}

bool operator==(const DatiTrasporto &lhs, const DatiTrasporto &rhs)
{
  return lhs.dati_anagrafici_vettore == rhs.dati_anagrafici_vettore
         && lhs.causale_trasporto == rhs.causale_trasporto
         && lhs.numero_colli == rhs.numero_colli
         && std::string(lhs.data_ora_consegna)
            == std::string(rhs.data_ora_consegna);
}

namespace dettagli
{

constexpr std::size_t CAUSALE_TRASPORTO_SIZE = 100;
constexpr std::size_t MAX_NUMERO_COLLI = 9999;

constexpr char CAUSALE_TRASPORTO_TAG[] = "CausaleTrasporto";
constexpr char NUMERO_COLLI_TAG[] = "NumeroColli";

bool carica(tinyxml2::XMLConstHandle &ft, DatiTrasporto *dt)
{
  auto h(ft.FirstChildElement(dt->tag().c_str()));
  if (!h.ToElement())
    return false;

  carica(h, &dt->dati_anagrafici_vettore);

  if (auto *he = h.FirstChildElement(CAUSALE_TRASPORTO_TAG).ToElement())
    dt->causale_trasporto = coalesce(he->GetText())
    .substr(0, CAUSALE_TRASPORTO_SIZE);

  if (auto *he = h.FirstChildElement(NUMERO_COLLI_TAG).ToElement())
    dt->numero_colli = std::stoi(coalesce(he->GetText()));

  carica(h, &dt->data_ora_consegna);

  return true;
}

void registra(tinyxml2::XMLPrinter &ft, const DatiTrasporto &dt)
{
  if (dt.empty())
    return;

  ft.OpenElement(dt.tag().c_str());

  if (!dt.dati_anagrafici_vettore.empty())
    registra(ft, dt.dati_anagrafici_vettore);

  if (!dt.causale_trasporto.empty())
  {
    ft.OpenElement(CAUSALE_TRASPORTO_TAG);
    push_text(ft, dt.causale_trasporto.substr(0, CAUSALE_TRASPORTO_SIZE));
    ft.CloseElement();
  }

  if (0 < dt.numero_colli && dt.numero_colli <= MAX_NUMERO_COLLI)
  {
    ft.OpenElement(NUMERO_COLLI_TAG);
    push_text(ft, std::to_string(dt.numero_colli));
    ft.CloseElement();
  }

  if (!dt.data_ora_consegna.empty())
    registra(ft, dt.data_ora_consegna);

  ft.CloseElement();
}

}  // namespace dettagli

}  // namespace felpa
