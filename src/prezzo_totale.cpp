/**
 *  \file
 *  \remark This file is part of FELPA.
 *
 *  \copyright Copyright (C) 2015-2018 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#include <cassert>

#include "prezzo_totale.hpp"
#include "fattura_elettronica_eccezioni.hpp"
#include "fattura_elettronica_dettagli.hpp"

namespace felpa
{

PrezzoTotale::PrezzoTotale(const std::string &p)
  : NumeroType("PrezzoTotale", 2, anche_negativo)
{

  assign(p);
}

PrezzoTotale::PrezzoTotale(double p)
  : NumeroType("PrezzoTotale", 2, anche_negativo)
{
  assign(p);
}

PrezzoTotale &PrezzoTotale::operator=(const std::string &p)
{
  return *this = PrezzoTotale(p);
}

PrezzoTotale &PrezzoTotale::operator=(double p)
{
  return *this = PrezzoTotale(p);
}

}  // namespace felpa
