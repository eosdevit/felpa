/**
 *  \file
 *  \remark This file is part of FELPA.
 *
 *  \copyright Copyright (C) 2015-2024 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#if !defined(FELPA_NUMERO_TYPE_HPP)
#define      FELPA_NUMERO_TYPE_HPP

#include <string>

namespace felpa
{

class NumeroType
{
public:
  enum neg_flag {solo_positivo = 0, anche_negativo};

  [[nodiscard]] const std::string &tag() const { return tag_; }

  [[nodiscard]] bool empty() const;

  void assign(const std::string &);
  void assign(double);

  [[nodiscard]] operator std::string () const;
  [[nodiscard]] double to_double() const;

protected:
  NumeroType(const std::string &, unsigned, neg_flag);

private:
  [[nodiscard]] static bool valido(const std::string &, neg_flag);
  [[nodiscard]] static bool valido(double, neg_flag);

  // ********* Private data members *********
  std::string tag_;
  unsigned precisione_;
  neg_flag flag_negativo_;
  std::string n_;
};  // class NumeroType

}  // namespace felpa

#endif  // include guard
