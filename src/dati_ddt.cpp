/**
 *  \file
 *  \remark This file is part of FELPA.
 *
 *  \copyright Copyright (C) 2015-2024 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#include <cassert>

#include "dati_ddt.hpp"
#include "fattura_elettronica_dettagli.hpp"
#include "fattura_elettronica_eccezioni.hpp"
#include "utility.hpp"

namespace felpa
{

bool DatiDDT::empty() const
{
  return numero_ddt.empty();
}

bool operator==(const DatiDDT &lhs, const DatiDDT &rhs)
{
  return lhs.riferimento_numero_linea == rhs.riferimento_numero_linea
         && lhs.numero_ddt == rhs.numero_ddt
         && std::string(lhs.data_ddt) == std::string(rhs.data_ddt);
}

const std::string &DatiDDT::tag()
{
  static const std::string TAG("DatiDDT");
  return TAG;
}

namespace dettagli
{

constexpr char NUMERO_DDT_TAG[] = "NumeroDDT";
constexpr char RIFERIMENTO_NUMERO_LINEA_TAG[] = "RiferimentoNumeroLinea";

constexpr std::size_t MAX_NUMERO_LINEA = 9999;
constexpr std::size_t NUMERO_DDT_SIZE = 20;

bool carica(tinyxml2::XMLConstHandle &ft, DatiDDT *dt)
{
  tinyxml2::XMLConstHandle h(ft.FirstChildElement(dt->tag().c_str()));
  if (!h.ToElement())
    h = ft;

  if (auto *he = h.FirstChildElement(NUMERO_DDT_TAG).ToElement())
    dt->numero_ddt = coalesce(he->GetText()).substr(0, NUMERO_DDT_SIZE);

  carica(h, &dt->data_ddt);

  dt->riferimento_numero_linea.clear();
  for (auto *he(h.FirstChildElement(RIFERIMENTO_NUMERO_LINEA_TAG).ToElement());
       he;
       he = he->NextSiblingElement(RIFERIMENTO_NUMERO_LINEA_TAG))
  {
    if (he->GetText())
      try
      {
        const auto val(std::stoi(he->GetText()));
        dt->riferimento_numero_linea.insert(val);
      }
      catch (std::logic_error &) { /* skip */ }
  }

  return !dt->numero_ddt.empty() && !dt->data_ddt.empty();
}

void registra(tinyxml2::XMLPrinter &ft, const DatiDDT &dt)
{
  if (dt.empty())
    return;

  ft.OpenElement(dt.tag().c_str());

  if (dt.numero_ddt.empty())
    throw eccezione::numero_doc_mancante(dt.tag()
                                         + ": numero DDT mancante");

  ft.OpenElement(NUMERO_DDT_TAG);
  push_text(ft, dt.numero_ddt.substr(0, NUMERO_DDT_SIZE));
  ft.CloseElement();

  if (dt.data_ddt.empty())
    throw eccezione::data_doc_mancante(dt.tag() + ": data DDT mancante");

  registra(ft, dt.data_ddt);

  for (const auto &rif : dt.riferimento_numero_linea)
  {
    if (rif == 0 || rif > MAX_NUMERO_LINEA)
      throw eccezione::dati_doc_corr_linea_scorretta(
        DatiDDT::tag() + ": riferimento numero linea scorretto");

    ft.OpenElement(RIFERIMENTO_NUMERO_LINEA_TAG);
    ft.PushText(rif);
    ft.CloseElement();
  }

  ft.CloseElement();
}

}  // namespace dettagli

}  // namespace felpa
