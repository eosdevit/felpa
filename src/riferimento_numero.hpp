/**
 *  \file
 *  \remark This file is part of FELPA.
 *
 *  \copyright Copyright (C) 2021 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#if !defined(FELPA_RIFERIMENTO_NUMERO_HPP)
#define      FELPA_RIFERIMENTO_NUMERO_HPP

#include <cmath>

#include "numero_type.hpp"

namespace felpa
{

///
/// Valore numerico riferito al blocco AltriDatiGestionali
///
class RiferimentoNumero : public NumeroType
{
public:
  explicit RiferimentoNumero(const std::string &i = "")
    : NumeroType("RiferimentoNumero", 2, anche_negativo)
  {
    assign(i);
  }

  explicit RiferimentoNumero(double i)
    : NumeroType("RiferimentoNumero", 2, anche_negativo)
  {
    assign(i);
  }

  RiferimentoNumero &operator=(const std::string &i)
  {
    return *this = RiferimentoNumero(i);
  }

  RiferimentoNumero &operator=(double i)
  {
    return *this = RiferimentoNumero(i);
  }
};  // class RiferimentoNumero

}  // namespace felpa

#endif  // include guard
