/**
 *  \file
 *  \remark This file is part of FELPA.
 *
 *  \copyright Copyright (C) 2015-2024 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#if !defined(FELPA_TIPO_DOCUMENTO_HPP)
#define      FELPA_TIPO_DOCUMENTO_HPP

#include <string>

namespace felpa
{

///
/// Serve per identificare la tipologia di un documento. I codici previsti
/// sono specificati in un insieme di valori associato.
///
class TipoDocumento
{
public:
  explicit TipoDocumento(const std::string & = "TD01");

  [[nodiscard]] operator std::string() const { return codice_; }

  TipoDocumento &operator=(const std::string &);

  [[nodiscard]] static const std::string &tag();

private:
  // Funzioni private di supporto.
  [[nodiscard]] bool valido(const std::string &);

  // Data member privati.
  std::string codice_;
};  // class TipoDocumento

[[nodiscard]] bool operator==(const TipoDocumento &, const TipoDocumento &);

}  // namespace felpa

#endif  // include guard
