/**
 *  \file
 *  \remark This file is part of FELPA.
 *
 *  \copyright Copyright (C) 2015-2019 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#if !defined(FELPA_SEDE_HPP)
#define      FELPA_SEDE_HPP

#include "indirizzo_type.hpp"

namespace felpa
{

///
/// Serve ad individuare la residenza/domicilio di un soggetto.
///
class Sede : public IndirizzoType
{
public:
  Sede() : IndirizzoType("Sede") {}

  bool operator==(const IndirizzoType &rhs) const
  { return IndirizzoType::operator==(rhs); }
};  // class Sede

}  // namespace felpa

#endif  // include guard
