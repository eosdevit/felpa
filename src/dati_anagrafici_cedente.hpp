/**
 *  \file
 *  \remark This file is part of FELPA.
 *
 *  \copyright Copyright (C) 2015 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#if !defined(FELPA_DATI_ANAGRAFICI_CEDENTE_HPP)
#define      FELPA_DATI_ANAGRAFICI_CEDENTE_HPP

#include "dati_anagrafici_type.hpp"

namespace felpa
{

class DatiAnagraficiCedente : public DatiAnagraficiType
{
public:
  DatiAnagraficiCedente();

  // ********* Data member pubblici *********
  /// Consente di inserire informazioni relative all'albo professionale cui
  /// l'interessato eventualmente appartiene.
  ///
  /// Viene valorizzato a discrezione dell'utente, seppur ragionevolmente nel
  /// rispetto di esigenze interpretative.
  ///
  /// \warning
  /// Nel caso in cui si proceda alla valorizzazione di questo campo, e'
  /// consigliabile, per completezza di informazione, la valorizzazione degli
  /// altri campi relativi all'albo profesisonale (anche se non e' previsto
  /// alcun obbligo / controllo al riguardo).
  std::string albo_professionale;

  /// Consente di inserire informazioni relative all'albo professionale cui
  /// l'interessato eventualmente appartiene.
  std::string provincia_albo;

  /// Consente di inserire informazioni relative all'albo professionale cui
  /// l'interessato eventualmente appartiene.
  std::string numero_iscrizione_albo;

  /// Consente di inserire informazioni relative all'albo professionale cui
  /// l'interessato eventualmente appartiene.
  DataIscrizioneAlbo data_iscrizione_albo;

  RegimeFiscale regime_fiscale;

  [[nodiscard]] bool operator==(const DatiAnagraficiCedente &) const;
};  // class DatiAnagraficiCedente

}  // namespace felpa

#endif  // include guard
