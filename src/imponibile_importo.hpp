/**
 *  \file
 *  \remark This file is part of FELPA.
 *
 *  \copyright Copyright (C) 2015-2018 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#if !defined(FELPA_IMPONIBILE_IMPORTO_HPP)
#define      FELPA_IMPONIBILE_IMPORTO_HPP

#include "fattura_elettronica_eccezioni.hpp"
#include "numero_type.hpp"

namespace felpa
{

class ImponibileImporto : public NumeroType
{
public:
  explicit ImponibileImporto(const std::string &p = "")
    : NumeroType("ImponibileImporto", 2, anche_negativo)
  {
    assign(p);
  }

  explicit ImponibileImporto(double p)
    : NumeroType("ImponibileImporto", 2, anche_negativo)
  {
    assign(p);
  }

  ImponibileImporto &operator=(const std::string &p)
  {
    assign(p);
    return *this;
  }

  ImponibileImporto &operator=(double p)
  {
    assign(p);
    return *this;
  }
};  // class ImponibileImporto

}  // namespace felpa

#endif  // include guard
