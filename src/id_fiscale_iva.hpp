/**
 *  \file
 *  \remark This file is part of FELPA.
 *
 *  \copyright Copyright (C) 2015-2021 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#if !defined(FELPA_ID_FISCALE_IVA_HPP)
#define      FELPA_ID_FISCALE_IVA_HPP

#include "id_fiscale_type.hpp"

namespace felpa
{

///
/// Consente l'inserimento della partita IVA italiana per soggetti
/// residenti in Italia e per quelli esteri operanti in Italia
/// identificati: attraverso una Stabile Organizzazione, mediante un
/// rappresentente fiscale, direttamente.
/// Negli altri casi consente l'inserimento dell'identificativo
/// fiscale assegnato dall'autorita' del proprio paese per i soggetti
/// non residenti.
///
class IdFiscaleIva : public IdFiscaleType
{
public:
  IdFiscaleIva() : IdFiscaleType("IdFiscaleIVA") {}

  bool operator==(const IdFiscaleType &rhs) const
  { return IdFiscaleType::operator==(rhs); }
};

}  // namespace felpa

#endif  // include guard
