/**
 *  \file
 *  \remark This file is part of FELPA.
 *
 *  \copyright Copyright (C) 2015-2018 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#if !defined(FELPA_IMPOSTA_HPP)
#define      FELPA_IMPOSTA_HPP

#include "numero_type.hpp"

namespace felpa
{

class Imposta : public NumeroType
{
public:
  explicit Imposta(const std::string & = "");
  explicit Imposta(double);

  Imposta &operator=(const std::string &);
  Imposta &operator=(double);
};  // class Imposta

}  // namespace felpa

#endif  // include guard
