/**
 *  \file
 *  \remark This file is part of libCommercio.
 *
 *  \copyright Copyright (C) 2023 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#include <regex>
#include <stdexcept>
#include <string>

#include "utils.hpp"

#if !defined(LIB_COMMERCIO_PARTITA_IVA_HPP)
#define      LIB_COMMERCIO_PARTITA_IVA_HPP

namespace lib_commercio
{

///
/// La partita IVA e' un numero che viene utilizzato per identificare
/// univocamente, ai fini fiscali, un'attivita' soggetta ad IVA.
///
class partita_iva
{
public:
  partita_iva(const std::string & = "");

  struct componenti
  {
    std::string contribuente {};
    std::string ufficio      {};
    char controllo        {'?'};
  };

  operator std::string() const { return pi_; }
  [[nodiscard]] componenti scomponi() const { return scomponi(pi_); }

  [[nodiscard]] static componenti scomponi(std::string);
  [[nodiscard]] static bool verifica(std::string);

#if defined(__BORLANDC__)
  partita_iva(const String &);
  [[nodiscard]] static bool verifica(const String &);
#endif

private:
  std::string pi_;
};

[[nodiscard]] bool operator==(const partita_iva &, const partita_iva &);
[[nodiscard]] bool operator<(const partita_iva &, const partita_iva &);


/*********************************************************************
 * DETTAGLI DI IMPLEMENTAZIONE
 *********************************************************************/
namespace details
{

[[nodiscard]] inline std::string semplifica_pi(const std::string &pi)
{
  if (pi.length() == 13
      && std::toupper(pi[0] == 'I') && std::toupper(pi[1] == 'T'))
    return pi.substr(2, 11);

  return pi;
}

}  // namespace details


/*********************************************************************
 * IMPLEMENTAZIONE
 *********************************************************************/
inline partita_iva::partita_iva(const std::string &pi)
  : pi_(details::semplifica_pi(erase_spaces(pi)))
{
  if (!verifica(pi))
    throw std::invalid_argument("Partita iva scorretta");
}

///
/// Verifica la correttezza formale di una partita iva.
///
/// \param[in] pi partita iva
/// \return       `true` se la partita iva e' corretta
///
/// La funzione verifica lunghezza e codice di controllo.
///
inline bool partita_iva::verifica(std::string pi)
{
  pi = details::semplifica_pi(pi);

  if (pi.length() != 11 || !std::all_of(pi.begin(), pi.end(), ::isdigit))
    return false;

  unsigned s(0);
  for (int i(0); i < 10; i += 2)
    s += chr_to_ui(pi[i]);

  for (int i(1); i < 10; i += 2)
  {
    unsigned c(2 * chr_to_ui(pi[i]));

    if (c > 9)
      c -= 9;

    s += c;
  }

  return (10 - s % 10) % 10 == chr_to_ui(pi.back());
}

///
/// Estrae le informazioni contenute nella partita iva.
///
/// \param[in] pi una partita iva
/// \return       componenti della partita iva
///
/// \note
/// Nel caso di partita iva troppo corta, viene restituita una struttura
/// vuota.
///
inline partita_iva::componenti partita_iva::scomponi(std::string pi)
{
  pi = details::semplifica_pi(pi);

  componenti ret;
  ret.contribuente = pi.substr(0, 7);
  ret.ufficio = pi.substr(7, 3);
  ret.controllo = pi.back();

  return ret;
}

inline bool operator==(const partita_iva &lhs, const partita_iva &rhs)
{
  return static_cast<std::string>(lhs) == static_cast<std::string>(rhs);
}

inline bool operator<(const partita_iva &lhs, const partita_iva &rhs)
{
  return static_cast<std::string>(lhs) < static_cast<std::string>(rhs);
}

#if defined(__BORLANDC__)
inline partita_iva::partita_iva(const String &pi)
  : partita_iva(details::to_stdstring(pi))
{
}

inline bool partita_iva::verifica(const String &pi)
{
  return verifica(details::to_stdstring(pi));
}
#endif


}  // namespace lib_commercio

#endif  // include guard
