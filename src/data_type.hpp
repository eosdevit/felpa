/**
 *  \file
 *  \remark This file is part of FELPA.
 *
 *  \copyright Copyright (C) 2015-2024 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#if !defined(FELPA_DATA_TYPE_HPP)
#define      FELPA_DATA_TYPE_HPP

#include <string>

namespace felpa
{

///
/// Contiene una data in formato ISO 8601:2004 con precisione YYYY-MM-DD.
///
class DataType
{
public:
  [[nodiscard]] const std::string &tag() const { return tag_; }

  [[nodiscard]] operator std::string () const { return data_; }

  [[nodiscard]] bool empty() const { return data_.empty(); }

  void assign(const std::string &);

  [[nodiscard]] bool operator==(const DataType &rhs) const
  { return data_ == rhs.data_; }

protected:
  explicit DataType(const std::string &, const std::string & = "");

private:
  // Funzioni private di supporto.
  [[nodiscard]] static bool valida(const std::string &);

  // ********* Data member privati *********
  std::string tag_;
  std::string data_;
};  // class DataType

}  // namespace felpa

#endif  // include guard
